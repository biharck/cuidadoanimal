<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>


<c:set var="submitLabel" value="${n:default('Pesquisar', TabelaFiltroTag.submitLabel)}" />
<c:set var="panelGridColumns" value="${n:default(2, TabelaFiltroTag.columns)}" />
<c:set var="panelGridStyleClass" value="${n:default('inputTable', TabelaFiltroTag.styleClass)}" />
<c:set var="panelGridColumnStylesClasses" value="${n:default('labelColumn, propertyColumn', TabelaFiltroTag.columnStyleClasses)}" />


<n:panelGrid columns="${panelGridColumns}"
	 style="${tag.style}"
	 colspan="${tag.colspan}"
	 columnStyleClasses="${panelGridColumnStylesClasses}"
	 columnStyles="${tag.columnStyles}"
	 dynamicAttributesMap="${tag.dynamicAttributesMap}"
	 rowStyles="${tag.rowStyles}"
	 styleClass="${panelGridStyleClass}"
	 propertyRenderAsDouble="${tag.propertyRenderAsDouble}" width="${tag.width}" rowStyleClasses="${tag.rowStyleClasses}" cellpadding="1">
		
		<t:propertyConfig mode="input" showLabel="${tag.propertyShowLabel}" renderAs="double">
			<n:doBody />
		</t:propertyConfig>

</n:panelGrid>

<c:if test="${tag.showSubmit}">
<div class="actionBar">
	<div id="filtar">
		<n:submit type="submit" action="${TabelaFiltroTag.submitAction}" validate="${TabelaFiltroTag.validateForm}" url="${TabelaFiltroTag.submitUrl}">${submitLabel}</n:submit>
	<span id="limpar">&nbsp;&nbsp;|&nbsp;&nbsp;
	<n:link url="#" type="submit" onclick="javascript:$dg.clearForm(\"form\");submitForm();" id="btn_limpar">Reiniciar Pesquisa</n:link>&nbsp;&nbsp;</span>
	</div>
</div>
</c:if>
<script type="text/javascript">
function submitForm() {
	$('#dialog_aguarde').dialog('open');
	var validar = form.validate;
	try {
		validateForm;
	} catch (e) {
		validar = false;
	}
	try {
		clearMessages();//limpa as mensagens que vieram do servidor
	} catch(e){
	}
	if(validar == 'true') {
		var valid = validateForm();
		if(valid) {
			form.submit();
		}
	} else {
		form.submit();
	}
}

</script>
