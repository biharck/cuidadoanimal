<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<div class="sectionTitle">
	Registros encontrados
</div>

<div class="resultWindow">
<n:doBody />
</div>
