<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<div id="dialog_retornar" title="Observa��o">
</div>
<%@page import="org.nextframework.core.web.NextWeb"%><t:tela titulo="${entradaTag.titulo}" validateForm="false">
		<c:if test="${consultar}">
			<input type="hidden" name="forcarConsulta" value="true"/>
		</c:if>
		<c:if test="${param.fromInsertOne == 'true'}">
			<input type="hidden" name="fromInsertOne" value="true"/>
		</c:if>
		<c:if test="${consultar}">
			<c:if test="${empty animalEdit}">
				<c:if test="${empty empresaCad}">
					<table class="outterTable" cellspacing="0" cellpadding="0" align="center">
						<tr class="outterTableHeader">
							<span class="outterTableHeaderRight">
								<n:link onmouseover="Tip(\"Criar\")" onclick="dialog()" action="criar" id="btn_novo">&nbsp&nbsp Novo</n:link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</span>
						</tr>
					</table>
				</c:if>
			</c:if>
		</c:if>
		<div class="linkBar">
			<c:if test="${entradaTag.showListagemLink || !empty entradaTag.linkArea}">
				${entradaTag.invokeLinkArea}			
				<c:if test="${!consultar}">
					<c:if test="${entradaTag.showListagemLink}">
						<a href="javascript:dialog(2)" action="listagem" id="btn_voltar" class="outterTableHeaderLink">Voltar</a>
					</c:if>				
				</c:if>
				<c:if test="${consultar}">
					<c:if test="${entradaTag.showListagemLink}">
						<n:link action="listagem" onclick="javascript:dialog(1)"  class="outterTableHeaderLink">Voltar</n:link>
					</c:if>				
				</c:if>
			</c:if>	
		</div>

		<div>
			<n:bean name="${TEMPLATE_beanName}">
			<n:doBody />
			</n:bean>
		</div>
</t:tela>
<script type="text/javascript">
	function dialog(valor){
		if(valor == 1){
			$('#dialog_aguarde').dialog('open');
		}	
		if(valor == 2){
			document.getElementById("dialog_retornar").innerHTML = '<h3> Deseja Voltar Sem Salvar? </h3>';	
			$('#dialog_retornar').dialog('open');
		}
	}
	
	var parametro;
	
	function passaParametro(aux){
		parametro = aux;
	}
	
	$(function() {
		$("#dialog_retornar").dialog({
			bgiframe: true,
			autoOpen: false,
			modal: true,
			buttons: {
				N�o: function() {
					$(this).dialog('close');	
				},
				Sim: function() {
					var url= "<%= NextWeb.getRequestContext().getServletRequest().getContextPath()+""+NextWeb.getRequestContext().getFirstRequestUrl()+"?ACAO=listagem" %>";
					location.href=""+url+"";	
				}
			}
		});
	});
</script>

