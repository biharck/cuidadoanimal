<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<div class="inputWindow">
	<n:tabPanel id="janelaEntrada">
		<n:doBody />
	</n:tabPanel>
	<div id="editButton">
		<c:if test="${janelaEntradaTag.showSubmit}">
			<div class="actionBar">
				<c:if test="${consultar}">
					<script language="javascript">
						function doeditar(){
							$('#dialog_aguarde').dialog('open');
							form['forcarConsulta'].value = false;
							return true;
						}
					</script>
					<n:submit action="editar"  validate="false" confirmationScript="doeditar()">Editar</n:submit>
				</c:if>
				<c:if test="${!consultar}">
					<n:submit action="${janelaEntradaTag.submitAction}" validate="true" confirmationScript="${janelaEntradaTag.submitConfirmationScript}">${janelaEntradaTag.submitLabel}</n:submit>
				</c:if>							
			</div>
		</c:if>
	</div>
</div>

