<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<n:form validate="false">
	<n:validation>
		<input type="hidden" name="notFirstTime" value="true"/>
		<table class="outterTable" cellspacing="0" cellpadding="0" align="center">
			<tr class="outterTableHeader">
				<td>
					<span class="pageTitle">
						${listagemTag.titulo}
					</span>
					<span class="outterTableHeaderRight">
						${listagemTag.invokeLinkArea} 
						<c:if test="${listagemTag.showNewLink || !empty listagemTag.linkArea}">
						<n:link onmouseover="Tip(\"Criar\")" onclick="dialog()" action="criar" id="btn_novo">&nbsp&nbsp Novo</n:link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</c:if>						
					</span>
				</td>
			</tr>
			<tr>
				<td>
					<n:doBody />
				</td>
			</tr>
		</table>
	</n:validation>
</n:form>
<script type="text/javascript">
	function dialog(){
		$('#dialog_aguarde').dialog('open');
	}
</script>