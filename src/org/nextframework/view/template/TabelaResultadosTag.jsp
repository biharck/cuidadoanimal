<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<div>
	<n:dataGrid itens="${TtabelaResultados.itens}" var="${TtabelaResultados.name}" cellspacing="0"
		rowondblclick="javascript:dialog();$dg.editarRegistro(this)"
		rowonclick="javascript:$dg.coloreLinha('tabelaResultados',this)" 
		rowonmouseover="javascript:$dg.mouseonOverTabela('tabelaResultados',this)" 
		rowonmouseout="javascript:$dg.mouseonOutTabela('tabelaResultados',this)" 
		id="tabelaResultados">
		
		<n:bean name="${TtabelaResultados.name}" valueType="${TtabelaResultados.valueType}">
			<n:getContent tagName="acaoTag" vars="acoes">
				<t:propertyConfig mode="output" renderAs="column">
					<n:doBody />
				</t:propertyConfig>
				<c:if test="${(!empty acoes) || (TtabelaResultados.showEditarLink) || (TtabelaResultados.showExcluirLink) || (TtabelaResultados.showConsultarLink)}">
					<n:column header="A��o" style="width: 1%; white-space: nowrap; padding-right: 3px;">
						${acoes}
							<script language="javascript">
							<c:catch var="exSelecionar">
								imprimirSelecionar(new Array(${n:hierarchy(TtabelaResultados.valueType)}), "${n:escape(n:valueToString(n:reevaluate(TtabelaResultados.name, pageContext)))}", "${n:escape(n:descriptionToString(n:reevaluate(TtabelaResultados.name, pageContext)))}");
							</c:catch>
							</script>
							<c:if test="${!empty exSelecionar}">
								<span style="font-size: 10px; color: red; white-space: pre; display:block;">Erro ao imprimir bot�o selecionar: ${exSelecionar.message} <c:catch>${exSelecionar.rootCause.message}</c:catch></span>
							</c:if>
						<c:if test="${TtabelaResultados.showConsultarLink}">
							<n:link action="consultar"  onclick="javascript:dialog()" class="consult" onmouseover="Tip(\"Consultar Registro\")" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}" img="/CuidadoAnimal/images/consultar_icon.gif"></n:link>&nbsp;&nbsp;
						</c:if>						
						<c:if test="${TtabelaResultados.showEditarLink}">
							<n:link action="editar" onmouseover="Tip(\"Editar Registro\")" class="activation" onclick="javascript:dialog()" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}" img="/CuidadoAnimal/images/edit.png"></n:link>&nbsp;&nbsp;
						</c:if>
						<c:if test="${TtabelaResultados.showExcluirLink}">				
							<n:link  confirmationMessage="Deseja realmente excluir esse registro?" onmouseover="Tip(\"Apagar Registro\")" action="excluir" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}" img="/CuidadoAnimal/images/delete.png"></n:link>
						</c:if>
					</n:column>
				</c:if>
			</n:getContent>
		</n:bean>
	</n:dataGrid>
</div>
<div class="divlast">
	<c:if test="${filtro.numberOfResults == 0}">
		<div align="center"><font style="font-size: 12px;"><br><b>N�o existem registros!</b></br></font></div>		
	</c:if>
<c:if test="${empty noNumberPages}">
	<div class="pagging" >
		P&aacute;gina <n:pagging currentPage="${currentPage}" totalNumberOfPages="${numberOfPages}" selectedClass="pageSelected" unselectedClass="pageUnselected" />
	</div>
</c:if>
</div>
<script type="text/javascript">
	function dialog(){
		$('#dialog_aguarde').dialog('open');
	}
</script>
