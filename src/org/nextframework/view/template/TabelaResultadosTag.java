/*
 * Next Framework http://www.nextframework.org
 * Copyright (C) 2009 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package org.nextframework.view.template;

import org.nextframework.util.Util;

/**
 * @author rogelgarcia
 * @since 03/02/2006
 * @version 1.1
 */
public class TabelaResultadosTag extends TemplateTag {
	
	protected boolean showEditarLink = true;
	protected boolean showExcluirLink = true;
	protected boolean showConsultarLink = true;
	
	protected Object itens;
	protected String name;
	protected Class valueType;

	public Class getValueType() {
		return valueType;
	}

	public void setValueType(Class valueType) {
		this.valueType = valueType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getItens() {
		return itens;
	}

	public void setItens(Object itens) {
		this.itens = itens;
	}

	public boolean isShowEditarLink() {
		return showEditarLink;
	}

	public void setShowEditarLink(boolean showEditarLink) {
		this.showEditarLink = showEditarLink;
	}

	public boolean isShowExcluirLink() {
		return showExcluirLink;
	}

	public void setShowExcluirLink(boolean showExcluirLink) {
		this.showExcluirLink = showExcluirLink;
	}

	@Override
	protected void doComponent() throws Exception {
		autowireValues();
		pushAttribute("TtabelaResultados", this);
		includeJspTemplate();
		popAttribute("TtabelaResultados");
	}

	private void autowireValues() {
		if(itens == null){
			itens = getRequest().getAttribute("lista");
		}
		if(valueType == null){
			valueType = (Class) getRequest().getAttribute("TEMPLATE_beanClass");
		}
		if(Util.strings.isEmpty(name)){
			name = (String) getRequest().getAttribute("TEMPLATE_beanNameUncaptalized");
			if(Util.strings.isEmpty(name) && Util.objects.isNotEmpty(valueType)){
				name = Util.strings.uncaptalize(valueType.getSimpleName());
			}
		}
	}

	public boolean isShowConsultarLink() {
		return showConsultarLink;
	}

	public void setShowConsultarLink(boolean showConsultarLink) {
		this.showConsultarLink = showConsultarLink;
	}

}
