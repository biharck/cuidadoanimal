<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<n:panel title="${Tdetalhe.detailDysplayName}">
	<div class="detailBlock">
	<n:dataGrid itens="${Tdetalhe.itens}" cellspacing="0" dynaLine="true" id="${Tdetalhe.tableId}"  var="${Tdetalhe.detailVar}">
		<n:bean name="${Tdetalhe.detailVar}" valueType="${Tdetalhe.detailClass}" propertyPrefix="${Tdetalhe.fullNestedName}" propertyIndex="${index}">
			<n:getContent tagName="acaoTag" vars="acoes">
				<t:propertyConfig mode="input" renderAs="column">
					<n:doBody />
				</t:propertyConfig>
				<c:if test="${Tdetalhe.showColunaAcao}">
				<n:column header="A��o" style="width: 1%; white-space: nowrap; padding-right: 3px;">
					${acoes}
					<c:if test="${Tdetalhe.showBotaoRemover}">
						<c:if test="${!consultar}">
							<c:if test="${!propertyConfigDisabled || dataGridDynaline}">
								<button type="button" onclick="excluirLinhaPorNome(this.id);reindexFormPorNome(this.id, forms[0], '${Tdetalhe.fullNestedName}', true)" id="button.excluir[table_id=${Tdetalhe.tableId}, indice=${rowIndex}]">
									remover
								</button>
							</c:if>
							<c:if test="${propertyConfigDisabled && !dataGridDynaline}">	
								<button type="button" disabled="disabled" onclick="excluirLinhaPorNome(this.id);reindexFormPorNome(this.id, forms[0], '${Tdetalhe.fullNestedName}', true)" id="button.excluir[table_id=${Tdetalhe.tableId}, indice=${rowIndex}]">
									remover
								</button>						
							</c:if>	
						</c:if>				
					</c:if>
				</n:column>
				</c:if>
			</n:getContent>
		</n:bean>
	</n:dataGrid>
	<c:if test="${!consultar}">
		<c:if test="${Tdetalhe.showBotaoNovaLinha}">
			<div align="center">
				<c:if test="${empty Tdetalhe.dynamicAttributesMap['labelnovalinha']}">
					<c:set value="Adicionar Registro" scope="page" var="labelnovalinha"/>
				</c:if>
				<c:if test="${!empty Tdetalhe.dynamicAttributesMap['labelnovalinha']}">
					<c:set value="${Tdetalhe.dynamicAttributesMap['labelnovalinha']}" scope="page" var="labelnovalinha"/>
				</c:if>
				
				<c:if test="${!propertyConfigDisabled}">
					<button class="mybutton" type="image"   onclick="newLine${Tdetalhe.tableId}();${Tdetalhe.onNewLine}"  >
						${labelnovalinha}<img src="/CuidadoAnimal/images/add.png"  />
					</button>
				</c:if>
				<c:if test="${propertyConfigDisabled}">
					<button class="mybutton" type="button" disabled="disabled" onclick="newLine${Tdetalhe.tableId}();${Tdetalhe.onNewLine}">
						${labelnovalinha}
					</button>
				</c:if>
			</div>
		</c:if>
	</c:if>
	</div>
</n:panel>
<style>
	.mybutton{
		border:none;
		background-color:#FFFFFF;
	}
</style>
