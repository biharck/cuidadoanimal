<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<c:if test="${Ttela.includeForm}">
<n:form validate="${Ttela.validateForm}" method="${Ttela.formMethod}" action="${Ttela.formAction}" validateFunction="validarFormulario">

	<n:validation functionName="validateForm">
		<script language="javascript">
			// caso seja alterada a fun��o validation ela ser� chamada ap�s a validacao do formulario
			var validation;
			function validarFormulario(){
				var valido = validateForm();
				if(validation){
					valido = validation(valido);
				}
				return valido;
			}
		</script>
		
		<div class="pageTitle">
			${Ttela.titulo}
		</div>

		<div>
			<n:doBody />
		</div>

	</n:validation>
</n:form>
</c:if>

<c:if test="${!Ttela.includeForm}">
		<div class="pageTitle">
			${Ttela.titulo}
		</div>

		<div>
			<n:doBody />
		</div>
</c:if>