<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>


<n:panel title="${TtabelaEntrada.title}" colspan="${TtabelaEntrada.colspan}">
	<n:panelGrid columns="${TtabelaEntrada.columns}" style="${TtabelaEntrada.style}" styleClass="${TtabelaEntrada.styleClass}" rowStyleClasses="${TtabelaEntrada.rowStyleClasses}" rowStyles="${TtabelaEntrada.rowStyles}"
		columnStyleClasses="${TtabelaEntrada.columnStyleClasses}" columnStyles="${TtabelaEntrada.columnStyles}" colspan="${TtabelaEntrada.colspan}" propertyRenderAsDouble="${TtabelaEntrada.propertyRenderAsDouble}"
		dynamicAttributesMap="${TtabelaEntrada.dynamicAttributesMap}" cellpadding="1" cellspacing="0">

		<t:propertyConfig mode="input" showLabel="${tag.propertyShowLabel}" disabled="${consultar}" >
			<n:doBody />
		</t:propertyConfig>

	</n:panelGrid>
</n:panel>
<c:if test="${!consultar}">
	<style type="text/css">
		input,select,textarea,html body textarea {
			vertical-align: middle;
			font-size: 12px;	
			font-family: Arial;	
			
			border:1px solid #bababa;
			color:#535353;
			
			background-clip:border;
			background-inline-policy:continuous;
			background-origin:padding;
			background:#FFFFFF url(/CuidadoAnimal/images/textfield.gif) repeat-x scroll center top;
			border:1px solid #A6A6A6;
			font-size:12px;
			padding:2px 1px;
		}
	</style>
</c:if>
<c:if test="${consultar}">
	<style type="text/css">
		input,select,textarea,html body textarea {
			vertical-align: middle;
			font-size: 12px;	
			font-family: Arial;	
			
			border:1px solid #bababa;
			color:#535353;
			
			background-clip:border;
			background-inline-policy:continuous;
			background-origin:padding;
			background:#F0F0F0;
			border:1px solid #BABABA;
			font-size:12px;
			padding:2px 1px;
		}
	</style>
</c:if>