/*
 * Next Framework http://www.nextframework.org
 * Copyright (C) 2009 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package org.nextframework.persistence;

import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import javax.persistence.Entity;
import javax.persistence.Transient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.nextframework.bean.BeanDescriptor;
import org.nextframework.bean.BeanDescriptorFactoryImpl;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.controller.ExtendedBeanWrapper;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.standard.Next;
import org.nextframework.types.File;
import org.nextframework.util.ReflectionCache;
import org.nextframework.util.ReflectionCacheFactory;
import org.nextframework.util.Util;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.support.TransactionTemplate;



/**
 * Classe que deve ser extendida pelas classes que necessitam fazer acesso ao banco de dados
 * @author rogelgarcia
 *
 * @param <BEAN>
 */
public class GenericDAO<BEAN> extends HibernateDaoSupport implements DAO<BEAN> {
	
	protected Log log = LogFactory.getLog(this.getClass());
	
	protected Class<BEAN> beanClass;
	protected String orderBy;
	protected String extraQuery;
	protected TransactionTemplate transactionTemplate;
	private JdbcTemplate jdbcTemplate;
	protected FileDAO<File> fileDAO;
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	
	/**
	 * Cria um saveOrUpdateStrategy e salva o objeto (N�o executa)
	 * @param entity
	 * @return
	 */
	protected SaveOrUpdateStrategy save(Object entity){
		return new SaveOrUpdateStrategy(getHibernateTemplate(), getTransactionTemplate(), entity).saveEntity();
	}
	
	protected final List<BEAN> empty() {
		return new ArrayList<BEAN>();
	}
	
	@Override
	protected HibernateTemplate createHibernateTemplate(SessionFactory sessionFactory) {
		if(getHibernateTemplate() != null){
			return getHibernateTemplate();
		}
		return super.createHibernateTemplate(sessionFactory);
	}
	
	public TransactionTemplate getTransactionTemplate() {
		return transactionTemplate;
	}

	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}

	public GenericDAO(Class<BEAN> beanClass){
		this.beanClass = beanClass;
		ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
		DefaultOrderBy orderBy = reflectionCache.getAnnotation(this.getClass(), DefaultOrderBy.class);
		if (orderBy != null) {
			this.orderBy = orderBy.value();
		}
		checkFileProperties();
	}
	


	@SuppressWarnings("unchecked")
	public GenericDAO(){
		Class[] classes = Util.generics.getGenericTypes(this.getClass());
		if(classes.length != 1){
			//tentar a outra forma de Generics
			{
				classes = Util.generics.getGenericTypes2(this.getClass());
				if(classes.length == 1 && !classes[0].equals(Object.class)){
					beanClass = classes[0];
					ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
					DefaultOrderBy orderBy = reflectionCache.getAnnotation(this.getClass(), DefaultOrderBy.class);
					if (orderBy != null) {
						this.orderBy = orderBy.value();
					}		
					checkFileProperties();
					return;
				}
			}
			throw new RuntimeException("A classe "+this.getClass().getName()+" deveria especificar um tipo generico BEAN" );
		}
		beanClass = classes[0];
		ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
		DefaultOrderBy orderBy = reflectionCache.getAnnotation(this.getClass(), DefaultOrderBy.class);
		if (orderBy != null) {
			this.orderBy = orderBy.value();
		}
		checkFileProperties();
	}
	
	protected void init(){
		
	}
	
	protected List<PropertyDescriptor> fileProperties = new ArrayList<PropertyDescriptor>();
	
	
	protected void checkFileProperties() {
		if(isDetectFileProperties()){
			ExtendedBeanWrapper beanWrapper = new ExtendedBeanWrapper(beanClass);
			PropertyDescriptor[] propertyDescriptors = beanWrapper.getPropertyDescriptors();
			for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
				if(File.class.isAssignableFrom(propertyDescriptor.getPropertyType())){
					if(propertyDescriptor.getReadMethod().getAnnotation(Transient.class) == null){
						fileProperties.add(propertyDescriptor);
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected final void initDao() throws Exception {
		init();
		if(fileProperties.size() > 0){
			//achar o DAO para os files...
			Map<String, FileDAO> beans = Next.getApplicationContext().getConfig().getDefaultListableBeanFactory().getBeansOfType(FileDAO.class);
			if(beans.size() == 1){
				this.fileDAO = beans.values().iterator().next();
			} else if(beans.size() == 0){
				//nao existe um DAO definido na aplicacao, definir um default
				this.fileDAO = new FileDAO((Class<File>)fileProperties.get(0).getPropertyType(), true);//todas as propriedades do tipo arquivo devem ser da mesma classe
				this.fileDAO.setHibernateTemplate(getHibernateTemplate());
				this.fileDAO.setJdbcTemplate(getJdbcTemplate());
				this.fileDAO.setSessionFactory(getSessionFactory());
				this.fileDAO.setTransactionTemplate(getTransactionTemplate());
			}
		}		
	}
	
	/**
	 * Indica se os atributos do tipo File devem ser detectados automaticamente para salvar e carregar ao carregar o bean
	 * @return
	 */
	protected boolean isDetectFileProperties() {
		return true;
	}

	/**
	 * Cria um QueryBuilder para esse DAO j� com o from configurado
	 * (O From pode ser alterado)
	 * @return
	 */
	protected QueryBuilder<BEAN> query() {
		return newQueryBuilder(beanClass)
					.from(beanClass);
	}
	
	/**
	 * Cria um query builder para ser utilizado pelo DAO
	 * @param <E>
	 * @param clazz
	 * @return
	 */
	public <E> QueryBuilder<E> newQueryBuilder(Class<E> clazz){
		return new QueryBuilder<E>(getHibernateTemplate());
	}
	
	protected QueryBuilder<BEAN> queryWithOrderBy() {
		QueryBuilder<BEAN> query = newQueryBuilder(beanClass)
					.from(beanClass);
		if(this.orderBy != null){
			query.orderBy(orderBy);
		}
		return query;
	}

	/* (non-Javadoc)
	 * @see org.nextframework.persistence.DAO#saveOrUpdate(BEAN)
	 */
	public void saveOrUpdate(final BEAN bean) {
		SaveOrUpdateStrategy save = save(bean);
		if(autoManageFileProperties() && fileDAO != null){
			for (final PropertyDescriptor pd : fileProperties) {
				save.attachBefore(new HibernateCallback<Object>(){
					public Object doInHibernate(Session session) throws HibernateException, SQLException {
						fileDAO.saveFile(bean, pd.getName());
						return null;
					}
				});
			}
		}
		updateSaveOrUpdate(save);
		save.execute();
		getHibernateTemplate().flush();
	}

	/* (non-Javadoc)
	 * @see org.nextframework.persistence.DAO#load(BEAN)
	 */
	public BEAN load(BEAN bean){
		if(bean == null){
			return null;
		}
		return query()
			.entity(bean)
			.unique();
	}
	
	/* (non-Javadoc)
	 * @see org.nextframework.persistence.DAO#loadForEntrada(BEAN)
	 */
	public BEAN loadForEntrada(BEAN bean){
		if(bean == null){
			return null;
		}
		QueryBuilder<BEAN> query = newQueryBuilder(beanClass)
			.from(beanClass)
			.entity(bean);
		//� provavel que se existir uma propriedade do tipo File ela deva ser carregada
		if(autoManageFileProperties() && fileDAO != null){
			for (PropertyDescriptor pd : fileProperties) {
				query.leftOuterJoinFetch(query.getAlias()+"."+pd.getName());
			}
		}
		updateEntradaQuery(query);
		return query
			.unique();
	}
	
	protected boolean autoManageFileProperties() {
		return true;
	}

	// campos uteis para fazer cache dos findAlls dos combos
	protected long cacheTime = 0;
	private static final long TEMPO_5_SEGUNDOS = 5*1000;	
	private QueryBuilderResultTranslator<?> translatorQueryFindForCombo;
	private String queryFindForCombo;
	private long lastRead;
	private WeakReference<List<?>> findForComboCache = new WeakReference<List<?>>(null);
	
	
	/* (non-Javadoc)
	 * @see org.nextframework.persistence.DAO#findForCombo(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public List<BEAN> findForCombo(String... extraFields){
		if(extraFields != null && extraFields.length > 0){
			return newQueryBuilder(beanClass)
						.select(getComboSelect(extraFields))
						.from(beanClass)
						.orderBy(orderBy)
						.list();
		} else {
			if(queryFindForCombo == null){
				initQueryFindForCombo();
			}
			List listCached = findForComboCache.get();
			if (listCached == null || (System.currentTimeMillis() - lastRead > cacheTime)) {
				listCached = getHibernateTemplate().find(queryFindForCombo);
				listCached = translatorQueryFindForCombo.translate(listCached);
				findForComboCache = new WeakReference<List<?>>(listCached);
				lastRead = System.currentTimeMillis();	
			}
			return listCached;	
		}
	}
	
	@Deprecated
	public List<BEAN> findForCombo(){
		return findForCombo((String[])null);
	}

	
	/* (non-Javadoc)
	 * @see org.nextframework.persistence.DAO#findBy(java.lang.Object)
	 */
	public List<BEAN> findBy(Object o){
		return findBy(o, false, (String[])null);
	}
	
	//cache
	private Map<Class<?>, String> mapaQueryFindByForCombo = new WeakHashMap<Class<?>, String>();
	private Map<Class<?>, String> mapaQueryFindBy = new WeakHashMap<Class<?>, String>();
	
	
	/* (non-Javadoc)
	 * @see org.nextframework.persistence.DAO#findBy(java.lang.Object, boolean, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public List<BEAN> findBy(Object o, boolean forCombo, String... extraFields){
		if(o == null){
			return new ArrayList<BEAN>();
		}
		Class<? extends Object> propertyClass = o.getClass();
		while(propertyClass.getName().contains("$$")){
			propertyClass = propertyClass.getSuperclass();
		}
		String queryString = null;
		if((extraFields != null && extraFields.length >0) || 
				(forCombo && (queryString = mapaQueryFindByForCombo.get(propertyClass)) == null) || 
				(!forCombo && (queryString = mapaQueryFindBy.get(propertyClass)) == null)){
			//inicializa a query para essa classe
			//System.out.println("\n\n\nLOADING CLASSE "+this.beanClass+"  PROPRIEDADE CLASSE: "+o.getClass());
			String[] propertiesForClass = findPropertiesForClass(propertyClass);
			if(propertiesForClass.length == 1){// achou uma propriedade
				String alias = Util.strings.uncaptalize(this.beanClass.getSimpleName());
				String property = propertiesForClass[0];
				QueryBuilder qb = queryWithOrderBy();
				qb.where(alias+"."+property+" = ? ", o);
				updateFindByQuery(qb);
				if(forCombo){
					if(extraFields != null && extraFields.length >0){
						//verifcar se precisa fazer joins extras
						int i = 0;
						for (int j = 0; j < extraFields.length; j++) {
							String extra = extraFields[j];
							BeanDescriptor<? extends Object> beanDescriptor = Next.getApplicationContext().getBeanDescriptor(null, this.beanClass);
							Type type = beanDescriptor.getPropertyDescriptor(extra).getType();
							if(type instanceof Class){
								if(((Class)type).isAnnotationPresent(Entity.class)){
									extra+= "." + Next.getApplicationContext().getBeanDescriptor(null, (Class)type).getDescriptionPropertyName();
								}
							}
							if(extra.contains(".")){
								int ultimoponto = extra.lastIndexOf(".");
								String path = extra.substring(0, ultimoponto);
								qb.join(alias+"."+path+" autojoin"+i);
								extraFields[j] = "autojoin"+i+extra.substring(ultimoponto);
							}
							i++;
						}
						//se for com extraFields n�o pode usar o cache (n�o existe cache do select quando tem extra properties)						
						qb.select(getComboSelect(extraFields));
						String hbquery = qb.getQuery();	
						queryString = hbquery;
						return qb.list();
					} else {
						qb.select(getComboSelect());
						String hbquery = qb.getQuery();	
						queryString = hbquery;
						mapaQueryFindByForCombo.put(propertyClass, queryString);
					}
				} else {
					String hbquery = qb.getQuery();	
					queryString = hbquery;
					mapaQueryFindByForCombo.put(propertyClass, queryString);
				}
			} else if(propertiesForClass.length > 1){// mais de uma propriedade do mesmo tipo
				throw new RuntimeException("N�o foi poss�vel executar findBy(..). Existe mais de uma propriedade da classe "+propertyClass.getName()+" na classe "+this.beanClass.getName()+"  "+Arrays.deepToString(propertiesForClass));
			} else {//nenhuma propriedade do tipo fornecido
				throw new RuntimeException("N�o foi poss�vel executar findBy(..). N�o existe nenhuma propriedade da classe "+propertyClass.getName()+" na classe "+this.beanClass.getName());
			}
		}

		if(extraQuery!=null)
			queryString = queryString.replace("WHERE", "WHERE "+extraQuery+" and ");
		
		List list = getHibernateTemplate().find(queryString, o);
		if (forCombo) {
			initQueryFindForCombo();
			list = translatorQueryFindForCombo.translate(list);
		}

		return list;
	}
	
	/**
	 * Atualiza as querys de findBy
	 * ex.: findBy(Object o, boolean forCombo, String... extraFields)
	 * @param query
	 */
	protected void updateFindByQuery(QueryBuilder<?> query) {
		
		
	}

	private String[] findPropertiesForClass(Class<? extends Object> propertyClass) {
		List<String> properties = new ArrayList<String>();
		ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
		Method[] methods = reflectionCache.getMethods(this.beanClass);
		for (Method method : methods) {
			if(Util.beans.isGetter(method)){
				if(method.getReturnType().equals(propertyClass)){
					properties.add(Util.beans.getPropertyFromGetter(method.getName()));
				}
			}
		}
		return properties.toArray(new String[properties.size()]);
	}



	//cache
	private String comboSelect;
	/**
	 * Retorna o select necess�rio para apenas o @Id e o @DescriptionProperty
	 * ex.: new QueryBuilder(...).select(getComboSelect()).from(X.class);
	 * @param extraFields Outros campos al�m do @Id e o @DescriptionProperty que devem ser carregados
	 * @return Clausula select montada
	 */
	public String getComboSelect(String... extraFields){
		if(comboSelect == null || extraFields != null && extraFields.length > 0){
			String alias = Util.strings.uncaptalize(beanClass.getSimpleName());
			String[] selectedProperties = getComboSelectedProperties(alias);
			selectedProperties = organizeExtraFields(alias, selectedProperties, extraFields);
			String comboSelect = "";
			for (int i = 0; i < selectedProperties.length; i++) {
				String prop = selectedProperties[i];
				comboSelect = comboSelect + prop;
				if(i+1 < selectedProperties.length){
					comboSelect = comboSelect + ", ";
				}
			}
			if(!(extraFields != null && extraFields.length > 0)){
				this.comboSelect = comboSelect;	
			} else {
				return comboSelect;
			}
		}
		return comboSelect;
	}
	

	protected String[] getComboSelectedProperties(String alias) {
		BeanDescriptor<BEAN> beanDescriptor = new BeanDescriptorFactoryImpl().createBeanDescriptor(null, beanClass);
		String descriptionPropertyName = beanDescriptor.getDescriptionPropertyName();
		String idPropertyName = beanDescriptor.getIdPropertyName();
		String[] selectedProperties;
		if(descriptionPropertyName == null){
			selectedProperties = new String[]{alias+"."+idPropertyName};
		} else {
			//verificar se o descriptionproperty utiliza outros campos
			String[] usingFields = getUsingFieldsForDescriptionProperty(beanDescriptor, descriptionPropertyName);
			
			if(usingFields != null && usingFields.length > 0 ){
				selectedProperties = new String[usingFields.length+1];
				selectedProperties[0] = alias+"."+idPropertyName;
				for (int i = 0; i < usingFields.length; i++) {
					selectedProperties[i+1] = alias+"."+usingFields[i];
				}
			} else {
				selectedProperties = new String[]{alias+"."+idPropertyName, alias+"."+descriptionPropertyName};	
			}
			
		}
		return selectedProperties;
	}

	private String[] getUsingFieldsForDescriptionProperty(BeanDescriptor<BEAN> beanDescriptor, String descriptionPropertyName) {
		Annotation[] annotations = beanDescriptor.getPropertyDescriptor(descriptionPropertyName).getAnnotations();
		DescriptionProperty descriptionProperty = null;
		for (Annotation annotation : annotations) {
			if(DescriptionProperty.class.isAssignableFrom(annotation.annotationType())){
				descriptionProperty = (DescriptionProperty) annotation;
				break;
			}
		}
		String[] usingFields = null;
		if(descriptionProperty != null){
			//TODO PROCURAR O @DescriptionProperty nas classes superiores
			usingFields = descriptionProperty.usingFields();
		}
		return usingFields;
	}
	
	private void initQueryFindForCombo() {
		if (translatorQueryFindForCombo == null) {
			//BeanDescriptor<BEAN> beanDescriptor = Next.getApplicationContext().getBeanDescriptor(null, beanClass);
			//String descriptionPropertyName = beanDescriptor.getDescriptionPropertyName();
			//String idPropertyName = beanDescriptor.getIdPropertyName();

			String alias = Util.strings.uncaptalize(beanClass.getSimpleName());
			String[] selectedProperties = getComboSelectedProperties(alias);
			String hbQueryFindForCombo = "select " + getComboSelect() + " " + "from " + beanClass.getName() + " " + alias;
			hbQueryFindForCombo = getQueryFindForCombo(hbQueryFindForCombo);
			if (orderBy != null && !hbQueryFindForCombo.contains("order by")) {
				hbQueryFindForCombo += "  order by " + orderBy;
			}
			translatorQueryFindForCombo = new QueryBuilderResultTranslatorImpl();
			translatorQueryFindForCombo.init(selectedProperties, new AliasMap[] { new AliasMap(alias, null, beanClass) });
			queryFindForCombo = hbQueryFindForCombo;
		}
	}

	private String[] organizeExtraFields(String alias, String[] selectedProperties, String... extraFields) {
		if(extraFields != null && extraFields.length > 0){
			for (int i = 0; i < extraFields.length; i++) {
				if (!extraFields[i].contains(".")) {
					extraFields[i] = alias + "." + extraFields[i];
				}
			}
			String[] oldselectedProperties = selectedProperties;
			selectedProperties = new String[selectedProperties.length+extraFields.length];
			System.arraycopy(oldselectedProperties, 0, selectedProperties, 0, oldselectedProperties.length);
			System.arraycopy(extraFields, 0, selectedProperties, oldselectedProperties.length, extraFields.length);
		}
		return selectedProperties;
	}
	
	//TODO RENOMEAR PARA UPDATEFINDFORCOMBOQUERY
	/**
	 * Permite alterar a query que ser� utilizada no findForCombo. O parametro � a query montada automaticamente.
	 * Ser� feito cache dessa query entao esse m�todo s� ser� chamado uma vez.
	 * @param hbQueryFindForCombo
	 * @return
	 */
	protected String getQueryFindForCombo(String hbQueryFindForCombo) {
		return hbQueryFindForCombo;
	}

	/* (non-Javadoc)
	 * @see org.nextframework.persistence.DAO#findAll()
	 */
	public List<BEAN> findAll(){
		return findAll(this.orderBy);
	}
	
	/* (non-Javadoc)
	 * @see org.nextframework.persistence.DAO#findAll(java.lang.String)
	 */
	public List<BEAN> findAll(String orderBy){
		return query()
				.orderBy(orderBy)
				.list();
	}
	
	/* (non-Javadoc)
	 * @see org.nextframework.persistence.DAO#findForListagem(org.nextframework.controller.crud.FiltroListagem)
	 */
	public ListagemResult<BEAN> findForListagem(FiltroListagem filtro){
		QueryBuilder<BEAN> query = query();
		if(orderBy == null){//ordena��o default para telas de listagem de dados
			query.orderBy(Util.strings.uncaptalize(beanClass.getSimpleName())+".id");
		} else {
			query.orderBy(orderBy);
		}
		updateListagemQuery(query, filtro);
		QueryBuilder<BEAN> queryBuilder = query;
		return new ListagemResult<BEAN>(queryBuilder, filtro);
	}
	
	/**
	 * Sobrescreva esse m�todo para atualizar a query de listagem de dados 
	 * @param query
	 * @param _filtro
	 */
	public void updateListagemQuery(QueryBuilder<BEAN> query, FiltroListagem _filtro) {
	}

	/**
	 * Sobrescreva esse m�todo para atualizar a query de entrada de dados
	 * @param query
	 */
	public void updateEntradaQuery(QueryBuilder<BEAN> query) {
	}
	
	/**
	 * Sobrescreva esse m�todo para atualizar a estrat�gia de salvar o bean
	 * @param save
	 */
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
	}

	/* (non-Javadoc)
	 * @see org.nextframework.persistence.DAO#delete(BEAN)
	 */
	public void delete(BEAN bean){
		// verifica se j� existe uma transa��o acontecendo.. se existir.. utiliza a atual
		// se nao .. cria uma nova
		Session session = SessionFactoryUtils.getSession(getHibernateTemplate().getSessionFactory(), getHibernateTemplate().getEntityInterceptor(), getHibernateTemplate().getJdbcExceptionTranslator());
		Transaction transaction = session.getTransaction();
		boolean intransaction = true;
		if(!transaction.isActive()){
			transaction = session.beginTransaction();	
			intransaction = false;
		}
		 
		try {
			if(autoManageFileProperties() && fileDAO != null){
				BEAN load = load(bean);
				if(load != null){
					bean = load;
				}
			}
			getHibernateTemplate().delete(bean);
			if(autoManageFileProperties() && fileDAO != null){
				for (PropertyDescriptor pd : fileProperties) {
					File fileToDelete = (File) pd.getReadMethod().invoke(bean);
					if(fileToDelete != null){
						fileDAO.delete(fileToDelete);
					}
				}				
			}
			session.flush();
			if (!intransaction) {
				transaction.commit();
			}
		} catch (HibernateException ex) {
			if (!intransaction) {
				transaction.rollback();
			}
			throw convertHibernateAccessException(ex);
		} catch (RuntimeException e) {
			if (!intransaction) {
				transaction.rollback();
			}
			throw e;
		} catch (Exception e){
			if (!intransaction) {
				transaction.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			
			SessionFactoryUtils.releaseSession(session, getHibernateTemplate().getSessionFactory());
				
		}
		
		
		/*
		// esse c�digo teve que ser feito porque o delete do hibernateTemplate tenta d� uns updates quando tem UserType na classe
		// n�o suportar� camposite pk
		// PARECE que foi consertado, se voltar a contecer updates na hora de deltar voltar com o c�digo e arrumar outra estrat�gia para fazer 
		// o log da operacoes no banco
		final ClassMetadata classMetadata = getHibernateTemplate().getSessionFactory().getClassMetadata(beanClass);
		final String id = classMetadata.getIdentifierPropertyName();
		getHibernateTemplate().execute(new HibernateCallback(){

			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Transaction transaction = session.beginTransaction();
								
				Serializable identifier = classMetadata.getIdentifier(bean, EntityMode.POJO);
				
				//querys manuais n�o chamam os listeners.. chamar aqui
								
				session
					.createQuery("delete "+beanClass.getName()+" where "+id+" = :id")
					.setParameter("id", identifier)
					.executeUpdate();
				
				
				transaction.commit();
				return null;
			}
			
		});
		//getHibernateTemplate().delete(bean);
		 */
		 
	}

	public void suggestLoadForListagem(String propertyName) {
		//System.out.println("\n\n"+this.getClass().getSimpleName()+"  SUGERIDO "+propertyName);
		//TODO SER� IMPLEMENTADO NA VERSAO 3.5.0 RC3
	}


}
