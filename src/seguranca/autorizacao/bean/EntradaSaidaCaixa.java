package seguranca.autorizacao.bean;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.Money;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_entrada_saida_caixa", sequenceName="sq_entrada_saida_caixa")
public class EntradaSaidaCaixa {
	private Integer idEntradaSaidaCaixa;
	private Date data;
	private Time hora;
	private Usuario usuario;
	private Money valor;
	private FormaPagamento formaPagamento;
	private Caixa caixa;
	private Boolean pendente;
	private TipoOperacao tipoOperacao;//determina entrada, saida, estorno...
	private ProdutoServico produtoServico;
	private Cliente cliente;
	private String motivo;
	
	
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	private Money valorConfirmacao;
	private EntradaSaidaCaixa entradaSaidaCaixa;
	
	@Transient
	@Required
	public Money getValorConfirmacao() {
		return valorConfirmacao;
	}
	@Transient
	@Required
	public EntradaSaidaCaixa getEntradaSaidaCaixa() {
		return entradaSaidaCaixa;
	}
	public void setValorConfirmacao(Money valorConfirmacao) {
		this.valorConfirmacao = valorConfirmacao;
	}
	public void setEntradaSaidaCaixa(EntradaSaidaCaixa entradaSaidaCaixa) {
		this.entradaSaidaCaixa = entradaSaidaCaixa;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_entrada_saida_caixa")
	public Integer getIdEntradaSaidaCaixa() {
		return idEntradaSaidaCaixa;
	}
	public void setIdEntradaSaidaCaixa(Integer idEntradaSaidaCaixa) {
		this.idEntradaSaidaCaixa = idEntradaSaidaCaixa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id")
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	@DescriptionProperty
	@Required
	public Money getValor() {
		return valor;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idFormaPagamento")
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idCaixa")
	public Caixa getCaixa() {
		return caixa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idTipoOperacao")
	public TipoOperacao getTipoOperacao() {
		return tipoOperacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idProdutoServico")
	public ProdutoServico getProdutoServico() {
		return produtoServico;
	}
	@ManyToOne
	@JoinColumn(name="idCliente")
	public Cliente getCliente() {
		return cliente;
	}
	public Boolean getPendente() {
		return pendente;
	}
	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	public void setPendente(Boolean pendente) {
		this.pendente = pendente;
	}
	public void setCaixa(Caixa caixa) {
		this.caixa = caixa;
	}
	public Date getData() {
		return data;
	}
	public Time getHora() {
		return hora;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setHora(Time hora) {
		this.hora = hora;
	}
	public void setTipoOperacao(TipoOperacao tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}
	public void setProdutoServico(ProdutoServico produtoServico) {
		this.produtoServico = produtoServico;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
