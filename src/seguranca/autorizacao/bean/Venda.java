package seguranca.autorizacao.bean;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Money;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_venda",sequenceName="sq_venda")
@DisplayName("Frente de Caixa")
public class Venda {
	private Integer idVenda;
	private Cliente cliente;
	private Usuario usuario;
	private List<ServicosPrestados> servicosPrestados;
	private Date data;
	private Time hora;
	private Money previaValor;
	private Boolean fechado;
	private List<Pagamento> listaPagamento;
	
	
	private Boolean idUpdate;
	@Transient
	public Boolean getIdUpdate() {
		return idUpdate;
	}
	public void setIdUpdate(Boolean idUpdate) {
		this.idUpdate = idUpdate;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_venda")
	public Integer getIdVenda() {
		return idVenda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idCliente")
	@Required
	public Cliente getCliente() {
		return cliente;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id")
	public Usuario getUsuario() {
		return usuario;
	}
	@OneToMany(mappedBy="venda")
	public List<Pagamento> getListaPagamento() {
		return listaPagamento;
	}
	public void setListaPagamento(List<Pagamento> listaPagamento) {
		this.listaPagamento = listaPagamento;
	}
	public void setIdVenda(Integer idVenda) {
		this.idVenda = idVenda;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Date getData() {
		return data;
	}
	public Time getHora() {
		return hora;
	}
	@OneToMany(mappedBy="venda")
	public List<ServicosPrestados> getServicosPrestados() {
		return servicosPrestados;
	}
	public void setServicosPrestados(List<ServicosPrestados> servicosPrestados) {
		this.servicosPrestados = servicosPrestados;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setHora(Time hora) {
		this.hora = hora;
	}
	public Boolean getFechado() {
		return fechado;
	}
	@Transient
	public Money getPreviaValor() {
		return previaValor;
	}
	public void setPreviaValor(Money previaValor) {
		this.previaValor = previaValor;
	}
	public void setFechado(Boolean fechado) {
		this.fechado = fechado;
	}
}
