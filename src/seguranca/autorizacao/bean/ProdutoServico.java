package seguranca.autorizacao.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_produto_servico",sequenceName="sq_produto_servico")
@DisplayName(value="Produtos e Servi�os")
public class ProdutoServico {
	
	private Integer idProdutoServico;
	private String codigo;
	private String nome;
	private TipoProduto tipoProduto;
	private SubTipoProduto subTipoProduto;
	private Integer stock;
	private String observacoes;
	private Boolean ativo;
	private Integer qtdMinima;
	private List<ValorProdutoServico> listaValorProdutoServico;
	private ValorProdutoServico valorProdutoServico;
	private List<ItemProduto> listaItensProdutos;
	
	private Integer qtdEntrada;
	private Integer qtdSaida;
	private ItemProduto itemProduto;
	private Integer qtdRestante;
	
	@Transient
	public Integer getQtdRestante() {
		return qtdRestante;
	}
	public void setQtdRestante(Integer qtdRestante) {
		this.qtdRestante = qtdRestante;
	}
	
	@Transient
	@DisplayName("Quantidade Entrada")
	public Integer getQtdEntrada() {
		return qtdEntrada;
	}
	@Transient
	@Required
	@DisplayName("Quantidade Sa�da")
	public Integer getQtdSaida() {
		return qtdSaida;
	}
	public void setQtdEntrada(Integer qtdEntrada) {
		this.qtdEntrada = qtdEntrada;
	}
	public void setQtdSaida(Integer qtdSaida) {
		this.qtdSaida = qtdSaida;
	}
	@Transient
	public ItemProduto getItemProduto() {
		return itemProduto;
	}
	public void setItemProduto(ItemProduto itemProduto) {
		this.itemProduto = itemProduto;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_produto_servico")
	public Integer getIdProdutoServico() {
		return idProdutoServico;
	}
	@Required
	public String getCodigo() {
		return codigo;
	}
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@Required
	@Transient
	public TipoProduto getTipoProduto() {
		return tipoProduto;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idSubTipoProduto")
	public SubTipoProduto getSubTipoProduto() {
		return subTipoProduto;
	}
	
	@DisplayName("Estoque atual")
	@Transient
	public Integer getStock() {
		return stock;
	}
	@MaxLength(1000)
	public String getObservacoes() {
		return observacoes;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Quantidade M�nima em Estoque")
	public Integer getQtdMinima() {
		return qtdMinima;
	}
	@OneToMany(mappedBy="produtoServico")
	public List<ValorProdutoServico> getListaValorProdutoServico() {
		return listaValorProdutoServico;
	}
	@Transient
	public ValorProdutoServico getValorProdutoServico() {
		return valorProdutoServico;
	}

	@OneToMany(mappedBy="produtoServico")
	@DisplayName("Estoque")
	public List<ItemProduto> getListaItensProdutos() {
		return listaItensProdutos;
	}
	public void setIdProdutoServico(Integer idProdutoServico) {
		this.idProdutoServico = idProdutoServico;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setTipoProduto(TipoProduto tipoProduto) {
		this.tipoProduto = tipoProduto;
	}
	public void setSubTipoProduto(SubTipoProduto subTipoProduto) {
		this.subTipoProduto = subTipoProduto;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setQtdMinima(Integer qtdMinima) {
		this.qtdMinima = qtdMinima;
	}
	public void setListaValorProdutoServico(List<ValorProdutoServico> listaValorProdutoServico) {
		this.listaValorProdutoServico = listaValorProdutoServico;
	}
	public void setValorProdutoServico(ValorProdutoServico valorProdutoServico) {
		this.valorProdutoServico = valorProdutoServico;
	}
	public void setListaItensProdutos(List<ItemProduto> listaItensProdutos) {
		this.listaItensProdutos = listaItensProdutos;
	}
	
}
