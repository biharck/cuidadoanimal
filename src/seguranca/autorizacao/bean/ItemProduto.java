package seguranca.autorizacao.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Money;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_iten_produto",sequenceName="sq_iten_produto")
@DisplayName("Itens")
public class ItemProduto {
	
	private Integer idItemProduto;
	private ProdutoServico produtoServico;
	private String lote;
	private Integer quantidadeEntrada;
	private Integer quantidadeRestante;
	private Integer quantidadeSaida;
	private Date dtEntrada;
	private Date dtVencimento;
	private Money valorEntrada;
	private Fornecedor fornecedor;
	private String motivo;
	
	@Id
	@GeneratedValue(generator="sq_iten_produto", strategy=GenerationType.AUTO)
	public Integer getIdItemProduto() {
		return idItemProduto;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idProdutoServico")
	public ProdutoServico getProdutoServico() {
		return produtoServico;
	}
	@Required
	@DescriptionProperty
	public String getLote() {
		return lote;
	}
	public Integer getQuantidadeSaida() {
		return quantidadeSaida;
	}
	@Required
	public Integer getQuantidadeEntrada() {
		return quantidadeEntrada;
	}
	@Transient
	public Integer getQuantidadeRestante() {
		return quantidadeRestante;
	}
	@Required
	public Date getDtEntrada() {
		return dtEntrada;
	}
	@Required
	public Date getDtVencimento() {
		return dtVencimento;
	}
	public Money getValorEntrada() {
		return valorEntrada;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idFornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@Required
	@MaxLength(1000)
	public String getMotivo() {
		return motivo;
	}
	public void setIdItemProduto(Integer idItemProduto) {
		this.idItemProduto = idItemProduto;
	}
	public void setProdutoServico(ProdutoServico produtoServico) {
		this.produtoServico = produtoServico;
	}
	public void setLote(String lote) {
		this.lote = lote;
	}
	public void setQuantidadeEntrada(Integer quantidadeEntrada) {
		this.quantidadeEntrada = quantidadeEntrada;
	}
	public void setQuantidadeRestante(Integer quantidadeRestante) {
		this.quantidadeRestante = quantidadeRestante;
	}
	public void setDtEntrada(Date dtEntrada) {
		this.dtEntrada = dtEntrada;
	}
	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	public void setValorEntrada(Money valorEntrada) {
		this.valorEntrada = valorEntrada;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public void setQuantidadeSaida(Integer quantidadeSaida) {
		this.quantidadeSaida = quantidadeSaida;
	}
}
