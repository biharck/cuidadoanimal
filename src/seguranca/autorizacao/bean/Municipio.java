package seguranca.autorizacao.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_municipio", sequenceName = "sq_municipio")
@DisplayName(value="Município")
public class Municipio {

	protected Integer idMunicipio;
	protected String nome;
	protected Uf uf;
	protected Integer idUsuarioAltera;
	protected Timestamp dtaltera;

	public Municipio(){}
	public Municipio(Integer idMunicipio){
		this.idMunicipio = idMunicipio;
	}
	public Municipio(Integer idMunicipio, Uf uf){
		this.idMunicipio = idMunicipio;
		this.uf = uf;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_municipio")
	public Integer getIdMunicipio() {
		return idMunicipio;
	}
	public void setIdMunicipio(Integer idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idUf")
	@DisplayName("UF")
	public Uf getUf() {
		return uf;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	

}

