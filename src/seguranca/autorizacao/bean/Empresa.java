package seguranca.autorizacao.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_empresa", sequenceName = "sq_empresa")
public class Empresa {
 
	private Integer idEmpresa;
	private String razaoSocial;
	private String cnpj;
	private String inscricaoEstadual;
	private String inscricaoMunicipal;
	private String logradouro;
	private String cep;
	private String bairro;
	private Municipio municipio;
	private String email;
	private String telefone1;
	private String telefone2;
	private List<Ramal> listaRamal;
	private Arquivo foto;
	
	//hor�rio funcionamento
	private String abertura;
	private String fechamento;
	private Boolean aberto24Horas;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfile")
	public Arquivo getFoto() {
		return foto;
	}
	public void setFoto(Arquivo foto) {
		this.foto = foto;
	}
	
	@Required
	@DisplayName("Aberto 24 Horas?")
	public Boolean getAberto24Horas() {
		return aberto24Horas;
	}
	
	public void setAberto24Horas(Boolean aberto24Horas) {
		this.aberto24Horas = aberto24Horas;
	}
	
	public String getAbertura() {
		return abertura;
	}
	public void setAbertura(String abertura) {
		this.abertura = abertura;
	}
	public String getFechamento() {
		return fechamento;
	}
	public void setFechamento(String fechamento) {
		this.fechamento = fechamento;
	}
	
	//transient
	private Uf uf;
	private String confirmaEmail;
	
	@Transient
	@Required
	@DisplayName("Unidade Federativa")
	public Uf getUf() {
		return uf;
	}
	@Transient
	@DisplayName("Confirma��o do e-mail")
	@MaxLength(100)
	@MinLength(5)
	public String getConfirmaEmail() {
		return confirmaEmail;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setConfirmaEmail(String confirmaEmail) {
		this.confirmaEmail = confirmaEmail;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_empresa")
	@DisplayName("Id")
	public Integer getIdEmpresa() {
		return idEmpresa;
	}
	@Required
	@DisplayName("Raz�o Social")
	@MaxLength(100)
	@MinLength(5)
	public String getRazaoSocial() {
		return razaoSocial;
	}
	@Required
	@DisplayName("CNPJ")	
	public String getCnpj() {
		return cnpj;
	}
	@Required
	@DisplayName("Inscri��o Estadual")
	@MaxLength(50)
	@MinLength(5)
	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}
	@Required
	@DisplayName("Incri��o Municipal")
	@MaxLength(50)
	@MinLength(5)
	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}
	@Required
	@DisplayName("Logradouro")
	@MaxLength(200)
	@MinLength(10)
	public String getLogradouro() {
		return logradouro;
	}
	@Required
	@DisplayName("CEP")
	public String getCep() {
		return cep;
	}
	@Required
	@DisplayName("Bairro")
	@MaxLength(50)
	@MinLength(5)
	public String getBairro() {
		return bairro;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idMunicipio")
	@Required
	@DisplayName("Municipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@Required
	@DisplayName("E-mail")
	@MaxLength(100)
	@MinLength(10)
	public String getEmail() {
		return email;
	}
	@Required
	@DisplayName("Telefone")
	public String getTelefone1() {
		return telefone1;
	}
	@DisplayName("Telefone")
	public String getTelefone2() {
		return telefone2;
	}
	@OneToMany(mappedBy="empresa")
	@DisplayName("Lista de Ramais")
	public List<Ramal> getListaRamal() {
		return listaRamal;
	}
	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}
	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}
	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}
	public void setListaRamal(List<Ramal> listaRamal) {
		this.listaRamal = listaRamal;
	}
	
	
	 
}
 
