package seguranca.autorizacao.bean;

import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@SequenceGenerator(name = "sq_responsavel", sequenceName = "sq_responsavel")
public class Responsavel {
 
	private Integer idResponsavel;
	private String nome;
	
	@Id
	@SequenceGenerator(name="sq_responsavel", sequenceName="sq_responsavel")
	public Integer getIdResponsavel() {
		return idResponsavel;
	}
	public String getNome() {
		return nome;
	}
	public void setIdResponsavel(Integer idResponsavel) {
		this.idResponsavel = idResponsavel;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	 
}
 
