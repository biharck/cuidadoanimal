package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.authorization.Role;
import org.nextframework.authorization.impl.AbstractPermission;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.Required;

/**
 * Representa a permiss�o para determinado papel em determinada tela
 */
@Entity
@SequenceGenerator(name="SQ_PERMISSAO",sequenceName="SQ_PERMISSAO")
public class Permissao extends AbstractPermission {
    
    private Long id;
    private Papel papel;
    private Tela tela;
    private String permissionString;
    
    @Transient
    public Role getRole() {
    	return getPapel();
    }
    
    @Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="SQ_PERMISSAO")
	public Long getId() {
		return id;
	}
    
	public void setId(Long id) {
		this.id = id;
	}

	@DescriptionProperty
	@Required
	@ManyToOne
	public Tela getTela() {
		return tela;
	}

	public void setTela(Tela tela) {
		this.tela = tela;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	public Papel getPapel() {
		return papel;
	}

	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	
	@Override
	public String getPermissionString() {
		return permissionString;
	}
	
	@Override
	public void setPermissionString(String permissionString) {
		this.permissionString = permissionString;
	}

}