package seguranca.autorizacao.bean;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_pagamento",sequenceName="sq_pagamento")
public class Pagamento {
	
	private Integer idPagamento;
	private Date dtPagamento;
	private Time horaPagamento;
	private Venda venda;
	private Usuario usuario;
	private List<VendaFormaPagamento> listaVendaFormaPagamento;
	@Id
	@GeneratedValue(generator="sq_pagamento",strategy=GenerationType.AUTO)
	public Integer getIdPagamento() {
		return idPagamento;
	}
	public Date getDtPagamento() {
		return dtPagamento;
	}
	public Time getHoraPagamento() {
		return horaPagamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idVenda")
	public Venda getVenda() {
		return venda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id")
	public Usuario getUsuario() {
		return usuario;
	}
	@OneToMany(mappedBy="pagamento")
	public List<VendaFormaPagamento> getListaVendaFormaPagamento() {
		return listaVendaFormaPagamento;
	}
	public void setIdPagamento(Integer idPagamento) {
		this.idPagamento = idPagamento;
	}
	public void setDtPagamento(Date dtPagamento) {
		this.dtPagamento = dtPagamento;
	}
	public void setHoraPagamento(Time horaPagamento) {
		this.horaPagamento = horaPagamento;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setListaVendaFormaPagamento(
			List<VendaFormaPagamento> listaVendaFormaPagamento) {
		this.listaVendaFormaPagamento = listaVendaFormaPagamento;
	}
}
