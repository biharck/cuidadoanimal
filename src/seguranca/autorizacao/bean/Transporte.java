package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_transporte",sequenceName="sq_transporte")
public class Transporte {
	
	private Integer idTransporte;
	private String descricao;
	private Boolean ativo;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_transporte")
	public Integer getIdTransporte() {
		return idTransporte;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	
	@Required
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public void setIdTransporte(Integer idTransporte) {
		this.idTransporte = idTransporte;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
