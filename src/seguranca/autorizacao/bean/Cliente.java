package seguranca.autorizacao.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_cliente", sequenceName = "sq_cliente")
public class Cliente {
 
	private Integer idCliente;
	private String nome;
	private String cpf;
	private String rg;
	private String telefone1;
	private String telefone2;
	private String celular;
	private String logradouro;
	private String cep;
	private String bairro;
	private Municipio municipio;
	private String email;
	private Date dtCadastro;
	
	private List<Animal> listaAnimal;
	
	//transient
	private Uf uf;
	private String confirmaEmail;
	
	@Transient
	@Required
	@DisplayName("Unidade Federativa")
	public Uf getUf() {
		return uf;
	}
	@Transient
	@DisplayName("Confirmação do e-mail")
	@MaxLength(100)
	@MinLength(5)
	public String getConfirmaEmail() {
		return confirmaEmail;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setConfirmaEmail(String confirmaEmail) {
		this.confirmaEmail = confirmaEmail;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_cliente")
	public Integer getIdCliente() {
		return idCliente;
	}
	@DescriptionProperty
	@DisplayName("Nome")
	@MaxLength(100)
	@Required
	@MinLength(10)
	public String getNome() {
		return nome;
	}
	@OneToMany(mappedBy="dono")
	@DisplayName("Animais do Cliente")
	public List<Animal> getListaAnimal() {
		return listaAnimal;
	}
	@Required
	public String getCpf() {
		return cpf;
	}
	@MaxLength(20)
	@DisplayName("Identidade")
	@Required
	public String getRg() {
		return rg;
	}
	@Required
	@DisplayName("Telefone Principal")
	public String getTelefone1() {
		return telefone1;
	}
	
	@DisplayName("Telefone")
	public String getTelefone2() {
		return telefone2;
	}
	@Required
	public String getCelular() {
		return celular;
	}
	@Required
	@DisplayName("Logradouro")
	@MaxLength(200)
	@MinLength(2)
	public String getLogradouro() {
		return logradouro;
	}
	@Required
	@DisplayName("CEP")
	public String getCep() {
		return cep;
	}
	@Required
	@DisplayName("Bairro")
	@MaxLength(50)
	@MinLength(2)
	public String getBairro() {
		return bairro;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idMunicipio")
	@Required
	@DisplayName("Municipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@DisplayName("E-mail")
	@MaxLength(100)
	@MinLength(10)
	public String getEmail() {
		return email;
	}
	public Date getDtCadastro() {
		return dtCadastro;
	}
	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}
	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setListaAnimal(List<Animal> listaAnimal) {
		this.listaAnimal = listaAnimal;
	}
	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}
}
 
