package seguranca.autorizacao.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

/**
 * Representa um papel no sistema, um n�vel de permiss�o.
 * Ex.: Administrador, Usu�rio, Financeiro
 */
@Entity
@SequenceGenerator(name="SQ_PAPEL",sequenceName="SQ_PAPEL")
@Table(name="PAPEL")
@DisplayName("Cargo")
public class Papel implements org.nextframework.authorization.Role {
    
	private Long id;
	private String descricao;
	private String nome;

	@Transient
	public String getName() {
		return getNome();
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="SQ_PAPEL")
	@Column(name="ID_PAPEL")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Required
	@MaxLength(value=100)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@Required
	@MaxLength(value=50)
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}