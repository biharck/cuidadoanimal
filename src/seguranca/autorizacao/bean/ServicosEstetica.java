package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_servicos_estetica",sequenceName="sq_servicos_estetica")
@DisplayName("Servi�os De Est�tica")
public class ServicosEstetica {
	
	private Integer idServicosEstetica;
	private String descricao;
	private Estetica estetica;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_servicos_estetica")
	public Integer getIdServicosEstetica() {
		return idServicosEstetica;
	}
	@Required
	public String getDescricao() {
		return descricao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idEstetica")
	public Estetica getEstetica() {
		return estetica;
	}
	public void setIdServicosEstetica(Integer idServicosEstetica) {
		this.idServicosEstetica = idServicosEstetica;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setEstetica(Estetica estetica) {
		this.estetica = estetica;
	}
}
