package seguranca.autorizacao.bean;

import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@SequenceGenerator(name = "sq_responsavelanimal", sequenceName = "sq_responsavelanimal")
public class ResponsavelAnimal {
 
	private Integer idResponsavelAnimal;
	private Animal animal;
	private Responsavel responsavel;
	
	@Id
	@SequenceGenerator(name="sq_responsavelanimal", sequenceName="sq_responsavelanimal")
	public Integer getIdResponsavelAnimal() {
		return idResponsavelAnimal;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idAnimal")
	public Animal getAnimal() {
		return animal;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idResponsavel")
	public Responsavel getResponsavel() {
		return responsavel;
	}
	public void setIdResponsavelAnimal(Integer idResponsavelAnimal) {
		this.idResponsavelAnimal = idResponsavelAnimal;
	}
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	public void setResponsavel(Responsavel responsavel) {
		this.responsavel = responsavel;
	}
	 
	
	
	 
}
 
