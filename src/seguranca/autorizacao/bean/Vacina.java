package seguranca.autorizacao.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_vacina",sequenceName="sq_vacina")
public class Vacina {
	
	private Integer idVacina;
	private Date dtAplicacao = new Date(System.currentTimeMillis());
	private Double peso;
	private Date dtProximaAplicacao;
	private Date dtAgendamento;
	private Dose dose;
	private Animal animal;
	private ProdutoServico produtoServico;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_vacina")
	public Integer getIdVacina() {
		return idVacina;
	}
	@Required
	@DisplayName("Data de Aplica��o")
	public Date getDtAplicacao() {
		return dtAplicacao;
	}
	@Required
	@DisplayName("Peso/KG")
	public Double getPeso() {
		return peso;
	}
	@DisplayName("Data da Pr�xima Aplica��o")
	public Date getDtProximaAplicacao() {
		return dtProximaAplicacao;
	}
	
	@DisplayName("Data De Agendamento")
	public Date getDtAgendamento() {
		return dtAgendamento;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idDose")
	public Dose getDose() {
		return dose;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idAnimal")
	public Animal getAnimal() {
		return animal;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idProdutoServico")
	public ProdutoServico getProdutoServico() {
		return produtoServico;
	}
	public void setIdVacina(Integer idVacina) {
		this.idVacina = idVacina;
	}
	public void setDtAplicacao(Date dtAplicacao) {
		this.dtAplicacao = dtAplicacao;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public void setDtProximaAplicacao(Date dtProximaAplicacao) {
		this.dtProximaAplicacao = dtProximaAplicacao;
	}
	public void setDose(Dose dose) {
		this.dose = dose;
	}
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	public void setProdutoServico(ProdutoServico produtoServico) {
		this.produtoServico = produtoServico;
	}
	public void setDtAgendamento(Date dtAgendamento) {
		this.dtAgendamento = dtAgendamento;
	}
}
