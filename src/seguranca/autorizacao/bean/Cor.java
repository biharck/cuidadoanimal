package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_cor", sequenceName = "sq_cor")
@DisplayName("Cor")
public class Cor {
 
	private Integer idCor;
	private String nome;
	private Boolean ativo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_cor")
	public Integer getIdCor() {
		return idCor;
	}
	@DisplayName("Nome")
	@DescriptionProperty
	@MaxLength(50)
	@MinLength(3)
	@Required
	public String getNome() {
		return nome;
	}
	@Required
	@DisplayName("Ativo?")
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setIdCor(Integer idCor) {
		this.idCor = idCor;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	 
}
 
