package seguranca.autorizacao.bean;

import java.util.Date;

import org.nextframework.bean.annotation.DisplayName;

public class HistoricoClinico {
	private Integer idConsultas;
	private String descricao;
	private String tipo;
	private Date dataRealizacao;
	
	public HistoricoClinico(String descricao, String tipo, java.util.Date date,Integer idConsultas){
		this.descricao = descricao;
		this.tipo = tipo;
		this.dataRealizacao = date;
		this.idConsultas = idConsultas;
	}
	public HistoricoClinico(){
		
	}
	@DisplayName("Histórico")
	public String getDescricao() {
		return descricao;
	}
	public Integer getIdConsultas() {
		return idConsultas;
	}
	public String getTipo() {
		return tipo;
	}
	public Date getDataRealizacao() {
		return dataRealizacao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setDataRealizacao(Date dataRealizacao) {
		this.dataRealizacao = dataRealizacao;
	}
	public void setIdConsultas(Integer idConsultas) {
		this.idConsultas = idConsultas;
	}
}
