package seguranca.autorizacao.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Money;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_servicos_prestados", sequenceName="sq_servicos_prestados")
public class ServicosPrestados {
	
	private Integer idServicosPrestados;
	private Date date = new Date(System.currentTimeMillis());
	private Animal animal;
	private ItemProduto itemProduto;
	private Integer qtd;
	private Venda venda;
	private Money desconto;
	private String produto;
	private Money valorIndividual;
	
	@Transient
	public String getProduto() {
		return produto;
	}
	@Transient
	public Money getValorIndividual() {
		return valorIndividual;
	}
	private Integer resto;
	@Transient
	public Integer getResto() {
		return resto;
	}
	public void setResto(Integer resto) {
		this.resto = resto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}
	public void setValorIndividual(Money valorIndividual) {
		this.valorIndividual = valorIndividual;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_servicos_prestados")
	public Integer getIdServicosPrestados() {
		return idServicosPrestados;
	}
	public void setIdServicosPrestados(Integer idServicosPrestados) {
		this.idServicosPrestados = idServicosPrestados;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idVenda")
	public Venda getVenda() {
		return venda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idAnimal")
	public Animal getAnimal() {
		return animal;
	}
	@Required
	@DisplayName("Quantidade")
	public Integer getQtd() {
		return qtd;
	}
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idItemProduto")
	public ItemProduto getItemProduto() {
		return itemProduto;
	}
	public Money getDesconto() {
		return desconto;
	}
	public void setItemProduto(ItemProduto itemProduto) {
		this.itemProduto = itemProduto;
	}
	public void setQtd(Integer qtd) {
		this.qtd = qtd;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
}
