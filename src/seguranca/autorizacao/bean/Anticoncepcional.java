package seguranca.autorizacao.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_anticoncepcional",sequenceName="sq_anticoncepcional")
public class Anticoncepcional {
	
	private Integer idAnticoncepcional;
	private Date dtAplicacao = new Date(System.currentTimeMillis());
	private Double peso;
	private Date dtProximaAplicacao;
	private Dose dose;
	private Animal animal;
	private ProdutoServico produtoServico;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_anticoncepcional")
	public Integer getIdAnticoncepcional() {
		return idAnticoncepcional;
	}
	@DisplayName("Data de Aplica��o")
	@Required
	public Date getDtAplicacao() {
		return dtAplicacao;
	}
	
	@Required
	@DisplayName("Peso/KG")
	public Double getPeso() {
		return peso;
	}
	@DisplayName("Data da Pr�xima Aplica��o")
	public Date getDtProximaAplicacao() {
		return dtProximaAplicacao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idDose")
	public Dose getDose() {
		return dose;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idAnimal")
	@Required
	public Animal getAnimal() {
		return animal;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idProdutoServico")
	public ProdutoServico getProdutoServico() {
		return produtoServico;
	}
	public void setIdAnticoncepcional(Integer idAnticoncepcional) {
		this.idAnticoncepcional = idAnticoncepcional;
	}
	public void setDtAplicacao(Date dtAplicacao) {
		this.dtAplicacao = dtAplicacao;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public void setDtProximaAplicacao(Date dtProximaAplicacao) {
		this.dtProximaAplicacao = dtProximaAplicacao;
	}
	public void setDose(Dose dose) {
		this.dose = dose;
	}
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	public void setProdutoServico(ProdutoServico produtoServico) {
		this.produtoServico = produtoServico;
	}
}
