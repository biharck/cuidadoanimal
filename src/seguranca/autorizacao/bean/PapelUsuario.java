package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.nextframework.validation.annotation.Required;

/**
 * Faz o relacionamento entre Usu�rio e Papel
 * Relacionamento muitos para muitos entre usuario e papel
 */
@Entity
@SequenceGenerator(name="SQ_PAPEL_USUARIO",sequenceName="SQ_PAPEL_USUARIO")
public class PapelUsuario {

	private Long id;
	private Usuario usuario;
	private Papel papel;
    
    @Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="SQ_PAPEL_USUARIO")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    
	@Required
	@ManyToOne
    public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	@Required
	@ManyToOne
	public Papel getPapel() {
		return papel;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
}
