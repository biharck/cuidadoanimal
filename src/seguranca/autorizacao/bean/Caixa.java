package seguranca.autorizacao.bean;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Money;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_caixa", sequenceName="sq_caixa")
public class Caixa {
	
	private Integer idCaixa;
	private List<EntradaSaidaCaixa> listaEntradaSaidaCaixa;
	private List<EntradaSaidaCaixa> listaEntradaSaidaCaixa2;
	private List<EntradaSaidaCaixa> listaEstorno;
	private Money valorAbertura;
	private Money valorFechamento;
	private Time horaAbertura;
	private Time horaFechamento;
	private Date dataAbertura;
	private Date dataFechamento;
	private Money diferencaCaixa;
	private Usuario usuario;//usuario que abriu o caixa
	
	private Money saldoParcial;
	
	@Transient
	public Money getSaldoParcial() {
		return saldoParcial;
	}
	public void setSaldoParcial(Money saldoParcial) {
		this.saldoParcial = saldoParcial;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_caixa")
	public Integer getIdCaixa() {
		return idCaixa;
	}
	@Transient
	@DisplayName("Entradas")
	public List<EntradaSaidaCaixa> getListaEntradaSaidaCaixa() {
		return listaEntradaSaidaCaixa;
	}
	@Transient
	@DisplayName("Sa�das")
	public List<EntradaSaidaCaixa> getListaEntradaSaidaCaixa2() {
		return listaEntradaSaidaCaixa2;
	}
	@Transient
	@DisplayName("Estornos")
	public List<EntradaSaidaCaixa> getListaEstorno() {
		return listaEstorno;
	}
	@Required
	public Money getValorFechamento() {
		return valorFechamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id")
	public Usuario getUsuario() {
		return usuario;
	}
	public void setIdCaixa(Integer idCaixa) {
		this.idCaixa = idCaixa;
	}
	public void setListaEntradaSaidaCaixa(
			List<EntradaSaidaCaixa> listaEntradaSaidaCaixa) {
		this.listaEntradaSaidaCaixa = listaEntradaSaidaCaixa;
	}
	public void setValorFechamento(Money valorFechamento) {
		this.valorFechamento = valorFechamento;
	}
	public Time getHoraAbertura() {
		return horaAbertura;
	}
	public void setHoraAbertura(Time horaAbertura) {
		this.horaAbertura = horaAbertura;
	}
	public Time getHoraFechamento() {
		return horaFechamento;
	}
	public void setHoraFechamento(Time horaFechamento) {
		this.horaFechamento = horaFechamento;
	}
	public Date getDataAbertura() {
		return dataAbertura;
	}
	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}
	public Date getDataFechamento() {
		return dataFechamento;
	}
	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}
	public Money getDiferencaCaixa() {
		return diferencaCaixa;
	}
	public void setDiferencaCaixa(Money diferencaCaixa) {
		this.diferencaCaixa = diferencaCaixa;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setListaEntradaSaidaCaixa2(
			List<EntradaSaidaCaixa> listaEntradaSaidaCaixa2) {
		this.listaEntradaSaidaCaixa2 = listaEntradaSaidaCaixa2;
	}
	public void setListaEstorno(List<EntradaSaidaCaixa> listaEstorno) {
		this.listaEstorno = listaEstorno;
	}
	public Money getValorAbertura() {
		return valorAbertura;
	}
	public void setValorAbertura(Money valorAbertura) {
		this.valorAbertura = valorAbertura;
	}
}
