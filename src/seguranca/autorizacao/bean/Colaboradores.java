package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_colaboradores", sequenceName = "sq_colaboradores")
public class Colaboradores {
 
	private Integer idColaborador;
	private Empresa empresa;
	private String nome;
	private String cpf;
	private String rg;
	private String ctps;
	private String telefone1;
	private String telefone2;
	private String celular;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_colaboradores")
	public Integer getIdColaborador() {
		return idColaborador;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idEmpresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public String getNome() {
		return nome;
	}
	public String getCpf() {
		return cpf;
	}
	public String getRg() {
		return rg;
	}
	public String getCtps() {
		return ctps;
	}
	public String getTelefone1() {
		return telefone1;
	}
	public String getTelefone2() {
		return telefone2;
	}
	public String getCelular() {
		return celular;
	}
	public void setIdColaborador(Integer idColaborador) {
		this.idColaborador = idColaborador;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public void setCtps(String ctps) {
		this.ctps = ctps;
	}
	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}
	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	
	
	 
	 
}
 
