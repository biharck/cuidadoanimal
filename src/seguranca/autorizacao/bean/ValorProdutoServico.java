package seguranca.autorizacao.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Money;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_valor_produto_servico",sequenceName="sq_valor_produto_servico")
public class ValorProdutoServico {
	
	private Integer idValorProdutoServico;
	private Money valor;
	private Date dataInicio;
	private Date dataFim;
	private ProdutoServico produtoServico;
	private Float margemLucro;
	private Money valorFim;
	
	@Id
	@GeneratedValue(generator="sq_valor_produto_servico", strategy=GenerationType.AUTO)
	public Integer getIdValorProdutoServico() {
		return idValorProdutoServico;
	}
	@Required
	public Money getValor() {
		return valor;
	}
	@Required
	public Date getDataInicio() {
		return dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idProdutoServico")
	public ProdutoServico getProdutoServico() {
		return produtoServico;
	}
	@Required
	@DisplayName("Margem de Lucro (%)")
	public Float getMargemLucro() {
		return margemLucro;
	}
	public Money getValorFim() {
		return valorFim;
	}
	public void setIdValorProdutoServico(Integer idValorProdutoServico) {
		this.idValorProdutoServico = idValorProdutoServico;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public void setProdutoServico(ProdutoServico produtoServico) {
		this.produtoServico = produtoServico;
	}
	public void setMargemLucro(Float margemLucro) {
		this.margemLucro = margemLucro;
	}
	public void setValorFim(Money valorFim) {
		this.valorFim = valorFim;
	}
}
