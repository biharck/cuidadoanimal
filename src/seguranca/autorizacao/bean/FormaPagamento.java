package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_forma_pagamento", sequenceName="sq_forma_pagamento")
public class FormaPagamento {
	private Integer idFormaPagamento;
	private String descricao;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_forma_pagamento")
	public Integer getIdFormaPagamento() {
		return idFormaPagamento;
	}
	@DisplayName("Forma de Pagamento")
	public void setIdFormaPagamento(Integer idFormaPagamento) {
		this.idFormaPagamento = idFormaPagamento;
	}
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
