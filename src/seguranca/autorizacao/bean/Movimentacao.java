package seguranca.autorizacao.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_movimentacao", sequenceName="sq_movimentacao")
public class Movimentacao {
	
	private Integer idMovimentacao;
	private Integer tipo;//1 = entrada 2 = saida
	private Date dtMovimentacao;
	private ProdutoServico produtoServico;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_movimentacao")
	public Integer getIdMovimentacao() {
		return idMovimentacao;
	}
	public Integer getTipo() {
		return tipo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idProdutoServico")
	public ProdutoServico getProdutoServico() {
		return produtoServico;
	}
	public Date getDtMovimentacao() {
		return dtMovimentacao;
	}
	public void setIdMovimentacao(Integer idMovimentacao) {
		this.idMovimentacao = idMovimentacao;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public void setDtMovimentacao(Date dtMovimentacao) {
		this.dtMovimentacao = dtMovimentacao;
	}
	public void setProdutoServico(ProdutoServico produtoServico) {
		this.produtoServico = produtoServico;
	}
}
