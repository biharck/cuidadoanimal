package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.Required;


@Entity
@SequenceGenerator(name="SQ_PESSOA",sequenceName="SQ_PESSOA")
@DisplayName("Pessoas")
public class Pessoa {
	
	private Integer idPessoa;
	private Usuario usuario;

	protected String email;
	protected String nome;
	protected String cpf;
	protected String cnpj;
	protected String observacao;
	protected Telefone telefone;
	protected Telefone celular;
	protected Telefone comercial;
	protected Telefone fax;
	protected String endereco;
	protected Uf uf;
	protected Municipio municipio;
	protected String cep;
	protected String bairro;
	
	protected String confirmaEmail;
	protected String confirmasenha;
	@Transient
	@Required
	public String getConfirmasenha() {
		return confirmasenha;
	}
	@Required
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	@Required
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setConfirmasenha(String confirmasenha) {
		this.confirmasenha = confirmasenha;
	}
	@Transient
	@Required
	public String getConfirmaEmail() {
		return confirmaEmail;
	}
	public void setConfirmaEmail(String confirmaEmail) {
		this.confirmaEmail = confirmaEmail;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="SQ_PESSOA")
	public Integer getIdPessoa() {
		return idPessoa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id")
	public Usuario getUsuario() {
		return usuario;
	}
	@Required
	public String getEmail() {
		return email;
	}
	@Required
	public String getNome() {
		return nome;
	}
	
	public String getCpf() {
		return cpf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getObservacao() {
		return observacao;
	}
	@Required
	public Telefone getTelefone() {
		return telefone;
	}
	@Required
	public Telefone getCelular() {
		return celular;
	}
	@Required
	public Telefone getComercial() {
		return comercial;
	}
	public Telefone getFax() {
		return fax;
	}
	@Required
	public String getEndereco() {
		return endereco;
	}
	@Required
	@Transient
	public Uf getUf() {
		return uf;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idMunicipio")
	@Required
	public Municipio getMunicipio() {
		return municipio;
	}
	public void setIdPessoa(Integer idPessoa) {
		this.idPessoa = idPessoa;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}
	public void setCelular(Telefone celular) {
		this.celular = celular;
	}
	public void setComercial(Telefone comercial) {
		this.comercial = comercial;
	}
	public void setFax(Telefone fax) {
		this.fax = fax;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	
	
	

}
