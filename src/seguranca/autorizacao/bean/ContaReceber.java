package seguranca.autorizacao.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Money;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_conta_receber", sequenceName="sq_conta_receber")
@DisplayName("Contas A Receber")
public class ContaReceber {
	
	private Integer idContaReceber;
	private Cliente cliente;
	private String numeroDoc;
	private FormaPagamento formaPagamento;
	private Date dtVencimento;
	private Money valor;
	private String obs;
	private Boolean status;
	private Date dtRecebimento;
	
	public Date getDtRecebimento() {
		return dtRecebimento;
	}
	public void setDtRecebimento(Date dtRecebimento) {
		this.dtRecebimento = dtRecebimento;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_conta_receber")
	public Integer getIdContaReceber() {
		return idContaReceber;
	}
	
	

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idCliente")
	public Cliente getCliente() {
		return cliente;
	}

	@Required
	@DisplayName("N�mero do Documento")
	public String getNumeroDoc() {
		return numeroDoc;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idFormaPagamento")
	@DisplayName("Forma de Pagamento")
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	@Required
	@DisplayName("Data de Vencimento")
	public Date getDtVencimento() {
		return dtVencimento;
	}

	@Required
	public Money getValor() {
		return valor;
	}
	@MaxLength(200)
	@DisplayName("Observa��o")
	public String getObs() {
		return obs;
	}

	@DisplayName("Situa��o")
	public Boolean getStatus() {
		return status;
	}
	public void setIdContaReceber(Integer idContaReceber) {
		this.idContaReceber = idContaReceber;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setNumeroDoc(String numeroDoc) {
		this.numeroDoc = numeroDoc;
	}
	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	

}
