package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_sub_tipo_produto",sequenceName="sq_sub_tipo_produto")
@DisplayName(value="Sub-Tipo de Produtos")
public class SubTipoProduto {
	
	private Integer idSubTipoProduto;
	private TipoProduto tipoProduto;
	private String descricao;
	private Boolean ativo;
	
	
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_sub_tipo_produto")
	@DisplayName("Id")
	public Integer getIdSubTipoProduto() {
		return idSubTipoProduto;
	}
	@DescriptionProperty
	@DisplayName("Descri��o")
	@Required
	public String getDescricao() {
		return descricao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idTipoProduto")
	public TipoProduto getTipoProduto() {
		return tipoProduto;
	}
	public void setIdSubTipoProduto(Integer idSubTipoProduto) {
		this.idSubTipoProduto = idSubTipoProduto;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setTipoProduto(TipoProduto tipoProduto) {
		this.tipoProduto = tipoProduto;
	}
	

}
