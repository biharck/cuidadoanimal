package seguranca.autorizacao.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_consultas" , sequenceName="sq_consultas")
public class Consultas {
	
	private Integer idConsultas;
	private String descricao;
	private Animal animal;
	private Double peso;
	private Usuario veterinario;
	private Date data = new Date(System.currentTimeMillis());
	private String receituario;
	private Boolean retorno;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_consultas")
	public Integer getIdConsultas() {
		return idConsultas;
	}
	@DisplayName("Consulta")
	@MaxLength(5000)
	@Required
	public String getDescricao() {
		return descricao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idAnimal")
	@Required
	public Animal getAnimal() {
		return animal;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id")
	@Required
	public Usuario getVeterinario() {
		return veterinario;
	}
	@DisplayName("Peso/KG")
	@Required
	public Double getPeso() {
		return peso;
	}
	@Required
	@DisplayName("Data da Consulta")
	public Date getData() {
		return data;
	}
	@DisplayName("Receituário")
	@MaxLength(5000)
	@Required
	public String getReceituario() {
		return receituario;
	}
	public Boolean getRetorno() {
		return retorno;
	}
	public void setIdConsultas(Integer idConsultas) {
		this.idConsultas = idConsultas;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public void setVeterinario(Usuario veterinario) {
		this.veterinario = veterinario;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setReceituario(String receituario) {
		this.receituario = receituario;
	}
	public void setRetorno(Boolean retorno) {
		this.retorno = retorno;
	}
}
