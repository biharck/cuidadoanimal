package seguranca.autorizacao.bean;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_animal", sequenceName = "sq_animal")
public class Animal {

	private Integer idAnimal;
	private String nome;
	private String apelido;
	private Cliente dono;
	private Cor cor;
	private Especie especie;
	private Raca raca;
	private Date dtNascimento;
	private Date dtObito;
	private String RGA;
	private String microchip;
	private String observacoes;
	private Arquivo foto;
	private Boolean desaparecido;
	private Date dtDesaparecimento;
	private Boolean doado;
	private List<Consultas> listaConsultas;
	private List<Vacina> listaVacinas;
	private List<Vermifugos> listaVermifugos;
	private List<Anticoncepcional> listaAnticoncepcional;
	private List<AntiPulgas> listaAntiPulgas;
	private List<ServicosPrestados> listaServicosPrestados;
	private List<ServicosPrestados> listaServicosPrestadosAntiga;
	private List<Estetica> listaEstetica;
	
	
	private List<HistoricoClinico> listaHistoricoClinico;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_animal")
	public Integer getIdAnimal() {
		return idAnimal;
	}
	
	@DisplayName("Consultas")
	@OneToMany(mappedBy="animal")
	public List<Consultas> getListaConsultas() {
		return listaConsultas;
	}
	@OneToMany(mappedBy="animal")
	@DisplayName("Vacinas")
	public List<Vacina> getListaVacinas() {
		return listaVacinas;
	}
	@OneToMany(mappedBy="animal")
	@DisplayName("Vermifugo")
	public List<Vermifugos> getListaVermifugos() {
		return listaVermifugos;
	}
	
	@OneToMany(mappedBy="animal")
	@DisplayName("Anticoncepcional")
	public List<Anticoncepcional> getListaAnticoncepcional() {
		return listaAnticoncepcional;
	}
	@OneToMany(mappedBy="animal")
	@DisplayName("Antipulgas")
	public List<AntiPulgas> getListaAntiPulgas() {
		return listaAntiPulgas;
	}
	@Transient
	@DisplayName("Hist�rico Cl�nico")
	public List<HistoricoClinico> getListaHistoricoClinico() {
		return listaHistoricoClinico;
	}
	@Transient
	@DisplayName("Produtos/Servi�oes Prestados")
	public List<ServicosPrestados> getListaServicosPrestados() {
		return listaServicosPrestados;
	}
	public Boolean getDoado() {
		return doado;
	}
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getApelido() {
		return apelido;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idCliente")
	@Required
	public Cliente getDono() {
		return dono;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idCor")
	@DisplayName("Cor")
	public Cor getCor() {
		return cor;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idEspecie")
	@Required
	public Especie getEspecie() {
		return especie;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idRaca")
	@Required
	public Raca getRaca() {
		return raca;
	}
	@Transient
	public List<ServicosPrestados> getListaServicosPrestadosAntiga() {
		return listaServicosPrestadosAntiga;
	}
	@Required
	public Date getDtNascimento() {
		return dtNascimento;
	}
	public Date getDtObito() {
		return dtObito;
	}
	public String getRGA() {
		return RGA;
	}
	public String getMicrochip() {
		return microchip;
	}
	public String getObservacoes() {
		return observacoes;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfile")
	public Arquivo getFoto() {
		return foto;
	}
	@DisplayName("Est�ticas")
	@OneToMany(mappedBy="animal")
	public List<Estetica> getListaEstetica() {
		return listaEstetica;
	}
	public Boolean getDesaparecido() {
		return desaparecido;
	}
	public Date getDtDesaparecimento() {
		return dtDesaparecimento;
	}
	public void setIdAnimal(Integer idAnimal) {
		this.idAnimal = idAnimal;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setApelido(String apelido) {
		this.apelido = apelido;
	}
	public void setDono(Cliente dono) {
		this.dono = dono;
	}
	public void setCor(Cor cor) {
		this.cor = cor;
	}
	public void setEspecie(Especie especie) {
		this.especie = especie;
	}
	public void setRaca(Raca raca) {
		this.raca = raca;
	}
	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}
	public void setRGA(String rGA) {
		RGA = rGA;
	}
	public void setMicrochip(String microchip) {
		this.microchip = microchip;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	public void setFoto(Arquivo foto) {
		this.foto = foto;
	}
	public void setDtObito(Date dtObito) {
		this.dtObito = dtObito;
	}
	
	public void setDtDesaparecimento(Date dtDesaparecimento) {
		this.dtDesaparecimento = dtDesaparecimento;
	}
	public void setDesaparecido(Boolean desaparecido) {
		this.desaparecido = desaparecido;
	}
	public void setDoado(Boolean doado) {
		this.doado = doado;
	}
	public void setListaConsultas(List<Consultas> listaConsultas) {
		this.listaConsultas = listaConsultas;
	}
	public void setListaVacinas(List<Vacina> listaVacinas) {
		this.listaVacinas = listaVacinas;
	}
	public void setListaVermifugos(List<Vermifugos> listaVermifugos) {
		this.listaVermifugos = listaVermifugos;
	}
	public void setListaAnticoncepcional(
			List<Anticoncepcional> listaAnticoncepcional) {
		this.listaAnticoncepcional = listaAnticoncepcional;
	}
	public void setListaAntiPulgas(List<AntiPulgas> listaAntiPulgas) {
		this.listaAntiPulgas = listaAntiPulgas;
	}
	public void setListaHistoricoClinico(List<HistoricoClinico> listaHistoricoClinico) {
		this.listaHistoricoClinico = listaHistoricoClinico;
	}
	public void setListaServicosPrestados(
			List<ServicosPrestados> listaServicosPrestados) {
		this.listaServicosPrestados = listaServicosPrestados;
	}
	public void setListaServicosPrestadosAntiga(List<ServicosPrestados> listaServicosPrestadosAntiga) {
		this.listaServicosPrestadosAntiga = listaServicosPrestadosAntiga;
	}
	public void setListaEstetica(List<Estetica> listaEstetica) {
		this.listaEstetica = listaEstetica;
	}
}
