package seguranca.autorizacao.bean;

import javax.management.DescriptorKey;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DescriptionProperty;

@Entity
@SequenceGenerator(name="sq_dose", sequenceName="sq_dose")
public class Dose {
	
	private Integer idDose;
	private String descricao;
	
	@Id
	@GeneratedValue(generator="sq_dose",strategy=GenerationType.AUTO)
	public Integer getIdDose() {
		return idDose;
	}
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public void setIdDose(Integer idDose) {
		this.idDose = idDose;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
