package seguranca.autorizacao.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.authorization.User;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Password;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

/**
 * Representa um usu�rio no sistema
 */
@Entity
@SequenceGenerator(name="SQ_USUARIO",sequenceName="SQ_USUARIO")
@DisplayName("Usu�rios")
public class Usuario implements User {
    
	private Long id;
	private String nome;
	private String login;
	private String senha;
	private List<Papel> papeis;
	private Date dtCadastro;
	private Boolean userTemp;
	private String senhaant;
	private Timestamp dtrequisicao ;	
	protected Boolean ativado;
	protected String confirmasenha;
	protected String email;
	private List<PapelUsuario> papeisUsuarios;
	
	protected String confirmaemail;
	
	public Boolean getUserTemp() {
		return userTemp;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Transient
	public String getConfirmaemail() {
		return confirmaemail;
	}
	@OneToMany(mappedBy="usuario")
	public List<PapelUsuario> getPapeisUsuarios() {
		return papeisUsuarios;
	}
	public void setPapeisUsuarios(List<PapelUsuario> papeisUsuarios) {
		this.papeisUsuarios = papeisUsuarios;
	}
	@Transient
	public void setConfirmaemail(String confirmaemail) {
		this.confirmaemail = confirmaemail;
	}
	public void setUserTemp(Boolean userTemp) {
		this.userTemp = userTemp;
	}
	
	public Date getDtCadastro() {
		return dtCadastro;
	}
	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}
    
	public String getSenhaant() {
		return senhaant;
	}
	public void setSenhaant(String senhaant) {
		this.senhaant = senhaant;
	}
	public Boolean getAtivado() {
		return ativado;
	}
	public void setAtivado(Boolean ativado) {
		this.ativado = ativado;
	}
	@Transient
	public String getConfirmasenha() {
		return confirmasenha;
	}
	public void setConfirmasenha(String confirmasenha) {
		this.confirmasenha = confirmasenha;
	}
	@Transient
	public String getPassword() {
		return getSenha();
	}
	public Timestamp getDtrequisicao() {
		return dtrequisicao;
	}
	public void setDtrequisicao(Timestamp dtrequisicao) {
		this.dtrequisicao = dtrequisicao;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="SQ_USUARIO")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@DescriptionProperty
    @Required
    @MaxLength(value=50)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Required
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Password
    @Required
    @MaxLength(value=20)
    @MinLength(value=4)
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Transient
	public List<Papel> getPapeis() {
		return papeis;
	}

	public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}
	/**/

	
	


}