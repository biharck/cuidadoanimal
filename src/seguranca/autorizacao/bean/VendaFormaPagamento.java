package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.nextframework.types.Money;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_venda_form_pagamento",sequenceName="sq_venda_form_pagamento")
public class VendaFormaPagamento {
	
	private Integer idValorFormaPagamento;
	private Pagamento pagamento;
	private FormaPagamento formaPagamento;
	private Money valorForma;
	private Integer parcelas;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_venda_form_pagamento")
	public Integer getIdValorFormaPagamento() {
		return idValorFormaPagamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idPagamento")
	public Pagamento getPagamento() {
		return pagamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idFormaPagamento")
	@Required
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}
	@Required
	public Integer getParcelas() {
		return parcelas;
	}
	@Required
	public Money getValorForma() {
		return valorForma;
	}
	public void setIdValorFormaPagamento(Integer idValorFormaPagamento) {
		this.idValorFormaPagamento = idValorFormaPagamento;
	}
	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	public void setValorForma(Money valorForma) {
		this.valorForma = valorForma;
	}
	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}
	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}
}
