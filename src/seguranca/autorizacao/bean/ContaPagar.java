package seguranca.autorizacao.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Money;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@SequenceGenerator(name="sq_conta_pagar", sequenceName="sq_conta_pagar")
@DisplayName("Contas A Pagar")
public class ContaPagar {
	
	private Integer idContaPagar;
	private Fornecedor fornecedor;
	private String numeroDoc;
	private Money valor;
	private Date dtVencimento;
	private FormaPagamento formaPagamento;
	private String tipo;
	private String obs;
	private Boolean status;
	private Date dtPagamento;
	
	
	@Id
	@GeneratedValue(generator="sq_conta_pagar",strategy=GenerationType.AUTO)
	public Integer getIdContaPagar() {
		return idContaPagar;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idFornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	@Required
	@DisplayName("N�mero do Documento")
	public String getNumeroDoc() {
		return numeroDoc;
	}

	@Required
	public Money getValor() {
		return valor;
	}

	@Required
	@DisplayName("Data de Vencimento")
	public Date getDtVencimento() {
		return dtVencimento;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idPagamento")
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}
	public String getTipo() {
		return tipo;
	}
	@MaxLength(200)
	@DisplayName("Observa��o")
	public String getObs() {
		return obs;
	}

	@DisplayName("Situa��o")
	public Boolean getStatus() {
		return status;
	}
	@DisplayName("Data de Pagamento")
	public Date getDtPagamento() {
		return dtPagamento;
	}
	public void setIdContaPagar(Integer idContaPagar) {
		this.idContaPagar = idContaPagar;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setNumeroDoc(String numeroDoc) {
		this.numeroDoc = numeroDoc;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public void setDtPagamento(Date dtPagamento) {
		this.dtPagamento = dtPagamento;
	}
}
