package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_ramal", sequenceName = "sq_ramal")
public class Ramal {
 
	private Integer idRamal;
	private Integer numero;
	private String localizacao;
	private Empresa empresa;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_ramal")
	public Integer getIdRamal() {
		return idRamal;
	}
	@MaxLength(5)
	@Required
	@DisplayName("N�mero")
	public Integer getNumero() {
		return numero;
	}
	@Required
	@MaxLength(20)
	@DisplayName("Localiza��o")
	public String getLocalizacao() {
		return localizacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idEmpresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setIdRamal(Integer idRamal) {
		this.idRamal = idRamal;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	 
	 
}
 
