package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="SQ_TELA",sequenceName="SQ_TELA")
public class Tela {
	private Long id;
	private String path;
	private String descricao;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="SQ_TELA")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@DescriptionProperty
	@Required
	@MaxLength(value=200)
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	@Required
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
