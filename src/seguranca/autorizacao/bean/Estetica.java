package seguranca.autorizacao.bean;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.nextframework.validation.annotation.Required;


@Entity
@SequenceGenerator(name="sq_estetica",sequenceName="sq_estetica")
public class Estetica {
	
	private Integer idEstetica;
	private Animal animal;
	private Usuario esteticista;
	private List<ServicosEstetica> listaEstetica;
	private String obs;
	private Date data;
	private Time horaChegada;
	private Time horaEntrega;
	private Boolean status;
	private Transporte transporte;
	private Boolean procedimentoClinico;
	private Boolean ligarCliente;
	private Boolean entregue;
	
	@Id
	@GeneratedValue(generator="sq_estetica", strategy=GenerationType.AUTO)
	public Integer getIdEstetica() {
		return idEstetica;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idAnimal")
	public Animal getAnimal() {
		return animal;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id")
	public Usuario getEsteticista() {
		return esteticista;
	}
	
	@OneToMany(mappedBy="estetica")
	public List<ServicosEstetica> getListaEstetica() {
		return listaEstetica;
	}
	public String getObs() {
		return obs;
	}
	@Required
	public Date getData() {
		return data;
	}
	@Required
	public Time getHoraChegada() {
		return horaChegada;
	}
	public Time getHoraEntrega() {
		return horaEntrega;
	}
	public Boolean getStatus() {
		return status;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idTransporte")
	public Transporte getTransporte() {
		return transporte;
	}
	@Required
	public Boolean getProcedimentoClinico() {
		return procedimentoClinico;
	}
	@Required
	public Boolean getLigarCliente() {
		return ligarCliente;
	}
	@Required
	public Boolean getEntregue() {
		return entregue;
	}
	
	public void setIdEstetica(Integer idEstetica) {
		this.idEstetica = idEstetica;
	}
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	public void setEsteticista(Usuario esteticista) {
		this.esteticista = esteticista;
	}
	public void setListaEstetica(List<ServicosEstetica> listaEstetica) {
		this.listaEstetica = listaEstetica;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setHoraChegada(Time horaChegada) {
		this.horaChegada = horaChegada;
	}
	public void setHoraEntrega(Time horaEntrega) {
		this.horaEntrega = horaEntrega;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public void setTransporte(Transporte transporte) {
		this.transporte = transporte;
	}
	public void setProcedimentoClinico(Boolean procedimentoClinico) {
		this.procedimentoClinico = procedimentoClinico;
	}
	public void setLigarCliente(Boolean ligarCliente) {
		this.ligarCliente = ligarCliente;
	}
	public void setEntregue(Boolean entregue) {
		this.entregue = entregue;
	}
	
	

}
