package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_calendar", sequenceName="sq_calendar")
public class Calendar {
	
	private Integer idCalendar;
	private String titulo;
	private String tarefa;
	private Integer dayStart;
	private String monthStart;
	private Integer yearStart;
	private Integer hourStart;
	private Integer minStart;
	private Integer dayEnd;
	private String monthEnd;
	private Integer hourEnd;
	private Integer minEnd;
	private Integer yearEnd;
	private Usuario responsavel;
	private Boolean particular;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_calendar")
	public Integer getIdCalendar() {
		return idCalendar;
	}
	public Boolean getParticular() {
		return particular;
	}
	public String getTarefa() {
		return tarefa;
	}
	public Integer getDayStart() {
		return dayStart;
	}
	public String getMonthStart() {
		return monthStart;
	}
	public Integer getHourStart() {
		return hourStart;
	}
	public Integer getMinStart() {
		return minStart;
	}
	public Integer getDayEnd() {
		return dayEnd;
	}
	public String getMonthEnd() {
		return monthEnd;
	}
	public Integer getHourEnd() {
		return hourEnd;
	}
	public Integer getMinEnd() {
		return minEnd;
	}
	public String getTitulo() {
		return titulo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id")
	public Usuario getResponsavel() {
		return responsavel;
	}
	public Integer getYearEnd() {
		return yearEnd;
	}
	public Integer getYearStart() {
		return yearStart;
	}
	public void setIdCalendar(Integer idCalendar) {
		this.idCalendar = idCalendar;
	}
	public void setTarefa(String tarefa) {
		this.tarefa = tarefa;
	}
	public void setDayStart(Integer dayStart) {
		this.dayStart = dayStart;
	}
	public void setMonthStart(String monthStart) {
		this.monthStart = monthStart;
	}
	public void setHourStart(Integer hourStart) {
		this.hourStart = hourStart;
	}
	public void setMinStart(Integer minStart) {
		this.minStart = minStart;
	}
	public void setDayEnd(Integer dayEnd) {
		this.dayEnd = dayEnd;
	}
	public void setMonthEnd(String monthEnd) {
		this.monthEnd = monthEnd;
	}
	public void setHourEnd(Integer hourEnd) {
		this.hourEnd = hourEnd;
	}
	public void setMinEnd(Integer minEnd) {
		this.minEnd = minEnd;
	}
	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public void setYearEnd(Integer yearEnd) {
		this.yearEnd = yearEnd;
	}
	public void setYearStart(Integer yearStart) {
		this.yearStart = yearStart;
	}
	public void setParticular(Boolean particular) {
		this.particular = particular;
	}
}
