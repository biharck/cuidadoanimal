package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_raca", sequenceName = "sq_raca")
@DisplayName("Ra�a")
public class Raca {
 
	private Integer idRaca;
	private String nome;
	private Especie especie;
	private Boolean ativo;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_raca")
	@DisplayName("Id")
	@Required
	public Integer getIdRaca() {
		return idRaca;
	}
	@DisplayName("Nome")
	@MaxLength(50)
	@DescriptionProperty
	@MinLength(3)
	@Required
	public String getNome() {
		return nome;
	}
	@Required
	@DisplayName("Ativo?")
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idEspecie")
	public Especie getEspecie() {
		return especie;
	}
	public void setIdRaca(Integer idRaca) {
		this.idRaca = idRaca;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setEspecie(Especie especie) {
		this.especie = especie;
	}
}
 
