package seguranca.autorizacao.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.File;

/**
 * Bean que representar� um arquivo.
 * Ter� uma tabela no banco que mapeia as propriedades dessa classe.
 * O NEO detectar� essa classe automaticamente para realizar algumas configura��es.
 * IMPORTANTE: S� PODER� EXISTIR UMA CLASSE QUE IMPLEMENTA File PARA QUE ESSAS CONFIGURA��ES 
 * SEJAM REALIZADAS!
 */
@Entity
public class Arquivo implements File {
	
	Long cdfile;
	String contenttype; //tipo do arquivo (ser� populado pelo neo automaticamente
	String name;//nome do arquivo
	Long size;//tamanho do arquivo
	Timestamp tsmodification;//data da ultima modificacao
	private List<Animal> listaAnimal;
	
	byte[] content;//conte�do do arquivo
	
	private Date date;

	//cdfile � o c�digo do arquivo no banco
	//o primary key � o campo id (que estamos retornando)
	@Id
	@SequenceGenerator(name="sq_arquivo", sequenceName="sq_arquivo")
	@GeneratedValue(generator="sq_arquivo")
	@Column(name="id")
	public Long getCdfile() {
		return cdfile;
	}

	//salvaremos o conte�do do arquivo em disco, por isso
	//o content (que � o conteudo em bytes do arquivo) � transiente
	public byte[] getContent() {
		return content;
	}

	public String getContenttype() {
		return contenttype;
	}
	
	@Transient
	public String getTipoconteudo() {
		return getContenttype();
	}

	@DescriptionProperty
	public String getName() {
		return name;
	}
	@OneToMany(mappedBy="foto")
	public List<Animal> getListaAnimal() {
		return listaAnimal;
	}
	public Long getSize() {
		return size;
	}

	public Timestamp getTsmodification() {
		return tsmodification;
	}
	@Transient
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	public void setCdfile(Long cdfile) {
		this.cdfile = cdfile;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public void setContenttype(String contenttype) {
		this.contenttype = contenttype;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public void setTsmodification(Timestamp tsmodification) {
		this.tsmodification = tsmodification;
	}
	public void setListaAnimal(List<Animal> listaAnimal) {
		this.listaAnimal = listaAnimal;
	}
}
