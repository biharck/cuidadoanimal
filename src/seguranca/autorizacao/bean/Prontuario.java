package seguranca.autorizacao.bean;

import java.util.Date;

public class Prontuario {
	private Integer idConsultas;
	private String descricaoConsultas;
	private String descricaoProcedimentos;
	private String tipo;
	private Date dataRealizacao;
	
	public Prontuario(String descricaoConsultas,String descricaoProcedimentos, String tipo, java.util.Date date,Integer idConsultas){
		this.descricaoConsultas = descricaoConsultas;
		this.descricaoProcedimentos = descricaoProcedimentos;
		this.tipo = tipo;
		this.dataRealizacao = date;
		this.idConsultas = idConsultas;
	}
	public Prontuario(){
		
	}
	public String getDescricaoConsultas() {
		return descricaoConsultas;
	}
	public String getDescricaoProcedimentos() {
		return descricaoProcedimentos;
	}
	public Integer getIdConsultas() {
		return idConsultas;
	}
	public String getTipo() {
		return tipo;
	}
	public Date getDataRealizacao() {
		return dataRealizacao;
	}
	public void setDescricaoConsultas(String descricaoConsultas) {
		this.descricaoConsultas = descricaoConsultas;
	}
	public void setDescricaoProcedimentos(String descricaoProcedimentos) {
		this.descricaoProcedimentos = descricaoProcedimentos;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setDataRealizacao(Date dataRealizacao) {
		this.dataRealizacao = dataRealizacao;
	}
	public void setIdConsultas(Integer idConsultas) {
		this.idConsultas = idConsultas;
	}
}
