package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_tipo_operacao",sequenceName="sq_tipo_operacao")
public class TipoOperacao {
	
	private Integer idTipoOperacao;
	private String descricao;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_tipo_operacao")
	public Integer getIdTipoOperacao() {
		return idTipoOperacao;
	}
	@DisplayName("Descri��o")
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public void setIdTipoOperacao(Integer idTipoOperacao) {
		this.idTipoOperacao = idTipoOperacao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
