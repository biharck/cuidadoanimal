package seguranca.autorizacao.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_especie", sequenceName = "sq_especie")
@DisplayName(value="Esp�cie")
public class Especie {
 
	private Integer idEspecie;
	private String nome;
	private Boolean ativo;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_especie")
	@DisplayName("Id")
	public Integer getIdEspecie() {
		return idEspecie;
	}
	@DisplayName("Nome")
	@DescriptionProperty
	@MaxLength(50)
	@MinLength(3)
	@Required
	public String getNome() {
		return nome;
	}
	public void setIdEspecie(Integer idEspecie) {
		this.idEspecie = idEspecie;
	}
	@Required
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
 
