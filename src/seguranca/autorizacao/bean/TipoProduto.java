package seguranca.autorizacao.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_tipo_produto",sequenceName="sq_tipo_produto")
@DisplayName(value="Tipos de Produtos")
public class TipoProduto {
	
	private Integer idtipoProduto;
	private String descricao;
	private List<SubTipoProduto> listaSubTipoProduto;
	private Boolean ativo;
	
	
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_tipo_produto")
	@DisplayName("Id")
	public Integer getIdtipoProduto() {
		return idtipoProduto;
	}
	@Required
	@DescriptionProperty
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@OneToMany(mappedBy="tipoProduto")
	public List<SubTipoProduto> getListaSubTipoProduto() {
		return listaSubTipoProduto;
	}
	public void setIdtipoProduto(Integer idtipoProduto) {
		this.idtipoProduto = idtipoProduto;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setListaSubTipoProduto(List<SubTipoProduto> listaSubTipoProduto) {
		this.listaSubTipoProduto = listaSubTipoProduto;
	}
}
