package seguranca.autorizacao.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
//@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_uf", sequenceName = "sq_uf")
@Table(name="uf")
public class Uf {

	protected Integer idUf;
	protected String nome;
	protected String sigla;
	protected List<Municipio> listaMunicipio;
	
	public static Uf MINAS_GERAIS = new Uf(13);
	
	public Uf(){}
	public Uf(Integer idUf){
		this.idUf = idUf;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_uf")
	public Integer getIdUf() {
		return idUf;
	}
	public void setIdUf(Integer idUf) {
		this.idUf = idUf;
	}

	
	@Required
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	@OneToMany(mappedBy="uf")
	public List<Municipio> getListaMunicipio() {
		return listaMunicipio;
	}
	
	@Required
	@MaxLength(2)
	@DescriptionProperty
	public String getSigla() {
		return sigla;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public void setListaMunicipio(List<Municipio> listaMunicipio) {
		this.listaMunicipio = listaMunicipio;
	}
}
