package seguranca.autorizacao.dao;

import java.sql.Date;
import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.SaveOrUpdateStrategy;

import seguranca.autenticacao.filter.VendaFiltro;
import seguranca.autorizacao.bean.Caixa;
import seguranca.autorizacao.bean.Venda;

public class VendaDAO extends GenericDAO<Venda> {
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Venda> query) {
		query.leftOuterJoinFetch("venda.servicosPrestados servicosPrestados");
		query.leftOuterJoinFetch("venda.cliente cliente");
	}
	
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("servicosPrestados");
		super.updateSaveOrUpdate(save);
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Venda> query,FiltroListagem _filtro) {
		VendaFiltro filtro = (VendaFiltro) _filtro;
		query
			.select("cliente.nome, venda.idVenda, venda.hora, venda.data,valorProdutoServico.valor, valorProdutoServico.margemLucro ,servicosPrestados.qtd,servicosPrestados.desconto")
			.leftOuterJoin("venda.cliente cliente")
			.leftOuterJoin("venda.servicosPrestados servicosPrestados")
			.leftOuterJoin("servicosPrestados.itemProduto itemProduto")
			.leftOuterJoin("itemProduto.produtoServico produtoServico")
			.leftOuterJoin("produtoServico.listaValorProdutoServico valorProdutoServico")
			.where("valorProdutoServico is not null")
			.where("cliente=?",filtro.getCliente())
			.where("venda.fechado is null ")
			;
	}
	
	public Venda getVendaById(Integer idVenda){
		return query()
			.select("cliente.nome, venda.idVenda, venda.hora, venda.data,valorProdutoServico.valor, valorProdutoServico.margemLucro ,servicosPrestados.qtd," +
					"produtoServico.nome,itemProduto.idItemProduto,servicosPrestados.idServicosPrestados,servicosPrestados.desconto")
			.leftOuterJoin("venda.cliente cliente")
			.leftOuterJoin("venda.servicosPrestados servicosPrestados")
			.leftOuterJoin("servicosPrestados.itemProduto itemProduto")
			.leftOuterJoin("itemProduto.produtoServico produtoServico")
			.leftOuterJoin("produtoServico.listaValorProdutoServico valorProdutoServico")
			.where("valorProdutoServico is not null")
			.where("venda.idVenda=?",idVenda)
			.unique()
			;
	}
	

	public Venda getVendaAndServicosPrestadosById(Integer idVenda){
		return query()
			.select("cliente.nome, venda.idVenda, venda.hora, venda.data,valorProdutoServico.valor, valorProdutoServico.margemLucro ,servicosPrestados.qtd," +
					"produtoServico.nome,produtoServico.idProdutoServico,servicosPrestados.idServicosPrestados,cliente.idCliente,itemProduto.idItemProduto,listaPagamento.idPagamento,servicosPrestados.desconto," +
					"formaPagamento.idFormaPagamento,animal.idAnimal")
			.leftOuterJoin("venda.cliente cliente")
			.leftOuterJoin("venda.servicosPrestados servicosPrestados")
			.leftOuterJoin("servicosPrestados.animal animal")
			.leftOuterJoin("servicosPrestados.itemProduto itemProduto")
			.leftOuterJoin("itemProduto.produtoServico produtoServico")
			.leftOuterJoin("produtoServico.listaValorProdutoServico valorProdutoServico")
			.leftOuterJoin("venda.listaPagamento listaPagamento")
			.leftOuterJoin("listaPagamento.listaVendaFormaPagamento listaVendaFormaPagamento")
			.leftOuterJoin("listaVendaFormaPagamento.formaPagamento formaPagamento")
			.where("venda.idVenda=?",idVenda)
			.unique()
			;
	}
	
	public void updateVenda(Integer idVenda){
		getJdbcTemplate().update("update venda set fechado = false where idVenda="+ idVenda);
	}
	
	public List<Venda> getVendaByCaixa(Caixa c){
		return query()
		.select("cliente.nome, venda.idVenda, venda.hora, venda.data,valorProdutoServico.valor, valorProdutoServico.margemLucro ,servicosPrestados.qtd," +
				"produtoServico.nome,produtoServico.idProdutoServico,servicosPrestados.idServicosPrestados,cliente.idCliente,itemProduto.idItemProduto,listaPagamento.idPagamento," +
				"formaPagamento.idFormaPagamento")
		.leftOuterJoin("venda.cliente cliente")
		.leftOuterJoin("venda.servicosPrestados servicosPrestados")
		.leftOuterJoin("servicosPrestados.itemProduto itemProduto")
		.leftOuterJoin("itemProduto.produtoServico produtoServico")
		.leftOuterJoin("produtoServico.listaValorProdutoServico valorProdutoServico")
		.leftOuterJoin("venda.listaPagamento listaPagamento")
		.leftOuterJoin("listaPagamento.listaVendaFormaPagamento listaVendaFormaPagamento")
		.leftOuterJoin("listaVendaFormaPagamento.formaPagamento formaPagamento")
		.where("venda.fechado is null")
		.list()
		;
	}
	
	public List<Venda> findAllByIds(String ids) {
		return query()
		.select("cliente.nome, venda.idVenda, venda.hora, venda.data,valorProdutoServico.valor, valorProdutoServico.margemLucro ,servicosPrestados.qtd,servicosPrestados.desconto")
		.leftOuterJoin("venda.cliente cliente")
		.leftOuterJoin("venda.servicosPrestados servicosPrestados")
		.leftOuterJoin("servicosPrestados.itemProduto itemProduto")
		.leftOuterJoin("itemProduto.produtoServico produtoServico")
		.leftOuterJoin("produtoServico.listaValorProdutoServico valorProdutoServico")
		.whereIn("venda.idVenda",ids).list()
		;
	}
	

}
