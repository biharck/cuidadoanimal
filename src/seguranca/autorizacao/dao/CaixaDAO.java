	package seguranca.autorizacao.dao;

import java.sql.Date;

import org.nextframework.persistence.GenericDAO;

import seguranca.autorizacao.bean.Caixa;

public class CaixaDAO extends GenericDAO<Caixa>{
	
	public Caixa getCaixaDia(){
		return query().select("caixa")
//		.where("caixa.dataAbertura =?",new Date(System.currentTimeMillis()))
		.where("caixa.dataFechamento is null")
		.unique();
	}

}
