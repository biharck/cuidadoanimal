package seguranca.autorizacao.dao;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.persistence.GenericDAO;

import seguranca.autorizacao.bean.Papel;
import seguranca.autorizacao.bean.PapelUsuario;
import seguranca.autorizacao.bean.Usuario;


public class PapelDAO extends GenericDAO<Papel> {

	
	public List<Papel> findByUsuario(Usuario usuario){
		if(usuario.getId() == null){
			return new ArrayList<Papel>();
		}
		return query()
				.select("papel")
				.from(PapelUsuario.class)
				.leftOuterJoin("papelUsuario.papel papel")
				.where("papelUsuario.usuario = ?", usuario)
				.list();
	}
	
	public void deleteByUsuario(Usuario usuario){
		getHibernateTemplate().bulkUpdate("delete from PapelUsuario where usuario = ?", usuario);
	}
}
