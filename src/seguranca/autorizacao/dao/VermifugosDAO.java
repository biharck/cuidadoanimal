package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.persistence.GenericDAO;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Vermifugos;

public class VermifugosDAO extends GenericDAO<Vermifugos> {
	
	public List<Vermifugos> getVermifugosByAnimal(Animal animal){
		return query()
					.select("vermifugos")
					.leftOuterJoin("vermifugos.animal a")
					.where("a=?",animal)
					.list();
	}

}
