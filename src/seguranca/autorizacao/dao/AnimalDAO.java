package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;

import seguranca.autenticacao.filter.AnimalFiltro;
import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Cliente;



public class AnimalDAO extends GenericDAO<Animal> {
	
	
	public List<Animal> getListaAnimalByCliente(Cliente cliente){
		return query()
		.select(" animal.idAnimal, animal.nome, animal.apelido, animal.dtNascimento, animal.dtObito, animal.RGA, " +
				" animal.microchip, animal.observacoes,animal.desaparecido,animal.dtObito, animal.dtNascimento, " +
				" animal.dtDesaparecimento, animal.doado, dono.idCliente, cor.idCor,especie.idEspecie,raca.idRaca,foto.cdfile ")
		.leftOuterJoin("animal.dono dono")
		.leftOuterJoin("animal.foto foto")
		.leftOuterJoin("animal.cor cor")
		.leftOuterJoin("animal.especie especie")
		.leftOuterJoin("animal.raca raca")
		.where("dono.idCliente = ?",cliente.getIdCliente())
		.list();
	}
	
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<Animal> query, FiltroListagem _filtro) {
		AnimalFiltro filtro = (AnimalFiltro) _filtro;
		query
			.select("animal.nome, animal.apelido, animal.idAnimal")
			.leftOuterJoin("animal.dono dono")
			.leftOuterJoin("animal.cor cor")
			.leftOuterJoin("animal.especie especie")
			.leftOuterJoin("animal.raca raca")
			.whereLikeIgnoreAll("animal.apelido",filtro.getApelidoAnimal())
			.where("dono=?",filtro.getCliente())
			.where("cor=?",filtro.getCor())
			.whereLikeIgnoreAll("dono.logradouro",filtro.getEnderecoResumido())
			.where("especie=?",filtro.getEspecie())
			.whereLikeIgnoreAll("animal.nome",filtro.getNomeAnimal())
			.where("raca=?",filtro.getRaca());			
		;
		
	}
	
	
	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<Animal> getListAnimalReport(AnimalFiltro filtro){
		return query()
			.select("animal.idAnimal,animal.nome, animal.apelido, dono.nome, especie.nome, raca.nome, animal.microchip")
			.leftOuterJoin("animal.dono dono")
			.leftOuterJoin("animal.cor cor")
			.leftOuterJoin("animal.especie especie")
			.leftOuterJoin("animal.raca raca")
			.whereLikeIgnoreAll("animal.apelido",filtro.getApelidoAnimal())
			.where("dono=?",filtro.getCliente())
			.where("cor=?",filtro.getCor())
			.whereLikeIgnoreAll("dono.logradouro",filtro.getEnderecoResumido())
			.where("especie=?",filtro.getEspecie())
			.whereLikeIgnoreAll("animal.nome",filtro.getNomeAnimal())
			.where("raca=?",filtro.getRaca())
			.list();
	}
	
//	@Override
//	public void updateEntradaQuery(QueryBuilder<Animal> query) {
//		query
//			.select("animal.idAnimal")
//			.leftOuterJoin("animal.listaConsultas listaConsultas")
//			.leftOuterJoin("animal.listaVacinas listaVacinas")
//			.leftOuterJoin("animal.listaVermifugos listaVermifugos")
//			.leftOuterJoin("animal.listaAnticoncepcional listaAnticoncepcional")
//			.leftOuterJoin("animal.listaAntiPulgas listaAntiPulgas")
//			.leftOuterJoin("animal.listaServicosPrestados listaServicosPrestados")
//			.leftOuterJoin("animal.dono dono")
//			.leftOuterJoin("animal.foto foto")
//			.leftOuterJoin("animal.cor cor")
//			.leftOuterJoin("animal.especie especie")
//			.leftOuterJoin("animal.raca raca")
//			;
//	}
////	
//	@Override
//	public Animal loadForEntrada(Animal bean) {
//		query().unique();
//		return super.loadForEntrada(bean);
//	}
//
	
	/**
	 * <p>Método responsável em retornar todo o histórico do animal</p>
	 */
	public Animal getHistoricoAnimal(Integer id){
		return query()
					.select("animal.idAnimal,animal.nome, animal.foto, " +
							"dono.nome, dono.idCliente,"+
							"lcon.idConsultas,lcon.data, lcon.descricao,lcon.receituario, lcon.retorno," +
							"lvac.idVacina,lvac.dtAplicacao, lvacps.nome, lvacps.idProdutoServico," +
							"lver.idVermifugo,lver.dtAplicacao, lverps.nome, lverps.idProdutoServico," +
							"lant.idAnticoncepcional,lant.dtAplicacao, lantps.nome, lantps.idProdutoServico," +
							"lanp.idAntiPulgas,lanp.dtAplicacao, lanpps.nome, lanpps.idProdutoServico," +
							"v.id, v.nome," +
							"lvacdose.idDose,lvacdose.descricao,lverdose.idDose,lverdose.descricao,lantdose.idDose,lantdose.descricao,lanpdose.idDose,lanpdose.descricao")
					.leftOuterJoin("animal.dono dono")
					.leftOuterJoin("animal.listaConsultas lcon")
					.leftOuterJoin("lcon.veterinario v")
					.leftOuterJoin("animal.listaVacinas lvac")
					.leftOuterJoin("lvac.produtoServico lvacps")
					.leftOuterJoin("lvac.dose lvacdose")
					.leftOuterJoin("animal.listaVermifugos lver")
					.leftOuterJoin("lver.produtoServico lverps")
					.leftOuterJoin("lver.dose lverdose")
					.leftOuterJoin("animal.listaAnticoncepcional lant")
					.leftOuterJoin("lant.produtoServico lantps")
					.leftOuterJoin("lant.dose lantdose")
					.leftOuterJoin("animal.listaAntiPulgas lanp")
					.leftOuterJoin("lanp.produtoServico lanpps")
					.leftOuterJoin("lanp.dose lanpdose")
					
					.where("animal.idAnimal=?",id)
					.unique();
		
	}
	
}
