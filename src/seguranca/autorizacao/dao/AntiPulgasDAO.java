package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.persistence.GenericDAO;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.AntiPulgas;

public class AntiPulgasDAO extends GenericDAO<AntiPulgas> {
	
	public List<AntiPulgas> getAntiPulgasByAnimal(Animal animal){
		return query()
					.select("antiPulgas")
					.leftOuterJoin("antiPulgas.animal a")
					.where("a=?",animal)
					.list();
	}
}
