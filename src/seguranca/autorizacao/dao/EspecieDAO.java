package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import seguranca.autorizacao.bean.Especie;


@DefaultOrderBy("especie.nome")
public class EspecieDAO extends GenericDAO<Especie> {
	
	@Override
	public List<Especie> findForCombo() {
		return query().select("especie").where("especie.ativo is true").list();
	}
	
	@Override
	public List<Especie> findAll() {
		return query().select("especie").where("especie.ativo is true").list();
	}
	
	@Override
	public List<Especie> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}
	
	@Override
	protected String[] getComboSelectedProperties(String alias) {
		// TODO Auto-generated method stub
		return super.getComboSelectedProperties(alias);
	}

}
