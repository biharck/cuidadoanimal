package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import seguranca.autorizacao.bean.SubTipoProduto;


@DefaultOrderBy("subTipoProduto.descricao")
public class SubTipoProdutoDAO extends GenericDAO<SubTipoProduto> {

	@Override
	public List<SubTipoProduto> findAll() {
		return query().where("subTipoProduto.ativo is true").list();
	}
	
	@Override
	public List<SubTipoProduto> findForCombo(String... extraFields) {
		// TODO Auto-generated method stub
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}
	
	public List<SubTipoProduto> findSubTiposByTipo(Integer id) {
		return query()
			.select("subTipoProduto.idSubTipoProduto, subTipoProduto.descricao")
			.join("subTipoProduto.tipoProduto tp")
			.where("subTipoProduto.ativo is true")
			.where("tp.idtipoProduto=?",id)
			.list();
	}	
	
	
	@Override
	public List<SubTipoProduto> findBy(Object o, boolean forCombo,String... extraFields) {
		// TODO Auto-generated method stub
		if(forCombo)
			extraQuery = " subTipoProduto.ativo is true ";
		return super.findBy(o, forCombo, extraFields);
	}

}
