package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;

import seguranca.autenticacao.filter.EntradaSaidaCaixaFiltro;
import seguranca.autorizacao.bean.EntradaSaidaCaixa;

public class EntradaSaidaCaixaDAO extends GenericDAO<EntradaSaidaCaixa> {
	
	public List<EntradaSaidaCaixa> getListaEntradaCaixaDiaCorrente(){
		return query()
				.select("entradaSaidaCaixa.idEntradaSaidaCaixa, entradaSaidaCaixa.valor,tipoOperacao.idTipoOperacao, entradaSaidaCaixa.data, entradaSaidaCaixa.hora," +
						"entradaSaidaCaixa.pendente,cliente.idCliente, cliente.nome,usuario.id, usuario.nome,formaPagamento.idFormaPagamento,formaPagamento.descricao")
				.leftOuterJoin("entradaSaidaCaixa.caixa caixa")
				.leftOuterJoin("entradaSaidaCaixa.tipoOperacao tipoOperacao")
				.leftOuterJoin("entradaSaidaCaixa.cliente cliente")
				.leftOuterJoin("entradaSaidaCaixa.usuario usuario")
				.leftOuterJoin("entradaSaidaCaixa.formaPagamento formaPagamento")
//				.where("caixa.dataAbertura =?",new Date(System.currentTimeMillis()))
				.where("caixa.dataFechamento is null")
				.where("tipoOperacao.idTipoOperacao=1")
				.list();
	}
	
	public List<EntradaSaidaCaixa> getListaSaidaCaixaDiaCorrente(){
		return query()
				.select("entradaSaidaCaixa")
				.leftOuterJoin("entradaSaidaCaixa.caixa caixa")
				.leftOuterJoin("entradaSaidaCaixa.tipoOperacao tipoOperacao")
//				.where("caixa.dataAbertura =?",new Date(System.currentTimeMillis()))
				.where("caixa.dataFechamento is null")
				.where("tipoOperacao.idTipoOperacao=5")
				.or()
				.where("tipoOperacao.idTipoOperacao=2")
				.list();
	}
	
	public EntradaSaidaCaixa getEntradaSaidaCaixaById(EntradaSaidaCaixa entradaSaidaCaixa){
		return query()
		.select("entradaSaidaCaixa.idEntradaSaidaCaixa, entradaSaidaCaixa.valor,tipoOperacao.idTipoOperacao, entradaSaidaCaixa.data, entradaSaidaCaixa.hora," +
				"entradaSaidaCaixa.pendente,cliente.idCliente, cliente.nome,usuario.id, usuario.nome,formaPagamento.idFormaPagamento,formaPagamento.descricao")
		.leftOuterJoin("entradaSaidaCaixa.caixa caixa")
		.leftOuterJoin("entradaSaidaCaixa.tipoOperacao tipoOperacao")
		.leftOuterJoin("entradaSaidaCaixa.cliente cliente")
		.leftOuterJoin("entradaSaidaCaixa.usuario usuario")
		.leftOuterJoin("entradaSaidaCaixa.formaPagamento formaPagamento")
		.where("entradaSaidaCaixa = ?",entradaSaidaCaixa)
		.unique();
	}
	public List<EntradaSaidaCaixa> getListaEstornoCaixaDiaCorrente(){
		return query()
		.select("entradaSaidaCaixa.idEntradaSaidaCaixa, entradaSaidaCaixa.valor,tipoOperacao.idTipoOperacao, entradaSaidaCaixa.data, entradaSaidaCaixa.hora," +
				"entradaSaidaCaixa.pendente,cliente.idCliente, cliente.nome,usuario.id, usuario.nome,formaPagamento.idFormaPagamento,formaPagamento.descricao," +
				"entradaSaidaCaixa.motivo")
		.leftOuterJoin("entradaSaidaCaixa.caixa caixa")
		.leftOuterJoin("entradaSaidaCaixa.tipoOperacao tipoOperacao")
		.leftOuterJoin("entradaSaidaCaixa.cliente cliente")
		.leftOuterJoin("entradaSaidaCaixa.usuario usuario")
		.leftOuterJoin("entradaSaidaCaixa.formaPagamento formaPagamento")
//		.where("caixa.dataAbertura =?",new Date(System.currentTimeMillis()))
		.where("caixa.dataFechamento is null")
		.where("tipoOperacao.idTipoOperacao=3")
		.list();
	}
	
	public List<EntradaSaidaCaixa> getListaEstornoCaixaDiaCorrenteExcetoEstorno(){
		return query()
		.select("entradaSaidaCaixa.idEntradaSaidaCaixa, entradaSaidaCaixa.valor,tipoOperacao.idTipoOperacao, entradaSaidaCaixa.data, entradaSaidaCaixa.hora," +
				"entradaSaidaCaixa.pendente,cliente.idCliente, cliente.nome,usuario.id, usuario.nome,formaPagamento.idFormaPagamento,formaPagamento.descricao," +
				"entradaSaidaCaixa.motivo")
		.leftOuterJoin("entradaSaidaCaixa.caixa caixa")
		.leftOuterJoin("entradaSaidaCaixa.tipoOperacao tipoOperacao")
		.leftOuterJoin("entradaSaidaCaixa.cliente cliente")
		.leftOuterJoin("entradaSaidaCaixa.usuario usuario")
		.leftOuterJoin("entradaSaidaCaixa.formaPagamento formaPagamento")
//		.where("caixa.dataAbertura =?",new Date(System.currentTimeMillis()))
		.where("caixa.dataFechamento is null")
		.where("tipoOperacao.idTipoOperacao<>3")
		.list();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<EntradaSaidaCaixa> query,
			FiltroListagem filtro) {
		query.select("entradaSaidaCaixa.idEntradaSaidaCaixa,entradaSaidaCaixa.valor,f.idFormaPagamento,f.descricao, t.idTipoOperacao, t.descricao")
		.leftOuterJoin("entradaSaidaCaixa.tipoOperacao t")
		.leftOuterJoin("entradaSaidaCaixa.formaPagamento f")
		.where("t.idTipoOperacao <> 3")
//		.where("entradaSaidaCaixa.data = ?",new Date(System.currentTimeMillis()))
		.where("caixa.dataFechamento is null")
		.list();
	}
	

	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<EntradaSaidaCaixa> getListEntradaSaidaCaixaReport(EntradaSaidaCaixaFiltro filtro){
		return query()
			.select("entradaSaidaCaixa.data,entradaSaidaCaixa.hora,entradaSaidaCaixa.idEntradaSaidaCaixa,entradaSaidaCaixa.valor,f.idFormaPagamento,f.descricao, t.idTipoOperacao, t.descricao")
			.leftOuterJoin("entradaSaidaCaixa.tipoOperacao t")
			.join("entradaSaidaCaixa.caixa caixa")
			.leftOuterJoin("entradaSaidaCaixa.formaPagamento f")
			.where("caixa.dataFechamento is null")
			.where("entradaSaidaCaixa.data between '"+filtro.getDtInicio()+"' and '"+filtro.getDtFim()+"'")
			.or()
			.where("caixa.dataFechamento is null")
			.list();
	}


}
