package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Estetica;

public class EsteticaDAO extends GenericDAO<Estetica>{

	
	public List<Estetica> getEsteticasByAnimal(Animal animal){
		return query()
					.select("estetica")
					.leftOuterJoin("estetica.animal a")
					.where("a=?",animal)
					.list();
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Estetica> query) {
		query.leftOuterJoinFetch("estetica.listaEstetica l");
	}
	@Override
	public Estetica loadForEntrada(Estetica bean) {
		query().leftOuterJoinFetch("estetica.listaEstetica l");
		return super.loadForEntrada(bean);
	}
}
