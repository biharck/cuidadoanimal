package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;

import seguranca.autenticacao.filter.ProdutoServicoFiltro;
import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.ProdutoServico;



public class ProdutoServicoDAO extends GenericDAO<ProdutoServico> {
	
	@Override
	public void updateEntradaQuery(QueryBuilder<ProdutoServico> query) {
		query.leftOuterJoinFetch("produtoServico.subTipoProduto subTipoProduto");
		query.unique();
	}
	
	public List<ProdutoServico> getProdutoServicoByTipo(Integer idTipoSubProduto){
		return query().select("produtoServico")
		.leftOuterJoin("produtoServico.subTipoProduto s")
		.where("s.idSubTipoProduto=?", idTipoSubProduto)
		.list();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<ProdutoServico> query, FiltroListagem _filtro) {
		ProdutoServicoFiltro filtro = (ProdutoServicoFiltro) _filtro;
		query.select("produtoServico.idProdutoServico,produtoServico.nome, produtoServico.codigo,l.idItemProduto,l.quantidadeSaida")
		.leftOuterJoin("produtoServico.listaItensProdutos l")
		.where("produtoServico.codigo=?",filtro.getCodProduto())
		.whereLikeIgnoreAll("produtoServico.nome", filtro.getNome())
		
		;
	}
	
	public ProdutoServico getProdutoByItem(ItemProduto itemProduto){
		return query().select("produtoServico.idProdutoServico, produtoServico.nome, produtoServico.codigo, l.idItemProduto, l.quantidadeSaida,l.quantidadeEntrada")
		.leftOuterJoin("produtoServico.listaItensProdutos l")
		.where("l=?",itemProduto)
		.unique()
		;
	}
	

	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<ProdutoServico> getListProdutoServicoReport(ProdutoServicoFiltro filtro){
		return query()
			.select("produtoServico.codigo,produtoServico.nome,s.descricao,tp.descricao,vps.valor,vps.margemLucro")
			.leftOuterJoin("produtoServico.subTipoProduto s")
			.leftOuterJoin("s.tipoProduto tp")
			.leftOuterJoin("produtoServico.listaValorProdutoServico vps")
			.whereLikeIgnoreAll("produtoServico.nome",filtro.getNome())
			.where("produtoServico.codProduto=?",filtro.getCodProduto())
			.where("vps.dataFim is not null")
			.list();
	}
	
//	public List<ProdutoServico> getListaProdutosAbaixoEstoqueMinimo(){
//		return query().
//		select("produtoServico.codigo,produtoServico.idProdutoServico, produtoServico.nome,produtoServico.qtdMinima, " +
//				"ip.idItemProduto, ip.quantidadeEntrada, ip.quantidadeSaida,s.descricao,tp.descricao")
//		.join("produtoServico.listaItensProdutos ip")
//		.join("produtoServico.subTipoProduto s")
//		.join("s.tipoProduto tp")
//		.where("(ip.quantidadeEntrada - ip.quantidadeSaida) < produtoServico.qtdMinima")
//		.or()
//		.where("(ip.quantidadeEntrada < ip.quantidadeSaida)")
//		.list();
//	}
	
	public List<ProdutoServico> getListaProdutosAbaixoEstoqueMinimo(){
		return query().
		select("produtoServico.codigo,produtoServico.idProdutoServico, produtoServico.nome,produtoServico.qtdMinima, " +
				"itemProduto.idItemProduto, itemProduto.quantidadeEntrada, itemProduto.quantidadeSaida,s.descricao,tp.descricao")
		.join("produtoServico.listaItensProdutos itemProduto")
		.join("produtoServico.subTipoProduto s")
		.join("s.tipoProduto tp")
		.groupBy("produtoServico.codigo,produtoServico.idProdutoServico, produtoServico.nome,produtoServico.qtdMinima, " +
				"itemProduto.idItemProduto, itemProduto.quantidadeEntrada, itemProduto.quantidadeSaida,s.descricao,tp.descricao,itemProduto.idItemProduto,s.idSubTipoProduto, tp.idtipoProduto", " sum(itemProduto.quantidadeEntrada - itemProduto.quantidadeSaida) < produtoServico.qtdMinima ")
		.list();
	}

}
