package seguranca.autorizacao.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.SaveOrUpdateStrategy;

import seguranca.autenticacao.filter.ClienteFiltro;
import seguranca.autorizacao.bean.Cliente;


@DefaultOrderBy("cliente.nome")
public class ClienteDAO extends GenericDAO<Cliente> {
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Cliente> query) {
		query.leftOuterJoinFetch("cliente.listaAnimal listaAnimal");
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaAnimal");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Cliente> query, FiltroListagem _filtro) {
		ClienteFiltro filtro = (ClienteFiltro) _filtro;
		query
			.select("cliente.nome, cliente.cpf, cliente.celular, cliente.email")
			.whereLikeIgnoreAll("cliente.nome", filtro.getNome())
			.where("cliente.cpf=?",filtro.getCpf())
			.unique()
			;
		
	}
	
	@Override
	public Cliente loadForEntrada(Cliente bean) {
		query().leftOuterJoinFetch("cliente.listaAnimal listaAnimal");
		return super.loadForEntrada(bean);
	}
	
	
	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<Cliente> getListClienteReport(ClienteFiltro cliente){
		return query()
			.select("cliente")
			.whereLikeIgnoreAll("cliente.nome",cliente.getNome())
			.where("cliente.cpf=?",cliente.getCpf())
			.where("cliente.dtCadastro >=",cliente.getDtInicio())
			.where("cliente.dtCadastro <=",cliente.getDtFim())
			.list();
	}

}
