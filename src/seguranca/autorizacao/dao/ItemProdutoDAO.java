package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;

import seguranca.autenticacao.filter.ItemProdutoFiltro;
import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.ProdutoServico;

public class ItemProdutoDAO extends GenericDAO<ItemProduto>{

	
	public List<ItemProduto> getListaItemProdutoByProduto(ProdutoServico produtoServico){
		return query()
		.select("itemProduto")
		.leftOuterJoin("itemProduto.produtoServico p")
		.where("p=?",produtoServico)
		.list();
		
	}
	
	public ItemProduto getItemByLoteAndProduto(Integer item){
		return query()
			.select("itemProduto.quantidadeEntrada, itemProduto.quantidadeSaida")
			.leftOuterJoin("itemProduto.produtoServico p")
			.where("itemProduto.idItemProduto=?",item)
			.unique();
	}
	public List<ItemProduto> getItemByLoteAndProdutoList(Integer item){
		return query()
			.select("itemProduto")
			.leftOuterJoin("itemProduto.produtoServico p")
			.where("p.idProdutoServico=?",item)
			.list();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<ItemProduto> query,FiltroListagem _filtro) {
		ItemProdutoFiltro filtro = (ItemProdutoFiltro) _filtro;
		query.select("itemProduto.idItemProduto,itemProduto.quantidadeSaida,itemProduto.quantidadeEntrada,itemProduto.lote,produtoServico.nome")
		.join("itemProduto.produtoServico produtoServico")
		.where("itemProduto.lote=?",filtro.getLote())
		.whereLikeIgnoreAll("produtoServico.nome", filtro.getNome())
		.where("produtoServico.codigo=?",filtro.getCodProduto())
		.where("itemProduto.quantidadeEntrada > itemProduto.quantidadeSaida")
		.orderBy("produtoServico.nome");
		
	}
	
	public ItemProduto getItemProdutoAndProdutoSerico(ItemProduto itemProduto){
		return query()
		.select("itemProduto.idItemProduto,itemProduto.lote,itemProduto.quantidadeEntrada,itemProduto.quantidadeSaida,itemProduto.dtEntrada," +
				"itemProduto.dtVencimento, itemProduto.motivo,f.idFornecedor,itemProduto.valorEntrada," +
				"ps.idProdutoServico, ps.codigo, ps.nome,ps.observacoes,ps.ativo,ps.qtdMinima,stp.idSubTipoProduto")
		.leftOuterJoin("itemProduto.produtoServico ps")
		.leftOuterJoin("itemProduto.fornecedor f")
		.leftOuterJoin("ps.subTipoProduto stp")
		.where("itemProduto=?",itemProduto)
		.unique();
	}
	

	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<ItemProduto> getListItemProdutoReport(ItemProdutoFiltro filtro){
		return query()
		.select("itemProduto.idItemProduto,itemProduto.lote,itemProduto.quantidadeSaida," +
				"itemProduto.dtVencimento,f.idFornecedor,f.razaoSocial," +
				" ps.codigo, ps.nome,ps.ativo,ps.qtdMinima")
		.join("itemProduto.produtoServico ps")
		.join("itemProduto.fornecedor f")
		.join("ps.subTipoProduto stp")
		.whereLikeIgnoreAll("ps.nome",filtro.getNome())
		.where("ps.codigo=?",filtro.getCodProduto())
		.orderBy("ps.nome")
		.list();
	}
	
	
	public List<ItemProduto> getListByIds(String ids){
		return query().select("itemProduto").whereIn("itemProduto.idItemProduto", ids).list();
	}
	
}
