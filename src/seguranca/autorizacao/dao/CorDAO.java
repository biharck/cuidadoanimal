package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import seguranca.autorizacao.bean.Cor;


@DefaultOrderBy("cor.nome")
public class CorDAO extends GenericDAO<Cor> {

	
	@Override
	public List<Cor> findAll() {
		return query().select("cor").where("cor.ativo is true").list();
	}
	
	@Override
	public List<Cor> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}
	
	@Override
	protected String[] getComboSelectedProperties(String alias) {
		// TODO Auto-generated method stub
		return super.getComboSelectedProperties(alias);
	}
}
