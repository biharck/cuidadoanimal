package seguranca.autorizacao.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;

import seguranca.autenticacao.filter.ContaReceberFiltro;
import seguranca.autorizacao.bean.ContaReceber;

public class ContaReceberDAO extends GenericDAO<ContaReceber>{

	

	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<ContaReceber> getListContaReceberReport(ContaReceberFiltro filtro){
		QueryBuilder<ContaReceber> query = query();
		
			query.select("contaReceber.valor,contaReceber.dtVencimento,contaReceber.numeroDoc,fp.descricao,c.nome,contaReceber.status,contaReceber.dtRecebimento")
			.leftOuterJoin("contaReceber.cliente c")
			.leftOuterJoin("contaReceber.formaPagamento fp")
			.where("contaReceber.numDoc=?",filtro.getNumeroDoc())
			.where("c=?",filtro.getCliente());
			if(filtro.getDtInicio()!=null && filtro.getDtFim()!=null)
				query.where("contaReceber.dtVencimento between '"+filtro.getDtInicio()+"' and '"+filtro.getDtFim()+"'");
			
			return query.list();
	}
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<ContaReceber> query,FiltroListagem _filtro) {
		ContaReceberFiltro filtro = (ContaReceberFiltro) _filtro;
		query.select("c.nome, contaReceber.numeroDoc, f.descricao, contaReceber.dtVencimento, contaReceber.valor,contaReceber.status,contaReceber.idContaReceber,contaReceber.dtRecebimento")
		.leftOuterJoin("contaReceber.cliente c")
		.leftOuterJoin("contaReceber.formaPagamento f")
		.where("c=?", filtro.getCliente())
		.where("contaReceber.numeroDoc=?", filtro.getNumeroDoc())
		;
	}

}
