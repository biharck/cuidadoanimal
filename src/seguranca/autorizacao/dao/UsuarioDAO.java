package seguranca.autorizacao.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.NextWeb;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.biharckgroup.util.CuidadoAnimalException;

import seguranca.autorizacao.bean.Papel;
import seguranca.autorizacao.bean.PapelUsuario;
import seguranca.autorizacao.bean.Usuario;



public class UsuarioDAO extends GenericDAO<Usuario> {
	
	PapelDAO papelDAO;
	PapelUsuarioDAO papelUsuarioDAO;
	
	public void setPapelUsuarioDAO(PapelUsuarioDAO papelUsuarioDAO) {
		this.papelUsuarioDAO = papelUsuarioDAO;
	}
	
	public void setPapelDAO(PapelDAO papelDAO) {
		this.papelDAO = papelDAO;
	}
	
	public Usuario findByLogin(String login) {
		return query()
			.where("UPPER(usuario.login) = ?", login.toUpperCase())
			.unique();
	}
	
	/**
	 * Apenas para vers�o de avalia��o
	 */
	@Override
	public void updateListagemQuery(QueryBuilder<Usuario> query,FiltroListagem filtro) {
		query.where("usuario=?",NextWeb.getUser());
	}

	@Override
	public Usuario loadForEntrada(Usuario bean) {
		bean = super.loadForEntrada(bean);
		bean.setPapeis(papelDAO.findByUsuario(bean));
		return bean;
	}
	

	@Override
	public void saveOrUpdate(final Usuario bean) {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				Usuario usuarioSalvo = findByLogin(bean.getLogin());
				if(usuarioSalvo!=null){
					boolean ehEdicao = bean.getId()!=null;
					boolean ehMesmoObjeto = usuarioSalvo.getId().equals(bean.getId());
					if (!ehEdicao || ehEdicao && !ehMesmoObjeto){
						throw new RuntimeException("O login j� existe, digite outro login");
					}
				}
				
				UsuarioDAO.super.saveOrUpdate(bean);
				papelDAO.deleteByUsuario(bean);
				if(bean.getPapeis() != null){
					for (Papel papel : bean.getPapeis()) {
						PapelUsuario papelUsuario = new PapelUsuario();
						papelUsuario.setPapel(papel);
						papelUsuario.setUsuario(bean);
						papelUsuarioDAO.saveOrUpdate(papelUsuario);
					}
				}
				return null;
			}
			
		});
	}
	/**
	 * Encontra uma pessoa que possua o respectivo email
	 * 
	 * @param
	 * @return Uma pessoa com base no cod
	 */
	public Usuario findUserEmail(String email) {
		if (email == null || "".equals(email)) {
			throw new CuidadoAnimalException("Email n�o deve ser NULL e/ou vazio.");
		} else {
			return query()
					.select("usuario.id, usuario.nome, usuario.login, usuario.senha, usuario.dtCadastro,usuario.userTemp,usuario.senhaant,usuario.dtrequisicao," +
							"usuario.ativado,usuario.email,papeisUsuarios.id, papel.id")
					.leftOuterJoin("usuario.papeisUsuarios papeisUsuarios")
					.leftOuterJoin("papeisUsuarios.papel papel")
					.where("usuario.email = ?",email)
					.unique();
		}
	}
	
	/**
	 * <p>
	 * Encontra uma pessoa que possua o respectivo cod
	 * </p>.
	 * 
	 * @param cod
	 *            {@link Integer} codigo da pessoa
	 * @return {@link Usuario}
	 */
	public Usuario findUserByCodigo(Long cod) {
		if (cod == null) {
			throw new CuidadoAnimalException("Par�metro C�digo n�o pode ser null.");
		}
		return query()
		.select("usuario.id, usuario.nome, usuario.login, usuario.senha, usuario.dtCadastro,usuario.userTemp,usuario.senhaant,usuario.dtrequisicao," +
				"usuario.ativado,usuario.email,papeisUsuarios.id, papel.id")
		.leftOuterJoin("usuario.papeisUsuarios papeisUsuarios")
		.leftOuterJoin("papeisUsuarios.papel papel")
		.where("usuario.id = ?",cod)
		.unique();
	}
	
	/**
	 * Encontra uma pessoa que possua o respectivo email
	 * 
	 * @param
	 * @return Uma pessoa com base no cod
	 */
	public Usuario findUserByLogin(String login) {
		if (login == null || "".equals(login)) {
			throw new CuidadoAnimalException("Email n�o deve ser NULL e/ou vazio.");
		} else {
			return query()
					.select("usuario.id, usuario.nome, usuario.login, usuario.senha, usuario.dtCadastro,usuario.userTemp,usuario.senhaant,usuario.dtrequisicao," +
							"usuario.ativado,usuario.email,papeisUsuarios.id, papel.id")
					.leftOuterJoin("usuario.papeisUsuarios papeisUsuarios")
					.leftOuterJoin("papeisUsuarios.papel papel")
					.where("usuario.login = ?",login)
					.unique();
		}
	}
}
