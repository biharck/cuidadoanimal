package seguranca.autorizacao.dao;

import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import seguranca.autenticacao.controller.CuidadoAnimalExceptions;
import seguranca.autorizacao.bean.Municipio;
import seguranca.autorizacao.bean.Uf;

@DefaultOrderBy("uf.nome")
public class UfDAO extends GenericDAO<Uf> {

	
	public Uf getUfByMunicipio(Municipio municipio){
		if(municipio==null)
			throw new CuidadoAnimalExceptions("Municipio n�o pode ser null");
		
		return query()
			.select("uf.idUf, uf.nome")
			.leftOuterJoin("uf.listaMunicipio m")
			.where("m = ?",municipio)
			.unique();
	}
}
