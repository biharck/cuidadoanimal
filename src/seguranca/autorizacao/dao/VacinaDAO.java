package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.persistence.GenericDAO;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Vacina;

public class VacinaDAO extends GenericDAO<Vacina> {
	
	public List<Vacina> getVacinaByAnimal(Animal animal){
		return query()
					.select("vacina")
					.leftOuterJoin("vacina.animal a")
					.where("a=?",animal)
					.list();
	}

}
