package seguranca.autorizacao.dao;

import java.sql.Date;

import org.nextframework.persistence.GenericDAO;
import org.nextframework.types.Money;

import seguranca.autorizacao.bean.ProdutoServico;
import seguranca.autorizacao.bean.ValorProdutoServico;

public class ValorProdutoServicoDAO extends GenericDAO<ValorProdutoServico>{
	
	public ValorProdutoServico getValorProdutoServicoByProduto(ProdutoServico produtoServico){
		return query().
					select("valorProdutoServico.idValorProdutoServico,valorProdutoServico.valor,valorProdutoServico.dataInicio,valorProdutoServico.dataFim," +
							"valorProdutoServico.margemLucro,p.idProdutoServico,valorProdutoServico.valorFim")
					.leftOuterJoin("valorProdutoServico.produtoServico p")
					.where("p=?",produtoServico)
					.where("valorProdutoServico.dataFim is null")
					.unique();
	}
}
