package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import seguranca.autorizacao.bean.Transporte;

@DefaultOrderBy("transporte.descricao")
public class TransporteDAO extends GenericDAO<Transporte>{
	
	@Override
	public List<Transporte> findForCombo() {
		return query().select("transporte").where("transporte.ativo is true").list();
	}
	
	

}
