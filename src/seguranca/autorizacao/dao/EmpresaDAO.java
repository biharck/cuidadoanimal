package seguranca.autorizacao.dao;

import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.SaveOrUpdateStrategy;

import seguranca.autorizacao.bean.Empresa;



public class EmpresaDAO extends GenericDAO<Empresa> {
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaRamal");
	}
	
//	@Override
//	public void updateEntradaQuery(QueryBuilder<Empresa> query) {
//		query.select("empresa.idEmpresa,empresa.razaoSocial, empresa.cnpj, empresa.inscricaoEstadual,empresa.aberto24Horas, empresa.inscricaoMunicipal, " +
//				"empresa.logradouro, empresa.cep, empresa.bairro, empresa.email, empresa.telefone1, empresa.telefone2,empresa.abertura, empresa.fechamento, " +
//				"municipio.idMunicipio, municipio.nome, uf.sigla, uf.idUf, uf.nome,listaRamal.numero, listaRamal.localizacao,listaRamal.idRamal, foto.cdfile")
//		.leftOuterJoin("empresa.listaRamal listaRamal")
//		.leftOuterJoin("empresa.municipio municipio")
//		.leftOuterJoin("empresa.foto foto")
//		.leftOuterJoin("municipio.uf uf");
//	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Empresa> query) {
		query.leftOuterJoinFetch("empresa.listaRamal listaRamal");
	}
	
	public Empresa getEmpresa(){
		return 	query().select("empresa").leftOuterJoinFetch("empresa.foto foto").setMaxResults(1).unique();
	}

}
