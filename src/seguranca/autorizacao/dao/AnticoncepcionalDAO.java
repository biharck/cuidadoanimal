package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.persistence.GenericDAO;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Anticoncepcional;

public class AnticoncepcionalDAO extends GenericDAO<Anticoncepcional> {
	
	public List<Anticoncepcional> getAnticoncecpionalByAnimal(Animal animal){
		return query()
					.select("anticoncepcional.idAnticoncepcional,anticoncepcional.dtAplicacao,anticoncepcional.peso," +
							"anticoncepcional.dtProximaAplicacao," +
							"ps.idProdutoServico, ps.nome, dose.idDose,dose.descricao ")
					.leftOuterJoin("anticoncepcional.animal a")
					.leftOuterJoin("anticoncepcional.produtoServico ps")
					.leftOuterJoin("anticoncepcional.dose dose")
					.where("a=?",animal)
					.list();
	}

}
