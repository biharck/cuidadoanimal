package seguranca.autorizacao.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;

import seguranca.autenticacao.filter.FornecedorFiltro;
import seguranca.autorizacao.bean.Fornecedor;



public class FornecedorDAO extends GenericDAO<Fornecedor> {

	
	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<Fornecedor> getListFornecedorReport(FornecedorFiltro filtro){
		return query()
			.select("fornecedor")
			.whereLikeIgnoreAll("fornecedor.razaoSocial",filtro.getRazaoSocial())
			.where("fornecedor.cnpj=?",filtro.getCnpj())
			.where("fornecedor.inscricaoEstadual=?",filtro.getInscricaoEstadual())
			.where("fornecedor.inscricaoMunicipal=?",filtro.getInscricaoMunicipal())
			.where("fornecedor.ativo=?",filtro.getAtivo())
			.where("fornecedor.dtCadastro >=",filtro.getDtInicio())
			.where("fornecedor.dtCadastro <=",filtro.getDtFim())
			.list();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Fornecedor> query,FiltroListagem _filtro) {
		FornecedorFiltro filtro = (FornecedorFiltro) _filtro;
		query
		.select("fornecedor")
		.whereLikeIgnoreAll("fornecedor.razaoSocial",filtro.getRazaoSocial())
		.where("fornecedor.cnpj=?",filtro.getCnpj())
		.where("fornecedor.inscricaoEstadual=?",filtro.getInscricaoEstadual())
		.where("fornecedor.inscricaoMunicipal=?",filtro.getInscricaoMunicipal())
		.where("fornecedor.dtCadastro >=",filtro.getDtInicio())
		.where("fornecedor.dtCadastro <=",filtro.getDtFim())
		.where("fornecedor.ativo is true")
		
		.list();
	}
}
