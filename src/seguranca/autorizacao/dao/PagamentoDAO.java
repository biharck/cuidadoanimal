package seguranca.autorizacao.dao;

import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.SaveOrUpdateStrategy;

import seguranca.autorizacao.bean.Pagamento;

public class PagamentoDAO extends GenericDAO<Pagamento>{
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaVendaFormaPagamento");
	}

}
