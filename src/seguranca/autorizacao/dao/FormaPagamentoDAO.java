package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.persistence.GenericDAO;

import seguranca.autorizacao.bean.FormaPagamento;

public class FormaPagamentoDAO extends GenericDAO<FormaPagamento>{
	
	public List<FormaPagamento> listaFormasReaisPagamento(){
		return query().select("formaPagamento")
		.where("formaPagamento.idFormaPagamento<>4")
		.where("formaPagamento.idFormaPagamento<>5")
		.list();
	}

}
