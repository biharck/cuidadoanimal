package seguranca.autorizacao.dao;

import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import seguranca.autorizacao.bean.Municipio;


@DefaultOrderBy("municipio.nome")
public class MunicipioDAO extends GenericDAO<Municipio> {

}
