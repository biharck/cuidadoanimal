package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.persistence.GenericDAO;

import seguranca.autenticacao.filter.ProdutoServicoFiltro;
import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.ServicosPrestados;

public class ServicosPrestadosDAO extends GenericDAO<ServicosPrestados>{
	
	public List<ServicosPrestados> getListaServicosPrestadosByAnimal(Animal animal){
		return query().select("servicosPrestados")
		.leftOuterJoin("servicosPrestados.animal a")
		.where("a=?",animal)
		.list();
	}
	
	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<ServicosPrestados> getListServicosPrestadosReport(ProdutoServicoFiltro filtro){
		return query()
			.select("servicosPrestados.idServicosPrestados,ps.nome, animal.nome, servicosPrestados.qtd")
			.leftOuterJoin("servicosPrestados.itemProduto ip")
			.leftOuterJoin("ip.produtoServico ps")
			.leftOuterJoin("servicosPrestados.animal animal")
			.where("servicosPrestados.date between '"+filtro.getDtInicio()+"' and '"+filtro.getDtFim()+"'")
			.where("animal=?",filtro.getAnimal())
			.list();
	}
}
