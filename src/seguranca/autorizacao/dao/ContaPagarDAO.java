package seguranca.autorizacao.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;

import seguranca.autenticacao.filter.ContaPagarFiltro;
import seguranca.autorizacao.bean.ContaPagar;

public class ContaPagarDAO extends GenericDAO<ContaPagar> {
	
	
	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<ContaPagar> getListContaPagarReport(ContaPagarFiltro filtro){
		QueryBuilder<ContaPagar> query = query();
		
		query.select("contaPagar.valor,contaPagar.dtVencimento,contaPagar.dtPagamento,contaPagar.numeroDoc,f.razaoSocial,fp.descricao,contaPagar.status")
			.leftOuterJoin("contaPagar.fornecedor f")
			.leftOuterJoin("contaPagar.formaPagamento fp")
			.where("contaPagar.numDoc=?",filtro.getNumDoc())
			.where("f=?",filtro.getFornecedor());
			if(filtro.getDtInicio()!=null && filtro.getDtFim()!=null)
				query().where("contaPagar.dtVencimento between '"+filtro.getDtInicio()+"' and '"+filtro.getDtFim()+"'");
		
		return query.list();
	}
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<ContaPagar> query,	FiltroListagem _filtro) {
		ContaPagarFiltro filtro = (ContaPagarFiltro) _filtro;
		query.select("contaPagar")
		.leftOuterJoin("contaPagar.fornecedor f")
		.where("f=?",filtro.getFornecedor())
		.where("contaPagar.numeroDoc=?",filtro.getNumDoc());
		if(filtro.getDtInicio()!=null && filtro.getDtFim()!=null)
			query.where("contaPagar.dtVencimento between '"+filtro.getDtInicio()+"' and '"+filtro.getDtFim()+"'");
		else if(filtro.getDtInicio()!=null)
			query.where("contaPagar.dtVencimento >= '"+filtro.getDtInicio()+"'");
		if(filtro.getDtFim()!=null)
			query.where("contaPagar.dtVencimento <= '"+filtro.getDtFim()+"'");
		;
	}

}
