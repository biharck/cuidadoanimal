package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.persistence.GenericDAO;

import seguranca.autenticacao.filter.ConsultaFiltro;
import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Consultas;

public class ConsultasDAO extends GenericDAO<Consultas>{
	
	public List<Consultas> getConsultasByAnimal(Animal animal){
		return query()
					.select("consultas")
					.leftOuterJoin("consultas.animal a")
					.where("a=?",animal)
					.list();
	}
	
	public List<Consultas> getConsultasById(Integer id){
		return query()
					.select("consultas.idConsultas,consultas.retorno,consultas.receituario,consultas.data, especie.nome,veterinario.nome,animal.nome,veterinario.nome,consultas.peso,raca.nome,dono.nome,consultas.descricao ")
					.leftOuterJoin("consultas.animal animal")
					.leftOuterJoin("consultas.veterinario veterinario")
					.leftOuterJoin("animal.dono dono")
					.leftOuterJoin("animal.especie especie")
					.leftOuterJoin("animal.raca raca")
					.where("consultas.idConsultas=?",id)
					.list();
	}
	
	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<Consultas> getListConsultasReport(ConsultaFiltro filtro){
		return query()
			.select("consultas.idConsultas,consultas.retorno,consultas.data, especie.nome,veterinario.nome,animal.nome,veterinario.nome,consultas.peso,raca.nome,dono.nome")
			.leftOuterJoin("consultas.animal animal")
			.leftOuterJoin("animal.dono dono")
			.leftOuterJoin("animal.especie especie")
			.leftOuterJoin("animal.raca raca")
			.leftOuterJoin("consultas.veterinario veterinario")
			.where("consultas.data=?",filtro.getData())
			.where("animal=?",filtro.getAnimal())
			.where("veterinario=?",filtro.getVeterinario())
			.list();
	}
	
}
