package seguranca.autorizacao.dao;

import java.awt.Image;
import java.beans.PropertyDescriptor;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.nextframework.persistence.FileDAO;
import org.nextframework.types.File;
import org.springframework.orm.hibernate3.SessionFactoryUtils;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Arquivo;
import br.com.biharckgroup.util.CuidadoAnimalException;

/**
 * O DAO do bean que implementa file deve extender FileDAO
 * @author rogelgarcia
 *
 */
public class ArquivoDAO extends FileDAO<Arquivo>{
	@Override
	protected void readFile(File arquivo) {
	}
	@Override
	protected void writeFile(File arquivoNovo, String nomeArquivo) throws IOException {
	}
	@Override
	public void delete(Arquivo bean) {
		Session session = SessionFactoryUtils.getSession(getHibernateTemplate().getSessionFactory(), getHibernateTemplate().getEntityInterceptor(), getHibernateTemplate().getJdbcExceptionTranslator());
		Transaction transaction = session.getTransaction();
		boolean intransaction = true;
		if(!transaction.isActive()){
			transaction = session.beginTransaction();	
			intransaction = false;
		}
		 
		try {
			if(autoManageFileProperties() && fileDAO != null){
				bean = load(bean);
			}
			getHibernateTemplate().delete(bean);
			if(autoManageFileProperties() && fileDAO != null){
				for (PropertyDescriptor pd : fileProperties) {
					File fileToDelete = (File) pd.getReadMethod().invoke(bean);
					fileDAO.delete(fileToDelete);
				}				
			}
			session.flush();
			if (!intransaction) {
				transaction.commit();
			}
		} catch (HibernateException ex) {
			if (!intransaction) {
				transaction.rollback();
			}
			throw convertHibernateAccessException(ex);
		} catch (RuntimeException e) {
			if (!intransaction) {
				transaction.rollback();
			}
			throw e;
		} catch (Exception e){
			if (!intransaction) {
				transaction.rollback();
			}
			throw new RuntimeException(e);
		} finally {
			
			SessionFactoryUtils.releaseSession(session, getHibernateTemplate().getSessionFactory());
				
		}
	}
	
	public void fillWithContents(File file) {
		readFile(file);
	}
	
	public Image loadAsImage(File file) {
		fillWithContents(file);
		try {
			return ImageIO.read(new ByteArrayInputStream(file.getContent()));
		}
		catch (Exception e) {
			throw new CuidadoAnimalException("Erro ao converter arquivo em imagem.");
		}
	}
	
	public Arquivo getFoto(Animal animal){
		return query()
				.select("arquivo")
				.leftOuterJoin("arquivo.listaAnimal a")
				.where("a=?",animal)
				.unique();
	}
	
	
}
