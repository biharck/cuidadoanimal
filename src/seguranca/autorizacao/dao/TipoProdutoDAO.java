package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;

import seguranca.autorizacao.bean.SubTipoProduto;
import seguranca.autorizacao.bean.TipoProduto;


@DefaultOrderBy("tipoProduto.descricao")
public class TipoProdutoDAO extends GenericDAO<TipoProduto> {
	
	public TipoProduto getTipoProdutoBySubTipo(SubTipoProduto subTipoProduto){
		return query()
					.select("tipoProduto")
					.leftOuterJoin("tipoProduto.listaSubTipoProduto l")
					.where("l=?",subTipoProduto)
					.unique();
	}

	
	@Override
	public List<TipoProduto> findAll(String orderBy) {
		// TODO Auto-generated method stub
		return query().where("tipoProduto.ativo is true").list();
	}
	
	@Override
	public List<TipoProduto> findForCombo(String... extraFields) {
		// TODO Auto-generated method stub
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}
	
	@Override
	protected String[] getComboSelectedProperties(String alias) {
		// TODO Auto-generated method stub
		return super.getComboSelectedProperties(alias);
	}
	
}
