package seguranca.autorizacao.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.GenericDAO;
import org.nextframework.persistence.QueryBuilder;

import seguranca.autenticacao.filter.RacaFiltro;
import seguranca.autorizacao.bean.Especie;
import seguranca.autorizacao.bean.Raca;


@DefaultOrderBy("raca.nome")
public class RacaDAO extends GenericDAO<Raca> {

	
	@Override
	public void updateListagemQuery(QueryBuilder<Raca> query,FiltroListagem _filtro) {
		RacaFiltro filtro = (RacaFiltro) _filtro;
		query
		.select("raca.nome, e.nome,raca.idRaca")
		.leftOuterJoin("raca.especie e")
		.where("e=?",filtro.getEspecie());
	}
	
	
	@Override
	public List<Raca> findAll() {
		return query().select("raca").where("raca.ativo is true").list();
	}
	
	@Override
	public List<Raca> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}
	
	@Override
	protected String[] getComboSelectedProperties(String alias) {
		// TODO Auto-generated method stub
		return super.getComboSelectedProperties(alias);
	}

}
