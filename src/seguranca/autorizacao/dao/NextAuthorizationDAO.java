package seguranca.autorizacao.dao;

import java.util.List;
import java.util.Map;

import org.nextframework.authorization.AuthorizationDAO;
import org.nextframework.authorization.Permission;
import org.nextframework.authorization.Role;
import org.nextframework.authorization.User;
import org.nextframework.persistence.QueryBuilder;
import org.springframework.orm.hibernate3.HibernateTemplate;

import seguranca.autorizacao.bean.Papel;
import seguranca.autorizacao.bean.PapelUsuario;
import seguranca.autorizacao.bean.Permissao;
import seguranca.autorizacao.bean.Tela;
import seguranca.autorizacao.bean.Usuario;

public class NextAuthorizationDAO implements AuthorizationDAO{
	
	private HibernateTemplate hibernateTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public Role[] findAllRoles() {
		List<Papel> roles = new QueryBuilder<Papel>(hibernateTemplate)
							.from(Papel.class)
							.list();
		return roles.toArray(new Role[roles.size()]);
	}

	public Permission findPermission(Role role, String controlName) {
		return new QueryBuilder<Permissao>(hibernateTemplate)
		.from(Permissao.class)
		.leftOuterJoinFetch("permissao.papel papel")
		.leftOuterJoinFetch("permissao.tela tela")
		.where("papel = ?", role)
		.where("tela.path = ?", controlName)
		.unique();
	}

	public User findUserByLogin(String login) {
		return new QueryBuilder<Usuario>(hibernateTemplate)
		.from(Usuario.class)
		.where("UPPER(usuario.login) = ?", login.toUpperCase())
		.unique();

	}

	public Role[] findUserRoles(User user) {
		java.util.List<PapelUsuario> list = new QueryBuilder<PapelUsuario>(hibernateTemplate)
		.select("papel")
		.from(PapelUsuario.class)
		.leftOuterJoin("papelUsuario.papel papel")
		.leftOuterJoin("papelUsuario.usuario usuario")
		.where("usuario = ?", user)
		.list();
		return list.toArray(new Role[list.size()]);
	}

	public Permission savePermission(String controlName, Role role, Map<String,String> permissionMap) {
		Permissao permissao;
		Tela tela = new QueryBuilder<Tela>(hibernateTemplate)
						.from(Tela.class)
						.where("tela.path = ?", controlName)
						.unique();
		if(tela == null){
			tela = new Tela();
			tela.setPath(controlName);
			if (controlName.contains("/")) {
				tela.setDescricao(controlName.substring(controlName.lastIndexOf('/') + 1));
			} else {
				tela.setDescricao(controlName);
			}
			hibernateTemplate.save(tela);
		}
		{
			//verificar se j� existe essa permissao no banco
			permissao = new QueryBuilder<Permissao>(hibernateTemplate)
					.from(Permissao.class)
					.where("permissao.tela.path = ?", controlName)
					.where("permissao.papel = ?", role)
					.unique();
		}
		if(permissao == null){
			//criar a permissao
			permissao = new Permissao();
			Papel papel = (Papel) role;
			permissao.setPapel(papel);
			permissao.setTela(tela);
			permissao.setPermissionMap(permissionMap);
			hibernateTemplate.save(permissao);
		} else {
			//atualizar a permissao
			Papel papel = (Papel) role;
			permissao.setPapel(papel);
			permissao.setTela(tela);
			permissao.setPermissionMap(permissionMap);
			hibernateTemplate.update(permissao);
		}
		return permissao;
	}
	
	

}
