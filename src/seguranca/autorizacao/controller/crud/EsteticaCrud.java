package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import seguranca.autenticacao.filter.EsteticaFiltro;
import seguranca.autorizacao.bean.Estetica;
import br.com.biharckgroup.util.CrudController;


@Controller(path="/autorizacao/crud/Estetica",authorizationModule=CrudAuthorizationModule.class)
public class EsteticaCrud extends CrudController<EsteticaFiltro,Estetica, Estetica>{

}
