package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autenticacao.filter.FornecedorFiltro;
import seguranca.autorizacao.bean.Fornecedor;
import seguranca.autorizacao.service.UfService;
import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalException;


@Controller(path="/autorizacao/estoque/crud/Fornecedor",authorizationModule=CrudAuthorizationModule.class)
public class FornecedorCrud extends CrudController<FornecedorFiltro, Fornecedor, Fornecedor>{
	
	private UfService ufService;
	
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Fornecedor form)
			throws CrudException {
		request.setAttribute("setIdForm", "form");
		if(form.getIdFornecedor()!=null)
			form.setUf(ufService.getUfByMunicipio(form.getMunicipio()));
		return super.doEntrada(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, FornecedorFiltro filtro)throws Exception {
		if(request.getAttribute("escondeMenu")!=null)
			request.setAttribute("escondeMenu", true);
		super.listagem(request, filtro);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FornecedorFiltro filtro) throws CrudException {
		Boolean parametro = new Boolean(request.getParameter("popup"));
		if(new Boolean(request.getParameter("escondeMenu")))
			request.setAttribute("escondeMenu", true);
		filtro.setAtivo(parametro);
		
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Fornecedor bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_cnpj_fornecedor")) throw new CuidadoAnimalException("CNPJ j� cadastrada no sistema.");
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_ie_fornecedor")) throw new CuidadoAnimalException("Inscri��o Estadual j� cadastrada no sistema.");
		}
	}

}
