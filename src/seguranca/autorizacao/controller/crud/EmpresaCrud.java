package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autorizacao.bean.Empresa;
import seguranca.autorizacao.service.ArquivoService;
import seguranca.autorizacao.service.UfService;
import br.com.biharckgroup.util.CrudController;


@Controller(path="/autorizacao/clinica/crud/Empresa",authorizationModule=CrudAuthorizationModule.class)
public class EmpresaCrud extends CrudController<FiltroListagem, Empresa, Empresa>{
	
	private UfService ufService;
	ArquivoService arquivoService;
	
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Empresa form)throws CrudException {
		request.setAttribute("setIdForm", "form");
		if(form!=null && form.getIdEmpresa()!=null)
			form.setUf(ufService.getUfByMunicipio(form.getMunicipio()));
		request.setAttribute("empresaCad", true);
		return super.doEntrada(request, form);
	}

	@Override
	protected Empresa carregar(WebRequestContext request, Empresa bean)throws Exception {
		request.setAttribute("exibeFoto", true);
		return super.carregar(request, bean);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Empresa bean)throws Exception {
		super.salvar(request, bean);
		
		try {	
			arquivoService.loadAsImage(bean.getFoto());
			if(bean.getFoto().getCdfile() != null){
				DownloadFileServlet.addCdfile(request.getSession(), bean.getFoto().getCdfile());
				request.getSession().setAttribute("exibeFotoEmpresa", true);
				request.getSession().setAttribute("idFotoEmpresa", bean.getFoto().getCdfile());
			}
		} catch (Exception ex) {
			request.getSession().setAttribute("exibeFotoEmpresa", false);
		}
	}
}
