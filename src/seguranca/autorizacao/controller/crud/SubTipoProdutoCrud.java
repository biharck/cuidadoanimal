package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalException;

import seguranca.autorizacao.bean.SubTipoProduto;

@Controller(path="/autorizacao/cadastros/crud/SubTipoProduto", authorizationModule=CrudAuthorizationModule.class)
public class SubTipoProdutoCrud extends CrudController<org.nextframework.controller.crud.FiltroListagem, SubTipoProduto, SubTipoProduto>{

	
	@Override
	protected void salvar(WebRequestContext request, SubTipoProduto bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_descricao_subtipoproduto")) throw new CuidadoAnimalException("Sub-Tipo de Produto j� cadastrado no sistema.");
		}
	}
}
