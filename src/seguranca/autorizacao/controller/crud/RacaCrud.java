package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalException;

import seguranca.autenticacao.filter.RacaFiltro;
import seguranca.autorizacao.bean.Raca;

@Controller(path="/autorizacao/cadastros/crud/Raca", authorizationModule=CrudAuthorizationModule.class)
public class RacaCrud extends CrudController<RacaFiltro, Raca, Raca>{
	
	@Override
	protected void salvar(WebRequestContext request, Raca bean)	throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_nome_raca")) throw new CuidadoAnimalException("Ra�a j� cadastrada no sistema.");
		}
	}

}
