package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import br.com.biharckgroup.util.CrudController;
import org.nextframework.controller.crud.FiltroListagem;

import seguranca.autorizacao.bean.Papel;

@Controller(path = "/autorizacao/seguranca/crud/papel", authorizationModule = CrudAuthorizationModule.class)
public class PapelCrud extends CrudController<FiltroListagem, Papel, Papel> {

}
