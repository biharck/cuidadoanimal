package seguranca.autorizacao.controller.crud;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.persistence.ListagemResult;
import org.nextframework.types.Money;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autenticacao.filter.VendaFiltro;
import seguranca.autorizacao.bean.Caixa;
import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.ProdutoServico;
import seguranca.autorizacao.bean.ServicosPrestados;
import seguranca.autorizacao.bean.Venda;
import seguranca.autorizacao.service.CaixaService;
import seguranca.autorizacao.service.ItemProdutoService;
import seguranca.autorizacao.service.ProdutoServicoService;
import seguranca.autorizacao.service.VendaService;
import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalUtil;

@Controller(path = "/autorizacao/clinica/crud/FrenteCaixa", authorizationModule = CrudAuthorizationModule.class)
public class VendaCrud extends CrudController<VendaFiltro, Venda, Venda> {

	private ProdutoServicoService produtoServicoService;
	private CaixaService caixaService;
	private VendaService vendaService;
	private ItemProdutoService itemProdutoService;
	
	public void setProdutoServicoService(ProdutoServicoService produtoServicoService) {
		this.produtoServicoService = produtoServicoService;
	}
	public void setCaixaService(CaixaService caixaService) {
		this.caixaService = caixaService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setItemProdutoService(ItemProdutoService itemProdutoService) {
		this.itemProdutoService = itemProdutoService;
	}
	
	@Override
	protected void listagem(WebRequestContext request, VendaFiltro filtro)throws Exception {
		Caixa c = caixaService.getCaixaDia();
		if(c==null)
			request.setAttribute("abrirCaixa", true);
		else{
			request.setAttribute("abrirCaixa", false);
		}
		request.setAttribute("imprimir", true);
		request.setAttribute("noNumberPages", true);
		super.listagem(request, filtro);
	}

	@Override
	protected ListagemResult<Venda> getLista(WebRequestContext request,VendaFiltro filtro) {
		ListagemResult<Venda> lista = super.getLista(request, filtro);
		List<Venda> listaIteracao = lista.list();
		Long valor = new Long(0);
		Long valorTotal = new Long(0);
		long desconto = 0l;
		for (Venda venda : listaIteracao) {
			valor = 0l;
			for (ServicosPrestados sp : venda.getServicosPrestados()) {
				long valorPrevio = sp.getItemProduto().getProdutoServico().getListaValorProdutoServico().get(0).getValor().toLong();
				long margemLucro = sp.getItemProduto().getProdutoServico().getListaValorProdutoServico().get(0).getMargemLucro().longValue();
				desconto += sp.getDesconto()==null?0l:sp.getDesconto().toLong();
				valorPrevio = valorPrevio + (valorPrevio * margemLucro)/100;
				valor += valorPrevio* (sp.getQtd()==null || sp.getQtd()==0?1:sp.getQtd());
			}
			venda.setPreviaValor(new Money(valor-(desconto),true));
			valorTotal += venda.getPreviaValor().toLong();
		}
		BigInteger b = new BigInteger(valorTotal.toString());
		request.setAttribute("valorTotal", new Money(b.doubleValue()/100));
		return lista;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Venda form)throws Exception {
		if(form.getIdVenda()!=null){
			for (ServicosPrestados s : form.getServicosPrestados()) {
				ProdutoServico ps = produtoServicoService.getProdutoByItem(s.getItemProduto()); 
				s.setProduto(ps.getNome());
				Integer total = 0;
				for (ItemProduto ip : ps.getListaItensProdutos()) {
					total += ip.getQuantidadeEntrada()-ip.getQuantidadeSaida();
				}
				s.setResto(total);
			}
			request.setAttribute("CONS", true);
		}
		super.entrada(request, form);
	}

	@Action("abreCaixa")
	public ModelAndView abreCaixa(WebRequestContext context) throws CrudException{
		if(caixaService.getCaixaDia()==null){
			Caixa c = new Caixa();
			c.setDataAbertura(new Date(System.currentTimeMillis()));
			c.setHoraAbertura(new Time(System.currentTimeMillis()));
			c.setUsuario(CuidadoAnimalUtil.getPessoaLogada());
			//pegar informações do caixa do dia anterior...saldo etc...
			caixaService.saveOrUpdate(c);	
		}
		return doListagem(context, new VendaFiltro());
	}
	
	@Action("cancelaCompra")
	public ModelAndView cancelaCompra(WebRequestContext context) throws CrudException{
		Venda venda = new Venda();
		venda.setIdVenda(CuidadoAnimalUtil.returnIdWithParameters(context.getParameter("idVenda")));
		venda = vendaService.getVendaById(venda.getIdVenda());
		venda.setFechado(false);
		venda.setIdUpdate(false);
		for (ServicosPrestados set : venda.getServicosPrestados()) {
			//baixando o restante dos itens
			ItemProduto ip = itemProdutoService.getItemProdutoAndProdutoSerico(set.getItemProduto());
			ip.setQuantidadeSaida(ip.getQuantidadeSaida()-set.getQtd());
			itemProdutoService.saveOrUpdate(ip);
		}
		vendaService.updateVenda(venda.getIdVenda());
		return doListagem(context, new VendaFiltro());
	}
}
