package seguranca.autorizacao.controller.crud;

import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;

import seguranca.autorizacao.bean.Pessoa;
import br.com.biharckgroup.util.CrudController;

@Controller(path="/pub/crud/Pessoa")
public class PessoaCrud extends CrudController<FiltroListagem, Pessoa, Pessoa>{
	
}
