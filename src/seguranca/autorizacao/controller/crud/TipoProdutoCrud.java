package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import br.com.biharckgroup.util.CrudController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.biharckgroup.util.CuidadoAnimalException;

import seguranca.autorizacao.bean.TipoProduto;

@Controller(path="/autorizacao/cadastros/crud/TipoProduto", authorizationModule=CrudAuthorizationModule.class)
public class TipoProdutoCrud extends CrudController<org.nextframework.controller.crud.FiltroListagem, TipoProduto, TipoProduto>{
	
	@Override
	protected void excluir(WebRequestContext request, TipoProduto bean)throws Exception {
		if(bean.getIdtipoProduto()<11)
			throw new CuidadoAnimalException("Este Registro � Est�tico e n�o pode ser eclu�do do sistema");
		else
			super.excluir(request, bean);
	}
	
	@Override
	protected void salvar(WebRequestContext request, TipoProduto bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_descricao_tipoproduto")) throw new CuidadoAnimalException("Tipo de Produto j� cadastrado no sistema.");
		}
	}

}
