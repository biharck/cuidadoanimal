package seguranca.autorizacao.controller.crud;

import java.util.List;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autenticacao.filter.ClienteFiltro;
import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Cliente;
import seguranca.autorizacao.service.AnimalService;
import seguranca.autorizacao.service.ArquivoService;
import seguranca.autorizacao.service.UfService;
import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalException;

@Controller(path = "/autorizacao/clinica/crud/Cliente", authorizationModule = CrudAuthorizationModule.class)
public class ClienteCrud extends CrudController<ClienteFiltro, Cliente, Cliente> {

	private UfService ufService;
	private ArquivoService arquivoService;
	private AnimalService animalService;

	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setAnimalService(AnimalService animalService) {
		this.animalService = animalService;
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Cliente form)throws CrudException {
		
		
		if(request.getAttribute("escondeMenu")!=null)
			request.setAttribute("escondeMenu", true);
		request.setAttribute("setIdForm", "form");
		if (form.getIdCliente() != null)
			form.setUf(ufService.getUfByMunicipio(form.getMunicipio()));
		else
			form.setListaAnimal(null);
		return super.doEntrada(request, form);
	}

	@Override
	protected Cliente carregar(WebRequestContext request, Cliente bean)throws Exception {
		request.setAttribute("exibeFoto", true);
		return super.carregar(request, bean);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Cliente bean) throws Exception {
		if(bean.getIdCliente()!=null){
			List<Animal> listaAnimais = animalService.getListaAnimalByCliente(bean);
			bean.setListaAnimal(listaAnimais);
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Cliente bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_cpf_cliente")) throw new CuidadoAnimalException("J� Existe um cliente com este CPF cadastrado no sistema.");
		}
	
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,ClienteFiltro filtro) throws CrudException {
		request.setAttribute("escondeMenu", request.getParameter("escondeMenu"));
		return super.doListagem(request, filtro);
	}
}
