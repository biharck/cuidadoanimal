package seguranca.autorizacao.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autenticacao.filter.AnimalFiltro;
import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Consultas;
import seguranca.autorizacao.bean.HistoricoClinico;
import seguranca.autorizacao.service.AnimalService;
import seguranca.autorizacao.service.ArquivoService;
import seguranca.autorizacao.service.ClienteService;
import seguranca.autorizacao.service.ConsultasService;
import seguranca.autorizacao.service.ProdutoServicoService;
import seguranca.autorizacao.service.ServicosPrestadosService;
import seguranca.autorizacao.service.TransporteService;
import br.com.biharckgroup.util.CrudController;

@Controller(path = "/autorizacao/clinica/crud/Animal", authorizationModule = CrudAuthorizationModule.class)
public class AnimalCrud extends CrudController<AnimalFiltro, Animal, Animal> {

	private AnimalService animalService;
	private ConsultasService consultasService;
	private ProdutoServicoService produtoServicoService;
	private ServicosPrestadosService servicosPrestadosService;
	private TransporteService transporteService;
	private ArquivoService arquivoService;
	private ClienteService clienteService;
	
	public void setAnimalService(AnimalService animalService) {
		this.animalService = animalService;
	}
	public void setConsultasService(ConsultasService consultasService) {
		this.consultasService = consultasService;
	}
	public void setProdutoServicoService(
			ProdutoServicoService produtoServicoService) {
		this.produtoServicoService = produtoServicoService;
	}
	public void setServicosPrestadosService(ServicosPrestadosService servicosPrestadosService) {
		this.servicosPrestadosService = servicosPrestadosService;
	}
	public void setTransporteService(TransporteService transporteService) {
		this.transporteService = transporteService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	@Override
	protected Animal carregar(WebRequestContext request, Animal bean)throws Exception {
		bean = animalService.loadForEntrada(bean);
		try {	
			arquivoService.loadAsImage(bean.getFoto());
			if(bean.getFoto().getCdfile() != null){
				DownloadFileServlet.addCdfile(request.getSession(), bean.getFoto().getCdfile());
				request.setAttribute("exibeFoto", true);
			}
		} catch (Exception e) {
			request.setAttribute("exibeFoto", false);
		}
//		request.setAttribute("exibeFoto", true);
		return super.carregar(request, bean);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Animal form) throws Exception {
		if (form.getIdAnimal()!=null) {
			List<HistoricoClinico> listaHistoricoClinico = new ArrayList<HistoricoClinico>();
			List<Consultas> listaConsultas = consultasService.getConsultasByAnimal(form);
			String consulta;
			String conduta;
			for (Consultas consultas : listaConsultas) {
				consulta = "CONSULTA: ";
				conduta = "\nCONDUTA: ";
				consulta += consultas.getDescricao()==null?"":consultas.getDescricao();
				conduta += consultas.getReceituario()==null?"":consultas.getReceituario();
				
				listaHistoricoClinico.add(new HistoricoClinico(consulta+conduta,consultas.getRetorno()!=null && consultas.getRetorno()?"Retorno": "Consultas/Receituário", consultas.getData(),consultas.getIdConsultas()));
			}
			form.setListaHistoricoClinico(listaHistoricoClinico);
			form = animalService.carregaListas(form);
			form.setListaServicosPrestadosAntiga(servicosPrestadosService.getListaServicosPrestadosByAnimal(form));
			request.setAttribute("listaVacinas", produtoServicoService.getProdutoServicoByTipo(2));
			request.setAttribute("listaAntiPulgas", produtoServicoService.getProdutoServicoByTipo(3));
			request.setAttribute("listaAntiConcepcional", produtoServicoService.getProdutoServicoByTipo(4));
			request.setAttribute("listaVermifugos", produtoServicoService.getProdutoServicoByTipo(5));
			request.setAttribute("listaTransportes", transporteService.findForComboEspecifico());
			request.setAttribute("animalEdit", true);
			request.setAttribute("nameDono", clienteService.loadForEntrada(form.getDono()).getNome());
			form.setListaConsultas(null);
		}
		super.entrada(request, form);
	}
	
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,AnimalFiltro filtro) throws CrudException {
		request.setAttribute("escondeMenu",request.getParameter("escondeMenu"));
		request.getSession().setAttribute("cadAnimal",request.getParameter("cadAnimal"));
		return super.doListagem(request, filtro);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Animal form)
			throws CrudException {
		// TODO Auto-generated method stub
		super.doSalvar(request, form);
		return super.doConsultar(request, form);
	}
}
