package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;

import seguranca.autenticacao.filter.EntradaSaidaCaixaFiltro;
import seguranca.autorizacao.bean.EntradaSaidaCaixa;
import br.com.biharckgroup.util.CrudController;

@Controller(path="/autorizacao/crud/EntradaSaidaCaixa",authorizationModule=CrudAuthorizationModule.class)
public class EntradaSaidaCaixaCrud extends CrudController<EntradaSaidaCaixaFiltro, EntradaSaidaCaixa, EntradaSaidaCaixa>{

	
	@Override
	protected void listagem(WebRequestContext request,EntradaSaidaCaixaFiltro filtro) throws Exception {
		request.setAttribute("escondeMenu", true);
		super.listagem(request, filtro);
	}
}
