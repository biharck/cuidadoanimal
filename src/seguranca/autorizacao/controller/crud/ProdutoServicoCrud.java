package seguranca.autorizacao.controller.crud;

import java.util.List;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.persistence.ListagemResult;
import org.springframework.dao.DataIntegrityViolationException;

import seguranca.autenticacao.filter.ProdutoServicoFiltro;
import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.ProdutoServico;
import seguranca.autorizacao.service.ItemProdutoService;
import seguranca.autorizacao.service.ProdutoServicoService;
import seguranca.autorizacao.service.TipoProdutoService;
import seguranca.autorizacao.service.ValorProdutoServicoService;
import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalException;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path="/autorizacao/estoque/crud/ProdutoServico",authorizationModule=CrudAuthorizationModule.class)
public class ProdutoServicoCrud extends CrudController<ProdutoServicoFiltro, ProdutoServico, ProdutoServico>{
	
	private TipoProdutoService tipoProdutoService;
	private ValorProdutoServicoService valorProdutoServicoService;
	private ItemProdutoService itemProdutoService;
	private ProdutoServicoService  produtoServicoService;
	
	public void setTipoProdutoService(TipoProdutoService tipoProdutoService) {
		this.tipoProdutoService = tipoProdutoService;
	}
	public void setValorProdutoServicoService(
			ValorProdutoServicoService valorProdutoServicoService) {
		this.valorProdutoServicoService = valorProdutoServicoService;
	}
	public void setItemProdutoService(ItemProdutoService itemProdutoService) {
		this.itemProdutoService = itemProdutoService;
	}
	public void setProdutoServicoService(
			ProdutoServicoService produtoServicoService) {
		this.produtoServicoService = produtoServicoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, ProdutoServico form)throws Exception {
		if(form!=null && form.getIdProdutoServico()!=null){
			form.setTipoProduto(tipoProdutoService.getTipoProdutoBySubTipo(form.getSubTipoProduto()));
			form.setValorProdutoServico(valorProdutoServicoService.getValorProdutoServicoByProduto(form));
			form.setListaItensProdutos(itemProdutoService.getListaItemProdutoByProduto(form));
			if(form.getListaItensProdutos()!=null)
				for (ItemProduto ip : form.getListaItensProdutos()) {
					ip.setQuantidadeRestante(ip.getQuantidadeEntrada()-ip.getQuantidadeSaida());
				}
			/**
			 * seta a quantidade de itens ainda dispon�vel...
			 */
			form.setStock(CuidadoAnimalUtil.returnProdutosRestante(form.getListaItensProdutos()));
			produtoServicoService.saveOrUpdate(form);
			
			request.setAttribute("editarRegistro", true);
		}else
			request.setAttribute("editarRegistro", false);
		
	}
	
	@Override
	protected void listagem(WebRequestContext request, ProdutoServicoFiltro filtro) throws Exception {
		request.setAttribute("exibirEstoque", request.getAttribute("exibirEstoque"));
	}
	
	@Override
	protected void salvar(WebRequestContext request, ProdutoServico bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "cod_prod_idx")) throw new CuidadoAnimalException("C�digo de produto j� cadastrado no sistema.");
		}
	}
	
	@Override
	protected ListagemResult<ProdutoServico> getLista(WebRequestContext request,ProdutoServicoFiltro filtro) {

		ListagemResult<ProdutoServico> listagemResult = super.getLista(request, filtro);
		List<ProdutoServico> lista = listagemResult.list();
		if (lista != null) {
			for (ProdutoServico ps:  lista) {
				ps.setQtdRestante(CuidadoAnimalUtil.returnProdutosRestante(ps.getListaItensProdutos()));
			}
		}
		return listagemResult;
	}

}
