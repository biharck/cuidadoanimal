package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import seguranca.autorizacao.bean.Uf;
import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalException;

@Controller(path="/autorizacao/cadastros/crud/Uf", authorizationModule=CrudAuthorizationModule.class)
public class UfCrud extends CrudController<org.nextframework.controller.crud.FiltroListagem, Uf, Uf>{
	
	@Override
	protected void salvar(WebRequestContext request, Uf bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "nome_uf_idx")) throw new CuidadoAnimalException("Uf j� cadastrada no sistema.");
		}
	}

}
