package seguranca.autorizacao.controller.crud;

import java.sql.Date;
import java.sql.Time;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.types.Money;
import org.springframework.dao.DataIntegrityViolationException;

import seguranca.autenticacao.filter.ContaPagarFiltro;
import seguranca.autorizacao.bean.Caixa;
import seguranca.autorizacao.bean.ContaPagar;
import seguranca.autorizacao.bean.ContaReceber;
import seguranca.autorizacao.bean.EntradaSaidaCaixa;
import seguranca.autorizacao.bean.TipoOperacao;
import seguranca.autorizacao.service.CaixaService;
import seguranca.autorizacao.service.EntradaSaidaCaixaService;
import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalException;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path="/autorizacao/clinica/crud/ContaPagar",authorizationModule = CrudAuthorizationModule.class)
public class ContaPagarCrud extends CrudController<ContaPagarFiltro, ContaPagar, ContaPagar>{
	
	private CaixaService caixaService;
	private EntradaSaidaCaixaService entradaSaidaCaixaService;
	
	public void setEntradaSaidaCaixaService(
			EntradaSaidaCaixaService entradaSaidaCaixaService) {
		this.entradaSaidaCaixaService = entradaSaidaCaixaService;
	}
	public void setCaixaService(CaixaService caixaService) {
		this.caixaService = caixaService;
	}
	
	
	@Override
	protected void salvar(WebRequestContext request, ContaPagar bean)	throws Exception {
		EntradaSaidaCaixa entradaSaidaCaixa = new EntradaSaidaCaixa();
		if(bean.getStatus()!=null && bean.getStatus()){
			entradaSaidaCaixa.setCaixa(caixaService.getCaixaDia());
			entradaSaidaCaixa.setData(new Date(System.currentTimeMillis()));
			entradaSaidaCaixa.setFormaPagamento(bean.getFormaPagamento());
			entradaSaidaCaixa.setHora(new Time(System.currentTimeMillis()));
			entradaSaidaCaixa.setPendente(false);
			TipoOperacao tipoOperacao = new TipoOperacao();
			tipoOperacao.setIdTipoOperacao(2);
			entradaSaidaCaixa.setTipoOperacao(tipoOperacao);
			entradaSaidaCaixa.setUsuario(CuidadoAnimalUtil.getPessoaLogada());
			entradaSaidaCaixa.setValor(bean.getValor());
			entradaSaidaCaixaService.saveOrUpdate(entradaSaidaCaixa);
		}

		try {
//			if(bean.getIdContaPagar()!=null)
//				bean.setValor(new Money(bean.getValor().toLong()/10000));
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_nun_contapagar")) throw new CuidadoAnimalException("N�mero de Documento j� existente no sistema.");
		}
		
	}
	
	@Override
	protected void listagem(WebRequestContext request, ContaPagarFiltro filtro)throws Exception {
		Caixa c = caixaService.getCaixaDia();
		if(c==null)
			request.setAttribute("abrirCaixa", true);
		else{
			request.setAttribute("abrirCaixa", false);
		}
		super.listagem(request, filtro);
	}

}
