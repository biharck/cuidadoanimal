package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalException;

import seguranca.autorizacao.bean.Cor;

@Controller(path="/autorizacao/cadastros/crud/Cor", authorizationModule=CrudAuthorizationModule.class)
public class CorCrud extends CrudController<org.nextframework.controller.crud.FiltroListagem, Cor, Cor>{
	
	@Override
	protected void salvar(WebRequestContext request, Cor bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_nome_cor")) throw new CuidadoAnimalException("Pelagem j� cadastrada no sistema.");
		}
	}

}
