package seguranca.autorizacao.controller.crud;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.types.Money;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autenticacao.filter.CaixaFiltro;
import seguranca.autorizacao.bean.Caixa;
import seguranca.autorizacao.bean.EntradaSaidaCaixa;
import seguranca.autorizacao.service.CaixaService;
import seguranca.autorizacao.service.EntradaSaidaCaixaService;
import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalUtil;

@Controller(path="/autorizacao/clinica/crud/Caixa",authorizationModule=CrudAuthorizationModule.class)
public class CaixaCrud extends CrudController<CaixaFiltro, Caixa, Caixa>{
	
	
	private EntradaSaidaCaixaService entradaSaidaCaixaService;
	private CaixaService caixaService;
	
	public void setEntradaSaidaCaixaService(
			EntradaSaidaCaixaService entradaSaidaCaixaService) {
		this.entradaSaidaCaixaService = entradaSaidaCaixaService;
	}
	public void setCaixaService(CaixaService caixaService) {
		this.caixaService = caixaService;
	}

	@Override
	protected void entrada(WebRequestContext request, Caixa form)throws Exception {
		if(caixaService.getCaixaDia()!=null){
			Money saldoAtual = new Money(0);
			Money saida = new Money(0);
			Money suprimento = new Money(0);
			Money sangria = new Money(0);
			Money saldoAtualMenosSaida = new Money(0);
		
			
			List<EntradaSaidaCaixa> listaEntrada = entradaSaidaCaixaService.getListaEntradaCaixaDiaCorrente();
			form.setListaEntradaSaidaCaixa(listaEntrada);
			for (EntradaSaidaCaixa entrada : listaEntrada) {
				saldoAtual = saldoAtual.add(entrada.getValor());
				if(entrada.getFormaPagamento().getIdFormaPagamento().equals(4))
					suprimento = suprimento.add(entrada.getValor());
				else if(entrada.getFormaPagamento().getIdFormaPagamento().equals(5))
					sangria = sangria.add(entrada.getValor());
			}
			List<EntradaSaidaCaixa> listaSaida = entradaSaidaCaixaService.getListaSaidaCaixaDiaCorrente();
			form.setListaEntradaSaidaCaixa2(listaSaida);
			for (EntradaSaidaCaixa entrada : listaSaida) {
				saida = saida.add(entrada.getValor());
			}
			saldoAtualMenosSaida = saldoAtualMenosSaida.add(saldoAtual);
			saldoAtualMenosSaida = saldoAtualMenosSaida.subtract(saida);
			form.setListaEstorno(entradaSaidaCaixaService.getListaEstornoCaixaDiaCorrente());
			request.setAttribute("saldoAtual", saldoAtualMenosSaida);
			request.setAttribute("entrada", saldoAtual);
			request.setAttribute("saida", saida);
			request.setAttribute("suprimento", suprimento);
			request.setAttribute("sangria", sangria);
		}
		Caixa c = caixaService.getCaixaDia();
		if(c==null)
			request.setAttribute("abrirCaixa", true);
		else{
			request.setAttribute("abrirCaixa", false);
		}
		super.entrada(request, form);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,CaixaFiltro filtro) throws CrudException {
		return super.doEntrada(request, new Caixa());
	}
	
	@Action("abreCaixa")
	public ModelAndView abreCaixa(WebRequestContext context) throws CrudException{
		Caixa c = caixaService.getCaixaDia();
		if(c==null){
			c = new Caixa();
			c.setDataAbertura(new Date(System.currentTimeMillis()));
			c.setHoraAbertura(new Time(System.currentTimeMillis()));
			c.setUsuario(CuidadoAnimalUtil.getPessoaLogada());
			//pegar informações do caixa do dia anterior...saldo etc...
			caixaService.saveOrUpdate(c);		
			return doEntrada(context, new Caixa());
		}else
			return doEntrada(context, c);
	}
	
	@Action("estornar")
	public ModelAndView estornar(WebRequestContext request, EntradaSaidaCaixa entradaSaidaCaixa) throws CrudException{
		
		Caixa c = caixaService.getCaixaDia();
		try {
			EntradaSaidaCaixa temp = new EntradaSaidaCaixa();
			entradaSaidaCaixa.setIdEntradaSaidaCaixa(Integer.parseInt(request.getParameter("idEntrada")));
			temp = entradaSaidaCaixaService.getEntradaSaidaCaixaById(entradaSaidaCaixa);
			temp.getTipoOperacao().setIdTipoOperacao(3);
			entradaSaidaCaixa = temp;
			entradaSaidaCaixa.setCaixa(caixaService.getCaixaDia());
			entradaSaidaCaixa.setData(new Date(System.currentTimeMillis()));
			entradaSaidaCaixa.setHora(new Time(System.currentTimeMillis()));
			entradaSaidaCaixa.setPendente(false);
			entradaSaidaCaixa.setUsuario(CuidadoAnimalUtil.getPessoaLogada());
			
			
			//realiza o save...
			entradaSaidaCaixaService.saveOrUpdate(entradaSaidaCaixa);
			request.setAttribute("mostraFim", true);
			request.addMessage("Estorno Realizado Com Sucesso!");
			
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Houve uma falha ao realizaro o estorno, entre em contato com o suporte para mais detalhes.");
		}
		return doEntrada(request, c);
	}

}
