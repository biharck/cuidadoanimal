package seguranca.autorizacao.controller.crud;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalException;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import seguranca.autorizacao.bean.Usuario;
import seguranca.autorizacao.service.PapelService;





@Controller(path="/autorizacao/seguranca/crud/usuario",authorizationModule=CrudAuthorizationModule.class)
public class UsuarioCrud extends CrudController<FiltroListagem,Usuario,Usuario> {
	
	private PapelService papelService;
	
	public void setPapelService(PapelService papelService) {
		this.papelService = papelService;
	}

	@Override
	protected void entrada(WebRequestContext request, Usuario form) throws Exception {
		request.setAttribute("listaPapel", papelService.findAll());
		super.entrada(request, form);
	}
	
	
	@Override
	protected void salvar(WebRequestContext request, Usuario bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_email_usuario")) throw new CuidadoAnimalException("E-Mail j� cadastrado no sistema.");
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_login")) throw new CuidadoAnimalException("Login j� cadastrado no sistema.");
		}
	}
	
}
