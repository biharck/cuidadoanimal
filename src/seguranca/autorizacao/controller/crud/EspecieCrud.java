package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalException;

import seguranca.autorizacao.bean.Especie;

@Controller(path="/autorizacao/cadastros/crud/Especie", authorizationModule=CrudAuthorizationModule.class)
public class EspecieCrud extends CrudController<org.nextframework.controller.crud.FiltroListagem, Especie, Especie>{
	
	@Override
	protected void salvar(WebRequestContext request, Especie bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_nome_especie")) throw new CuidadoAnimalException("Esp�cie j� cadastrada no sistema.");
		}
	}

}
