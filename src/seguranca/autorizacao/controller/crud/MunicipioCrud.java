package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import seguranca.autorizacao.bean.Municipio;
import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalException;

@Controller(path="/autorizacao/cadastros/crud/Municipio", authorizationModule=CrudAuthorizationModule.class)
public class MunicipioCrud extends CrudController<FiltroListagem, Municipio, Municipio> {
	@Override
	protected void salvar(WebRequestContext request, Municipio bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_nome_municipio")) throw new CuidadoAnimalException("Munic�pio j� cadastrada no sistema.");
		}
	}
	
}