package seguranca.autorizacao.controller.crud;

import java.util.List;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.persistence.ListagemResult;

import seguranca.autenticacao.filter.ItemProdutoFiltro;
import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.Venda;
import br.com.biharckgroup.util.CrudController;

@Controller(path="/autorizacao/crud/ItemProduto",authorizationModule=CrudAuthorizationModule.class)
public class ItemProdutoCrud extends CrudController<ItemProdutoFiltro, ItemProduto, ItemProduto>{

	
	@Override
	protected void listagem(WebRequestContext request, ItemProdutoFiltro filtro)
			throws Exception {
		request.setAttribute("escondeMenu", request.getParameter("escondeMenu"));
		
		super.listagem(request, filtro);
	}
	
	@Override
	protected ListagemResult<ItemProduto> getLista(WebRequestContext request,ItemProdutoFiltro filtro) {
		ListagemResult<ItemProduto> lista = super.getLista(request, filtro);
		List<ItemProduto> listaIteracao = lista.list();
		if(listaIteracao!=null)
		for (ItemProduto itemProduto : listaIteracao) {
			itemProduto.setQuantidadeRestante(itemProduto.getQuantidadeEntrada()-itemProduto.getQuantidadeSaida());
		}
		return lista;
	}
}
