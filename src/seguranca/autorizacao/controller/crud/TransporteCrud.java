package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import seguranca.autorizacao.bean.Transporte;
import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalException;


@Controller(path="/autorizacao/cadastros/crud/Transporte",authorizationModule=CrudAuthorizationModule.class)
public class TransporteCrud extends CrudController<FiltroListagem,Transporte, Transporte>{
	
	@Override
	protected void salvar(WebRequestContext request, Transporte bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_descricao_transporte")) throw new CuidadoAnimalException("Transporte j� cadastrado no sistema.");
		}
	}

}
