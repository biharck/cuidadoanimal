package seguranca.autorizacao.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import seguranca.autorizacao.bean.ServicosEstetica;
import br.com.biharckgroup.util.CrudController;
import br.com.biharckgroup.util.CuidadoAnimalException;


@Controller(path="/autorizacao/crud/ServicosEstetica",authorizationModule=CrudAuthorizationModule.class)
public class ServicosEsteticaCrud extends CrudController<FiltroListagem,ServicosEstetica, ServicosEstetica>{
	
	@Override
	protected void salvar(WebRequestContext request, ServicosEstetica bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (br.com.biharckgroup.util.DatabaseError.isKeyPresent(e, "idx_descricao_servicoestetica")) throw new CuidadoAnimalException("Servi�os de Est�tica j� cadastrado no sistema.");
		}
	}

}
