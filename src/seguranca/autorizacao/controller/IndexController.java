package seguranca.autorizacao.controller;

import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.NextWeb;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autorizacao.bean.Calendar;
import seguranca.autorizacao.service.CalendarService;
import seguranca.autorizacao.service.ItemProdutoService;
import seguranca.autorizacao.service.ProdutoServicoService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;

@Controller(path="/autorizacao",authorizationModule=ProcessAuthorizationModule.class)
public class IndexController extends MultiActionController {

	private CalendarService calendarService;
	private ProdutoServicoService produtoServicoService;
	public void setCalendarService(CalendarService calendarService) {
		this.calendarService = calendarService;
	}
	public void setProdutoServicoService(ProdutoServicoService produtoServicoService) {
		this.produtoServicoService = produtoServicoService;
	}
	
	@DefaultAction
	public ModelAndView action(WebRequestContext request){
		
		
		WebRequestContext requestContext = NextWeb.getRequestContext();
		System.out.println("-->"+ requestContext.getFirstRequestUrl());
		
		List<Calendar> listaCalendar = calendarService.findAll();
		StringBuilder sb = new StringBuilder();
		sb.append("events : [ ");
		for (Calendar calendar : listaCalendar) {
			sb.append(CuidadoAnimalUtil.returnCalendarJQuery(calendar));
		}
		if(sb!=null && sb.length()>0){
			sb = new StringBuilder(sb.substring(0, sb.length()-1));
			sb.append(" ]");
		}
		request.getSession().setAttribute("calendarComp", sb.toString());
		
		if(produtoServicoService.getListaProdutosAbaixoEstoqueMinimo()!=null)
			request.setAttribute("abaixoEstoqueMinimo", true);
		return new ModelAndView("index");
	}
}
