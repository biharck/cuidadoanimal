package seguranca.autorizacao.controller.process;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.NextWeb;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.types.Money;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autorizacao.bean.Caixa;
import seguranca.autorizacao.bean.EntradaSaidaCaixa;
import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.Pagamento;
import seguranca.autorizacao.bean.ProdutoServico;
import seguranca.autorizacao.bean.ServicosPrestados;
import seguranca.autorizacao.bean.TipoOperacao;
import seguranca.autorizacao.bean.Usuario;
import seguranca.autorizacao.bean.Venda;
import seguranca.autorizacao.bean.VendaFormaPagamento;
import seguranca.autorizacao.service.CaixaService;
import seguranca.autorizacao.service.EntradaSaidaCaixaService;
import seguranca.autorizacao.service.FormaPagamentoService;
import seguranca.autorizacao.service.PagamentoService;
import seguranca.autorizacao.service.ProdutoServicoService;
import seguranca.autorizacao.service.VendaService;
import br.com.biharckgroup.util.CuidadoAnimalException;
import br.com.biharckgroup.util.CuidadoAnimalUtil;

@Controller(path="/autorizacao/crud/Pagamento", authorizationModule=ProcessAuthorizationModule.class)
public class PagamentoProcess extends MultiActionController{
	
	private VendaService vendaService;
	private ProdutoServicoService produtoServicoService;
	private PagamentoService pagamentoService;
	private CaixaService caixaService;
	private EntradaSaidaCaixaService entradaSaidaCaixaService;
	private FormaPagamentoService formaPagamentoService;

	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setProdutoServicoService(ProdutoServicoService produtoServicoService) {
		this.produtoServicoService = produtoServicoService;
	}
	public void setPagamentoService(PagamentoService pagamentoService) {
		this.pagamentoService = pagamentoService;
	}
	public void setCaixaService(CaixaService caixaService) {
		this.caixaService = caixaService;
	}
	public void setEntradaSaidaCaixaService(
			EntradaSaidaCaixaService entradaSaidaCaixaService) {
		this.entradaSaidaCaixaService = entradaSaidaCaixaService;
	}
	public void setFormaPagamentoService(
			FormaPagamentoService formaPagamentoService) {
		this.formaPagamentoService = formaPagamentoService;
	}
	
	@DefaultAction
	public ModelAndView acao(WebRequestContext request){
		request.setAttribute("escondeMenu", true);
		Integer idVenda = Integer.parseInt(request.getParameter("venda"));
		Venda v = vendaService.getVendaById(idVenda);
		request.getSession().setAttribute("cliente", v.getCliente().getNome());
		request.setAttribute("servicosPrestados",v.getServicosPrestados());
		request.setAttribute("venda", v);
		Pagamento pagamento = new Pagamento();
		pagamento.setVenda(v);
		Long valor = new Long(0);
//		for (ServicosPrestados sp : v.getServicosPrestados()) {
//			long valorPrevio = sp.getItemProduto().getProdutoServico().getListaValorProdutoServico().get(0).getValor().toLong();
//			long margemLucro = sp.getItemProduto().getProdutoServico().getListaValorProdutoServico().get(0).getMargemLucro().longValue() * 10;
//			valor += (valorPrevio / margemLucro)* (sp.getQtd()==null || sp.getQtd()==0?1:sp.getQtd());
//		}
		Long valorTotal = new Long(0);
		long desconto = 0l;
		for (ServicosPrestados sp : v.getServicosPrestados()) {
			long valorPrevio = sp.getItemProduto().getProdutoServico().getListaValorProdutoServico().get(0).getValor().toLong();
			long margemLucro = sp.getItemProduto().getProdutoServico().getListaValorProdutoServico().get(0).getMargemLucro().longValue();
			desconto += sp.getDesconto()==null?0l:sp.getDesconto().toLong();
			valorPrevio = valorPrevio + (valorPrevio * margemLucro)/100;
			valor += valorPrevio* (sp.getQtd()==null || sp.getQtd()==0?1:sp.getQtd());
		}
		valorTotal += valor;
		request.getSession().setAttribute("valorVenda", new Money(valor-(desconto),true));
		List<VendaFormaPagamento> lista = new ArrayList<VendaFormaPagamento>();
		lista.add(new VendaFormaPagamento());
		pagamento.setListaVendaFormaPagamento(lista);
		request.setAttribute("listaFormasPagamento", formaPagamentoService.listaFormasReaisPagamento());
		return new ModelAndView("crud/pagamento", "pagamento",pagamento);
	}
	
	@Action("salvar")
	public ModelAndView salvar(WebRequestContext request, Pagamento pagamento){
		Long valorTotal = 0l;
		Money m = (Money) request.getSession().getAttribute("valorVenda");
		for (VendaFormaPagamento vfp: pagamento.getListaVendaFormaPagamento()) {
			valorTotal += vfp.getValorForma().toLong();
		}
		if(!valorTotal.equals(m.toLong()))
			throw new CuidadoAnimalException("O valor do pagamento deve ser o mesmo do valor da venda.");
		
		pagamento.setDtPagamento(new Date(System.currentTimeMillis()));
		pagamento.setHoraPagamento(new Time(System.currentTimeMillis()));
		pagamento.setUsuario((Usuario)NextWeb.getUser());
		
		pagamentoService.saveOrUpdate(pagamento);
		pagamento.setVenda(vendaService.getVendaAndServicosPrestadosById(pagamento.getVenda().getIdVenda()));
		pagamento.getVenda().setFechado(true);
		pagamento.getVenda().setUsuario(CuidadoAnimalUtil.getPessoaLogada());
		vendaService.saveOrUpdate(pagamento.getVenda());
		request.setAttribute("mostraFim", true);
		request.setAttribute("escondeMenu", true);
		
//		org.nextframework.view.ajax.View.getCurrent().println("<script>window.opener.reloadPage();</script>");
//		org.nextframework.view.ajax.View.getCurrent().println("<script>window.close();</script>");
//		org.nextframework.view.ajax.View.getCurrent().println("<script>window.opener.focus();</script>");
		
		Venda ve = vendaService.getVendaAndServicosPrestadosById(pagamento.getVenda().getIdVenda());
		TipoOperacao tipoOperacao = new TipoOperacao();
		tipoOperacao.setIdTipoOperacao(1);
		
		for (ServicosPrestados sp : ve.getServicosPrestados()) {
			//salva dados de entrada de caixa....
			EntradaSaidaCaixa entradaSaidaCaixa = new EntradaSaidaCaixa();
			entradaSaidaCaixa.setCaixa(caixaService.getCaixaDia());
			entradaSaidaCaixa.setCliente(pagamento.getVenda().getCliente());
			entradaSaidaCaixa.setData(new Date(System.currentTimeMillis()));
			entradaSaidaCaixa.setHora(new Time(System.currentTimeMillis()));
			entradaSaidaCaixa.setPendente(false);
			entradaSaidaCaixa.setProdutoServico(sp.getItemProduto().getProdutoServico());
			entradaSaidaCaixa.setTipoOperacao(tipoOperacao);
			for (VendaFormaPagamento vfp : pagamento.getListaVendaFormaPagamento()) {
				entradaSaidaCaixa.setFormaPagamento(vfp.getFormaPagamento());
				entradaSaidaCaixa.setIdEntradaSaidaCaixa(null);
				entradaSaidaCaixa.setUsuario(CuidadoAnimalUtil.getPessoaLogada());
				entradaSaidaCaixa.setValor(vfp.getValorForma());
				entradaSaidaCaixaService.saveOrUpdate(entradaSaidaCaixa);
			}
		}
		return new ModelAndView("crud/pagamento", "pagamento",pagamento);
	}

	@Action("getProduto")
	public void getProduto(WebRequestContext context,Pagamento pagamento) throws IOException{
		ItemProduto itemProduto = new ItemProduto();
		String parametro = context.getParameter("id");
		String values[] = parametro.split(",");
		itemProduto.setIdItemProduto(Integer.parseInt(values[0]));
		ProdutoServico produtoServico = produtoServicoService.getProdutoByItem(itemProduto);
		context.getServletResponse().getWriter().println( "var msg = '"+produtoServico.getNome()+"';var idx = "+ context.getParameter("idx")+";");
		Integer qtd = 0;
		for (ItemProduto ip : produtoServico.getListaItensProdutos()) {
				qtd += ip.getQuantidadeEntrada() - ip.getQuantidadeSaida();
		}
		context.getServletResponse().getWriter().println( "var resto = "+qtd+";");
	}
	
	@Action("verificaCaixa")
	public void verificaCaixa(WebRequestContext context){
		Caixa c = caixaService.getCaixaDia();
		if(c==null)
			org.nextframework.view.ajax.View.getCurrent().println("var retorno = true;var abreCaixa = false;var noError = false;");
		else{
			org.nextframework.view.ajax.View.getCurrent().println("var retorno = false;var abreCaixa = false;var noError = false;var abertura = '"+c.getDataAbertura()+ " �s: "+c.getHoraAbertura()+"';");
		}
	}
	
	@Action("abreCaixa")
	public void abreCaixa(WebRequestContext context){
		Caixa c = new Caixa();
		c.setDataAbertura(new Date(System.currentTimeMillis()));
		c.setHoraAbertura(new Time(System.currentTimeMillis()));
		c.setUsuario(CuidadoAnimalUtil.getPessoaLogada());
		//pegar informa��es do caixa do dia anterior...saldo etc...
		caixaService.saveOrUpdate(c);
		org.nextframework.view.ajax.View.getCurrent().println("var retorno = false; var abreCaixa = true;var noError = true;var abertura = '"+c.getDataAbertura()+ " �s: "+c.getHoraAbertura()+"';");
	}
	
}

