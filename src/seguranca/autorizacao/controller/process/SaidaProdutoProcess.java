package seguranca.autorizacao.controller.process;

import java.io.IOException;
import java.sql.Date;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.Movimentacao;
import seguranca.autorizacao.bean.ProdutoServico;
import seguranca.autorizacao.service.ItemProdutoService;
import seguranca.autorizacao.service.MovimentacaoService;
import seguranca.autorizacao.service.ProdutoServicoService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;

@Controller(path="/autorizacao/crud/SaidaEstoque", authorizationModule=ProcessAuthorizationModule.class)
public class SaidaProdutoProcess extends MultiActionController{
	
	private ProdutoServicoService produtoServicoService;
	private MovimentacaoService movimentacaoService;
	private ItemProdutoService itemProdutoService;
	
	public void setProdutoServicoService(ProdutoServicoService produtoServicoService) {
		this.produtoServicoService = produtoServicoService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setItemProdutoService(ItemProdutoService itemProdutoService) {
		this.itemProdutoService = itemProdutoService;
	}
	
	@DefaultAction
	public ModelAndView acao(WebRequestContext request){
		Integer id = Integer.parseInt((request.getParameter("codProduto")));
		ProdutoServico produtoServico = new ProdutoServico();
		produtoServico.setIdProdutoServico(id);
		produtoServico = produtoServicoService.loadForEntrada(produtoServico);
		request.setAttribute("listaLote",itemProdutoService.getItemByLoteAndProdutoList(produtoServico.getIdProdutoServico()));
		request.setAttribute("escondeMenu", true);
		return new ModelAndView("crud/saidaEstoque", "produtoServico", produtoServico);
	}
	
	@Action("salvar")
	public void salvar(WebRequestContext request) throws IOException{
		try {
			Integer itemProd = CuidadoAnimalUtil.returnIdWithParameters(request.getParameter("itemprod"));
			ProdutoServico produtoServico = new ProdutoServico();
			produtoServico.setIdProdutoServico(Integer.parseInt(request.getParameter("idProdutoServico")));
			produtoServico.setQtdSaida(Integer.parseInt(request.getParameter("qtdSaida")));
//			ProdutoServico produtoServicoTem = produtoServicoService.loadForEntrada(produtoServico);
			ItemProduto it = new ItemProduto();
			it.setIdItemProduto(itemProd);
			produtoServico.setItemProduto(itemProdutoService.loadForEntrada(it));
			produtoServico.getItemProduto().setQuantidadeSaida(produtoServico.getQtdSaida());
			produtoServico.getItemProduto().setMotivo(request.getParameter("motivo"));
			Integer atual = produtoServico.getItemProduto().getQuantidadeEntrada() - produtoServico.getItemProduto().getQuantidadeSaida();
			if(produtoServico.getQtdSaida()>atual){
				request.getServletResponse().getWriter().println("var saveOk = false;var resp = 'O estoque atual e inferior ao solicitado para retirada';");				
			}
			else{
				
				Movimentacao movimentacao = new Movimentacao();
				movimentacao.setDtMovimentacao(new Date(System.currentTimeMillis()));
				movimentacao.setTipo(2);
				movimentacao.setProdutoServico(produtoServico);
				movimentacaoService.saveOrUpdate(movimentacao);
				
				itemProdutoService.saveOrUpdate(produtoServico.getItemProduto());
				
				request.getServletResponse().getWriter().println("var saveOk = true;");
			}
			request.setAttribute("escondeMenu", true);
			request.setAttribute("mostraFim", true);
		}catch (Exception e) {
			request.getServletResponse().getWriter().println("var saveOk = false;");
			e.printStackTrace();
		}
	}


	@Action("getQuantidade")
	public void getQuantidade(WebRequestContext context)throws IOException{
		Integer id = CuidadoAnimalUtil.returnIdWithParameters((String)context.getParameter("id"));
		Integer entrada =0, saida = 0;
		ItemProduto it = itemProdutoService.getItemByLoteAndProduto(id);
		entrada = it.getQuantidadeEntrada();
		saida   = it.getQuantidadeSaida();
		context.getServletResponse().getWriter().println( entrada - saida );
	}
	
}

