package seguranca.autorizacao.controller.process;

import java.util.Date;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.NextWeb;
import org.nextframework.core.web.WebRequestContext;

import seguranca.autorizacao.bean.Calendar;
import seguranca.autorizacao.bean.Usuario;
import seguranca.autorizacao.dao.UsuarioDAO;
import seguranca.autorizacao.service.CalendarService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;

@Controller(path="/autorizacao/crud/Agenda", authorizationModule=ProcessAuthorizationModule.class)
public class AgendaProcess extends MultiActionController{
	
	private CalendarService calendarService;
	private UsuarioDAO usuarioDAO;
	
	public void setCalendarService(CalendarService calendarService) {
		this.calendarService = calendarService;
	}
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	
	@DefaultAction
	public void defaultAction(WebRequestContext request){
		
	}
	
	@Action("saveOrUpdate")
	public void saveOrUpdateTarefa(WebRequestContext request,Calendar c){
		if(request.getParameter("id")!=null){
			c.setIdCalendar(Integer.parseInt(request.getParameter("id")));
		}
		
//		Fri Feb 19 2010 12:00:00 GMT-0200 (BRT)
//		Fri Feb 19 2010 13:00:00 GMT-0200 (BRT)
		c.setDayStart(Integer.parseInt(request.getParameter("start").substring(8,10)));
		c.setYearStart(Integer.parseInt(request.getParameter("start").substring(11,15)));
		c.setMonthStart(request.getParameter("start").substring(4,7));
		c.setHourStart(Integer.parseInt(request.getParameter("start").substring(16,18)));
		c.setMinStart(Integer.parseInt(request.getParameter("start").substring(19,21)));
		
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		java.util.Calendar atual = java.util.Calendar.getInstance(); 
		calendar.set(c.getYearStart(), CuidadoAnimalUtil.getMonth(c), c.getDayStart());
		
		atual.setTime(new Date(System.currentTimeMillis()));
		if(request.getParameter("title")==null || request.getParameter("title").equals("")){
			org.nextframework.view.ajax.View.getCurrent().println("document.getElementById(\"dialog_agenda\").innerHTML = \"<h3>O Compromisso deve ter um t�tulo!</h3>\"");
			org.nextframework.view.ajax.View.getCurrent().println("$('#dialog_agenda').dialog('open');");			
		}else if(calendar.before(atual)){
			org.nextframework.view.ajax.View.getCurrent().println("document.getElementById(\"dialog_agenda\").innerHTML = \"<h3>Este compromisso n�o pode ser inserido, pois a data � anterior a data atual!</h3>\"");
			org.nextframework.view.ajax.View.getCurrent().println("$('#dialog_agenda').dialog('open');");
		}else{
		
			
			c.setDayEnd(Integer.parseInt(request.getParameter("end").substring(8,10)));
			c.setYearEnd(Integer.parseInt(request.getParameter("end").substring(11,15)));
			c.setMonthEnd(request.getParameter("end").substring(4,7));
			c.setHourEnd(Integer.parseInt(request.getParameter("end").substring(16,18)));
			c.setMinEnd(Integer.parseInt(request.getParameter("end").substring(19,21)));
			
			c.setTitulo(request.getParameter("title"));
			c.setTarefa(request.getParameter("body"));
			c.setResponsavel(usuarioDAO.findUserByLogin(NextWeb.getUser().getLogin()));
			c.setParticular(Boolean.valueOf(request.getParameter("particular")));
			calendarService.saveOrUpdate(c);
			org.nextframework.view.ajax.View.getCurrent().println("document.getElementById(\"dialog_agenda\").innerHTML = \"<h3>Compromisso Inserido Com Sucesso!</h3>\"");
			org.nextframework.view.ajax.View.getCurrent().println("$('#dialog_agenda').dialog('open');");
		}
	}
	
	@Action("delete")
	public void deleteTarefa(WebRequestContext request,Calendar c){
		c.setIdCalendar(Integer.parseInt(request.getParameter("id")));
		c = calendarService.loadForEntrada(c);
		java.util.Calendar calendar = java.util.Calendar.getInstance();
		java.util.Calendar atual = java.util.Calendar.getInstance(); 
		calendar.set(c.getYearStart(), CuidadoAnimalUtil.getMonth(c), c.getDayStart());
		
		atual.setTime(new Date(System.currentTimeMillis()));
		
		if(calendar.before(atual)){
			org.nextframework.view.ajax.View.getCurrent().println("document.getElementById(\"dialog_agenda\").innerHTML = \"<h3>Este compromisso n�o pode ser exclu�do, pois j� passou da data de execu��o!</h3>\"");
			org.nextframework.view.ajax.View.getCurrent().println("$('#dialog_agenda').dialog('open');");
		}else{
			calendarService.delete(c);
			org.nextframework.view.ajax.View.getCurrent().println("document.getElementById(\"dialog_agenda\").innerHTML = \"<h3>Compromisso exclu�do Com Sucesso!</h3>\"");
			org.nextframework.view.ajax.View.getCurrent().println("$('#dialog_agenda').dialog('open');");
		}
	}

}
