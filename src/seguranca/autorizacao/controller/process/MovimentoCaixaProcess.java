package seguranca.autorizacao.controller.process;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.types.Money;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autorizacao.bean.Caixa;
import seguranca.autorizacao.bean.EntradaSaidaCaixa;
import seguranca.autorizacao.bean.FormaPagamento;
import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.ServicosPrestados;
import seguranca.autorizacao.bean.TipoOperacao;
import seguranca.autorizacao.bean.Venda;
import seguranca.autorizacao.service.CaixaService;
import seguranca.autorizacao.service.EntradaSaidaCaixaService;
import seguranca.autorizacao.service.ItemProdutoService;
import seguranca.autorizacao.service.ProdutoServicoService;
import seguranca.autorizacao.service.VendaService;
import br.com.biharckgroup.util.CuidadoAnimalException;
import br.com.biharckgroup.util.CuidadoAnimalUtil;

@Controller(path="/autorizacao/crud/MovimentoCaixa", authorizationModule=ProcessAuthorizationModule.class)
public class MovimentoCaixaProcess extends MultiActionController{
	
	private EntradaSaidaCaixaService entradaSaidaCaixaService;
	private CaixaService  caixaService;
	private VendaService vendaService;
	private ItemProdutoService itemProdutoService;
	private ProdutoServicoService produtoServicoService;
	
	public void setEntradaSaidaCaixaService(
			EntradaSaidaCaixaService entradaSaidaCaixaService) {
		this.entradaSaidaCaixaService = entradaSaidaCaixaService;
	}
	public void setCaixaService(CaixaService caixaService) {
		this.caixaService = caixaService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setItemProdutoService(ItemProdutoService itemProdutoService) {
		this.itemProdutoService = itemProdutoService;
	}
	public void setProdutoServicoService(
			ProdutoServicoService produtoServicoService) {
		this.produtoServicoService = produtoServicoService;
	}
	
	@DefaultAction
	public ModelAndView acao(WebRequestContext request){
		request.setAttribute("escondeMenu", true);
		Integer type = Integer.parseInt(request.getParameter("type"));
		TipoOperacao tipoOperacao = new TipoOperacao();
		EntradaSaidaCaixa entradaSaidaCaixa = new EntradaSaidaCaixa();
		FormaPagamento formaPagamento = new FormaPagamento();
		switch (type) {
		case 1://para inserir suprimento de caixa
			tipoOperacao.setIdTipoOperacao(1);
			tipoOperacao.setDescricao("Suprimento");
			entradaSaidaCaixa.setTipoOperacao(tipoOperacao);
			formaPagamento.setIdFormaPagamento(4);
			entradaSaidaCaixa.setFormaPagamento(formaPagamento);
			return new ModelAndView("crud/movimentoCaixaSup", "entradaSaidaCaixa", entradaSaidaCaixa);

		case 2://para retirar sangria de um caixa
			tipoOperacao.setIdTipoOperacao(5);
			tipoOperacao.setDescricao("Sangria");
			entradaSaidaCaixa.setTipoOperacao(tipoOperacao);
			formaPagamento.setIdFormaPagamento(5);
			entradaSaidaCaixa.setFormaPagamento(formaPagamento);
			return new ModelAndView("crud/movimentoCaixaSangria", "entradaSaidaCaixa", entradaSaidaCaixa);

		case 3://para realizacao de um estorno
			tipoOperacao.setIdTipoOperacao(3);
			tipoOperacao.setDescricao("Estorno");
			entradaSaidaCaixa.setTipoOperacao(tipoOperacao);
			formaPagamento.setIdFormaPagamento(6);
			entradaSaidaCaixa.setFormaPagamento(formaPagamento);
			request.setAttribute("listaPossivelEstorno", entradaSaidaCaixaService.getListaEstornoCaixaDiaCorrenteExcetoEstorno());
			return new ModelAndView("crud/movimentoCaixaEstorno", "entradaSaidaCaixa", entradaSaidaCaixa);

			/**
			 * antes de fechar deve mudar o status das contas em aberto...
			 */
		case 4://para fechamento de caixa
			Caixa c = caixaService.getCaixaDia();
			if(c!=null){
				Money saldoAtual = new Money(0);
				Money saida = new Money(0);
				Money suprimento = new Money(0);
				Money sangria = new Money(0);
				
				Money tempValue = new Money(0);
			
				
				List<EntradaSaidaCaixa> listaEntrada = entradaSaidaCaixaService.getListaEntradaCaixaDiaCorrente();
				for (EntradaSaidaCaixa entrada : listaEntrada) {
						saldoAtual = saldoAtual.add(entrada.getValor());
					if(entrada.getFormaPagamento().getIdFormaPagamento().equals(4))
						suprimento = suprimento.add(entrada.getValor());
					else if(entrada.getFormaPagamento().getIdFormaPagamento().equals(5))
						sangria = sangria.add(entrada.getValor());
				}
				List<EntradaSaidaCaixa> listaSaida = entradaSaidaCaixaService.getListaSaidaCaixaDiaCorrente();
				for (EntradaSaidaCaixa entrada : listaSaida) {
					saida = saida.add(entrada.getValor());
				}
				tempValue = saldoAtual.subtract(saida);
				c.setSaldoParcial(tempValue);
			}else
				throw new CuidadoAnimalException("N�o se pode fechar um caixa sem movimenta��o.");
				
			return new ModelAndView("crud/movimentoCaixaFechamento", "caixa",c);

		case 5://para impressao do relatorio
			
			break;
		default:
			break;
		}
		return null;
	}
	
	@Action("salvar")
	public ModelAndView salvar(WebRequestContext request, EntradaSaidaCaixa entradaSaidaCaixa){
		
		entradaSaidaCaixa.setCaixa(caixaService.getCaixaDia());
		entradaSaidaCaixa.setData(new Date(System.currentTimeMillis()));
		entradaSaidaCaixa.setHora(new Time(System.currentTimeMillis()));
		entradaSaidaCaixa.setPendente(false);
		entradaSaidaCaixa.setUsuario(CuidadoAnimalUtil.getPessoaLogada());
				
		//realiza o save...
		entradaSaidaCaixaService.saveOrUpdate(entradaSaidaCaixa);
		request.setAttribute("mostraFim", true);
		request.setAttribute("escondeMenu", true);
		
		if(entradaSaidaCaixa.getTipoOperacao().getIdTipoOperacao().equals(5))
			return new ModelAndView("crud/movimentoCaixaSangria", "entradaSaidaCaixa", entradaSaidaCaixa);
		else if(entradaSaidaCaixa.getTipoOperacao().getIdTipoOperacao().equals(1))
			return new ModelAndView("crud/movimentoCaixaSup", "entradaSaidaCaixa", entradaSaidaCaixa);
		
		return null;
//		org.nextframework.view.ajax.View.getCurrent().println("<script>window.opener.reloadPage();</script>");
//		org.nextframework.view.ajax.View.getCurrent().println("<script>window.close();</script>");
//		org.nextframework.view.ajax.View.getCurrent().println("<script>window.opener.focus();</script>");
		
	}
	
	
	@Action("fechaCaixa")
	public ModelAndView fechaCaixa(WebRequestContext context,Caixa caixa){
		
		Caixa c = caixaService.getCaixaDia();
//		List<Venda> listaVendas = vendaService.getVendaByCaixa(c);
//		for (Venda venda : listaVendas) {
//			for (ServicosPrestados sp : venda.getServicosPrestados()) {
//				//baixando o restante dos itens
//				ItemProduto ip = itemProdutoService.getItemProdutoAndProdutoSerico(sp.getItemProduto());
//				ip.setQuantidadeRestante(ip.getQuantidadeRestante()+sp.getQtd());
//				itemProdutoService.saveOrUpdate(ip);
//				Integer estoqueAtual = ip.getProdutoServico().getStock();
//				Integer utilizado = sp.getQtd();
//				ip.getProdutoServico().setStock(estoqueAtual+utilizado);
//				produtoServicoService.saveOrUpdate(ip.getProdutoServico());
//			}
//			venda.setFechado(false);
//			vendaService.saveOrUpdate(venda);
//		}
		
		c.setDataFechamento(new Date(System.currentTimeMillis()));
		c.setHoraFechamento(new Time(System.currentTimeMillis()));
		c.setValorFechamento(caixa.getValorFechamento());
		c.setDiferencaCaixa(caixa.getDiferencaCaixa());
		caixaService.saveOrUpdate(c);
		context.setAttribute("mostraFim", true);
		context.setAttribute("escondeMenu", true);
		return new ModelAndView("crud/movimentoCaixaFechamento");
//		org.nextframework.view.ajax.View.getCurrent().println("<script>window.opener.redirectPage();</script>");
//		org.nextframework.view.ajax.View.getCurrent().println("<script>window.close();</script>");
//		org.nextframework.view.ajax.View.getCurrent().println("<script>window.opener.focus();</script>");
	}

	@Action("ajaxVerificaCaixa")
	public void verificaSaldoAjax(WebRequestContext context){
		Float valor1 = Float.parseFloat(context.getParameter("valor1").replace(",","."));
		Float valor2 = Float.parseFloat(context.getParameter("valor2").replace(",","."));
		Float resul = valor2 - valor1;
		if(!valor1.equals(valor2))
			org.nextframework.view.ajax.View.getCurrent().println("var diferenca = true; var resultado ="+ resul);
		else
			org.nextframework.view.ajax.View.getCurrent().println("var diferenca = false; var resultado = 0");
	}
	
	
}

