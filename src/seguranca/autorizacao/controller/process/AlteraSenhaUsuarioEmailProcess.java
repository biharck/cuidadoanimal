package seguranca.autorizacao.controller.process;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.nextframework.bean.annotation.Bean;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autenticacao.filter.AlteraSenhaUsuarioEmailFiltro;
import seguranca.autorizacao.bean.Usuario;
import seguranca.autorizacao.service.UsuarioService;

@Controller(path = "/pub/alterasenhausuarioemail")
public class AlteraSenhaUsuarioEmailProcess extends MultiActionController {

	private UsuarioService usuarioService;

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	/**
	 * M�todo para verificar se o usuario logado tem papel de administrador,
	 * carrega uma listagem usuarios que ser� usada no jsp.
	 * 
	 * @param request
	 * @param filtro
	 * @return ModelAndView
	 */
	@DefaultAction
	public ModelAndView buscar(WebRequestContext request,AlteraSenhaUsuarioEmailFiltro filtro) throws Exception {
		String user = usuarioService.dsencrypta(String.valueOf(request.getParameter("pwk_tgrd")));
		String pass = usuarioService.dsencrypta(String.valueOf(request.getParameter("svgw")));
		Usuario u = usuarioService.findUserEmail(user);
		filtro.setUsuario(u);
		Calendar dataExpira = Calendar.getInstance();
		Date data = u.getDtrequisicao();
		dataExpira.setTime(data);
		dataExpira.add(Calendar.DAY_OF_MONTH, 1);
		Timestamp dataExpira2 = new Timestamp(dataExpira.getTimeInMillis());
		Timestamp dataAtual = new Timestamp(System.currentTimeMillis());
		if (dataAtual.after(dataExpira2)) {
			request.setAttribute("exibeMSGError", true);
			request.setAttribute("descricaoMSG", "Usuario n�o autorizado a alterar senha.");
		} else {
			if ((u == null || user.equals(""))) {
				request.setAttribute("exibeMSGError", true);
				request.setAttribute("descricaoMSG", "Usuario n�o autorizado a alterar senha.");
				return new ModelAndView("alterasenhausuarioemail", "filtro",filtro);
			}
		}
		return new ModelAndView("alterasenhausuarioemail", "filtro", filtro);
	}

	/**
	 * M�todo para verificar se a senha atual � valida, se a nova senha tem
	 * 6 posi��es e se a nova senha e a confirma senha s�o iguais. Se as
	 * condi��es forem verdadeiras a senha ser� alterada.
	 * 
	 * @param request
	 * @param filtro
	 * @throws Exception
	 * @return ModelAndView
	 */
	@Action("alterar")
	public ModelAndView salvar(WebRequestContext request,AlteraSenhaUsuarioEmailFiltro filtro) throws Exception {
		Usuario u = usuarioService.findUserByCodigo(filtro.getUsuario().getId());
		StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
		if (encryptor.checkPassword(filtro.getNewsenha(), u.getSenhaant())) {
			request.setAttribute("exibeMSGError", true);
			request.setAttribute("descricaoMSG", "Sua nova senha n�o pode ser igual a senha anterior.");
			return new ModelAndView("alterasenhausuarioemail", "filtro", filtro);
		}
		u.setSenha(encryptor.encryptPassword(filtro.getNewsenha()));
		u.setAtivado(true);
		u.setSenhaant(null);
		Timestamp time = new Timestamp(1970 - 01 - 01); // '1970-01-01 -> primeira data valida = null
		u.setDtrequisicao(time);
		u.setConfirmasenha(u.getSenha());
		usuarioService.saveOrUpdate(u);
		request.setAttribute("exibeMSG", true);
		return new ModelAndView("alterasenhausuarioemail", "filtro", filtro);
	}
}
