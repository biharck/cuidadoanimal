package seguranca.autorizacao.controller.process;

import java.sql.Date;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.Movimentacao;
import seguranca.autorizacao.bean.ProdutoServico;
import seguranca.autorizacao.service.ItemProdutoService;
import seguranca.autorizacao.service.MovimentacaoService;
import seguranca.autorizacao.service.ProdutoServicoService;

@Controller(path="/autorizacao/crud/EntradaEstoque", authorizationModule=ProcessAuthorizationModule.class)
public class EntradaProdutoProcess extends MultiActionController{
	
	private ProdutoServicoService produtoServicoService;
	private MovimentacaoService movimentacaoService;
	private ItemProdutoService itemProdutoService;
	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setProdutoServicoService(ProdutoServicoService produtoServicoService) {
		this.produtoServicoService = produtoServicoService;
	}
	public void setItemProdutoService(ItemProdutoService itemProdutoService) {
		this.itemProdutoService = itemProdutoService;
	}
	
	@DefaultAction
	public ModelAndView acao(WebRequestContext request){
		Integer id = Integer.parseInt((request.getParameter("codProduto")));
		ProdutoServico produtoServico = new ProdutoServico();
		produtoServico.setIdProdutoServico(id);
		produtoServico = produtoServicoService.loadForEntrada(produtoServico);
		request.setAttribute("escondeMenu", true);
		return new ModelAndView("crud/entradaEstoque", "produtoServico", produtoServico);
	}
	
	@Action("salvar")
	public ModelAndView salvar(WebRequestContext request, ProdutoServico produtoServico){
		//atualiza estoque de produto servico
		ProdutoServico produtoServicoTem = produtoServicoService.loadForEntrada(produtoServico);
		produtoServicoService.saveOrUpdate(produtoServicoTem);
		
		ItemProduto itemProduto = new ItemProduto();
		itemProduto = produtoServico.getItemProduto();
		itemProduto.setProdutoServico(produtoServicoTem);
		itemProduto.setDtEntrada(new Date(System.currentTimeMillis()));
		itemProduto.setQuantidadeSaida(0);
		itemProdutoService.saveOrUpdate(itemProduto);
		Movimentacao movimentacao = new Movimentacao();
		movimentacao.setDtMovimentacao(new Date(System.currentTimeMillis()));
		movimentacao.setTipo(1);
		movimentacao.setProdutoServico(produtoServico);
		movimentacaoService.saveOrUpdate(movimentacao);
		request.setAttribute("escondeMenu", true);
		request.setAttribute("mostraFim", true);
		return new ModelAndView("crud/entradaEstoque");
	}

	
	
}

