package seguranca.autorizacao.controller;

import org.nextframework.authorization.DefaultAuthorizationProcess;
import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.springframework.web.servlet.ModelAndView;

@Controller(path="/autorizacao/seguranca/permissao",authorizationModule=ProcessAuthorizationModule.class)
public class PermissaoController extends DefaultAuthorizationProcess {
	@Override
	protected ModelAndView getModelAndView() {
		return new ModelAndView("permissao");
	}
}
