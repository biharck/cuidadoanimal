package seguranca.autorizacao.service;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import org.nextframework.core.web.NextWeb;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.service.GenericService;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import seguranca.autenticacao.filter.AnimalFiltro;
import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.AntiPulgas;
import seguranca.autorizacao.bean.Anticoncepcional;
import seguranca.autorizacao.bean.Cliente;
import seguranca.autorizacao.bean.Consultas;
import seguranca.autorizacao.bean.Estetica;
import seguranca.autorizacao.bean.ServicosPrestados;
import seguranca.autorizacao.bean.Usuario;
import seguranca.autorizacao.bean.Vacina;
import seguranca.autorizacao.bean.Venda;
import seguranca.autorizacao.bean.Vermifugos;
import seguranca.autorizacao.dao.AnimalDAO;
import br.com.biharckgroup.util.CuidadoAnimalUtil;

public class AnimalService extends GenericService<Animal> {
	
	private AnimalDAO animalDAO;
	private AnticoncepcionalService anticoncepcionalService;
	private AntiPulgasService antiPulgasService;
	private ConsultasService consultasService;
	private VacinaService vacinaService;
	private TransactionTemplate transactionTemplate;
	private VermifugosService vermifugosService;
	private ServicosPrestadosService servicosPrestadosService;
	private VendaService vendaService;
	private ItemProdutoService itemProdutoService;
	private ProdutoServicoService produtoServicoService;
	private EsteticaService esteticaService;
	
	public void setAnticoncepcionalService(AnticoncepcionalService anticoncepcionalService) {
		this.anticoncepcionalService = anticoncepcionalService;
	}
	public void setAntiPulgasService(AntiPulgasService antiPulgasService) {
		this.antiPulgasService = antiPulgasService;
	}
	public void setConsultasService(ConsultasService consultasService) {
		this.consultasService = consultasService;
	}
	public void setVacinaService(VacinaService vacinaService) {
		this.vacinaService = vacinaService;
	}
	public void setAnimalDAO(AnimalDAO animalDAO) {
		this.animalDAO = animalDAO;
	}
	public void setVermifugosService(VermifugosService vermifugosService) {
		this.vermifugosService = vermifugosService;
	}
	public void setItemProdutoService(ItemProdutoService itemProdutoService) {
		this.itemProdutoService = itemProdutoService;
	}
	
	public List<Animal> getListaAnimalByCliente(Cliente cliente){
		return animalDAO.getListaAnimalByCliente(cliente);
	}
	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}
	public void setServicosPrestadosService(ServicosPrestadosService servicosPrestadosService) {
		this.servicosPrestadosService = servicosPrestadosService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setProdutoServicoService(
			ProdutoServicoService produtoServicoService) {
		this.produtoServicoService = produtoServicoService;
	}
	public void setEsteticaService(EsteticaService esteticaService) {
		this.esteticaService = esteticaService;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void saveOrUpdate(Animal bean) {
		
		Animal tmp = loadForEntrada(bean);
		
		super.saveOrUpdate(bean);
		if(bean.getListaAnticoncepcional()!=null)
		for (Anticoncepcional a : bean.getListaAnticoncepcional()) {
			a.setAnimal(bean);
		}
		if(bean.getListaAntiPulgas()!=null)
		for (AntiPulgas ap : bean.getListaAntiPulgas()) {
			ap.setAnimal(bean);
		}
		if(bean.getListaConsultas()!=null)
		for (Consultas c : bean.getListaConsultas()) {
			c.setAnimal(bean);
		}
		if(bean.getListaVacinas()!=null)
		for (Vacina v : bean.getListaVacinas()) {
			v.setAnimal(bean);
		}
		if(bean.getListaVermifugos()!=null)
		for (Vermifugos v : bean.getListaVermifugos()) {
			v.setAnimal(bean);
		}
		if(bean.getListaServicosPrestados()!=null)
		for (ServicosPrestados sp : bean.getListaServicosPrestados()) {
			sp.setAnimal(bean);
		}
		if(bean.getListaEstetica()!=null)
			for (Estetica sp : bean.getListaEstetica()) {
				sp.setAnimal(bean);
			}
		
		
		salvaListas(NextWeb.getRequestContext(), bean.getListaAnticoncepcional(), bean.getListaAntiPulgas(), 
				bean.getListaConsultas(),bean.getListaVacinas(), bean.getListaVermifugos(),bean.getListaServicosPrestados(),bean.getListaEstetica());
		
		if(tmp.getListaEstetica()!=null && bean.getListaEstetica()!=null){
			Estetica esteticaTmp = tmp.getListaEstetica().get(tmp.getListaEstetica().size()-1);
			Estetica estetica = bean.getListaEstetica().get(bean.getListaEstetica().size()-1);
			if(estetica.getStatus()&&(esteticaTmp.getStatus()==null || !esteticaTmp.getStatus())){
				try {
					Usuario u = new Usuario();
					u.setNome("seu animal est� pronto");
					u.setEmail("biharck@gmail.com");
					System.out.println("email");
					CuidadoAnimalUtil.enviaEmailRecuperaSenha(u, null, "");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	@SuppressWarnings("unchecked")
	public void salvaListas(final WebRequestContext request, final List<? extends Object> ... list){
		try {
			
		transactionTemplate.execute(new TransactionCallback(){			
			public Object doInTransaction(TransactionStatus status)  {
				if(list[0]!=null){
					for (Anticoncepcional set : (List<Anticoncepcional>)list[0]) {
						anticoncepcionalService.saveOrUpdate(set);
					}
				}
				if(list[1]!=null){
					for (AntiPulgas set : (List<AntiPulgas>)list[1]) {
						antiPulgasService.saveOrUpdate(set);
					}
				}
				if(list[2]!=null){
					for (Consultas set : (List<Consultas>)list[2]) {
						consultasService.saveOrUpdate(set);
					}
				}
				if(list[3]!=null){
					for (Vacina set : (List<Vacina>)list[3]) {
						vacinaService.saveOrUpdate(set);
					}
				}
				if(list[4]!=null){
					for (Vermifugos set : (List<Vermifugos>)list[4]) {
						vermifugosService.saveOrUpdate(set);
					}
				}
				if(list[5]!=null){
					try {
						Venda venda = new Venda();
						venda.setData(new Date(System.currentTimeMillis()));
						venda.setHora(new Time(System.currentTimeMillis()));
						venda.setUsuario(CuidadoAnimalUtil.getPessoaLogada());
						List<ServicosPrestados> lista = (List<ServicosPrestados>)list[5];
						venda.setCliente(lista.get(0).getAnimal().getDono());
						venda.setServicosPrestados(lista);
						vendaService.saveOrUpdate(venda);
//						for (ServicosPrestados set : (List<ServicosPrestados>)list[5]) {
//							set.setVenda(venda);
//							//realizar baixa no estoque em produto servico
//							
//							//baixando o restante dos itens
//							ItemProduto ip = itemProdutoService.getItemProdutoAndProdutoSerico(set.getItemProduto());
//							ip.setQuantidadeRestante(ip.getQuantidadeRestante()-set.getQtd());
//							Integer estoqueAtual = ip.getProdutoServico().getStock();
//							Integer utilizado = set.getQtd();
//							ip.getProdutoServico().setStock(estoqueAtual-utilizado);
//							produtoServicoService.saveOrUpdate(ip.getProdutoServico());
//							//
//							servicosPrestadosService.saveOrUpdate(set);
//						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(list[6]!=null){
					for (Estetica set : (List<Estetica>)list[6]) {
						esteticaService.saveOrUpdate(set);
					}
				}
				return null;
			}
		});		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * M�todo respons�vel em carregas as listas dos detalhes do animal
	 * @param bean
	 * @return
	 */
	public Animal carregaListas(Animal bean){
		bean.setListaAnticoncepcional(anticoncepcionalService.getAnticoncecpionalByAnimal(bean));
		bean.setListaAntiPulgas(antiPulgasService.getAntiPulgasByAnimal(bean));
		bean.setListaConsultas(consultasService.getConsultasByAnimal(bean));
		bean.setListaVacinas(vacinaService.getVacinaByAnimal(bean));
		bean.setListaVermifugos(vermifugosService.getVermifugosByAnimal(bean));
		bean.setListaEstetica(esteticaService.getEsteticasByAnimal(bean));
		return bean;
	}
	
	
	public java.util.List<Animal> getListAnimalReport(AnimalFiltro filtro){
		return animalDAO.getListAnimalReport(filtro);
	}
	
	public Animal getHistoricoAnimal(Integer id){
		return animalDAO.getHistoricoAnimal(id);
	}
	
}
