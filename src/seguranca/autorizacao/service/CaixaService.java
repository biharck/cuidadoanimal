package seguranca.autorizacao.service;

import org.nextframework.service.GenericService;

import seguranca.autorizacao.bean.Caixa;
import seguranca.autorizacao.dao.CaixaDAO;

public class CaixaService extends GenericService<Caixa>{
	
	private CaixaDAO caixaDAO;
	
	public void setCaixaDAO(CaixaDAO caixaDAO) {
		this.caixaDAO = caixaDAO;
	}
	
	public Caixa getCaixaDia(){
		return caixaDAO.getCaixaDia();
	}


}
