package seguranca.autorizacao.service;

import java.util.List;

import org.nextframework.service.GenericService;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Anticoncepcional;
import seguranca.autorizacao.dao.AnticoncepcionalDAO;

public class AnticoncepcionalService extends GenericService<Anticoncepcional> {
	
	private AnticoncepcionalDAO anticoncepcionalDAO;
	public void setAnticoncepcionalDAO(AnticoncepcionalDAO anticoncepcionalDAO) {
		this.anticoncepcionalDAO = anticoncepcionalDAO;
	}
	
	public List<Anticoncepcional> getAnticoncecpionalByAnimal(Animal animal){
		return anticoncepcionalDAO.getAnticoncecpionalByAnimal(animal);
	}

}
