package seguranca.autorizacao.service;

import java.util.List;

import org.nextframework.service.GenericService;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Vacina;
import seguranca.autorizacao.bean.Vermifugos;
import seguranca.autorizacao.dao.VermifugosDAO;

public class VermifugosService extends GenericService<Vermifugos> {
	
	private VermifugosDAO vermifugosDAO;
	public void setVermifugosDAO(VermifugosDAO vermifugosDAO) {
		this.vermifugosDAO = vermifugosDAO;
	}
	
	public List<Vermifugos> getVermifugosByAnimal(Animal animal){
		return vermifugosDAO.getVermifugosByAnimal(animal);
	}

}
