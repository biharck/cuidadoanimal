package seguranca.autorizacao.service;

import java.awt.Image;

import org.nextframework.core.standard.Next;
import org.nextframework.service.GenericService;
import org.nextframework.types.File;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Arquivo;
import seguranca.autorizacao.dao.ArquivoDAO;

public class ArquivoService extends GenericService<Arquivo> {

	protected ArquivoDAO arquivoDAO;

	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}

	public <E extends File> E loadWithContents(E bean) {
		return arquivoDAO.loadWithContents(bean);
	}

	public void fillWithContents(File bean) {
		arquivoDAO.fillWithContents(bean);
	}

	public void delete(Arquivo bean) {
		arquivoDAO.delete(bean);
	}

	public Image loadAsImage(File bean) {
		return arquivoDAO.loadAsImage(bean);
	}

	public Arquivo getFotoByAnimalAndSave(Animal animal){
		return arquivoDAO.getFoto(animal);
	}
	
	/* singleton */
	private static ArquivoService instance;

	public static ArquivoService getInstance() {
		if (instance == null) {
			instance = Next.getObject(ArquivoService.class);
		}
		return instance;
	}

}
