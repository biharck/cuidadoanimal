package seguranca.autorizacao.service;

import org.nextframework.service.GenericService;

import seguranca.autorizacao.bean.Municipio;
import seguranca.autorizacao.bean.Uf;
import seguranca.autorizacao.dao.UfDAO;

public class UfService extends GenericService<Uf> {
	private UfDAO ufDAO;
	public void setUfDAO(UfDAO ufDAO) {
		this.ufDAO = ufDAO;
	}
	
	
	public Uf getUfByMunicipio(Municipio municipio){
		return ufDAO.getUfByMunicipio(municipio);
	}
}
