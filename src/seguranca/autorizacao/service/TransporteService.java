package seguranca.autorizacao.service;

import java.util.List;

import org.nextframework.service.GenericService;

import seguranca.autorizacao.bean.Transporte;
import seguranca.autorizacao.dao.TransporteDAO;

public class TransporteService extends GenericService<Transporte>{
	
	private TransporteDAO transporteDAO;
	
	public void setTransporteDAO(TransporteDAO transporteDAO) {
		this.transporteDAO = transporteDAO;
	}
	
	public List<Transporte> findForComboEspecifico() {
		return transporteDAO.findForCombo();
	}

}
