package seguranca.autorizacao.service;

import java.util.List;

import org.nextframework.service.GenericService;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.AntiPulgas;
import seguranca.autorizacao.dao.AntiPulgasDAO;

public class AntiPulgasService extends GenericService<AntiPulgas> {
	
	private AntiPulgasDAO antiPulgasDAO;
	public void setAntiPulgasDAO(AntiPulgasDAO antiPulgasDAO) {
		this.antiPulgasDAO = antiPulgasDAO;
	}
	
	public List<AntiPulgas> getAntiPulgasByAnimal(Animal animal){
		return antiPulgasDAO.getAntiPulgasByAnimal(animal);
	}

}
