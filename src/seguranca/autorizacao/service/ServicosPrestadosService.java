package seguranca.autorizacao.service;

import java.util.List;

import org.nextframework.service.GenericService;

import seguranca.autenticacao.filter.ProdutoServicoFiltro;
import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.ProdutoServico;
import seguranca.autorizacao.bean.ServicosPrestados;
import seguranca.autorizacao.dao.ServicosPrestadosDAO;
import br.com.biharckgroup.util.CuidadoAnimalException;

public class ServicosPrestadosService extends GenericService<ServicosPrestados>{
	
	private ServicosPrestadosDAO servicosPrestadosDAO;
	private ProdutoServicoService produtoServicoService;
	private ItemProdutoService itemProdutoService;
	public void setServicosPrestadosDAO(ServicosPrestadosDAO servicosPrestadosDAO) {
		this.servicosPrestadosDAO = servicosPrestadosDAO;
	}
	public void setProdutoServicoService(
			ProdutoServicoService produtoServicoService) {
		this.produtoServicoService = produtoServicoService;
	}
	public void setItemProdutoService(ItemProdutoService itemProdutoService) {
		this.itemProdutoService = itemProdutoService;
	}
	
	public List<ServicosPrestados> getListaServicosPrestadosByAnimal(Animal animal){
		return servicosPrestadosDAO.getListaServicosPrestadosByAnimal(animal);
	}
	
	public List<ServicosPrestados> getListServicosPrestadosReport(ProdutoServicoFiltro filtro){
		return servicosPrestadosDAO.getListServicosPrestadosReport(filtro);
	}
	
	
	@Override
	public void saveOrUpdate(ServicosPrestados bean) {
		//realizar baixa no estoque dos produtos/servi�os prestados
		ProdutoServico ps = produtoServicoService.getProdutoByItem(bean.getItemProduto());
		Integer totalStock = ps.getStock();
		Integer qtdComprada = bean.getQtd();
		if(totalStock==null || totalStock < bean.getQtd())
			throw new CuidadoAnimalException("A quantidade solicitada de "+bean.getItemProduto().getProdutoServico().getNome()+" � superior ao estoque atual.");
		
		ps.setStock(totalStock - qtdComprada);
		bean.setItemProduto(itemProdutoService.loadForEntrada(bean.getItemProduto()));
		if(bean.getItemProduto().getQuantidadeRestante()==null || qtdComprada==null || bean.getItemProduto().getQuantidadeRestante()< qtdComprada)
			throw new CuidadoAnimalException("A quantidade solicitada de "+bean.getItemProduto().getProdutoServico().getNome()+" � superior ao estoque atual.");
		
		bean.getItemProduto().setQuantidadeRestante(bean.getItemProduto().getQuantidadeRestante()-qtdComprada);
		produtoServicoService.saveOrUpdate(ps);
		itemProdutoService.saveOrUpdate(bean.getItemProduto());
		super.saveOrUpdate(bean);
	}

}
