package seguranca.autorizacao.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.nextframework.service.GenericService;

import seguranca.autorizacao.bean.Papel;
import seguranca.autorizacao.bean.PapelUsuario;
import seguranca.autorizacao.bean.Pessoa;
import seguranca.autorizacao.bean.Usuario;
import seguranca.autorizacao.dao.UsuarioDAO;

public class UsuarioService extends GenericService<Usuario> {
	UsuarioDAO usuarioDAO;
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	
	@Override
	public void saveOrUpdate(Usuario bean) {
		if(bean == null || bean.getId()==null ){
			StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
			bean.setSenha(encryptor.encryptPassword(bean.getSenha()));
			bean.setDtCadastro(new Date(System.currentTimeMillis()));
		}
		
		List<Papel> lista = new ArrayList<Papel>();
		if(bean.getPapeisUsuarios()!=null)
		for (PapelUsuario p : bean.getPapeisUsuarios()) {
			lista.add(p.getPapel());
		}
		bean.setPapeis(lista);
		usuarioDAO.saveOrUpdate(bean);
	}
	
	/**
	 * Encontra uma pessoa que possua o respectivo email
	 * @param
	 * @return Uma pessoa com base no cod
	 */
	public Usuario findUserEmail(String email) {
		return usuarioDAO.findUserEmail(email);
	}
	/**
	 * M�todo respons�vel por criptografar a senha do usuario.
	 * Criptografa somente quando ela � alterada ou se � registro novo
	 * 
	 * @see  br.com.linkcom.neo.persistence.GenericDAO#load
	 * @see  org.jasypt.util.password.StrongPasswordEncryptor#encryptPassword
	 * @param bean
	 */
	private void encryptPassword(Usuario bean){
		Usuario load = usuarioDAO.load(bean);
		if(load == null || !load.getSenha().equals(bean.getSenha())) {
			StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
			bean.setSenha(encryptor.encryptPassword(bean.getSenha()));
		}
	}
	
	/**
	 * M�todo respons�vem em criptografar algum valor manualmente
	 * @param bean
	 */
	public String encrypta(String valor){
		String valor2 = "";
		char cletra; 
		int iletra;
		for (int i = 0; i < valor.length(); i++) {
			cletra =  valor.charAt(i);
			iletra =  cletra;
			iletra = iletra + 5;
			valor2 = valor2 + (char) iletra;
		}	
		return  valor2;
	}
	
	/**
	 * <p> M�todo respons�vem em descriptografar algum valor manualmente
	 * @param valor
	 * @return {@link String}
	 */
	public String dsencrypta(String valor){
		String valor2 = "";
		char cletra; 
		int iletra;
		for (int i = 0; i < valor.length(); i++) {
			cletra =  valor.charAt(i);
			iletra =  cletra;
			iletra = iletra - 5;
			valor2 = valor2 + (char) iletra;
		}	
		return  valor2;
	}

	/**
	 * <p>Encontra uma pessoa que possua o respectivo cod</p>.
	 * @param cod {@link Integer} codigo da pessoa
	 * @return {@link Pessoa}
	 */
	public Usuario findUserByCodigo(Long cod) {
		return usuarioDAO.findUserByCodigo(cod);
	}
	
	/**
	 * Encontra uma pessoa que possua o respectivo email
	 * 
	 * @param
	 * @return Uma pessoa com base no cod
	 */
	public Usuario findUserByLogin(String login) {
		return usuarioDAO.findUserByLogin(login);
	}
	
}
