package seguranca.autorizacao.service;

import java.util.List;

import org.nextframework.service.GenericService;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Estetica;
import seguranca.autorizacao.dao.EsteticaDAO;

public class EsteticaService extends GenericService<Estetica>{

	private EsteticaDAO esteticaDAO;
	public void setEsteticaDAO(EsteticaDAO esteticaDAO) {
		this.esteticaDAO = esteticaDAO;
	}
	
	public List<Estetica> getEsteticasByAnimal(Animal animal){
		return esteticaDAO.getEsteticasByAnimal(animal);
	}
}
