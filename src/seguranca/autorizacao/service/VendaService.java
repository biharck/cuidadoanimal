package seguranca.autorizacao.service;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import org.nextframework.core.web.NextWeb;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.service.GenericService;
import org.nextframework.types.Money;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autorizacao.bean.Caixa;
import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.ProdutoServico;
import seguranca.autorizacao.bean.ServicosPrestados;
import seguranca.autorizacao.bean.Venda;
import seguranca.autorizacao.dao.VendaDAO;

public class VendaService extends GenericService<Venda>{
	
	private VendaDAO vendaDAO;
	private ItemProdutoService itemProdutoService;
	
	public void setVendaDAO(VendaDAO vendaDAO) {
		this.vendaDAO = vendaDAO;
	}
	public void setItemProdutoService(ItemProdutoService itemProdutoService) {
		this.itemProdutoService = itemProdutoService;
	}
	
	@Override
	public void saveOrUpdate(Venda bean) {
		bean.setData(new Date(System.currentTimeMillis()));
		bean.setHora(new Time(System.currentTimeMillis()));
		ProdutoServico ps = null;
		Money total = new Money(0);
		if(bean.getIdUpdate()==null)
			for (ServicosPrestados set : bean.getServicosPrestados()) {
				//baixando o restante dos itens
				ItemProduto ip = itemProdutoService.getItemProdutoAndProdutoSerico(set.getItemProduto());
				ip.setQuantidadeSaida(ip.getQuantidadeSaida()+set.getQtd());
				itemProdutoService.saveOrUpdate(ip);
			}
		super.saveOrUpdate(bean);
	}
//	protected ModelAndView sendRedirectToAction(String action) {
//		WebRequestContext requestContext = NextWeb.getRequestContext();
//		String requestQuery = requestContext.getRequestQuery();
//		String query = "?" + "ACAO" + "=" + action;
//		return new ModelAndView("redirect:" + requestQuery + (action == null ? "" : query) );
//	}
	
	public Venda getVendaById(Integer idVenda){
		return vendaDAO.getVendaById(idVenda);
	}
	

	public Venda getVendaAndServicosPrestadosById(Integer idVenda){
		return vendaDAO.getVendaAndServicosPrestadosById(idVenda);
	}
	public void updateVenda(Integer idVenda){
		vendaDAO.updateVenda(idVenda);
	}
	
	public List<Venda> getVendaByCaixa(Caixa c){
		return vendaDAO.getVendaByCaixa(c);
	}
	public List<Venda> findAllByIds(String ids) {
		return vendaDAO.findAllByIds(ids);
	}

}
