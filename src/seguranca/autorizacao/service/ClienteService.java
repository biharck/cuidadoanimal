package seguranca.autorizacao.service;

import java.io.IOException;
import java.sql.Date;

import org.nextframework.service.GenericService;

import seguranca.autenticacao.filter.ClienteFiltro;
import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Arquivo;
import seguranca.autorizacao.bean.Cliente;
import seguranca.autorizacao.dao.ClienteDAO;
import br.com.biharckgroup.util.CuidadoAnimalException;
import br.com.biharckgroup.util.RedimensionaImagem;

public class ClienteService extends GenericService<Cliente> {

	private ArquivoService arquivoService;
	private ClienteDAO clienteDAO;

	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}

	public void setClienteDAO(ClienteDAO clienteDAO) {
		this.clienteDAO = clienteDAO;
	}

	@Override
	public void saveOrUpdate(Cliente bean) {
		if(bean.getIdCliente()==null)
			bean.setDtCadastro(new Date(System.currentTimeMillis()));
		// carega o arquivo
		if(bean.getListaAnimal()!=null)
		for (Animal a : bean.getListaAnimal()) {
			a.setFoto(a.getFoto());
			if (a.getFoto() != null && a.getFoto().getContent() != null) {
				RedimensionaImagem redimensionaImagem = new RedimensionaImagem();
				try {
					Arquivo arquivoFoto = redimensionaImagem.redimensionaImagem(a.getFoto(), 110, 150);
					if (arquivoFoto != null)
						a.setFoto(arquivoFoto);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (NullPointerException nullPointer) {
					throw new CuidadoAnimalException("Foto inv�lida.");
				}
				arquivoService.saveOrUpdate(a.getFoto());
				
			}
		}
		clienteDAO.saveOrUpdate(bean);
	}
	
	public java.util.List<Cliente> getListClienteReport(ClienteFiltro cliente){
		return clienteDAO.getListClienteReport(cliente);
	}
	
}
