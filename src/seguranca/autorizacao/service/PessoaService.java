package seguranca.autorizacao.service;

import java.sql.Date;

import org.nextframework.service.GenericService;

import br.com.biharckgroup.util.CuidadoAnimalUtil;

import seguranca.autorizacao.bean.Papel;
import seguranca.autorizacao.bean.PapelUsuario;
import seguranca.autorizacao.bean.Pessoa;

public class PessoaService extends GenericService<Pessoa>{
	
	PapelUsuarioService papelUsuarioService;
	UsuarioService usuarioService;
	
	public void setPapelUsuarioService(PapelUsuarioService papelUsuarioService) {
		this.papelUsuarioService = papelUsuarioService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Override
	public void saveOrUpdate(Pessoa bean) {
		bean.getUsuario().setDtCadastro(new Date(System.currentTimeMillis()));
		bean.getUsuario().setUserTemp(true);
		bean.getUsuario().setNome(bean.getNome());
		bean.getUsuario().setAtivado(true);
		bean.setObservacao(bean.getUsuario().getSenha());
		usuarioService.saveOrUpdate(bean.getUsuario());
		
		PapelUsuario papelUsuario = new PapelUsuario();
		Papel papel = new Papel();
		papel.setId(new Long(4));
		papelUsuario.setPapel(papel);		
		papelUsuario.setUsuario(bean.getUsuario());
		papelUsuarioService.saveOrUpdate(papelUsuario);
		super.saveOrUpdate(bean);
		enviaEmailInstrucoes(bean);
	}
	
	
	
	public void enviaEmailInstrucoes(Pessoa bean){
		try {
			CuidadoAnimalUtil.enviaEmailInstrucoes(bean, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
