package seguranca.autorizacao.service;

import org.nextframework.service.GenericService;

import seguranca.autorizacao.bean.ProdutoServico;
import seguranca.autorizacao.bean.ValorProdutoServico;
import seguranca.autorizacao.dao.ValorProdutoServicoDAO;

public class ValorProdutoServicoService extends GenericService<ValorProdutoServico>{

	private ValorProdutoServicoDAO valorProdutoServicoDAO;
	public void setValorProdutoServicoDAO(
			ValorProdutoServicoDAO valorProdutoServicoDAO) {
		this.valorProdutoServicoDAO = valorProdutoServicoDAO;
	}
	
	public ValorProdutoServico getValorProdutoServicoByProduto(ProdutoServico produtoServico){
		return valorProdutoServicoDAO.getValorProdutoServicoByProduto(produtoServico);
	}
}
