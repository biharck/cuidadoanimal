package seguranca.autorizacao.service;

import org.nextframework.core.standard.Next;
import org.nextframework.service.GenericService;

import seguranca.autorizacao.bean.Empresa;
import seguranca.autorizacao.bean.Municipio;
import seguranca.autorizacao.dao.EmpresaDAO;

public class EmpresaService extends GenericService<Municipio> {

	private EmpresaDAO empresaDAO;
	
	public void setEmpresaDAO(EmpresaDAO empresaDAO) {
		this.empresaDAO = empresaDAO;
	}
	
	/* singleton */
	private static EmpresaService instance;

	public static EmpresaService getInstance() {
		if (instance == null) {
			instance = Next.getObject(EmpresaService.class);
		}
		return instance;
	}
	
	public Empresa getEmpresa(){
		return empresaDAO.getEmpresa();
	}
}
