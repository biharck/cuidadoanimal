package seguranca.autorizacao.service;

import java.util.List;

import org.nextframework.service.GenericService;

import seguranca.autorizacao.bean.Fornecedor;
import seguranca.autorizacao.bean.SubTipoProduto;
import seguranca.autorizacao.bean.TipoProduto;
import seguranca.autorizacao.dao.TipoProdutoDAO;

public class TipoProdutoService extends GenericService<Fornecedor> {
	
	private TipoProdutoDAO tipoProdutoDAO;
	public void setTipoProdutoDAO(TipoProdutoDAO tipoProdutoDAO) {
		this.tipoProdutoDAO = tipoProdutoDAO;
	}
	
	public TipoProduto getTipoProdutoBySubTipo(SubTipoProduto subTipoProduto){
		return tipoProdutoDAO.getTipoProdutoBySubTipo(subTipoProduto);
	}
	

}
