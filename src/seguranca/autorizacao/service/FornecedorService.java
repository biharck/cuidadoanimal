package seguranca.autorizacao.service;

import java.sql.Date;

import org.nextframework.service.GenericService;

import seguranca.autenticacao.filter.FornecedorFiltro;
import seguranca.autorizacao.bean.Fornecedor;
import seguranca.autorizacao.dao.FornecedorDAO;

public class FornecedorService extends GenericService<Fornecedor> {

	private FornecedorDAO fornecedorDAO;
	
	public void setFornecedorDAO(FornecedorDAO fornecedorDAO) {
		this.fornecedorDAO = fornecedorDAO;
	}
	
	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<Fornecedor> getListFornecedorReport(FornecedorFiltro filtro){
		return fornecedorDAO.getListFornecedorReport(filtro);
	}
	
	@Override
	public void saveOrUpdate(Fornecedor bean) {
		if(bean.getIdFornecedor()==null)
			bean.setDtCadastro(new Date(System.currentTimeMillis()));
		super.saveOrUpdate(bean);
	}
}
