package seguranca.autorizacao.service;

import org.nextframework.service.GenericService;

import seguranca.autenticacao.filter.ContaReceberFiltro;
import seguranca.autorizacao.bean.ContaReceber;
import seguranca.autorizacao.dao.ContaReceberDAO;

public class ContaReceberService extends GenericService<ContaReceber> {

	
	private ContaReceberDAO contaReceberDAO;
	public void setContaReceberDAO(ContaReceberDAO contaReceberDAO) {
		this.contaReceberDAO = contaReceberDAO;
	}
	
	public java.util.List<ContaReceber> getListContaReceberReport(ContaReceberFiltro filtro){
		return contaReceberDAO.getListContaReceberReport(filtro);
	}
}
