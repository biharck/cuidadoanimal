package seguranca.autorizacao.service;

import java.util.List;

import org.nextframework.service.GenericService;

import seguranca.autenticacao.filter.ConsultaFiltro;
import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Consultas;
import seguranca.autorizacao.dao.ConsultasDAO;

public class ConsultasService extends GenericService<Consultas>{

	private ConsultasDAO consultasDAO;
	public void setConsultasDAO(ConsultasDAO consultasDAO) {
		this.consultasDAO = consultasDAO;
	}
	
	public List<Consultas> getConsultasByAnimal(Animal animal){
		return consultasDAO.getConsultasByAnimal(animal);
	}
	
	public List<Consultas> getConsultasById(Integer id){
		return consultasDAO.getConsultasById(id);
	}
	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<Consultas> getListConsultasReport(ConsultaFiltro filtro){
		return consultasDAO.getListConsultasReport(filtro);
	}
}
