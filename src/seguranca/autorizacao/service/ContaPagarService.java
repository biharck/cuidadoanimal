package seguranca.autorizacao.service;

import org.nextframework.service.GenericService;

import seguranca.autenticacao.filter.ContaPagarFiltro;
import seguranca.autorizacao.bean.ContaPagar;
import seguranca.autorizacao.dao.ContaPagarDAO;

public class ContaPagarService extends GenericService<ContaPagar>{

	
	private ContaPagarDAO contaPagarDAO;
	
	public void setContaPagarDAO(ContaPagarDAO contaPagarDAO) {
		this.contaPagarDAO = contaPagarDAO;
	}
	
	public java.util.List<ContaPagar> getListContaPagarReport(ContaPagarFiltro filtro){
		return contaPagarDAO.getListContaPagarReport(filtro);
	}
}
