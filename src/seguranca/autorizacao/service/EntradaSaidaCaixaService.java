package seguranca.autorizacao.service;

import java.util.List;

import org.nextframework.service.GenericService;

import seguranca.autenticacao.filter.EntradaSaidaCaixaFiltro;
import seguranca.autorizacao.bean.EntradaSaidaCaixa;
import seguranca.autorizacao.dao.EntradaSaidaCaixaDAO;

public class EntradaSaidaCaixaService extends GenericService<EntradaSaidaCaixa>{

	private EntradaSaidaCaixaDAO entradaSaidaCaixaDAO;
	
	public void setEntradaSaidaCaixaDAO(EntradaSaidaCaixaDAO entradaSaidaCaixaDAO) {
		this.entradaSaidaCaixaDAO = entradaSaidaCaixaDAO;
	}
	
	public List<EntradaSaidaCaixa> getListaEntradaCaixaDiaCorrente(){
		return entradaSaidaCaixaDAO.getListaEntradaCaixaDiaCorrente();
	}
	
	public List<EntradaSaidaCaixa> getListaSaidaCaixaDiaCorrente(){
		return entradaSaidaCaixaDAO.getListaSaidaCaixaDiaCorrente();
	}
	public EntradaSaidaCaixa getEntradaSaidaCaixaById(EntradaSaidaCaixa entradaSaidaCaixa){
		return entradaSaidaCaixaDAO.getEntradaSaidaCaixaById(entradaSaidaCaixa);
	}
	public List<EntradaSaidaCaixa> getListaEstornoCaixaDiaCorrente(){
		return entradaSaidaCaixaDAO.getListaEstornoCaixaDiaCorrente();
	}
	
	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<EntradaSaidaCaixa> getListEntradaSaidaCaixaReport(EntradaSaidaCaixaFiltro filtro){
		return entradaSaidaCaixaDAO.getListEntradaSaidaCaixaReport(filtro);
	}

	
	public List<EntradaSaidaCaixa> getListaEstornoCaixaDiaCorrenteExcetoEstorno(){
		return entradaSaidaCaixaDAO.getListaEstornoCaixaDiaCorrenteExcetoEstorno();
	}
}
