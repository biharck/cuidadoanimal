package seguranca.autorizacao.service;

import java.util.List;

import org.nextframework.core.standard.Next;
import org.nextframework.service.GenericService;

import seguranca.autenticacao.filter.ItemProdutoFiltro;
import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.ProdutoServico;
import seguranca.autorizacao.dao.ItemProdutoDAO;

public class ItemProdutoService extends GenericService<ItemProduto>{
	
	private ItemProdutoDAO itemProdutoDAO;
	public void setItemProdutoDAO(ItemProdutoDAO itemProdutoDAO) {
		this.itemProdutoDAO = itemProdutoDAO;
	}
	
	public List<ItemProduto> getListaItemProdutoByProduto(ProdutoServico produtoServico){
		return itemProdutoDAO.getListaItemProdutoByProduto(produtoServico);
		
	}

	public ItemProduto getItemByLoteAndProduto(Integer produto){
		return itemProdutoDAO.getItemByLoteAndProduto(produto);
	}
	public List<ItemProduto> getItemByLoteAndProdutoList(Integer produto){
		return itemProdutoDAO.getItemByLoteAndProdutoList(produto);
	}
	
	@Override
	public void saveOrUpdate(ItemProduto bean) {
		if(bean==null || bean.getIdItemProduto()==null)
			bean.setQuantidadeRestante(bean.getQuantidadeEntrada());
		super.saveOrUpdate(bean);
	}
	public ItemProduto getItemProdutoAndProdutoSerico(ItemProduto itemProduto){
		return itemProdutoDAO.getItemProdutoAndProdutoSerico(itemProduto);
	}
	
	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<ItemProduto> getListItemProdutoReport(ItemProdutoFiltro filtro){
		return itemProdutoDAO.getListItemProdutoReport(filtro);
	}
	
	public List<ItemProduto> getListByIds(String ids){
		return itemProdutoDAO.getListByIds(ids);
	}
	
	private static ItemProdutoService instance;

	public static ItemProdutoService getInstance() {
		if (instance == null) {
			instance = Next.getObject(ItemProdutoService.class);
		}
		return instance;
	}

}
