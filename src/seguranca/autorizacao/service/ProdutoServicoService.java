package seguranca.autorizacao.service;

import java.sql.Date;
import java.util.List;

import org.nextframework.service.GenericService;

import seguranca.autenticacao.filter.ProdutoServicoFiltro;
import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.ProdutoServico;
import seguranca.autorizacao.bean.ValorProdutoServico;
import seguranca.autorizacao.dao.ProdutoServicoDAO;

public class ProdutoServicoService extends GenericService<ProdutoServico> {

	private ValorProdutoServicoService valorProdutoServicoService;
	private ProdutoServicoDAO produtoServicoDAO;
	public void setValorProdutoServicoService(
			ValorProdutoServicoService valorProdutoServicoService) {
		this.valorProdutoServicoService = valorProdutoServicoService;
	}
	public void setProdutoServicoDAO(ProdutoServicoDAO produtoServicoDAO) {
		this.produtoServicoDAO = produtoServicoDAO;
	}
	
	
	@Override
	public void saveOrUpdate(ProdutoServico bean) {
		if(bean.getQtdMinima()==null)
			bean.setQtdMinima(0);
		/**
		 * caso seja a inser��o de um novo registro
		 */
		if(bean.getIdProdutoServico()==null){
			super.saveOrUpdate(bean);
			bean.getValorProdutoServico().setProdutoServico(bean);
			bean.getValorProdutoServico().setDataInicio(new Date(System.currentTimeMillis()));
			valorProdutoServicoService.saveOrUpdate(bean.getValorProdutoServico());
		}else{
			super.saveOrUpdate(bean);
			ValorProdutoServico valorProdutoServicoTemp = valorProdutoServicoService.getValorProdutoServicoByProduto(bean);
			if(bean.getValorProdutoServico()!=null && bean.getValorProdutoServico().getValor()!=null && bean.getValorProdutoServico().getMargemLucro()!=null&&
					valorProdutoServicoTemp!=null && valorProdutoServicoTemp.getValor()!=null)
			if(!valorProdutoServicoTemp.getValor().equals(bean.getValorProdutoServico().getValor())||
					!valorProdutoServicoTemp.getMargemLucro().equals(bean.getValorProdutoServico().getMargemLucro())){
				
				bean.getValorProdutoServico().setDataFim(new Date(System.currentTimeMillis()));
				bean.getValorProdutoServico().setProdutoServico(bean);
				bean.getValorProdutoServico().setDataInicio(valorProdutoServicoTemp.getDataInicio());
				valorProdutoServicoService.saveOrUpdate(bean.getValorProdutoServico());
				bean.getValorProdutoServico().setDataFim(null);
				bean.getValorProdutoServico().setDataInicio(new Date(System.currentTimeMillis()));
				bean.getValorProdutoServico().setIdValorProdutoServico(null);
				bean.getValorProdutoServico().setProdutoServico(bean);
				valorProdutoServicoService.saveOrUpdate(bean.getValorProdutoServico());
			}
		}
	}
	
	public List<ProdutoServico> getProdutoServicoByTipo(Integer idTipoSubProduto){
		return produtoServicoDAO.getProdutoServicoByTipo(idTipoSubProduto);
	}
	
	public ProdutoServico getProdutoByItem(ItemProduto itemProduto){
		return produtoServicoDAO.getProdutoByItem(itemProduto);
	}

	

	/**
	 * Usado no report
	 * @param cliente
	 * @return
	 */
	public java.util.List<ProdutoServico> getListProdutoServicoReport(ProdutoServicoFiltro filtro){
		return produtoServicoDAO.getListProdutoServicoReport(filtro);
	}
	
	public List<ProdutoServico> getListaProdutosAbaixoEstoqueMinimo(){
		return produtoServicoDAO.getListaProdutosAbaixoEstoqueMinimo();
	}
}
