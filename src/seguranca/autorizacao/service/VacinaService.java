package seguranca.autorizacao.service;

import java.util.List;

import org.nextframework.service.GenericService;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Vacina;
import seguranca.autorizacao.dao.VacinaDAO;

public class VacinaService extends GenericService<Vacina> {
	
	private VacinaDAO vacinaDAO;
	public void setVacinaDAO(VacinaDAO vacinaDAO) {
		this.vacinaDAO = vacinaDAO;
	}
	
	public List<Vacina> getVacinaByAnimal(Animal animal){
		return vacinaDAO.getVacinaByAnimal(animal);
	}

}
