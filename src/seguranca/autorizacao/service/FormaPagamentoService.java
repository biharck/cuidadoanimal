package seguranca.autorizacao.service;

import java.util.List;

import org.nextframework.service.GenericService;

import seguranca.autorizacao.bean.FormaPagamento;
import seguranca.autorizacao.dao.FormaPagamentoDAO;

public class FormaPagamentoService extends GenericService<FormaPagamento> {

	private FormaPagamentoDAO formaPagamentoDAO;
	public void setFormaPagamentoDAO(FormaPagamentoDAO formaPagamentoDAO) {
		this.formaPagamentoDAO = formaPagamentoDAO;
	}
	
	public List<FormaPagamento> listaFormasReaisPagamento(){
		return formaPagamentoDAO.listaFormasReaisPagamento();
	}

}
