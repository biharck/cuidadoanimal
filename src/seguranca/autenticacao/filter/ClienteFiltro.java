package seguranca.autenticacao.filter;

import java.sql.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

public class ClienteFiltro  extends FiltroListagem{
	
	private String nome;
	private String cpf;
	private Date dtInicio;
	private Date dtFim;
	
	public String getNome() {
		return nome;
	}
	public String getCpf() {
		return cpf;
	}
	@DisplayName("Data de In�cio")
	public Date getDtInicio() {
		return dtInicio;
	}
	@DisplayName("Data Fim")
	public Date getDtFim() {
		return dtFim;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	
	

}
