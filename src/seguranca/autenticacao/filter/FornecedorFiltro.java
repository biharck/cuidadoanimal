package seguranca.autenticacao.filter;

import java.sql.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import seguranca.autorizacao.bean.ProdutoServico;

public class FornecedorFiltro  extends FiltroListagem{
	
	private String razaoSocial;
	private String cnpj;
	private String inscricaoEstadual;
	private String inscricaoMunicipal;
	private Boolean ativo;
	private ProdutoServico produtoServico;
	private Date dtInicio;
	private Date dtFim;
	
	@DisplayName("Raz�o Social")
	public String getRazaoSocial() {
		return razaoSocial;
	}
	@DisplayName("CNPJ")
	public String getCnpj() {
		return cnpj;
	}
	@DisplayName("Inscri��o Estadual")
	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}
	@DisplayName("Inscri��o Municipal")
	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Produto/Servi�o")
	public ProdutoServico getProdutoServico() {
		return produtoServico;
	}
	@DisplayName("Data Fim")
	public Date getDtFim() {
		return dtFim;
	}
	@DisplayName("Data de In�cio")
	public Date getDtInicio() {
		return dtInicio;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}
	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setProdutoServico(ProdutoServico produtoServico) {
		this.produtoServico = produtoServico;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
}
