package seguranca.autenticacao.filter;

import org.nextframework.controller.crud.FiltroListagem;

import seguranca.autorizacao.bean.Cliente;

public class VendaFiltro extends FiltroListagem{
	private Cliente cliente;

	
	public VendaFiltro(){
		this.pageSize = 100000000;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
