package seguranca.autenticacao.filter;


import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.nextframework.core.standard.Next;

public class AuthenticationFilter implements Filter {

	String loginPage = "/autenticacao/login";

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if(Next.getUser() == null){
			request.getRequestDispatcher(loginPage).forward(request, response);
		} else {
			String modulo = "";
			HttpServletRequest req = (HttpServletRequest) request;
			if (modulo.matches("/.+?/.+?/.*")) {
				modulo = modulo.split("/")[2];
				if(modulo.equals("cadastros"))
					req.getSession().setAttribute("modulo", 1);
				else if(modulo.equals("clinica"))
					req.getSession().setAttribute("modulo", 2);
				else if(modulo.equals("estoque"))
					req.getSession().setAttribute("modulo", 3);
				else if(modulo.equals("seguranca"))
					req.getSession().setAttribute("modulo", 4);
				else if(modulo.equals("relatorios"))
					req.getSession().setAttribute("modulo", 6);
				else if(modulo.equals("calendar"))
					req.getSession().setAttribute("modulo", 5);
				System.out.println("dentro do auhte:"+ req.getRequestURI());
			}
			chain.doFilter(request, response);
		}
	}

	public void destroy() {}
	public void init(FilterConfig config) throws ServletException {}

}