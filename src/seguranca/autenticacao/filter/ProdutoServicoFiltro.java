package seguranca.autenticacao.filter;

import java.sql.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.validation.annotation.Required;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.SubTipoProduto;
import seguranca.autorizacao.bean.TipoProduto;

public class ProdutoServicoFiltro extends FiltroListagem{
	private Animal animal;
	private String codProduto;
	private String nome;
	private TipoProduto tipoProduto;
	private SubTipoProduto subTipoProduto;
	private Date dtInicio;
	private Date dtFim;

	
	@DisplayName("Tipo de Produto")
	public TipoProduto getTipoProduto() {
		return tipoProduto;
	}
	@DisplayName("Sub-Tipo de Produto")
	public SubTipoProduto getSubTipoProduto() {
		return subTipoProduto;
	}
	@DisplayName("C�digo")
	public String getCodProduto() {
		return codProduto;
	}
	@DisplayName("Animal")
	public Animal getAnimal() {
		return animal;
	}
	public String getNome() {
		return nome;
	}
	@DisplayName("Data Fim")
	@Required
	public Date getDtFim() {
		return dtFim;
	}
	@DisplayName("Data de In�cio")
	@Required
	public Date getDtInicio() {
		return dtInicio;
	}
	
	public void setCodProduto(String codProduto) {
		this.codProduto = codProduto;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setTipoProduto(TipoProduto tipoProduto) {
		this.tipoProduto = tipoProduto;
	}
	public void setSubTipoProduto(SubTipoProduto subTipoProduto) {
		this.subTipoProduto = subTipoProduto;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
}
