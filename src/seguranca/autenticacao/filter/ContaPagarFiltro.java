package seguranca.autenticacao.filter;

import java.sql.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import seguranca.autorizacao.bean.Fornecedor;

public class ContaPagarFiltro extends FiltroListagem{
	
	private Fornecedor fornecedor;
	private String numDoc;
	private Date dtInicio;
	private Date dtFim;
	
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("N�mero do Documento")
	public String getNumDoc() {
		return numDoc;
	}
	@DisplayName("Data Final")
	public Date getDtFim() {
		return dtFim;
	}
	@DisplayName("Data de In�cio")
	public Date getDtInicio() {
		return dtInicio;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setNumDoc(String numDoc) {
		this.numDoc = numDoc;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}

}
