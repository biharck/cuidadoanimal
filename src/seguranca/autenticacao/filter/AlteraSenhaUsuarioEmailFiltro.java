package seguranca.autenticacao.filter;


import javax.persistence.Transient;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.standard.Next;
import org.nextframework.types.Password;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

import seguranca.autorizacao.bean.Usuario;


public class AlteraSenhaUsuarioEmailFiltro extends FiltroListagem {
	
	protected Usuario usuario = getColaboradorDefault();
	protected Boolean admin; 
	protected String oldsenha;
	protected String newsenha;
	protected String confirmasenha;

	@Required
	@DisplayName("Usu�rio")
	public Usuario getUsuario() {
		return usuario;
	}
	public Boolean getAdmin() {
		return admin;
	}
	@Transient
	@Password
	@Required
	@MaxLength(30)
	@DisplayName("CONFIRME A NOVA SENHA")
	public String getConfirmasenha() {
		return confirmasenha;
	}

	@Transient
	@Password
	@Required
	@MaxLength(30)
	@DisplayName("DIGITE SUA SENHA")
	public String getOldsenha() {
		return oldsenha;
	}

	@Transient
	@Password
	@Required
	@MaxLength(30)
	@DisplayName("NOVA SENHA")
	public String getNewsenha() {
		return newsenha;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}

	public void setOldsenha(String oldsenha) {
		this.oldsenha = oldsenha;
	}

	public void setNewsenha(String newsenha) {
		this.newsenha = newsenha;
	}

	public void setConfirmasenha(String confirmasenha) {
		this.confirmasenha = confirmasenha;
	}
	private Usuario getColaboradorDefault() {
		return (Usuario)Next.getUser();
	}
		
}
