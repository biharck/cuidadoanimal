package seguranca.autenticacao.filter;

import java.sql.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Cliente;
import seguranca.autorizacao.bean.Transporte;

public class EsteticaFiltro extends FiltroListagem{
	
	private Date dataInicio;
	private Date dataFim;
	private Cliente cliente;
	private Animal animal;
	private Transporte transporte;
	
	@DisplayName("Data de In�cio")
	public Date getDataInicio() {
		return dataInicio;
	}
	@DisplayName("Data Fim")
	public Date getDataFim() {
		return dataFim;
	}
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Animal")
	public Animal getAnimal() {
		return animal;
	}
	@DisplayName("Transporte")
	public Transporte getTransporte() {
		return transporte;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	public void setTransporte(Transporte transporte) {
		this.transporte = transporte;
	}
	
	

}
