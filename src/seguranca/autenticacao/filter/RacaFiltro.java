package seguranca.autenticacao.filter;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import seguranca.autorizacao.bean.Especie;

public class RacaFiltro extends FiltroListagem {
	private Especie especie;
	
	@DisplayName("Esp�cie")
	public Especie getEspecie() {
		return especie;
	}
	public void setEspecie(Especie especie) {
		this.especie = especie;
	}
}
