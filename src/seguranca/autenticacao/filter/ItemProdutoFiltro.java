package seguranca.autenticacao.filter;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

public class ItemProdutoFiltro extends FiltroListagem{
	private String nome;
	private String lote;
	private String codProduto;
	
	public String getNome() {
		return nome;
	}
	public String getLote() {
		return lote;
	}
	@DisplayName("C�digo")
	public String getCodProduto() {
		return codProduto;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setLote(String lote) {
		this.lote = lote;
	}
	public void setCodProduto(String codProduto) {
		this.codProduto = codProduto;
	}
	
}
