package seguranca.autenticacao.filter;

import java.sql.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.types.Money;
import org.nextframework.validation.annotation.Required;

import seguranca.autorizacao.bean.TipoOperacao;

public class EntradaSaidaCaixaFiltro extends FiltroListagem{

	private TipoOperacao tipoOperacao;
	private Money valor;
	private Date data;
	private Date dtInicio;
	private Date dtFim;
	
	@DisplayName("Tipo de Opera��o")
	public TipoOperacao getTipoOperacao() {
		return tipoOperacao;
	}
	@DisplayName("Valor")
	public Money getValor() {
		return valor;
	}
	@DisplayName("Data")
	public Date getData() {
		return data;
	}
	@DisplayName("Data de In�cio")
	@Required
	public Date getDtInicio() {
		return dtInicio;
	}
	@DisplayName("Data Fim")
	@Required
	public Date getDtFim() {
		return dtFim;
	}
	public void setTipoOperacao(TipoOperacao tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	
}
