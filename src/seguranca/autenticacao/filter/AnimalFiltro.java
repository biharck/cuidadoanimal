package seguranca.autenticacao.filter;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.validation.annotation.MaxLength;

import seguranca.autorizacao.bean.Cliente;
import seguranca.autorizacao.bean.Cor;
import seguranca.autorizacao.bean.Especie;
import seguranca.autorizacao.bean.Raca;

public class AnimalFiltro extends FiltroListagem{
	
	private Cliente cliente;
	private String enderecoResumido;
	private String nomeAnimal;
	private String apelidoAnimal;
	private Cor cor;
	private Especie especie;
	private Raca raca;
	private Integer numeroRegistro;
	
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Endere�o Resumido")
	@MaxLength(50)
	public String getEnderecoResumido() {
		return enderecoResumido;
	}
	@DisplayName("Nome do Animal")
	public String getNomeAnimal() {
		return nomeAnimal;
	}
	@DisplayName("Apelido do animal")
	public String getApelidoAnimal() {
		return apelidoAnimal;
	}
	@DisplayName("Cor")
	public Cor getCor() {
		return cor;
	}
	public Especie getEspecie() {
		return especie;
	}
	@DisplayName("Ra�a")
	public Raca getRaca() {
		return raca;
	}
	@DisplayName("N�mero do Registro")
	public Integer getNumeroRegistro() {
		return numeroRegistro;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEnderecoResumido(String enderecoResumido) {
		this.enderecoResumido = enderecoResumido;
	}
	public void setNomeAnimal(String nomeAnimal) {
		this.nomeAnimal = nomeAnimal;
	}
	public void setCor(Cor cor) {
		this.cor = cor;
	}
	public void setRaca(Raca raca) {
		this.raca = raca;
	}
	public void setNumeroRegistro(Integer numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}
	public void setApelidoAnimal(String apelidoAnimal) {
		this.apelidoAnimal = apelidoAnimal;
	}
	public void setEspecie(Especie especie) {
		this.especie = especie;
	}
}
