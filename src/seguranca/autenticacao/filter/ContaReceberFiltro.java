package seguranca.autenticacao.filter;

import java.sql.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.validation.annotation.Required;

import seguranca.autorizacao.bean.Cliente;

public class ContaReceberFiltro extends FiltroListagem{

	
	private Cliente cliente;
	private String numeroDoc;
	private Date dtInicio;
	private Date dtFim;
	
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("N�mero do Documento")
	public String getNumeroDoc() {
		return numeroDoc;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setNumeroDoc(String numeroDoc) {
		this.numeroDoc = numeroDoc;
	}
	@DisplayName("Data Fim")
	public Date getDtFim() {
		return dtFim;
	}
	@DisplayName("Data de In�cio")
	public Date getDtInicio() {
		return dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	
}
