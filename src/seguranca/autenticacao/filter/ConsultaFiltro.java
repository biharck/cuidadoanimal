package seguranca.autenticacao.filter;

import java.util.Date;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.controller.crud.FiltroListagem;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.Usuario;

public class ConsultaFiltro  extends FiltroListagem{
	
	private Animal animal;
	private Usuario veterinario;
	private Date data ;

	@DisplayName("Animal")
	public Animal getAnimal() {
		return animal;
	}
	@DisplayName("Veterinário")
	public Usuario getVeterinario() {
		return veterinario;
	}
	@DisplayName("Data")
	public Date getData() {
		return data;
	}
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	public void setVeterinario(Usuario veterinario) {
		this.veterinario = veterinario;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
	
	

}
