package seguranca.autenticacao.report;

import java.awt.Image;
import java.text.SimpleDateFormat;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.bean.AntiPulgas;
import seguranca.autorizacao.bean.Anticoncepcional;
import seguranca.autorizacao.bean.Consultas;
import seguranca.autorizacao.bean.Prontuario;
import seguranca.autorizacao.bean.Vacina;
import seguranca.autorizacao.bean.Vermifugos;
import seguranca.autorizacao.service.AnimalService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path = "/autorizacao/relatorio/Prontuario",authorizationModule=ReportAuthorizationModule.class)
public class ProntuarioReport extends ReportController<FiltroListagem> {

	private AnimalService animalService;
	
	public void setAnimalService(AnimalService animalService) {
		this.animalService = animalService;
	}
	
	@Override
	public IReport createReport(WebRequestContext context, FiltroListagem filtro)throws Exception {
		Prontuario p = new Prontuario();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		Animal a = animalService.getHistoricoAnimal(Integer.parseInt(context.getParameter("id")));
		StringBuilder consultas     = new StringBuilder();
		StringBuilder procedimentos = new StringBuilder();
		for (Consultas c: a.getListaConsultas()) {
			consultas.append("Data da Consulta: ").append(format.format(c.getData()));
			consultas.append("\n");
			consultas.append("Veterin�rio: ").append(c.getVeterinario().getNome());
			consultas.append("\n");
			consultas.append("Informa��es coletadas na consulta: ").append(c.getDescricao());
			consultas.append("\n");
			consultas.append("Conduta: ").append(c.getReceituario());
			consultas.append("\n");
			consultas.append("Tipo: ").append(c.getRetorno()!=null && c.getRetorno()?"Retorno":"Consulta");
			consultas.append("\n\n");
		}
		
		if(a.getListaVacinas()!=null && a.getListaVacinas().size()>0)
			procedimentos.append("Vacinas Administradas\n\n");
		for (Vacina v: a.getListaVacinas()) {
			procedimentos.append("Data: ").append(format.format(v.getDtAplicacao()));
			procedimentos.append("\n");
			procedimentos.append("Nome: ").append(v.getProdutoServico().getNome());
			procedimentos.append("\n");
			procedimentos.append("Dose: ").append(v.getDose().getDescricao());
			procedimentos.append("\n");
			procedimentos.append("\n\n");
		}
		if(a.getListaVermifugos()!=null && a.getListaVermifugos().size()>0)
			procedimentos.append("Vermifugos Administrados\n\n");
		for (Vermifugos v : a.getListaVermifugos()) {
			procedimentos.append("Data: ").append(format.format(v.getDtAplicacao()));
			procedimentos.append("\n");
			procedimentos.append("Nome: ").append(v.getProdutoServico().getNome());
			procedimentos.append("\n");
			procedimentos.append("Dose: ").append(v.getDose().getDescricao());
			procedimentos.append("\n\n");
		}
		if(a.getListaAnticoncepcional()!=null && a.getListaAnticoncepcional().size()>0)
			procedimentos.append("Anticoncepcionais Administrados\n\n");
		for (Anticoncepcional v : a.getListaAnticoncepcional()) {
			procedimentos.append("Data: ").append(format.format(v.getDtAplicacao()));
			procedimentos.append("\n");
			procedimentos.append("Nome: ").append(v.getProdutoServico().getNome());
			procedimentos.append("\n");
			procedimentos.append("Dose: ").append(v.getDose().getDescricao());
			procedimentos.append("\n\n");
		}
		if(a.getListaAntiPulgas()!=null && a.getListaAntiPulgas().size()>0)
			procedimentos.append("Anti-Pulgas Administradas\n\n");
		for (AntiPulgas v : a.getListaAntiPulgas()) {
			procedimentos.append("Data: ").append(format.format(v.getDtAplicacao()));
			procedimentos.append("\n");
			procedimentos.append("Nome: ").append(v.getProdutoServico().getNome());
			procedimentos.append("\n");
			procedimentos.append("Dose: ").append(v.getDose().getDescricao());
			procedimentos.append("\n\n");
		}
		
		p.setDescricaoConsultas(consultas.toString());
		p.setDescricaoProcedimentos(procedimentos.toString());
		
		String consulta;
		String conduta;
		//montar lista de prontu�rio, preciso ver um modelo
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Report r = new Report("autorizacao/prontuario");
		r.addParameter("LOGO", image);
		r.addParameter("prontuario", p);
		r.addParameter("animal", a.getNome());
		r.addParameter("dono", a.getDono().getNome());
		
		return r;
	}

}

