package seguranca.autenticacao.report;

import java.awt.Image;
import java.sql.Date;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import seguranca.autenticacao.filter.EntradaSaidaCaixaFiltro;
import seguranca.autorizacao.bean.EntradaSaidaCaixa;
import seguranca.autorizacao.service.EntradaSaidaCaixaService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path = "/autorizacao/relatorio/MovimentoCaixa",authorizationModule=ReportAuthorizationModule.class)
public class MovimentoCaixaReport extends ReportController<EntradaSaidaCaixaFiltro> {

	private EntradaSaidaCaixaService entradaSaidaCaixaService;
	
	public void setEntradaSaidaCaixaService(EntradaSaidaCaixaService entradaSaidaCaixaService) {
		this.entradaSaidaCaixaService = entradaSaidaCaixaService;
	}
	
	@Override
	public IReport createReport(WebRequestContext context, EntradaSaidaCaixaFiltro filtro)throws Exception {
		if(context.getParameter("time")!=null && context.getParameter("time").equals("defaul")){
			filtro.setDtInicio(new Date(System.currentTimeMillis()));
			filtro.setDtFim(new Date(System.currentTimeMillis()));
		}
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<EntradaSaidaCaixa> lista = entradaSaidaCaixaService.getListEntradaSaidaCaixaReport(filtro);
		Report r = new Report("autorizacao/movimentocaixa");
		r.addParameter("LOGO", image);
		r.setDataSource(lista);
		return r;
	}
 

}

