package seguranca.autenticacao.report;

import java.awt.Image;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import seguranca.autenticacao.filter.AnimalFiltro;
import seguranca.autorizacao.bean.Animal;
import seguranca.autorizacao.service.AnimalService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;



@Controller(path = "/autorizacao/relatorio/Animais",authorizationModule=ReportAuthorizationModule.class)
public class AnimaisReport extends ReportController<AnimalFiltro> {

	private AnimalService animalService;
	
	public void setAnimalService(AnimalService animalService) {
		this.animalService = animalService;
	}
	
	@Override
	public IReport createReport(WebRequestContext context, AnimalFiltro filtro)throws Exception {
		List<Animal> lista = animalService.getListAnimalReport(filtro);
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Report r = new Report("autorizacao/animais");
		r.addParameter("LOGO", image);
		r.setDataSource(lista);
		return r;
	}

}

