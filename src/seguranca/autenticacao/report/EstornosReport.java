package seguranca.autenticacao.report;

import java.awt.Image;
import java.io.IOException;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;
import org.nextframework.util.NextImageResolver;

import br.com.biharckgroup.util.CuidadoAnimalUtil;

import seguranca.autenticacao.filter.EntradaSaidaCaixaFiltro;
import seguranca.autorizacao.bean.EntradaSaidaCaixa;
import seguranca.autorizacao.service.EntradaSaidaCaixaService;


@Controller(path = "/autorizacao/relatorio/Estornos",authorizationModule=ReportAuthorizationModule.class)
public class EstornosReport extends ReportController<EntradaSaidaCaixaFiltro> {

	private EntradaSaidaCaixaService entradaSaidaCaixaService;
	public void setEntradaSaidaCaixaService(EntradaSaidaCaixaService entradaSaidaCaixaService) {
		this.entradaSaidaCaixaService = entradaSaidaCaixaService;
	}
	
	@Override
	public IReport createReport(WebRequestContext context, EntradaSaidaCaixaFiltro filtro)throws Exception {
		List<EntradaSaidaCaixa> lista =  entradaSaidaCaixaService.getListEntradaSaidaCaixaReport(filtro);
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Report r = new Report("autorizacao/estorno");
		r.addParameter("LOGO", image);
		r.setDataSource(lista);
		return r;
	}
 

}

