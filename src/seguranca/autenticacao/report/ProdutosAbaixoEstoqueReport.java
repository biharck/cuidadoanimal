package seguranca.autenticacao.report;

import java.awt.Image;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import seguranca.autenticacao.filter.ProdutoServicoFiltro;
import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.ProdutoServico;
import seguranca.autorizacao.service.ProdutoServicoService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path = "/autorizacao/relatorio/Produtoabaixoestoque",authorizationModule=ReportAuthorizationModule.class)
public class ProdutosAbaixoEstoqueReport extends ReportController<ProdutoServicoFiltro> {
	
	private ProdutoServicoService produtoServicoService;
	
	public void setProdutoServicoService(ProdutoServicoService produtoServicoService) {
		this.produtoServicoService = produtoServicoService;
	}
	
	@Override
	public IReport createReport(WebRequestContext context, ProdutoServicoFiltro filtro)throws Exception {
		List<ProdutoServico> lista = produtoServicoService.getListaProdutosAbaixoEstoqueMinimo();
		Integer total = 0;
		for (ProdutoServico produtoServico : lista) {
			for (ItemProduto it : produtoServico.getListaItensProdutos()) {
				total += it.getQuantidadeEntrada()-it.getQuantidadeSaida();
			}
			produtoServico.setQtdRestante(total);
		}
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Report r = new Report("autorizacao/produtoabaixoestoque");
		r.setDataSource(lista);
		r.addParameter("LOGO", image);
		return r;
	}
 

}

