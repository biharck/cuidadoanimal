package seguranca.autenticacao.report;

import java.awt.Image;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import seguranca.autenticacao.filter.ConsultaFiltro;
import seguranca.autorizacao.bean.Consultas;
import seguranca.autorizacao.service.ConsultasService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path = "/autorizacao/relatorio/Consulta",authorizationModule=ReportAuthorizationModule.class)
public class ConsultaReport extends ReportController<ConsultaFiltro> {

	private ConsultasService consultasService;
	
	public void setConsultasService(ConsultasService consultasService) {
		this.consultasService = consultasService;
	}
	@Override
	public IReport createReport(WebRequestContext arg0, ConsultaFiltro filtro)throws Exception {
		List<Consultas> lista = consultasService.getListConsultasReport(filtro);
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Report r = new Report("autorizacao/consulta");
		r.setDataSource(lista);
		r.addParameter("LOGO", image);
		return r;
	}
 

}

