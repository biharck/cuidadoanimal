package seguranca.autenticacao.report;

import java.awt.Image;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import seguranca.autenticacao.filter.ItemProdutoFiltro;
import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.service.ItemProdutoService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path = "/autorizacao/relatorio/Estoque",authorizationModule=ReportAuthorizationModule.class)
public class EstoqueReport extends ReportController<ItemProdutoFiltro> {

	private ItemProdutoService itemProdutoService;

	public void setItemProdutoService(ItemProdutoService itemProdutoService) {
		this.itemProdutoService = itemProdutoService;
	}
	
	@Override
	public IReport createReport(WebRequestContext context, ItemProdutoFiltro filtro)throws Exception {
		List<ItemProduto> lista = itemProdutoService.getListItemProdutoReport(filtro);
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Report r = new Report("autorizacao/estoque");
		r.addParameter("LOGO", image);
		r.setDataSource(lista);
		return r;
	}
 

}

