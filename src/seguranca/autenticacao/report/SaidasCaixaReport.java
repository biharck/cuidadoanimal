package seguranca.autenticacao.report;

import java.awt.Image;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path = "/autorizacao/relatorio/SaidasCaixa",authorizationModule=ReportAuthorizationModule.class)
public class SaidasCaixaReport extends ReportController<FiltroListagem> {

	@Override
	public IReport createReport(WebRequestContext arg0, FiltroListagem filtroListagem)throws Exception {
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Report r = new Report("autorizacao/saidacaixa");
		r.addParameter("LOGO", image);
		return r;
	}
 

}

