package seguranca.autenticacao.report;

import java.awt.Image;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import seguranca.autenticacao.filter.ContaPagarFiltro;
import seguranca.autorizacao.bean.ContaPagar;
import seguranca.autorizacao.service.ContaPagarService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path = "/autorizacao/relatorio/ContaPagar",authorizationModule=ReportAuthorizationModule.class)
public class ContaPagarReport extends ReportController<ContaPagarFiltro> {

	private ContaPagarService contaPagarService;
	
	public void setContaPagarService(ContaPagarService contaPagarService) {
		this.contaPagarService = contaPagarService;
	}
	@Override
	public IReport createReport(WebRequestContext context, ContaPagarFiltro filtro)throws Exception {
		List<ContaPagar> lista = contaPagarService.getListContaPagarReport(filtro);
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Report r = new Report("autorizacao/contapagar");
		r.setDataSource(lista);
		r.addParameter("LOGO", image);
		return r;
	}
}

