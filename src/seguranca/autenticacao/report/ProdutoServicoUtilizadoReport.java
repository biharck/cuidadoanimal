package seguranca.autenticacao.report;

import java.awt.Image;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import seguranca.autenticacao.filter.ProdutoServicoFiltro;
import seguranca.autorizacao.bean.ServicosPrestados;
import seguranca.autorizacao.service.ServicosPrestadosService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path = "/autorizacao/relatorio/ProdutosServicosUtilizados",authorizationModule=ReportAuthorizationModule.class)
public class ProdutoServicoUtilizadoReport extends ReportController<ProdutoServicoFiltro> {

	private ServicosPrestadosService servicosPrestadosService;
	public void setServicosPrestadosService(ServicosPrestadosService servicosPrestadosService) {
		this.servicosPrestadosService = servicosPrestadosService;
	}
	
	@Override
	public IReport createReport(WebRequestContext arg0, ProdutoServicoFiltro filtro)throws Exception {
		List<ServicosPrestados> lista = servicosPrestadosService.getListServicosPrestadosReport(filtro);
		Report r = new Report("autorizacao/produtoservicoutilizado");
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}
		r.setDataSource(lista);
		r.addParameter("LOGO", image);
		return r;
	}
 

}

