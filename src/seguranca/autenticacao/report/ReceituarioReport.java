package seguranca.autenticacao.report;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import seguranca.autorizacao.bean.Consultas;
import seguranca.autorizacao.bean.HistoricoClinico;
import seguranca.autorizacao.service.ConsultasService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path = "/autorizacao/relatorio/Receituario",authorizationModule=ReportAuthorizationModule.class)
public class ReceituarioReport extends ReportController<FiltroListagem> {

	private ConsultasService consultasService;
	public void setConsultasService(ConsultasService consultasService) {
		this.consultasService = consultasService;
	}
	
	@Override
	public IReport createReport(WebRequestContext context, FiltroListagem filtro)throws Exception {
		List<HistoricoClinico> listaHistoricoClinico = new ArrayList<HistoricoClinico>();
		List<Consultas> listaConsultas = consultasService.getConsultasById(Integer.parseInt(context.getParameter("id")));
		String conduta = "";
		for (Consultas consultas : listaConsultas) {
			conduta += consultas.getReceituario()==null?"":consultas.getReceituario();
			listaHistoricoClinico.add(new HistoricoClinico(conduta,"", consultas.getData(),consultas.getIdConsultas()));
		}
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Report r = new Report("autorizacao/receituario");
		r.setDataSource(listaHistoricoClinico);
		r.addParameter("animal",listaConsultas.get(0).getAnimal().getNome());
		r.addParameter("veterinario",listaConsultas.get(0).getVeterinario().getNome());
		r.addParameter("data",listaConsultas.get(0).getData());
		r.addParameter("LOGO", image);
		return r;
	}

}

