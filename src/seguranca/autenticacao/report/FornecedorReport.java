package seguranca.autenticacao.report;

import java.awt.Image;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import seguranca.autenticacao.filter.FornecedorFiltro;
import seguranca.autorizacao.bean.Fornecedor;
import seguranca.autorizacao.service.FornecedorService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path = "/autorizacao/relatorio/Fornecedor",authorizationModule=ReportAuthorizationModule.class)
public class FornecedorReport extends ReportController<FornecedorFiltro> {

	private FornecedorService fornecedorService;
	
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	@Override
	public IReport createReport(WebRequestContext context, FornecedorFiltro filtro)throws Exception {
		List<Fornecedor> lista = fornecedorService.getListFornecedorReport(filtro);
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Report r = new Report("autorizacao/fornecedor");
		r.addParameter("LOGO", image);
		r.setDataSource(lista);
		return r;
	}
 

}

