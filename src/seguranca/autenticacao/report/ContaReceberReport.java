package seguranca.autenticacao.report;

import java.awt.Image;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import seguranca.autenticacao.filter.ContaReceberFiltro;
import seguranca.autorizacao.bean.ContaReceber;
import seguranca.autorizacao.service.ContaReceberService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path = "/autorizacao/relatorio/ContasReceber",authorizationModule=ReportAuthorizationModule.class)
public class ContaReceberReport extends ReportController<ContaReceberFiltro> {

	private ContaReceberService contaReceberService;
	
	public void setContaReceberService(ContaReceberService contaReceberService) {
		this.contaReceberService = contaReceberService;
	}
	@Override
	public IReport createReport(WebRequestContext context, ContaReceberFiltro filtro)throws Exception {
		List<ContaReceber> lista = contaReceberService.getListContaReceberReport(filtro);
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Report r = new Report("autorizacao/contareceber");
		r.addParameter("LOGO", image);
		r.setDataSource(lista);
		return r;
	}
 

}

