package seguranca.autenticacao.report;

import java.awt.Image;
import java.util.List;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

import seguranca.autenticacao.filter.ClienteFiltro;
import seguranca.autorizacao.bean.Cliente;
import seguranca.autorizacao.service.ClienteService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path = "/autorizacao/relatorio/Cliente",authorizationModule=ReportAuthorizationModule.class)
public class ClienteReport extends ReportController<ClienteFiltro> {

	private ClienteService clienteService;
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	@Override
	public IReport createReport(WebRequestContext arg0, ClienteFiltro filtro)throws Exception {
		List<Cliente> lista = clienteService.getListClienteReport(filtro);
		Image image = null;
		try {
			image = CuidadoAnimalUtil.getLogoEmpresa();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Report r = new Report("autorizacao/cliente");
		r.addParameter("LOGO", image);
		r.setDataSource(lista);
		return r;
	}
 

}

