package seguranca.autenticacao.controller;

import java.sql.Date;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.nextframework.authorization.User;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MessageType;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.nextframework.view.menu.MenuTag;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autorizacao.bean.Empresa;
import seguranca.autorizacao.bean.Usuario;
import seguranca.autorizacao.dao.EmpresaDAO;
import seguranca.autorizacao.dao.NextAuthorizationDAO;
import seguranca.autorizacao.service.ArquivoService;
import seguranca.autorizacao.service.UsuarioService;


/**
 * Controller que far� o login do usu�rio na aplica��o.
 * Se o login for efetuado com sucesso ir� redirecionar para /secured/Index
 */
@Controller(path="/autenticacao/login")
public class LoginController extends MultiActionController {
private static final String AFTER_LOGIN_GO_TO = "/";
	
	NextAuthorizationDAO authorizationDAO;
	UsuarioService us;
	EmpresaDAO empresaDAO;
	ArquivoService arquivoService;
	
	public void setAuthorizationDAO(NextAuthorizationDAO authorizationDAO) {
		this.authorizationDAO = authorizationDAO;
	}
	public void setUs(UsuarioService us) {
		this.us = us;
	}
	public void setEmpresaDAO(EmpresaDAO empresaDAO) {
		this.empresaDAO = empresaDAO;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	/**
	 * Action que envia para a p�gina de login
	 */
	@DefaultAction
	public ModelAndView doPage(WebRequestContext request, Usuario usuario){
		return new ModelAndView("login", "usuario", usuario);
	}
	
//	/**
//	 * Efetua o login do usu�rio
//	 */
//	public ModelAndView doLogin(WebRequestContext request, Usuario usuario){
//		String login = usuario.getLogin();
//		//se foi passado o login na requisi��o, iremos verificar se o usu�rio existe e a senha est� correta
//		if(login != null){
//			//buscamos o usu�rio do banco pelo login
//			User userByLogin = authorizationDAO.findUserByLogin(login);
//			StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
//			Date dataCadastro = us.findUserByLogin(userByLogin.getLogin()).getDtCadastro();
//			if(dataCadastro!=null){
//				Date dataLimite   = new Date(dataCadastro.getTime()+(1000*24*60*60)*15);
//
//				//verificar se � anterior a 15 dias de teste.
//				if(new Date(System.currentTimeMillis()).after(dataLimite)){
//					request.addMessage("O per�odo de teste se expirou, entre em contato conosco e adquira sua licen�a �nica!", MessageType.ERROR);
//				}
//				// se o usu�rio existe e a senha est� correta
//				else if(userByLogin != null && encryptor.checkPassword(usuario.getPassword(), userByLogin.getPassword())){
//					//Setando o atributo de se��o USER fazemos o login do usu�rio no sistema. 
//					request.setUserAttribute("USER", userByLogin);
//					
//					//Limpamos o cache de permiss�es o menu.
//					//O menu ser� refeito levando em considera��o as permiss�es do usu�rio
//					request.setUserAttribute(MenuTag.MENU_CACHE_MAP, null);
//					return new ModelAndView("redirect:"+AFTER_LOGIN_GO_TO);				
//				}
//			}
//		}
//
//		request.addMessage("Login e/ou senha inv�lidos", MessageType.ERROR);
//		//limpar o campo senha, e enviar para a tela de login j� que o processo falhou
//		usuario.setSenha(null);
//		return doPage(request, usuario);
//	}
	
	/**
	 * Efetua o login do usu�rio
	 */
	public ModelAndView doLogin(WebRequestContext request, Usuario usuario){
		String login = usuario.getLogin();
		//se foi passado o login na requisi��o, iremos verificar se o usu�rio existe e a senha est� correta
		if(login != null){
			//buscamos o usu�rio do banco pelo login
			User userByLogin = authorizationDAO.findUserByLogin(login);
			StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
				// se o usu�rio existe e a senha est� correta
			if(userByLogin != null && encryptor.checkPassword(usuario.getPassword(), userByLogin.getPassword())){
				//Setando o atributo de se��o USER fazemos o login do usu�rio no sistema. 
				request.setUserAttribute("USER", userByLogin);
				
				//Limpamos o cache de permiss�es o menu.
				//O menu ser� refeito levando em considera��o as permiss�es do usu�rio
				request.setUserAttribute(MenuTag.MENU_CACHE_MAP, null);
				Empresa e = empresaDAO.getEmpresa();
				try {	
					arquivoService.loadAsImage(e.getFoto());
					if(e.getFoto().getCdfile() != null){
						DownloadFileServlet.addCdfile(request.getSession(), e.getFoto().getCdfile());
						request.getSession().setAttribute("exibeFotoEmpresa", true);
						request.getSession().setAttribute("idFotoEmpresa", e.getFoto().getCdfile());
					}
				} catch (Exception ex) {
					request.getSession().setAttribute("exibeFotoEmpresa", false);
				}
				return new ModelAndView("redirect:"+AFTER_LOGIN_GO_TO);				
			}
		}

		request.addMessage("Login e/ou senha inv�lidos", MessageType.ERROR);
		//limpar o campo senha, e enviar para a tela de login j� que o processo falhou
		usuario.setSenha(null);
		return doPage(request, usuario);
	}

}