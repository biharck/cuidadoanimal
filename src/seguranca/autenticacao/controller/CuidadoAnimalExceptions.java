package seguranca.autenticacao.controller;

public class CuidadoAnimalExceptions extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CuidadoAnimalExceptions() {
		super();
	}

	public CuidadoAnimalExceptions(String message, Throwable cause) {
		super(message, cause);
	}

	public CuidadoAnimalExceptions(String message) {
		super(message);
	}

	public CuidadoAnimalExceptions(Throwable cause) {
		super(cause);
	}




}
