package seguranca.autenticacao.controller;

import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

@Controller(path="/autenticacao/403")
public class Erro403Controller extends MultiActionController {
	@DefaultAction
	public ModelAndView doPage(WebRequestContext request){
		return new ModelAndView("403");
	}
}