package seguranca;

import java.util.HashMap;
import java.util.Map;

import org.nextframework.authorization.AuthorizationDAO;
import org.nextframework.controller.Controller;
import org.nextframework.core.standard.Next;
import org.nextframework.core.standard.NextStandard;

import seguranca.autorizacao.bean.Papel;
import seguranca.autorizacao.bean.PapelUsuario;
import seguranca.autorizacao.bean.Usuario;
import seguranca.autorizacao.controller.PermissaoController;
import seguranca.autorizacao.dao.PapelDAO;
import seguranca.autorizacao.dao.PapelUsuarioDAO;
import seguranca.autorizacao.dao.UsuarioDAO;

public class Carga {
	
	
	//Para rodar essa classe � necess�rio adicionar a biblioteca log4j-1.2.15.jar ao classpath
	//Ela nao foi incluida pois da conflito com as libs do jboss
	public static void main(String[] args) {
		insereAdministrador();
	}
	private static void insereAdministrador(){
		NextStandard.createNextContext();
		
		UsuarioDAO usuarioDAO = Next.getObject(UsuarioDAO.class);
		PapelDAO papelDAO = Next.getObject(PapelDAO.class);
		PapelUsuarioDAO papelUsuarioDAO = Next.getObject(PapelUsuarioDAO.class);
		
		Usuario usuario = new Usuario();
		usuario.setNome("Administrador");
		usuario.setLogin("admin");
		usuario.setSenha("admin");

		Papel papel = new Papel();
		papel.setNome("Admiministrador");
		papel.setDescricao("Administrador do sistema");
		
		PapelUsuario papelUsuario = new PapelUsuario();
		papelUsuario.setUsuario(usuario);
		papelUsuario.setPapel(papel);
		
		usuarioDAO.saveOrUpdate(usuario);
		papelDAO.saveOrUpdate(papel);
		papelUsuarioDAO.saveOrUpdate(papelUsuario);
		
		AuthorizationDAO authorizationDAO = Next.getObject(AuthorizationDAO.class);
		
		String permissionControllerPath = PermissaoController.class.getAnnotation(Controller.class).path()[0];
		Map<String, String> permissionMap = new HashMap<String, String>();
		permissionMap.put("execute", "true");
		authorizationDAO.savePermission(permissionControllerPath, papel, permissionMap);
		
		System.out.println("Usu�rio Admin configurado com sucesso!");
		System.out.println("Fa�a login como Administrador, acesse a tela de permiss�o e configure as permiss�es do Administrador");		
	}
}
