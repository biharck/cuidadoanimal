package seguranca;


import java.sql.Timestamp;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import seguranca.autorizacao.bean.Usuario;
import seguranca.autorizacao.service.UsuarioService;
import br.com.biharckgroup.util.CuidadoAnimalUtil;


@Controller(path = "/pub/Enviasenha")
public class EnviaSenha extends MultiActionController {

	private UsuarioService usuarioService;
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request) {
		return new ModelAndView("enviasenha");
	}

	@Action("pesquisar")
	public ModelAndView pesquisarDados(WebRequestContext context) {

		String email = String.valueOf(context.getParameter("email"));
		Usuario u = usuarioService.findUserEmail(email);
		if (u == null) {
			context.setAttribute("MSGEmailInvalid", true);
			return new ModelAndView("enviasenha");
		} else {
			
			Timestamp dtHoraAtual = new Timestamp(System.currentTimeMillis());
			u.setSenhaant(u.getSenha());
			u.setDtrequisicao(dtHoraAtual);
		    StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();

		    u.setSenha(encryptor.encryptPassword(CuidadoAnimalUtil.randon()));
		    u.setConfirmasenha(u.getSenha());
			u.setAtivado(false);
			usuarioService.saveOrUpdate(u);
			
			String mail = usuarioService.encrypta(u.getEmail());
			String pass = usuarioService.encrypta(u.getSenha());
			StringBuilder url = new StringBuilder("http://www.orionx.com.br/CuidadoAnimal/pub/alterasenhausuarioemail?pwk_tgrd="+mail+"&svgw="+pass);
			try {
				CuidadoAnimalUtil.enviaEmailRecuperaSenha(u, null, url.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			context.setAttribute("exibeMSG", true);
		}
		return new ModelAndView("enviasenha");
	}

	/**
	 * M�todo utilizado para validar o email informado pelo usu�rio.
	 * 
	 * @param request
	 * @return ModelAndView
	 */
	public ModelAndView valida(WebRequestContext request) {
		return index(request);
	}
}
