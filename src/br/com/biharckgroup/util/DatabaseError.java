package br.com.biharckgroup.util;

import java.sql.SQLException;

import org.springframework.dao.DataIntegrityViolationException;


public class DatabaseError {
	
	public static Boolean isKeyPresent(DataIntegrityViolationException e,String key) {
		SQLException sqlException = (SQLException)e.getCause();
		SQLException nextException = sqlException.getNextException();
		
		String message = nextException==null?sqlException.getMessage():nextException.getMessage();
		if (message != null && !"".equals(message)) message = message.toUpperCase();			
		if (key != null && !"".equals(key)) key = key.toUpperCase();
		
		return message.contains(key);
	}
}	
