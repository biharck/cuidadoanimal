package br.com.biharckgroup.util;

import org.nextframework.core.web.NextWeb;

public class CuidadoAnimalException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int NO_UPDATE = 1;
	
	private int errorCode = 0;

	public CuidadoAnimalException() {
		super();
	}

	public CuidadoAnimalException(String message, Throwable cause) {
		super(message, cause);
	}

	public CuidadoAnimalException(String message) {
		super(message);
		NextWeb.getRequestContext().setAttribute("msgError", message);
	}

	public CuidadoAnimalException(Throwable cause) {
		super(cause);
	}
	
	public int getErrorCode() {
		return errorCode;
	}
	public CuidadoAnimalException setErrorCode(int errorCode) {
		this.errorCode = errorCode;
		return this;
	}
	
}
