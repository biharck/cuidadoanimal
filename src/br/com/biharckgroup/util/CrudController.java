package br.com.biharckgroup.util;

import java.lang.reflect.Method;

import javax.persistence.Id;

import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.util.ReflectionCache;
import org.nextframework.util.ReflectionCacheFactory;
import org.nextframework.util.Util;
import org.nextframework.view.ajax.View;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

public class CrudController<FILTRO extends FiltroListagem, FORMBEAN, BEAN> extends org.nextframework.controller.crud.CrudController<FILTRO, FORMBEAN, BEAN>{
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request,FORMBEAN form) {
		request.setAttribute("isEditar",true);
		request.setAttribute("isCriar",true);
		return new ModelAndView("crud/"+getBeanName()+"Entrada");
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, FORMBEAN form)throws CrudException {
		return super.doCriar(request, form);
	}

	@Override
	protected void excluir(WebRequestContext request, BEAN bean) throws Exception {
		String itens = request.getParameter("itenstodelete");
		if(itens != null && !itens.equals("")){
			ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
			Method[] methods = reflectionCache.getMethods(beanClass);
			Method methodId = null;
			for (Method method : methods) {
				if(reflectionCache.isAnnotationPresent(method, Id.class)){
					methodId = method;
					break;
				}
			}
			
			String propertyFromGetter = Util.beans.getPropertyFromGetter(methodId.getName());
			methodId = Util.beans.getSetterMethod(beanClass, propertyFromGetter);
			String[] codes = itens.split(",");
			for (String code : codes) {
				if(!"".equals(code) && code != null){
					BEAN obj = beanClass.newInstance();
					methodId.invoke(obj, Integer.parseInt(code));
					if(validaExcluirEmMassa(request, obj)){
							try {
								genericService.delete(obj);
							} catch (DataIntegrityViolationException e) {
								throw new CuidadoAnimalException("O(s) registro(s) n�o pode(m) ser exclu�do(s), j� possui(em) refer�ncias em outros registros do sistema.");
							}
					}
				}
			}
			
		} else {
			try {
				super.excluir(request, bean);
			} catch (DataIntegrityViolationException da) {
				throw new CuidadoAnimalException("O(s) registro(s) n�o pode(m) ser exclu�do(s), j� possui(em) refer�ncias em outros registros do sistema.");
			}	
		}
		request.addMessage("Registro(s) exclu�do(s) com sucesso.");
	}
		
	@Override
	public ModelAndView doEditar(WebRequestContext request, FORMBEAN form)throws CrudException {
		return super.doEditar(request, form);
	}
	
	

	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, BEAN bean) {
		request.getServletResponse().setContentType("text/html");
		if("true".equals(request.getParameter("fromInsertOne"))){
			Object id = Util.beans.getId(bean);
			String description = Util.strings.toStringDescription(bean);
			View.getCurrent()
				.println("<html>" +
						"<script language=\"JavaScript\" src=\""+request.getServletRequest().getContextPath()+"/resource/js/util.js\"></script>" +
						"<script language=\"JavaScript\">selecionar('"+id+"', '"+description+"', true);</script>" +
						"</html>");
			return null;
		} else {
			try {
				request.setAttribute(CONSULTAR, true);
				return doEditar(request, beanToForm(bean));
			} catch (CrudException e) {
				return null;
			}
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, BEAN bean) throws Exception {
		
		genericService.saveOrUpdate(bean);
		request.addMessage("Registro salvo com sucesso.");
	}
	

	public Boolean validaExcluirEmMassa(WebRequestContext request, BEAN bean) {
		return true;
	}

}
