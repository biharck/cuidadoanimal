package br.com.biharckgroup.util;



import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import seguranca.autorizacao.bean.Arquivo;


/**
 * Classe para redimensionamento de imagens.
 * @author Pedro Gon�alves
 *
 */
public class RedimensionaImagem{
	
	/**
	 * M�todo para redimensionar imagens.
	 * 
	 * @see #calculaXy(int, int, int, int)
	 * @param arquivo
	 * @param redimensionax
	 * @param redimensionay
	 * @return
	 * @throws IOException
	 * @author Pedro Gon�alves
	 */
	public Arquivo redimensionaImagem(Arquivo arquivo,int redimensionax, int redimensionay) throws IOException {
		
		if (arquivo.getTipoconteudo().matches(".*gif.*") || arquivo.getTipoconteudo().matches(".*png.*") || arquivo.getTipoconteudo().matches(".*jpe?g.*")) {
			ByteArrayInputStream entrada = new ByteArrayInputStream(arquivo.getContent());
			BufferedImage imagem = ImageIO.read(entrada);
			entrada.close();
			
			int largura;
			int altura;	
			
			int larguraImagem = imagem.getWidth(null);
			int alturaImagem = imagem.getHeight(null);
			
			Pontos pontos = this.calculaXy(redimensionax, redimensionay, larguraImagem, alturaImagem);
			largura = pontos.getX();
			altura = pontos.getY();
			
			BufferedImage imagemNova;
			if (larguraImagem <= largura && alturaImagem <= altura) {
				imagemNova = imagem;
			} else {

				if (larguraImagem >= alturaImagem) {
					altura = -1;
				}
				else {
					largura = -1;
				}

				// Redimensiona a foto.
				Image imagemTemp = imagem.getScaledInstance(largura, altura, Image.SCALE_DEFAULT);
				imagemNova = new BufferedImage(imagemTemp.getWidth(null), imagemTemp.getHeight(null), BufferedImage.TYPE_INT_RGB);
				Graphics2D graphics2D = imagemNova.createGraphics();
				graphics2D.drawImage(imagemTemp, 0, 0, null);
				graphics2D.dispose();
				
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				BufferedOutputStream saida = new BufferedOutputStream(byteArrayOutputStream);
				

				if (arquivo.getTipoconteudo().matches(".*png.*")) {
					ImageIO.write(imagemNova, "png", saida);
				}
				else if (arquivo.getTipoconteudo().matches(".*jpe?g.*")) {
					ImageIO.write(imagemNova, "jpg", saida);
				}
				else if (arquivo.getTipoconteudo().matches(".*gif.*")) {
					ImageIO.write(imagemNova, "gif", saida);
				} else {
					return arquivo;
				}

				byte[] conteudo = byteArrayOutputStream.toByteArray();
				arquivo.setContent(conteudo);
				arquivo.setSize((long)conteudo.length);
				
				return arquivo;
			}
			
		}
		return null;
		
	}
	
	/**
	 * x : tamanho para redimensionamento
	 * ix: tamanho original 
	 * y : tamanho para redimensionamento
	 * iy: tamanho original 
	 * 
	 * @param x
	 * @param y
	 * @param ix
	 * @param iy
	 * @return
	 */
	public Pontos calculaXy(int x,int y,int ix,int iy) {
		Pontos pontos = new Pontos();
		if ( ix > x || iy > y) {
			int nx,ny; 			
			nx = (ix*y) / iy;
			ny = (iy*x) / ix ;

			if ( nx > x || ny > y) {
				if (nx > x) {
					nx = x;
					ny = (x*iy) / ix;
				} else if (ny > y) {
					ny = y;
					nx = (y*ix) / iy;
				}
			}
			pontos.setX(nx);
			pontos.setY(ny);
			
		} else {
			pontos.setX(x);
			pontos.setY(y);
		}
		return pontos;
		
	}
	
	public class Pontos {
		int x;
		int y;
		
		public int getX() {
			return x;
		}
		public int getY() {
			return y;
		}
		public void setX(int x) {
			this.x = x;
		}
		public void setY(int y) {
			this.y = y;
		}
	}	
}
