package br.com.biharckgroup.util;



import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EmailUtil {
	private String remetente;
	private String login;
	private String senha;
	private String smtp;
	private String porta;
	private String autenticacao;
	private String destinatario;
	private String assunto;
	private String corpo;
	protected Multipart multipart;
	protected MimeMessage message;

	public EmailUtil(String remetente, String login, String senha, String smtp,
			String porta, String autenticacao, String destinatario,
			String assunto, String corpo) {
		this.remetente = remetente;
		this.login = login;
		this.senha = senha;
		this.smtp = smtp;
		this.porta = porta;
		this.autenticacao = autenticacao;
		this.destinatario = destinatario;
		this.assunto = assunto;
		this.corpo = corpo;		
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getCorpo() {
		return corpo;
	}

	public void setCorpo(String corpo) {
		this.corpo = corpo;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getRemetente() {
		return remetente;
	}

	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSmtp() {
		return smtp;
	}

	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}

	public String getPorta() {
		return porta;
	}

	public void setPorta(String porta) {
		this.porta = porta;
	}

	public String getAutenticacao() {
		return autenticacao;
	}

	public void setAutenticacao(String autenticacao) {
		this.autenticacao = autenticacao;
	}

	public void enviar() throws AddressException, MessagingException {
		Properties p = new Properties();
		p.put("mail.smtp.host", getSmtp());
		p.put("mail.smtp.port", getPorta());
		p.put("mail.smtp.auth", getAutenticacao());
		Authenticator auth = new UsuarioSenhaEmail(getLogin(), getSenha());
		Session session = Session.getInstance(p, auth);

		MimeMessage mimeMsg = new MimeMessage(session);
		mimeMsg.setFrom(new InternetAddress(getRemetente()));
		mimeMsg.setRecipient(Message.RecipientType.TO, new InternetAddress(getDestinatario()));
		mimeMsg.setSentDate(new Date());
		mimeMsg.setSubject(getAssunto());
		mimeMsg.setText(getCorpo());
		mimeMsg.setContent(multipart);
		
		Transport.send(mimeMsg);
	}

	public static void enviar(String remetente, String login, String senha,
			String smtp, String porta, String autenticacao,
			String destinatario, String assunto, String corpo)
			throws AddressException, MessagingException {
		new EmailUtil(remetente, login, senha, smtp, porta, autenticacao,
				destinatario, assunto, corpo).enviar();
	}
	public void addPart(MimeBodyPart part) throws Exception {
		multipart = new MimeMultipart();
		multipart.addBodyPart(part);
	}
	
	public EmailUtil addHtmlText(String text) throws Exception {
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart = new MimeBodyPart();
		messageBodyPart.setText(text);
		messageBodyPart.setHeader("Content-Type", "text/html");	
		addPart(messageBodyPart);
		return this;
	}

}
