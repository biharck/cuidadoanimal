package br.com.biharckgroup.util;

public enum EnumMes {
	
	JAN("jan",0),
	FEB("feb",1),
	MAR("mar",2),
	APR("apr",3),
	MAI("mai",4),
	JUN("jun",5),
	JUL("jul",6),
	AUG("aug",7),
	SEP("sep",8),
	OCT("oct",9),
	NOV("nov",10),
	DEC("dec",11);
	
	private String mes;
	private Integer idMes;
	
	private EnumMes(String mes, Integer idMes){
		this.idMes= idMes;
		this.mes = mes;
	}
	
	public String getMes() {
		return mes;
	}
	public Integer getIdMes() {
		return idMes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public void setIdMes(Integer idMes) {
		this.idMes = idMes;
	}
	
}
