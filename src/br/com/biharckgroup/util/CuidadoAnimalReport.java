package br.com.biharckgroup.util;

import java.awt.Image;
import java.io.IOException;

import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;
import org.nextframework.util.NextFormater;
import org.nextframework.util.NextImageResolver;

/**
 *
 * @param <FILTRO>
 */
public abstract class CuidadoAnimalReport<FILTRO> extends ReportController<FILTRO> {

	private NextImageResolver nextImageResolver;
	
	public void setNextImageResolver(NextImageResolver nextImageResolver) {
		this.nextImageResolver = nextImageResolver;
	}
	
	/**
	 * Gera um novo relat�rio, e seta os par�metros default por requisi��o.
	 * @see #configurarParametrosWms(WebRequestContext, Report)
	 */
	@Override
	public IReport createReport(WebRequestContext request, FILTRO command)
			throws Exception {

		org.nextframework.report.Report report = (Report) createReportPlanoCuidados(request, command);
		
//		configurarParametrosPlanoCuidados(request, report);
		return report;
	}
	
	/**
	 * Seta os par�metros padr�es que ser�o enviados ao relat�rio, como A logo, o neo formater entre outros.
	 * 
	 * @param request
	 * @param report
	 */
	protected void configurarParametrosPlanoCuidados(WebRequestContext request, Report report) {
		String nome = "";
		
		Image image = null;
		try {
			image = nextImageResolver.getImage("/images/header.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
		report.addParameter("LOGO", image);
		report.addParameter("NEOFORMATER", NextFormater.getInstance());
		report.addParameter("TITULO", getTitulo());
		report.addParameter("USUARIO", nome);
	}
	
	public abstract IReport createReportPlanoCuidados(WebRequestContext request, FILTRO filtro) throws Exception;

	public abstract String getTitulo();

}
