package br.com.biharckgroup.util;

import java.awt.Image;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nextframework.authorization.User;
import org.nextframework.core.web.NextWeb;
import org.nextframework.util.CollectionsUtil;

import seguranca.autorizacao.bean.Calendar;
import seguranca.autorizacao.bean.ItemProduto;
import seguranca.autorizacao.bean.Pessoa;
import seguranca.autorizacao.bean.Usuario;
import seguranca.autorizacao.service.ArquivoService;
import seguranca.autorizacao.service.EmpresaService;
import seguranca.autorizacao.service.ItemProdutoService;

public class CuidadoAnimalUtil {
	/**
	 * 
	 *<p>M�todo respons�vel em receber uma string contendo o caminho de um dado e retornar<br>
	 *o id.
	 *
	 *
	 * @param values valores das requisi��es
	 * @return {@link Integer}id
	 * 
	 * @author Biharck Ara�jo
	 * @since Sep 17, 2008
	 */
	public static Integer returnIdWithParameters(String ...values) {
		Pattern pattern = Pattern.compile("\\d");
		StringBuilder result = new StringBuilder();
		Matcher matcher = pattern.matcher(values[0]);
		while(matcher.find())
			result.append(matcher.group());
		return Integer.parseInt(result.toString());
	}
	

	/**
	 * M�todo para pegar o usu�rio logado no sistema 
	 * Alterado para retornar somente o nome do usuario
	 * @return user
	 * @author Biharck
	 */
	public static Usuario getPessoaLogada(){
		User user = NextWeb.getRequestContext().getUser();
		if(user instanceof Usuario){
			Usuario pessoa=(Usuario) user;
			return pessoa;
		} else {
			return null;
		}
	}
	
	
	public static String returnCalendarJQuery(Calendar c){
		Integer id = c.getIdCalendar(),monthEnd = 0,monthStart = 0;
		
		for (EnumMes em : EnumMes.values()) {
			if(c.getMonthEnd().toUpperCase().equals(em.getMes().toUpperCase())){
				monthEnd = em.getIdMes();
			}
			if(c.getMonthStart().toUpperCase().equals(em.getMes().toUpperCase())){
				monthStart = em.getIdMes();
			}
		}
		Integer dayStart= c.getDayStart(), yearStart= c.getYearStart(), hourStart= c.getHourStart(), minStart= c.getMinStart();
		Integer dayEnd= c.getDayEnd(),yearEnd= c.getYearEnd(), hourEnd= c.getHourEnd(), minEnd= c.getMinEnd();
		
		String description= c.getTarefa();
		String titulo = c.getTitulo();
		StringBuilder calendar = new StringBuilder("");
		
		calendar
				.append("{")
				.append("   'id':"+id+",")
				.append("   'start': new Date("+yearStart+","+monthStart+","+dayStart+","+hourStart+","+minStart+"),")
				.append("   'end': new Date("+yearEnd+","+monthEnd+","+dayEnd+","+hourEnd+","+minEnd+"),")
				.append("   'title':'"+titulo+"',")
				.append("   'body':'"+description+"',")
				.append("   'particular':'"+true+"'")
				.append("},");
		return calendar.toString();
	}
	
	
	public static Integer getMonth(Calendar c){
		for (EnumMes em : EnumMes.values()) {
			if(c.getMonthStart().toUpperCase().equals(em.getMes().toUpperCase())){
				return em.getIdMes();
			}
		}
		return null;
	}
	/**
	 * 
	 * @return retorna um valor randon
	 */
	public static String randon() {
		String valor = "";
		for (int i = 0; i < 50; i++) {
			if (1 + (int) (Math.random() * 10) >= 5)
				valor = valor + (char) (Math.random() * 26 + 65);
			else
				valor = valor + (int) (Math.random() * 10);
		}
		return valor;
	}
	/**
	 * 
	 * @param pessoa
	 * @param template
	 * @param url
	 * @throws Exception
	 */
	public static void enviaEmailRecuperaSenha(Usuario u, String template,
			String url) throws Exception {
		try {
			template = new TemplateManager("WEB-INF/template/templateEnvioEmail.tpl")
					.assign("nome",	u.getNome()).assign("email", u.getEmail())
					.assign("link", url).getTemplate();
			EmailUtil em = new EmailUtil("contato@orionx.com.br",
					"contato@orionx.com.br", "30081986",
					"smtp.orionx.com.br", "587", "true", u.getEmail(),
					"Solicita��o de Recadastramento de Nova Senha.", "");
			
			em.addHtmlText(template);
			em.enviar();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param pessoa
	 * @param template
	 * @param url
	 * @throws Exception
	 */
	public static void enviaEmailInstrucoes(Pessoa p, String template) throws Exception {
		try {
			template = new TemplateManager("WEB-INF/template/templateInstrucoes.tpl")
					.assign("nome",	p.getUsuario().getLogin()).assign("senha", p.getObservacao())
					.assign("link", "www.orionx.com.br/CuidadoAnimal").getTemplate();
			EmailUtil em = new EmailUtil("contato@orionx.com.br",
					"contato@orionx.com.br", "30081986",
					"smtp.orionx.com.br", "587", "true", p.getEmail(),
					"Instru��es sobre utiliza��o", "");
			
			em.addHtmlText(template);
			em.enviar();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * <p>M�todo que retorna a quantidade de produtos restantes
	 * @param lit {@link List}{@link ItemProduto}
	 * @return quantidade de produtos restantes
	 */
	public static Integer returnProdutosRestante(List<ItemProduto> lit){
		Integer entrada = 0;
		Integer saida = 0;
		if(lit!=null && lit.size()>0){
			if(lit.get(0).getQuantidadeEntrada()==null){
				String ids = CollectionsUtil.listAndConcatenate(lit, "idItemProduto", ",");
				lit = ItemProdutoService.getInstance().getListByIds(ids); 
			}
			for (ItemProduto itemProduto : lit) {
				entrada += itemProduto.getQuantidadeEntrada();
				saida   += itemProduto.getQuantidadeSaida();
			}
		}
		return entrada - saida;
	}
	
	public static Image getLogoEmpresa(){
		return ArquivoService.getInstance().loadAsImage(EmpresaService.getInstance().getEmpresa().getFoto());
	}
}
