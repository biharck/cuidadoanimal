package br.com.biharckgroup.util;


import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

class UsuarioSenhaEmail extends Authenticator {

	private String username;
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UsuarioSenhaEmail(String username, String password) {
		this.username = username;
		this.password = password;
		getPasswordAuthentication();
	}

	public PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(getUsername(), getPassword());
	}

}
