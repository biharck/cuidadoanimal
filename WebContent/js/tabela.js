function DataGridUtil (){
	var selectedIndex = null;
} 

/**
 * Acessa o link que tem a classe activation
 */
DataGridUtil.prototype.editarRegistro = function (obj){
	var acv = $(".consult",obj);
	if(acv.size() > 0)
		window.location = $(".consult",obj).attr("href");
	$('#dialog_aguarde').dialog('close');	
}

/**
 * Colore a linha que est� o mouse no DG
 */
DataGridUtil.prototype.coloreLinha = function (tabelaId,elementoSelecionado){
	var tabela = document.getElementById(tabelaId);
	var cellRows = tabela.rows;
	
	for(i = 0; i< cellRows.length ; i++){
		cellRows[i].style.backgroundColor = "";
	}
	
	elementoSelecionado.style.backgroundColor = "#7FCEFF";
	this.selectedIndex = elementoSelecionado.rowIndex;
}

/**
 * Controla o evento de entrada
 */
DataGridUtil.prototype.mouseonOverTabela = function (tabelaId,elementoSelecionado){
	if(elementoSelecionado.rowIndex != this.selectedIndex)
		elementoSelecionado.style.backgroundColor = '#B2E1FF';
}

/**
 * Controla o evento de sa�da
 */
DataGridUtil.prototype.mouseonOutTabela = function (tabelaId,elementoSelecionado){
	if(elementoSelecionado.rowIndex != this.selectedIndex)
		elementoSelecionado.style.backgroundColor = '';
}

DataGridUtil.prototype.changeCheckState = function(id){
	if(!id)	id = "tabelaResultados";
		
	var check = $("#selectAll").attr("checked");
	
	$("#"+id+" input[@type=checkbox][@name=selecteditens]").each(function(){
		if(check) $(this).attr("checked",check);
		else $(this).removeAttr("checked");
	});
}

DataGridUtil.prototype.changeCheckStateagendamento = function(id){
	if(!id)	id = "tabelaResultados";
	
	var check = $("#selectAll").attr("checked");
	
	$("#"+id+" input[@type=checkbox][@id=check]").each(function(){
		if(check) $(this).attr("checked",check);
		else $(this).removeAttr("checked");
	});
}

DataGridUtil.prototype.getSelectedValues = function(id){
	if(!id)	id = "tabelaResultados";
	var selectedValues = "";
	$("#"+id+" input[@name=selecteditens][@checked]").each(function(){
		selectedValues += $(this).val()+",";		
	});
	if(selectedValues != ""){
		selectedValues = selectedValues.substr(0,(selectedValues.length -1));
	}
	return selectedValues;
}

DataGridUtil.prototype.getArraySelectedValues = function(id){
	if(!id)	id = "tabelaResultados";
	var selectedValues = new Array();
	$("#"+id+" input[@name=selecteditens][@checked]").each(function(){
		selectedValues.push(this.value);
	});
	return selectedValues;
}

DataGridUtil.prototype.validateSelectedValues = function(){
alert(this)
	if(this.getSelectedValues() == ""){
		alert("Nenhum �tem foi selecionado.");
		return false;
	} else {
		if(confirm("Voc� deseja excluir os �tens selecionados?"))
			return true;
		else
			return false;
	}
}
DataGridUtil.prototype.clearForm = function(name){
	
	$("form[@name="+name+"] input").each(function(){
		var el = $(this);
		var type = el.attr("type");
		if((type == "text")||(type == "date"))
			el.val("");
	});		
	$("form[@name="+name+"] select").each(function(){
		var el = $(this);
		el.selectOptions('<null>');
	});
}



var $dg = new DataGridUtil();