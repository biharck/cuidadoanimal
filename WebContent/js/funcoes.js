function validaDataAtual(dt){
	var dtSplit = dt.value.split('/');
	var ano = dtSplit[2];
	var mes = dtSplit[1];
	var dia = dtSplit[0];
	var dtAlterada = mes+"/"+dia+"/"+ano;
	var myDate=new Date(dtAlterada);
	var today = new Date();
	today.setHours(0, 0, 0, 0);
	if (myDate<today){
		document.getElementById("dialog_default").innerHTML = "<center></center><br><br><center><h3>A data informada deve ser superior a data atual.</h3></center>"
		$('#dialog_default').dialog('open');
		//alert("A data informada deve ser superior a data atual.");
  		dt.value = "";  		
    }  
}

function validaDataNasc(dt){
	var dtSplit = dt.value.split('/');
	var ano = dtSplit[2];
	var mes = dtSplit[1];
	var dia = dtSplit[0];
	var dtAlterada = mes+"/"+dia+"/"+ano;
	var myDate=new Date(dtAlterada);
	var today = new Date();
	if (myDate>today){
		document.getElementById("dialog_default").innerHTML = "<center></center><br><br><center><h3>A data informada deve ser superior a data atual.</h3></center>"
		$('#dialog_default').dialog('open');
  		//alert("A data informada deve ser superior a data atual.");
  		dt.value = "";  		
    }  
}