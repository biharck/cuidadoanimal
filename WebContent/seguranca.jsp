<div id="cadastroseguranca" style="display:none;">
	<script language="JavaScript" src="/CuidadoAnimal/js/wz_tooltip.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/jquery.maskedinput-1.1.1.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/funcoes.js"></script>
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery.min.js'></script> 
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery-ui.min.js'></script>
	<script type='text/javascript' src='/CuidadoAnimal/js/fg.menu.js'></script>
			
		<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/le-frog/jquery-ui-1.8.2.custom.css' />
	<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/fg.menu.css' />
	
	<a tabindex="0" href="#seguranca" class="fg-button4 fg-button-icon-right4 ui-widget ui-state-default ui-corner-all" id="flat4"><span class="ui-icon ui-icon-triangle-1-s"></span>Seguran�a</a> 
	<div id="seguranca" class="hidden">
	<ul> 
		<li><a href="/CuidadoAnimal/autorizacao/seguranca/crud/papel">Cargo</a></li>
		<li><a href="/CuidadoAnimal/autorizacao/seguranca/permissao">Permiss�o</a></li>
		<li><a href="/CuidadoAnimal/autorizacao/seguranca/crud/usuario">Usu�rio</a></li>
	</ul>
	</div>
	
	<style type="text/css"> 
		#menuLog { font-size:1.4em; margin:10px 20px 20px; }
		.hidden { position:absolute; top:0; left:-9999px; width:1px; height:1px; overflow:hidden; }
		
		.fg-button4 { clear:left; margin:0 0 0 0; padding: .4em 1em; text-decoration:none !important; cursor:pointer; position: relative; text-align: center; zoom: 1; }
		.fg-button4 .ui-icon { position: absolute; top: 50%; margin-top: -8px; left: 50%; margin-left: -8px; }
		a.fg-button4 { float:left;  }
		button.fg-button4 { width:auto; overflow:visible; } /* removes extra button width in IE */
		
		.fg-button-icon-left4 { padding-left: 2.1em; }
		.fg-button-icon-right4 { padding-right: 2.1em; }
		.fg-button-icon-left4 .ui-icon { right: auto; left: .2em; margin-left: 0; }
		.fg-button-icon-right4 .ui-icon { left: auto; right: .2em; margin-left: 0; }
		.fg-button-icon-solo4 { display:block; width:8px; text-indent: -9999px; }	 /* solo icon buttons must have block properties for the text-indent to work */	
		
		.fg-button4.ui-state-loading .ui-icon { background: url(spinner_bar.gif) no-repeat 0 0; }
	</style>
	<script type="text/javascript">    
	    $(function(){
	    	// BUTTONS
	    	$('.fg-button4').hover(
	    		function(){ $(this).removeClass('ui-state-default').addClass('ui-state-focus'); },
	    		function(){ $(this).removeClass('ui-state-focus').addClass('ui-state-default'); }
	    	);
	    	
	    	// MENUS    	
			$('#flat4').menu({ 
				content: $('#flat4').next().html(), // grab content from this page
				showSpeed: 400 
			});
			
			$('#hierarchy').menu({
				content: $('#hierarchy').next().html(),
				crumbDefaultText: ' '
			});
			
			$('#hierarchybreadcrumb').menu({
				content: $('#hierarchybreadcrumb').next().html(),
				backLink: false
			});
			
	    });
	
	    
	    </script>
</div>