<div id="cadastroclinica" style="display:none;">
	<script language="JavaScript" src="/CuidadoAnimal/js/wz_tooltip.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/jquery.maskedinput-1.1.1.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/funcoes.js"></script>
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery.min.js'></script> 
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery-ui.min.js'></script>
	<script type='text/javascript' src='/CuidadoAnimal/js/fg.menu.js'></script>
			
	<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/le-frog/jquery-ui-1.8.2.custom.css' />
	<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/fg.menu.css' />
	
	<a tabindex="0" href="#clinica" class="fg-button2 fg-button-icon-right2 ui-widget ui-state-default ui-corner-all" id="flat2"><span class="ui-icon ui-icon-triangle-1-s"></span>Clinica</a> 
	<div id="clinica" class="hidden">
	<ul> 
		<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/clinica/crud/Animal?cadAnimal=')">Atendimento</a></li>
		<li><a href="#">Caixa</a>
			<ul>
				<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/clinica/crud/FrenteCaixa')">Frente de Caixa</a></li>
				<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/clinica/crud/Caixa')">Movimento</a></li>
			</ul>
		</li>
		<li><a href="#">Financeiro</a>
			<ul>
				<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/clinica/crud/ContaPagar')">Contas a Pagar</a></li>
				<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/clinica/crud/ContaReceber')">Contas a Receber</a></li>		
			</ul>
		</li>
		<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/clinica/crud/Empresa')">Empresa</a></li>
	</ul>
	</div>
	
	<style type="text/css"> 
		#menuLog { font-size:1.4em; margin:10px 20px 20px; }
		.hidden { position:absolute; top:0; left:-9999px; width:1px; height:1px; overflow:hidden; }
		
		.fg-button2 { clear:left; margin:0 0 0 0; padding: .4em 1em; text-decoration:none !important; cursor:pointer; position: relative; text-align: center; zoom: 1; }
		.fg-button2 .ui-icon { position: absolute; top: 50%; margin-top: -8px; left: 50%; margin-left: -8px; }
		a.fg-button2 { float:left;  }
		button.fg-button2 { width:auto; overflow:visible; } /* removes extra button width in IE */
		
		.fg-button-icon-left2 { padding-left: 2.1em; }
		.fg-button-icon-right2 { padding-right: 2.1em; }
		.fg-button-icon-left2 .ui-icon { right: auto; left: .2em; margin-left: 0; }
		.fg-button-icon-right2 .ui-icon { left: auto; right: .2em; margin-left: 0; }
		.fg-button-icon-solo2 { display:block; width:8px; text-indent: -9999px; }	 /* solo icon buttons must have block properties for the text-indent to work */	
		
		.fg-button2.ui-state-loading .ui-icon { background: url(spinner_bar.gif) no-repeat 0 0; }
	</style>
	<script type="text/javascript">    
	    $(function(){
	    	// BUTTONS
	    	$('.fg-button2').hover(
	    		function(){ $(this).removeClass('ui-state-default').addClass('ui-state-focus'); },
	    		function(){ $(this).removeClass('ui-state-focus').addClass('ui-state-default'); }
	    	);
	    	
	    	// MENUS    	
			$('#flat2').menu({ 
				content: $('#flat2').next().html(), // grab content from this page
				showSpeed: 400 
			});
			
			$('#hierarchy').menu({
				content: $('#hierarchy').next().html(),
				crumbDefaultText: ' '
			});
			
			$('#hierarchybreadcrumb').menu({
				content: $('#hierarchybreadcrumb').next().html(),
				backLink: false
			});
			
	    });
	
	    
	    </script>
</div>