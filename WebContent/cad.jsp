<div id="cadastromenu" style="display:none;">
	<script language="JavaScript" src="/CuidadoAnimal/js/wz_tooltip.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/jquery.maskedinput-1.1.1.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/funcoes.js"></script>
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery.min.js'></script> 
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery-ui.min.js'></script>
	<script type='text/javascript' src='/CuidadoAnimal/js/fg.menu.js'></script>
			
	<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/le-frog/jquery-ui-1.8.2.custom.css' />
	<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/fg.menu.css' />
	
	<a tabindex="0" href="#search-engines" class="fg-button fg-button-icon-right ui-widget ui-state-default ui-corner-all" id="flat"><span class="ui-icon ui-icon-triangle-1-s"></span>Cadastros</a> 
	<div id="search-engines" class="hidden">
	<ul> 
		<li><a href="#">Animal</a>
			<ul>
				<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/clinica/crud/Animal?cadAnimal=true')">Cadastro do Animal</a></li>
				<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/cadastros/crud/Especie')">Esp�cie</a></li>
				<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/cadastros/crud/Cor')">Cor</a></li>
				<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/cadastros/crud/Raca')">Ra�a</a></li>
			</ul>	
		</li>
		<li><a href="#">Utilit�rios</a>
			<ul>
				<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/cadastros/crud/Municipio')">Munic�pio</a></li>
				<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/cadastros/crud/Transporte')">Transporte</a></li>
				<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/cadastros/crud/Uf')">Unidade Federativa</a></li>
			</ul>
		</li>
		<li><a href="#">Servi�os Gerais</a>
			<ul>
				<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/cadastros/crud/SubTipoProduto')">Sub-Tipo de Produtos</a></li>
				<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/cadastros/crud/TipoProduto')">Tipos de Produtos</a></li>		
			</ul>
		</li>
		<li><a href="javascript:redirectLink('/CuidadoAnimal/autorizacao/clinica/crud/Cliente')">Cadastro do Cliente</a></li>
	</ul>
	<style type="text/css"> 
		#menuLog { font-size:1.4em; margin:10px 20px 20px; }
		.hidden { position:absolute; top:0; left:-9999px; width:1px; height:1px; overflow:hidden; }
		
		.fg-button { clear:left; margin:0 0 0 0; padding: .4em 1em; text-decoration:none !important; cursor:pointer; position: relative; text-align: center; zoom: 1; }
		.fg-button .ui-icon { position: absolute; top: 50%; margin-top: -8px; left: 50%; margin-left: -8px; }
		a.fg-button { float:left;  }
		button.fg-button { width:auto; overflow:visible; } /* removes extra button width in IE */
		
		.fg-button-icon-left { padding-left: 2.1em; }
		.fg-button-icon-right { padding-right: 2.1em; }
		.fg-button-icon-left .ui-icon { right: auto; left: .2em; margin-left: 0; }
		.fg-button-icon-right .ui-icon { left: auto; right: .2em; margin-left: 0; }
		.fg-button-icon-solo { display:block; width:8px; text-indent: -9999px; }	 /* solo icon buttons must have block properties for the text-indent to work */	
		
		.fg-button.ui-state-loading .ui-icon { background: url(spinner_bar.gif) no-repeat 0 0; }
	</style>
	<script type="text/javascript">    
	    $(function(){
	    	// BUTTONS
	    	$('.fg-button').hover(
	    		function(){ $(this).removeClass('ui-state-default').addClass('ui-state-focus'); },
	    		function(){ $(this).removeClass('ui-state-focus').addClass('ui-state-default'); }
	    	);
	    	
	    	// MENUS    	
			$('#flat').menu({ 
				content: $('#flat').next().html(), // grab content from this page
				showSpeed: 400 
			});
			
			$('#hierarchy').menu({
				content: $('#hierarchy').next().html(),
				crumbDefaultText: ' '
			});
			
			$('#hierarchybreadcrumb').menu({
				content: $('#hierarchybreadcrumb').next().html(),
				backLink: false
			});
			
	    });


	    </script>
</div></div>