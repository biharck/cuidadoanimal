<div id="cadastrorelatorios" style="display:none;">
	<script language="JavaScript" src="/CuidadoAnimal/js/wz_tooltip.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/jquery.maskedinput-1.1.1.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/funcoes.js"></script>
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery.min.js'></script> 
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery-ui.min.js'></script>
	<script type='text/javascript' src='/CuidadoAnimal/js/fg.menu.js'></script>
	
	<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/le-frog/jquery-ui-1.8.2.custom.css' />
	<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/fg.menu.css' />
	
	<a tabindex="0" href="#relatorios" class="fg-button5 fg-button-icon-right5 ui-widget ui-state-default ui-corner-all" id="flat5"><span class="ui-icon ui-icon-triangle-1-s"></span>Relat�rios</a> 
	<div id="relatorios" class="hidden">
	<ul> 
		<li><a href="/CuidadoAnimal/autorizacao/relatorio/Animais">Rela��o de Animais</a></li>
		<li><a href="/CuidadoAnimal/autorizacao/relatorio/Cliente">Rela��o de Clientes</a></li>
		<li><a href="/CuidadoAnimal/autorizacao/relatorio/Consulta">Rela��o de Consultas</a></li>
		<li><a href="/CuidadoAnimal/autorizacao/relatorio/ContaPagar">Rela��o de Contas a Pagar</a></li>
		<li><a href="/CuidadoAnimal/autorizacao/relatorio/ContasReceber">Rela��o de Contas a Receber</a></li>
		<li><a href="/CuidadoAnimal/autorizacao/relatorio/Estoque">Rela��o de Estoque</a></li>
		<li><a href="/CuidadoAnimal/autorizacao/relatorio/Fornecedor">Rela��o de Forncedor</a></li>
		<li><a href="/CuidadoAnimal/autorizacao/relatorio/MovimentoCaixa">Rela��o de Movimento de Caixa</a></li>
		<li><a href="/CuidadoAnimal/autorizacao/relatorio/Produto">Rela��o de Produtos</a></li>
		<li><a href="/CuidadoAnimal/autorizacao/relatorio/ProdutosServicosUtilizados">Rela��o de Produtos / Servi�os Utilizados</a></li>
	</ul>
	</div>
	
	<style type="text/css"> 
		#menuLog { font-size:1.4em; margin:10px 20px 20px; }
		.hidden { position:absolute; top:0; left:-9999px; width:1px; height:1px; overflow:hidden; }
		
		.fg-button5 { clear:left; margin:0 0 0 0; padding: .4em 1em; text-decoration:none !important; cursor:pointer; position: relative; text-align: center; zoom: 1; }
		.fg-button5 .ui-icon { position: absolute; top: 50%; margin-top: -8px; left: 50%; margin-left: -8px; }
		a.fg-button5 { float:left;  }
		button.fg-button5 { width:auto; overflow:visible; } /* removes extra button width in IE */
		
		.fg-button-icon-left5 { padding-left: 2.1em; }
		.fg-button-icon-right5 { padding-right: 2.1em; }
		.fg-button-icon-left5 .ui-icon { right: auto; left: .2em; margin-left: 0; }
		.fg-button-icon-right5 .ui-icon { left: auto; right: .2em; margin-left: 0; }
		.fg-button-icon-solo5 { display:block; width:8px; text-indent: -9999px; }	 /* solo icon buttons must have block properties for the text-indent to work */	
		
		.fg-button5.ui-state-loading .ui-icon { background: url(spinner_bar.gif) no-repeat 0 0; }
	</style>
	<script type="text/javascript">    
	    $(function(){
	    	// BUTTONS
	    	$('.fg-button5').hover(
	    		function(){ $(this).removeClass('ui-state-default').addClass('ui-state-focus'); },
	    		function(){ $(this).removeClass('ui-state-focus').addClass('ui-state-default'); }
	    	);
	    	
	    	// MENUS    	
			$('#flat5').menu({ 
				content: $('#flat5').next().html(), // grab content from this page
				showSpeed: 400 
			});
			
			$('#hierarchy').menu({
				content: $('#hierarchy').next().html(),
				crumbDefaultText: ' '
			});
			
			$('#hierarchybreadcrumb').menu({
				content: $('#hierarchybreadcrumb').next().html(),
				backLink: false
			});
			
	    });
	
	    
	    </script>
</div>