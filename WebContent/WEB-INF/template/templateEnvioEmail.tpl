Uma nova senha foi solicitada no sistema Cuidado Animal, e ela está associada a este e-mail.
<br>
<br>
<b>Dados Cadastrados no sistema:</b>
<br>
<b>Nome:</b> {nome}<br>
<b>Email:</b> {email}<br>

<br>
<br>
Para alterar sua senha entre no link abaixo e siga as instruções.
<br><br>
Para autenticar no sistema novamente, acesse o link <a href="{link}">Clique Aqui Para Alterar Sua Senha</a><br>
<br>
<br>
<p>1. Acesse o sistema através do link fornecido;</p>
<p>2. Insira sua nova senha;</p>
<p>3. Confirme sua nova senha;</p>
<p>6. Confirme clicando no botão ALTERAR SENHA.</p>
<br>
<br>
<br>
<br>
<i>
----------------------------------------------------------------------------------------------------------------------------------------------<br>
ESTE E-MAIL FOI GERADO AUTOMATICAMENTE, FAVOR NÃO RESPONDER A MENSAGEM.<br>
----------------------------------------------------------------------------------------------------------------------------------------------<br>
</i>

