<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">

<head profile="http://gmpg.org/xfn/11">
	<n:head/>
<title>Cuidado Animal</title>

</head>
<div id="dialog_default" title="Observa��o">
</div>
<div id="dialog_agenda" title="Observa��o">
</div>
<div id="dialog_aguarde" title="Aguarde" >
</div>	

<div id="dialog_SairExcluir" title="Observa��o">
</div>
<div id="dialog_detalhe" title="Observa��o">
</div>
<div id="dialog_report" title="Observa��o">
</div>
<div id="dialog_estorno" title="Aten��o">
</div>


	<body>
	<script language="JavaScript" src="/CuidadoAnimal/js/wz_tooltip.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/jquery.maskedinput-1.1.1.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/funcoes.js"></script>
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery.js'></script>
	
		
	<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/le-frog/jquery-ui-1.8.2.custom.css' />
	
	<script type="text/javascript" >
	function applyMask(){
		jQuery(function($){
			$("#numeroRegistro").mask("99999-999");
		});
		
		
	}
	function redirectLink(param){
    	$('#dialog_aguarde').dialog('open');
		location.href=param;
	}
	
	$(document).ready(function(){
		var url = window.location+"";
		var temp = url.split('/');
		modulo = temp[5];

		
		if(url=="http://localhost:8080/CuidadoAnimal/autorizacao/clinica/crud/Animal?ACAO=criar")
			changeMenu(1);
		else if(url == "http://localhost:8080/CuidadoAnimal/autorizacao/clinica/crud/Cliente")
			changeMenu(1);
		else{
			seek = new RegExp("cadAnimal=true","b");
			if(url.search(seek) != -1)
				changeMenu(1); 
			else if(modulo == 'cadastros')
				changeMenu(1);
			else if(modulo == 'clinica')
				changeMenu(2);
			else if(modulo == 'estoque')
				changeMenu(3);
			else if(modulo == 'seguranca')
				changeMenu(4);
			else if(modulo == 'relatorio')
				changeMenu(6);
			else 
				changeMenu(5);
		}
		
		document.getElementById("dialog_aguarde").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando</h3></center>"
	})
	$(function() {
		$("#dialog_default").dialog({
			bgiframe: true,
			autoOpen: false,
			modal: true,
			buttons: {
				Ok: function() {
					$('#dialog_aguarde').dialog('close');
					$(this).dialog('close');	
				},
			}
		});
		$("#dialog_agenda").dialog({
			bgiframe: true,
			autoOpen: false,
			modal: true,
			buttons: {
				Ok: function() {
					
					$(this).dialog('close');
					window.location.href="/CuidadoAnimal";
					$('#dialog_aguarde').dialog('open');	
				},
			}
		});
		
		$("#dialog_report").dialog({
			bgiframe: true,
			autoOpen: false,
			modal: true,
			buttons: {
				Fehcar: function() {
					$(this).dialog('close');
				},
			}
		});
			
		$("#dialog_aguarde").dialog({
			bgiframe: true,
			autoOpen: false,
			modal: true,
		});
	});

	$(function() {
		$("a, input").tooltip();
	});	
	
	</script>
		<div class="main">
			<div class="main-width">
				<div class="header">
					<div class="menu">
												
						<c:if test="${empty escondeMenu}">
							<ul>
								<li class="page_item page-item-29 logomarca" id="logomarca" onmouseover='Tip("Empresa")'>
									<div id="logomarca">
										<c:if test="${exibeFotoEmpresa}">
											<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${idFotoEmpresa}" id="imagem" class="imageLogo"/>
										</c:if>
										<c:if test="${!exibeFotoEmpresa}">
											<div class="image">
												<img src="${pageContext.request.contextPath}/images/imgnaodisponivel.jpg" id="imagem" class="imageLogo" style="padding-top:13px;"/>
											</div>
										</c:if>
									</div>
								</li>
								<li class="page_item page-item-29 menu1" id="menu1" onclick="changeMenu(1)" onmouseover='Tip("Cadastros")'>
								</li>
								<li class="page_item page-item-29 menu2" id="menu2" onclick="changeMenu(2)" onmouseover='Tip("Cl�nica")'>
								</li>
								<li class="page_item page-item-29 menu3" id="menu3" onclick="changeMenu(3)" onmouseover='Tip("Estoque")'>
								</li>
								<li class="page_item page-item-29 menu4" id="menu4" onclick="changeMenu(4)" onmouseover='Tip("Seguran�a")'>
								</li>
								<li class="page_item page-item-29 menu5" id="menu5" onclick="changeMenu(51)" onmouseover='Tip("Agenda")'>
								</li>
								<li class="page_item page-item-29 menu6" id="menu6" onclick="changeMenu(6)" onmouseover='Tip("Relat�rios")'>
								</li>
								<li class="page_item page-item-29 menu7">									
									<a href="/CuidadoAnimal/autenticacao/logout">Logout</a>
								</li>
								
							</ul>
						</c:if>
					</div>

				</div>
				<div class="content">				
					<div class="column-center">

						<div class="border-left">
							<div class="border-right">
								<div class="border-top">
									<div class="border-bot">
										<div class="corner-left-top">
											<div class="corner-right-top">
												<div class="corner-left-bot">
													<div class="corner-right-bot">
														<jsp:include page="../../cad.jsp"/>
														<jsp:include page="../../clinica.jsp"/>
														<jsp:include page="../../estoque.jsp"/>
														<jsp:include page="../../seguranca.jsp"/>
														<jsp:include page="../../relatorios.jsp"/>
														<br><br><br><br>
														<n:messages/>
														<br>
														<jsp:include page="${bodyPage}" />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>

			</div>
		</div>

			<div class="footer">
			<c:if test="${empty escondeMenu}">
				<div class="width">
					
				</div>
			</c:if>
			</div>
<script type="text/javascript">
	function changeMenu(valor){
		closeAll();
		disableAllIcons();
		switch(valor){
			case 1: 
				$("#cadastromenu").fadeIn();
				document.getElementById("menu1").className = "menu1-active";
				break;
			case 2:
				$("#cadastroclinica").fadeIn();
				document.getElementById("menu2").className = "menu2-active";
				break;
			case 3:
				$("#cadastroestoque").fadeIn();
				document.getElementById("menu3").className = "menu3-active";
				break;
			case 4:
				$("#cadastroseguranca").fadeIn();
				document.getElementById("menu4").className = "menu4-active";
				break;
			case 5:
				document.getElementById("menu5").className = "menu5-active";
				//location.href = "/CuidadoAnimal/autorizacao";
				break;
			case 6:
				$("#cadastrorelatorios").fadeIn();
				document.getElementById("menu6").className = "menu6-active";
				break;
			case 51:
				document.getElementById("menu5").className = "menu5-active";
				location.href = "/CuidadoAnimal/autorizacao";
				break;
			default:
				closeAll();
				disableAllIcons();
		}
	}
	function closeAll(){
		$("#cadastromenu").hide();
		$("#cadastroclinica").hide();
		$("#cadastroestoque").hide();
		$("#cadastroseguranca").hide();
		$("#cadastrorelatorios").hide();
	}

	function disableAllIcons(){
		document.getElementById("menu1").className = "menu1";
		document.getElementById("menu2").className = "menu2";
		document.getElementById("menu3").className = "menu3";
		document.getElementById("menu4").className = "menu4";
		document.getElementById("menu5").className = "menu5";
		document.getElementById("menu6").className = "menu6";
	}

</script>			
<style type="text/css">
.ui-corner-all {
	-moz-border-radius:0 0 0 0;
	border-radius:0 0 0 0;
}

</style>			
	</body>
</html>
