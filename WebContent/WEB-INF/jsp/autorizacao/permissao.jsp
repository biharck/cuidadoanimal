<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="Permiss�o">
	<t:janelaFiltro>
		<t:tabelaFiltro showSubmit="false">
			<t:property name="role" itens="${roles}" reloadOnChange="true" label="Papel"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	
	<c:forEach items="${filtro.groupAuthorizationMap}" var="item">
		<t:janelaResultados>
			<n:panelGrid columns="1" rowStyleClasses="filtro1" width="100%">
				<n:panel style="color: #333333">&nbsp;&nbsp;&nbsp;<b><i>${item.key}</i></b></n:panel>
			</n:panelGrid>
			<n:dataGrid itens="${item.value}" width="100%" cellspacing="1" headerStyleClass="listagemHeader" bodyStyleClasses="listagemBody1, listagemBody2" footerStyleClass="listagemFooter">
				<n:bean name="row" propertyPrefix="groupAuthorizationMap[${item.key}][${index}]" valueType="${authorizationProcessItemFilterClass}">

				<n:column header="Tela">
					<t:property name="description"/>				
					<t:property name="path" mode="input" type="hidden"/>						
				</n:column>
				
				<c:forEach items="${mapaGroupModule[item.key].authorizationItens}" var="authorizationItem">
					<n:column header="${authorizationItem.nome}" width="80px">
						<c:if test="${fn:length(authorizationItem.valores) == 2}">
							<%-- Possibilidade de ser true false --%>
							<n:property name="permissionMap[${authorizationItem.id}]">
								<n:input type="checkbox"/>							
							</n:property>
						</c:if>
						<c:if test="${fn:length(authorizationItem.valores) != 2}">
							(N�o implementado ainda)
							<n:input itens="${authorizationItem.valores}"/>
						</c:if>						
					</n:column>
				</c:forEach>					
				</n:bean>
			</n:dataGrid>
			
		</t:janelaResultados>
	</c:forEach>
	<c:if test="${!empty filtro.role}">
		<t:janelaResultados>
		<div class="filtro1" width="100%"><n:submit action="salvar">Salvar</n:submit></div>
		</t:janelaResultados>
	</c:if>
</t:tela>