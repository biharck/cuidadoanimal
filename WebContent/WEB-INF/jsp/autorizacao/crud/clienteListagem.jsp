<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro >
			<t:property name="nome" style="width:400px;"/>
			<t:property name="cpf" id="cpf" style='width:100px;' type="CPF"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados showExcluirLink="false">
			<t:property name="nome" />
			<t:property name="cpf" />
			<t:property name="celular" />
			<t:property name="email" />
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">
$(document).ready(function() {
	$("#cpf").mask("999.999.999-99");
});
</script>