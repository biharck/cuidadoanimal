
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<c:if test="${!abrirCaixa}">
	<t:entrada showListagemLink="false" >
	<div id="aberto">
	</div>
	<div align="center">
		<n:link url="javascript:abrepopUpMovimentacao(1)" onmouseover="Tip(\"Inserir Novo Suprimento\")" img="/CuidadoAnimal/images/entradacaixa.png"></n:link>
		<n:link url="javascript:abrepopUpMovimentacao(2)" onmouseover="Tip(\"Inserir Nova Sa�da de Caixa\")" img="/CuidadoAnimal/images/saidacaixa.png"></n:link>
		<n:link url="javascript:submitReportMovimentacao()" onmouseover="Tip(\"Imprimir Rela��o\")" img="/CuidadoAnimal/images/imprimir.png"></n:link>
		<n:link url="javascript:abrepopUpMovimentacao(4)" onmouseover="Tip(\"Fechar Caixa\")" img="/CuidadoAnimal/images/baixar.png"></n:link>
	</div>
				<t:property name="idCaixa" type="hidden" write="false" label=""/>
		<t:janelaEntrada submitAction="false" showSubmit="false" >
				<input type="text" name="data" />
			<t:tabelaEntrada>
			</t:tabelaEntrada>
			<input type="hidden" name="temp" id="temp"/>
				<t:detalhe name="listaEntradaSaidaCaixa" showBotaoNovaLinha="false" showBotaoRemover="false" showColunaAcao="false">
					<t:property name="idEntradaSaidaCaixa" label="codigo" mode="output"/>
					<t:property name="data" mode="output"/>
					<t:property name="usuario" mode="output"/>
					<t:property name="valor" mode="output"/>
					<t:property name="formaPagamento" mode="output"/>
					<n:column header="Pendente?">
							<center><t:property name="pendente" mode="output"/></center>
					</n:column>
					<t:property name="cliente" mode="output"/>
					<n:column header="Estornar?">
						<center><n:link url="javascript:geraEstorno(${entradaSaidaCaixa.idEntradaSaidaCaixa})" onmouseover="Tip(\"Gerar um Estorno\")" img="/CuidadoAnimal/images/estorno.png"></n:link></center>
					</n:column>
				</t:detalhe>
				<t:detalhe name="listaEntradaSaidaCaixa2" showBotaoNovaLinha="false" showBotaoRemover="false" showColunaAcao="false">
					<t:property name="idEntradaSaidaCaixa" label="codigo" mode="output"/>
					<t:property name="data" mode="output"/>
					<t:property name="usuario" mode="output"/>
					<t:property name="valor" mode="output"/>
					<t:property name="formaPagamento" mode="output"/>
					<n:column header="Pendente?">
							<center><t:property name="pendente" mode="output"/></center>
					</n:column>
					<n:column header="Estornar?">
							<center><n:link url="javascript:geraEstorno(${entradaSaidaCaixa.idEntradaSaidaCaixa})" onmouseover="Tip(\"Gerar um Estorno\")" img="/CuidadoAnimal/images/estorno.png"></n:link></center>
					</n:column>
				</t:detalhe>
				<t:detalhe name="listaEstorno" showBotaoRemover="false"  showBotaoNovaLinha="false" showColunaAcao="false">
					<t:property name="idEntradaSaidaCaixa" label="codigo" mode="output"/>
					<t:property name="data" mode="output"/>
					<t:property name="usuario" mode="output"/>
					<t:property name="valor" mode="output"/>
					<t:property name="formaPagamento" mode="output"/>
					<t:property name="pendente" mode="output"/>
					<t:property name="motivo" mode="output"/>
				</t:detalhe>
		</t:janelaEntrada>
	</t:entrada>
	<br>
	<br>
	<table style="width:100%; font-size:12px; text-align:center;">
		<tr>
			<td>
			</td>
			<td>
				Saldo Atual:
			</td>
			<td>
				Entradas:
			</td>
			<td>
				Suprimento:
			</td>
			<td>
				Sa�da:
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
				<b>R$ ${saldoAtual}</b>
			</td>
			<td>
				<b>R$ ${entrada}</b>
			</td>
			<td>
				<b>R$ ${suprimento}</b>
			</td>
			<td>
				<b>R$ ${saida}</b>
			</td>
		</tr>
	</table>
</c:if>
<c:if test="${abrirCaixa}">
	<div class="info_alert">O Caixa Encontra-ser Fechado no momento.<br>Clique <b><a class="linkme" href="/CuidadoAnimal/autorizacao/clinica/crud/FrenteCaixa?ACAO=abreCaixa">Aqui</a></b> Para Abrir o Caixa?</div>
</c:if>
<script type="text/javascript">

	$(document).ready(function() {
		$("#janelaEntrada_link_0").hide();
		showjanelaEntrada("janelaEntrada_1", 1, "janelaEntrada_link_1");
		verificaCaixaAberto();
	});


	function submitReportMovimentacao(){
		form.ACAO.value ='gerar';
		form.action = '/CuidadoAnimal/autorizacao/relatorio/MovimentoCaixa?ACAO=gerar&time=defaul'; 
		form.validate = 'true'; 
		submitForm(1);
	}
	
	function abrepopUpMovimentacao(param){
		$('#dialog_aguarde').dialog('open');
		window.open('/CuidadoAnimal/autorizacao/crud/MovimentoCaixa?type='+param,'page','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1010,height=600'); 
	}

	var idEntrada = "";
	function geraEstorno(id){
		idEntrada = id;
		document.getElementById("dialog_estorno").innerHTML = "<center><h3>Deseja Realmente Estornar esta Conta?</h3></center>"
		$("#dialog_estorno").dialog('open');	
	}

	$("#dialog_estorno").dialog({
		bgiframe: true,
		autoOpen: false,
		modal: true,
		buttons: {
			Sim: function() {
				form.ACAO.value ='estornar';
				form.action = '/CuidadoAnimal/autorizacao/clinica/crud/Caixa?ACAO=estornar&idEntrada='+idEntrada; 
				form.validate = 'false'; 
				submitForm();
				$(this).dialog('close');	
			},
			N�o: function() {
				$(this).dialog('close');	
			},
		}
	});
	
	function reloadPage(){
		window.location.reload();
	}

	function redirectPage(){
		window.location.href="/CuidadoAnimal";
	}

	function verificaCaixaAberto(){
		sendRequest('/CuidadoAnimal/autorizacao/crud/Pagamento','ACAO=verificaCaixa','POST', ajaxCallback, erroCallback);	
	}

	function ajaxCallback(data){
		eval(data);
		if(retorno == true){
			document.getElementById("aberto").innerHTML='O Caixa Encontra-se Fechado';
			var confirmacao = confirm('O Caixa Ainda N�o Foi Aberto Na Data de Hoje,\nDeseja Abrir o Caixa?');
			if(confirmacao){
				ajaxAbreCaixa();
			}
			else{
				location.href="/CuidadoAnimal/autorizacao";
			}
		}
		else if(abreCaixa == true && noError == true){
			document.getElementById("aberto").innerHTML='Caixa Aberto em: '+abertura;
			//alert(abertura);
			alert('Abertura de Caixa Realizada Com sucesso!');
		}
		else if(retorno == false && abreCaixa == false && noError == false){
			document.getElementById("aberto").innerHTML='Caixa Aberto em: '+abertura;
			return false;
		}
		else{
			document.getElementById("aberto").innerHTML='O Caixa Encontra-se Fechado'; 
			alert('Falha ao realizar a abertura do Caixa, Fa�a a abertura do caixa manualmente.');
		}
		
	}

	function ajaxAbreCaixa(){
		sendRequest('/CuidadoAnimal/autorizacao/crud/Pagamento','ACAO=abreCaixa','POST', ajaxCallback, erroCallback);
	}

	function erroCallback(request){
	    alert('Erro no ajax!\n' + request.responseText);
	}
	function submitForm(param) {
		if(param == 1){
			document.getElementById("dialog_report").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando.<br>Ap�s o t�rmino clique em fechar.</h3></center>"
			$('#dialog_report').dialog('open');
		}
		else
			$('#dialog_aguarde').dialog('open');
		
		var validar = form.validate;
		try {
			validarFormulario;
		} catch (e) {
			validar = false;
		}
		try {
			clearMessages();//limpa as mensagens que vieram do servidor
		} catch(e){
		}
		if(validar == 'true') {
			var valid = validarFormulario();
			if(valid) {
				form.submit();
			}
		} else {
			form.submit();
		}
	}	
		
</script>
<style type="text/css">
/* next css*/
.dataGrid {
	width: 100%;
	
}
.dataGridHeader *  {
	font-weight: normal;
	background:#259bcb;
	background-repeat: repeat-x;
}
.dataGrid TD {
	font-size: 12px;
}
.dataGrid TH {
	font-size: 12px;
	text-align: left;
	padding: 3px;	
	border:none;
	border:none;
}

.dataGrid THEAD * {
	border-botton:none;
	color:#fff;
	border:1px solid;
}



a{
	text-decoration:none;
	color:#4A555B;
}

.dataGridBody1{
	background-color:#EAF0D8;
	
	border: none;
}

.dataGrid TBODY TD {
	font-size: 11px;
	padding: 3px;
	border-bottom: 0px;
}
.linkBar {
	font-size: 12px;
	font-family: verdana;
	padding-bottom: 6px;
	padding-top: 6px;
	padding-left: 2px;
	border: 1px solid #83AADA;
	background: #DDECFE;
	background-position: center;
}

.linkBar A {
	color: black;
	text-decoration: none;
	padding: 7px;
	padding-top: 4px;
	padding-bottom: 4px;
	width: 180px;
	min-width: 120px;
} 
.linkBar A:hover {
	border: 1px solid #83AADA;
	text-decoration: none;
	padding-left: 6px;
	padding-right: 6px;
	background: #CBE2FD;
}

</style>







