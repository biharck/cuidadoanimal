<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem >
	<t:janelaFiltro >
		<t:tabelaFiltro >
			<t:property name="codProduto" id="codProduto"/>
			<t:property name="nome" />
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados >
		<t:tabelaResultados showExcluirLink="false">
			<t:property name="codigo" />
			<t:property name="nome" />
			<c:if test="${empty exibirEstoque}">
			<n:column header="Entrada no Estoque" style="text-align:center" >
				<center>
					<n:link url="javascript:abrepopUpEntrada(${produtoServico.idProdutoServico})" img="/CuidadoAnimal/images/up.png"></n:link>
				</center>
				</n:column>
				<n:column header="Baixa no Estoque" style="text-align:center">
				<center>
					<n:link url="javascript:abrepopUpSaida(${produtoServico.idProdutoServico})" img="/CuidadoAnimal/images/down.png"></n:link>
				</center>
				</n:column>
			</c:if>
			<n:column header="Quantidade Disponível">
				<center><t:property name="qtdRestante"/></center>
			</n:column>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">

	function abrepopUpEntrada(param){
		$('#dialog_aguarde').dialog('open');
		var Id= $("#codProduto").val();
		window.open('/CuidadoAnimal/autorizacao/crud/EntradaEstoque?codProduto='+param,'page','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1010,height=600'); 
	}
	function abrepopUpSaida(param){
		$('#dialog_aguarde').dialog('open');
		var Id= $("#codProduto").val();
		window.open('/CuidadoAnimal/autorizacao/crud/SaidaEstoque?codProduto='+param,'page','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1010,height=600'); 
	}
	function reloadPage(){
		window.location.reload();
	}
</script>
