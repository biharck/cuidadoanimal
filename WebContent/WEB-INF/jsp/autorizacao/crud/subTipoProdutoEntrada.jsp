<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>


<t:entrada>	
	<t:property name="idSubTipoProduto" type="hidden" write="false"/>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<t:property name="tipoProduto"/>
			<t:property name="descricao"/>
			<t:property name="ativo" />
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>