<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="Estorno">
<c:if test="${empty mostraFim}">
<div class="divheader"></div>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:bean name="entradaSaidaCaixa" >
				<t:propertyConfig renderAs="double">
					<t:property name="idEntradaSaidaCaixa" type="hidden" write="false" label=""/>
					<t:property name="tipoOperacao.idTipoOperacao" type="hidden" write="false" label=""/>
					<t:property name="formaPagamento.idFormaPagamento" type="hidden" write="false" label=""/>
					<t:property name="tipoOperacao.descricao" mode="output"/>
					<t:property name="entradaSaidaCaixa" itens="${listaPossivelEstorno}" />
				</t:propertyConfig>
			</n:bean>
		</t:tabelaEntrada>
	</t:janelaEntrada>
	<div class="actionBar">
		<button title="" onclick="validateAllData();" type="button">Salvar</button>
	</div>
</c:if>
<c:if test="${mostraFim}">
	<div class="messageOk">
		Estorno realizado com sucesso!
	</div>
</c:if>
	<div align="center">
			<button onclick="javascript:fechaJanela();" >Fechar</button>
	</div>

</t:tela>
<script type="text/javascript">
function fechaJanela(){
	window.close();
}
$(document).ready(function() {
	$("#editButton").hide();
});

function validateAllData(){
		form.ACAO.value ='salvar';
		form.action = '/CuidadoAnimal/autorizacao/crud/MovimentoCaixa'; 
		form.validate = 'false'; 
		submitForm();
}
var form = document.forms["form"];
form.validate ='true';
function submitForm() {
	var validar = form.validate;
	try {
		validarFormulario;
	} catch (e) {
		validar = false;
	}
	try {
		clearMessages();//limpa as mensagens que vieram do servidor
	} catch(e){
	}
	var valid = validarFormulario();
	if(valid) {
		form.submit();
	}
}
function invalidFields(form, fields, msgs, validationName){
	alert(msgs.join('\n'));
	hasFocus = false;
}
</script>
<style type="text/css">
.messageOk {
	background:#E2F9E3 url(/CuidadoAnimal/images/alertgood_icon.gif) no-repeat scroll left center;
	border-color:#99CC99;
	color:#000000;
	font-size:14px;
	padding:10px 10px 10px 23px; 
}
</style>



