<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<div class="tooltip"></div>
<t:entrada >	
	<t:property name="idEmpresa" type="hidden" write="false"/>
	<t:janelaEntrada >
		<t:tabelaEntrada >
			<n:panel>				
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" style="width:600px;" columnStyles="width:200px,width:900px;">
						<n:panel colspan="2" style="text-align:center;">
							<div align="center" id="divFoto">
								<c:if test="${exibeFoto}">
									<n:link url="/DOWNLOADFILE/${empresa.foto.cdfile}">
										<img src="/CuidadoAnimal/DOWNLOADFILE/${empresa.foto.cdfile}" id="imagem" class="foto"/>
									</n:link>
									<c:if test="${param.ACAO == 'criar' || param.ACAO == 'editar'}">
										<br><br>
										<button id="foto_removerbtn" type="button" style="text-transform: none" onclick="document.getElementById('foto_excludeField').value='true';">Remover</button>
									</c:if>				
								</c:if>
								<c:if test="${!exibeFoto}">
									<div class="image">
										<img src="/CuidadoAnimal/imagens/imgnaodisponivel.jpg" id="imagem" style="padding-top:13px;"/>
									</div>
								</c:if>
							</div>
						</n:panel>
						<t:property name="foto" onblur="validaImagem();" id="foto" colspan="4" showRemoverButton="false" showFileLink="false"/>
						<t:property name="razaoSocial" style='width:500px;' id="razaosocial"/>
						<t:property name="cnpj"  id="cnpj" style='width:110px;'/>
						<t:property name="logradouro"  style='width:500px;' id="endereco"/>
						<t:property name="bairro" style='width:300px;' id="bairro"/>
						<t:property name="cep" id="idcep" style='width:60px;'/>
						<n:comboReloadGroup useAjax="true">
							<t:property name="uf"/>
							<t:property name="municipio"/>
						</n:comboReloadGroup>
						<t:property name="email" style='width:300px;' id="email" />
						<c:if test="${empty empresa.idEmpresa}">
							<n:panel>Confirma��o de email</n:panel>
								<n:panel>
									<t:property name="confirmaEmail" id="novoemail" style='width:300px;' onblur="validaEmail()" class="required"/>
									<span id="confirm_alert_email" style='width:300px;' class="warning" > A confirma��o de email n�o confere</span>
							</n:panel>
						</c:if>
						<t:property name="inscricaoEstadual"/>
						<t:property name="inscricaoMunicipal"/>
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						
					</n:panelGrid>
				</n:panelGrid>
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="telefone1" id="idtelefone" style='width:80px;'/>
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="telefone2" id="idtelefone2" style='width:80px;'/>
					</n:panelGrid>
				</n:panelGrid>
				<t:detalhe name="listaRamal" labelnovalinha="Adicionar Ramal">
					<t:property name="idRamal" type="hidden" renderAs="single"/>
					<t:property name="numero"/>
					<t:property name="localizacao" style='width:300px;'/>
				</t:detalhe>
			</n:panel>
		
		
		
			
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">



$(document).ready(function() {
	$("#foto_removerbtn").click(function(){
		$("#imagem").fadeOut();
		$(this).fadeOut();
	});
	if($("#imagem").css("height") < 10){
		$("#divFoto").hide();
	}
});


function validaHorario(param){
	alert(param);
}

function validaEmail(){
    boolEmail = $("#email").val() == $("#novoemail").val();	
	alertConfEmail(boolEmail);

}

function alertConfEmail(show){
	if(show){
		$("#confirm_alert_email").fadeOut();
	}else{
		$("#confirm_alert_email").fadeIn();
	}
}
function valida(){
	boolEmail = $("#email").val() == $("#novoemail").val();	
	if(boolEmail){
		alert('A Redigita��o do E-mail N�o Confere.');
		alertConfEmail(boolEmail);
		return false;
	}
	else return true;
}

function imagemValida(){
	var arquivoValue = $("#foto").val();
		var extensao = arquivoValue.substring(arquivoValue.length-3,arquivoValue.length).toUpperCase();
		if (arquivoValue != ""){
			if(extensao == "PNG" || extensao == "JPG" || extensao == "JPEG" || extensao == "GIF" || extensao == "BMP"){
				return true;
			} else {
				return false;
			}
		}else return true;
}

function validaImagem(){
	var bool = imagemValida(); 
	if(!bool){
		document.getElementById("dialog_default").innerHTML = "<center><h3>Extens�o de imagem n�o permitida.\nExtens�es permitidas: png, jpg, jpeg, gif e bmp.</h3></center>";
		$('#dialog_default').dialog('open');			
		form['foto'].value = "";
	}
	return bool;
}
</script>
