<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>


<t:entrada  >	
		<t:property name="idContaPagar" type="hidden" write="false" label=""/>
	<t:janelaEntrada showSubmit="${!contaPagar.status}" submitConfirmationScript="validaDataPagamento()">
		<t:tabelaEntrada>
		
		<c:if test="${contaPagar.status == true}">
			<t:property name="fornecedor" mode="output"/>
			<t:property name="numeroDoc" mode="output"/>
			<t:property name="valor" mode="output"/>
			<t:property name="dtVencimento"  mode="output"/>
			<t:property name="formaPagamento" mode="output"/>
			<t:property name="tipo" mode="output"/>
			<t:property name="obs" mode="output"/>
			<t:property name="status" trueFalseNullLabels="Pago, Cancelado, Em Aberto" mode="output"/>
			<t:property name="dtPagamento" mode="output"/>
		</c:if>
		<c:if test="${empty contaPagar.status}">
			<t:property name="fornecedor" />
			<t:property name="numeroDoc"/>
			<t:property name="valor"/>
			<t:property name="dtVencimento"  onchange="validaDataAtual(this)" id="datepicker"/>
			<t:property name="formaPagamento"/>
			<t:property name="tipo"/>
			<t:property name="obs"/>
			<t:property name="status" id="status" trueFalseNullLabels="Pago, Cancelado, Em Aberto"/>
			<t:property name="dtPagamento" id="dtPagamento"/>
		</c:if>
		<c:if test="${contaPagar.status == false}">
			<t:property name="fornecedor"/>
			<t:property name="numeroDoc"/>
			<t:property name="valor"/>
			<t:property name="dtVencimento"  onchange="validaDataAtual(this)" id="datepicker"/>
			<t:property name="formaPagamento"/>
			<t:property name="tipo"/>
			<t:property name="obs"/>
			<t:property name="status" id="status" trueFalseNullLabels="Pago, Cancelado, Em Aberto"/>
			<t:property name="dtPagamento" id="dtPagamento" />
		</c:if>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">

$('#datepicker').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});

$('#dtRecebimento').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});

function validaDataPagamento(){
	var status = $("#status").val();
	var dtPagamento = $("#dtPagamento").val();
	if(status=='true'){
		if(dtPagamento == ""){
			document.getElementById("dialog_default").innerHTML = "<center><h3>A data de pagamento deve ser preenchida.</h3></center>"
				$('#dialog_default').dialog('open');
				return false;
		}else return true;
	}else
		return true;
	
}

function invalidFields(form, fields, msgs, validationName){
	document.getElementById("dialog_default").innerHTML = '<h3>'+msgs.join('\n')+'</h3>';
	afterRequired();
	hasFocus = false;
}

function afterRequired(){
	$('#dialog_aguarde').dialog('close');
	$('#dialog_default').dialog('open');
	return true;		
}	
function submitForm(param) {
	if(param == 1){
		document.getElementById("dialog_report").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando.<br>Ap�s o t�rmino clique em fechar.</h3></center>"
		$('#dialog_report').dialog('open');
	}else
		$('#dialog_aguarde').dialog('open');
	var validar = form.validate;
	try {
		validateForm;
	} catch (e) {
		validar = false;
	}
	try {
		clearMessages();//limpa as mensagens que vieram do servidor
	} catch(e){
	}
	if(validar == 'true') {
		var valid = validateForm();
		if(valid) {
			form.submit();
		}
	} else {
		form.submit();
	}
}


</script>