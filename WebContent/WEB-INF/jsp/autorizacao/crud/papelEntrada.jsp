<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<t:property name="id"/>
			<t:property name="nome"/>
			<t:property name="descricao"/>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
