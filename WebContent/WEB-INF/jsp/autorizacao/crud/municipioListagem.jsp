<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaResultados>
		<t:tabelaResultados showExcluirLink="false" showEditarLink="false">
			<t:property name="nome" />
			<t:property name="uf.sigla" label="UF"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>