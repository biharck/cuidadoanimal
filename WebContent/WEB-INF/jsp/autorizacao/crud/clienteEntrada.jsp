<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<div class="tooltip"></div>
<t:entrada >	
	<t:property name="idCliente" type="hidden" write="false"/>
	<t:janelaEntrada >
		<t:tabelaEntrada >
			<n:panel>				
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" style="width:600px;" columnStyles="width:200px,width:900px;">
						<t:property name="nome" style='width:500px;' id="razaosocial"/>
						<t:property name="cpf"  id="cpf" type="cpf" style='width:110px;'/>
						<t:property name="rg"  id="rg" style='width:110px;'/>						
						<t:property name="logradouro"  style='width:500px;' id="endereco"/>
						<t:property name="bairro" style='width:300px;' id="bairro"/>
						<t:property name="cep" type="CEP" id="idcep" style='width:60px;'/>
						<n:comboReloadGroup useAjax="true">
							<t:property name="uf"/>
							<t:property name="municipio"/>
						</n:comboReloadGroup>
						<t:property name="email" style='width:300px;' id="email" />
						<c:if test="${empty cliente.idCliente}">
							<n:panel>Confirma��o de email</n:panel>
								<n:panel>
									<t:property name="confirmaEmail" id="novoemail" style='width:300px;' onblur="validaEmail()" class="required"/>
									<span id="confirm_alert_email" style='width:300px;' class="warning" > A confirma��o de email n�o confere</span>
							</n:panel>
						</c:if>
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
					</n:panelGrid>
				</n:panelGrid>
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="telefone1" type="TELEFONE" id="idtelefone" style='width:80px;'/>
						<t:property name="celular" type="TELEFONE" id="idcelular" style='width:80px;'/>
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="telefone2" type="TELEFONE" id="idtelefone2" style='width:80px;'/>
					</n:panelGrid>
				</n:panelGrid>
			</n:panel>
		</t:tabelaEntrada>
		<t:detalhe name="listaAnimal"  showBotaoRemover="false" >
			<n:column header="Hist�rico" >
				<n:panelGrid columns="4" style="text-transform: uppercase; width:300px;">
					<t:propertyConfig mode="input" showLabel="true" renderAs="double">
						<t:property name="foto"  colspan="4" showRemoverButton="false" showFileLink="false" type="hidden" label=""/>
						<t:property name="nome" colspan="4" style="width:500px;"/>
						<t:property name="apelido"/>
						<t:property name="cor"/>
						<t:property name="especie" label="Esp�cie"/>
						<t:property name="raca" label="Ra�a"/>
						<t:property name="dtNascimento" onchange="validaDataNasc(this)"/>
						<t:property name="dtObito"/>
						<t:property name="RGA"/>
						<t:property name="microchip"/>
						<t:property name="desaparecido" id="idDesaparecido" onclick="enableDisable(this);" label="Animal Desaparecido?"/>
						<t:property name="doado" label="Animal Doado?" />
						<t:property name="dtDesaparecimento" colspan="4" id="idDtDesaparecimento" disabled="disabled"/>
						<t:property name="observacoes" rows="5" cols="100" colspan="4"/>
						<t:acao>
							<t:propertyConfig renderAs="single">
								<t:property name="idAnimal" type="hidden" write="false" label=""/>
							</t:propertyConfig>
						</t:acao>
						<c:if test="${!empty animal.idAnimal}">
							<t:acao>
								<n:submit url="/autorizacao/clinica/crud/Animal" action="consultar" parameters="idAnimal=${animal.idAnimal}">Ver Animal</n:submit>
							</t:acao>
						</c:if>
					</t:propertyConfig>
				</n:panelGrid>
			</n:column>
			
			

		</t:detalhe>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
function validaEmail(){
    boolEmail = $("#email").val() == $("#novoemail").val();	
	alertConfEmail(boolEmail);

}

function alertConfEmail(show){
	if(show){
		$("#confirm_alert_email").fadeOut();
	}else{
		$("#confirm_alert_email").fadeIn();
	}
}
function valida(){
	boolEmail = $("#email").val() == $("#novoemail").val();	
	if(boolEmail){
		alert('A Redigita��o do E-mail N�o Confere.');
		alertConfEmail(boolEmail);
		return false;
	}
	else return true;
}
function imagemValida(){
	var arquivoValue = $("#foto").val();
		var extensao = arquivoValue.substring(arquivoValue.length-3,arquivoValue.length).toUpperCase();
		if (arquivoValue != ""){
			if(extensao == "PNG" || extensao == "JPG" || extensao == "JPEG" || extensao == "GIF" || extensao == "BMP"){
				return true;
			} else {
				return false;
			}
		}else return true;
}

function validaImagem(){
	var bool = imagemValida();
	if(!bool){
		$s.showAlertMessage("Extens�o de imagem n�o permitida.\nExtens�es permitidas: png, jpg, jpeg, gif e bmp.");
		form['foto'].value = "";
	}else{
		$s.hideAlertMessage();
	}
	return bool;
}

function submitForm() {
	$('#dialog_aguarde').dialog('open');
	var validar = form.validate;
	try {
		validateForm;
	} catch (e) {
		validar = false;
	}
	try {
		clearMessages();//limpa as mensagens que vieram do servidor
	} catch(e){
	}
	if(validar == 'true') {
		var valid = validateForm();
		if(valid) {
			form.submit();
		}
	} else {
		form.submit();
	}
}

</script>
