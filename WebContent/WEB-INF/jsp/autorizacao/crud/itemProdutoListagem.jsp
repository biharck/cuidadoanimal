<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<t:property name="codProduto"/>
			<t:property name="nome"/>
			<t:property name="lote"/>
		</t:tabelaFiltro>
	</t:janelaFiltro >
	<t:janelaResultados >
		<t:tabelaResultados showExcluirLink="false" >
			<t:property name="produtoServico"/>
			<t:property name="lote" />
			<t:property name="quantidadeRestante"/>			
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
