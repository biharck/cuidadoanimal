<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>


<c:if test="${cadAnimal}">
<t:listagem >
	<t:janelaFiltro >
		<t:tabelaFiltro columns="4" >
			<t:property name="cliente" class="disabled-input"  id="cliente" />
			<t:property name="cor" id="cor"/>
			<t:property name="enderecoResumido" id="enderecoResumido" colspan="4"/>
			<t:property name="nomeAnimal" id="nomeAnimal"/>
			<t:property name="apelidoAnimal" id="apelidoAnimal"/>
			<n:comboReloadGroup useAjax="true">
				<t:property name="especie" id="especie" label="Esp�cie"/>
				<t:property name="raca" id="raca" label="Ra�a"/>
			</n:comboReloadGroup>
			<t:property name="numeroRegistro" id="numeroRegistro" OnKeyPress="formatar(this, '#####-###')"/>
		</t:tabelaFiltro>
		<div class="actionBar">
			<n:submit type="submit" action="${TabelaFiltroTag.submitAction}" validate="${TabelaFiltroTag.validateForm}" url="${TabelaFiltroTag.submitUrl}">Pesquisar</n:submit>
			<span id="limpar2">&nbsp;&nbsp;|&nbsp;&nbsp;
			<a href="#" title=""   id='btn_limpar' onclick='javascript:clearForm();submitForm();' onmouseover='Tip("Limpar filtro")'>Reiniciar Pesquisa</a>&nbsp;&nbsp;</span>
		</div>
		
	</t:janelaFiltro >
	<t:janelaResultados  >
		<t:tabelaResultados showExcluirLink="false"  showEditarLink="false"  >
			<t:property name="nome" />
			<t:property name="apelido" />
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
</c:if>

<c:if test="${empty cadAnimal}">
<t:listagem  showNewLink="false" titulo="Atendimento do Animal">
	<t:janelaFiltro >
		<t:tabelaFiltro columns="4" >
			<t:property name="cliente" class="disabled-input"  id="cliente" />
			<t:property name="cor" id="cor"/>
			<t:property name="enderecoResumido" id="enderecoResumido" colspan="4"/>
			<t:property name="nomeAnimal" id="nomeAnimal"/>
			<t:property name="apelidoAnimal" id="apelidoAnimal"/>
			<n:comboReloadGroup useAjax="true">
				<t:property name="especie" id="especie" label="Esp�cie"/>
				<t:property name="raca" id="raca" label="Ra�a"/>
			</n:comboReloadGroup>
			<t:property name="numeroRegistro" id="numeroRegistro" OnKeyPress="formatar(this, '#####-###')"/>
		</t:tabelaFiltro>
		<div class="actionBar">
			<n:submit type="submit" action="${TabelaFiltroTag.submitAction}" validate="${TabelaFiltroTag.validateForm}" url="${TabelaFiltroTag.submitUrl}">Pesquisar</n:submit>
			<span id="limpar2">&nbsp;&nbsp;|&nbsp;&nbsp;
			<a href="#" title=""   id='btn_limpar' onclick='javascript:clearForm();submitForm();' onmouseover='Tip("Limpar filtro")'>Reiniciar Pesquisa</a>&nbsp;&nbsp;</span>
		</div>
		
	</t:janelaFiltro >
	<t:janelaResultados  >
			<t:tabelaResultados showExcluirLink="false"  showEditarLink="false" showConsultarLink="false">
				<t:property name="nome" />
				<t:property name="apelido" />
				<n:column header="Atender" style="width:5%;" >
					<n:link action="editar" onmouseover="Tip(\"Realizar Atendimento\")" class="activation" onclick="javascript:dialog()" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}&cadAnimal=true" img="/CuidadoAnimal/images/atender.png"></n:link>&nbsp;&nbsp;
				</n:column>
			</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
</c:if>



<script type="text/javascript">
function formatar(src, mask){
  var i = src.value.length;
  var saida = mask.substring(0,1);
  var texto = mask.substring(i)
if (texto.substring(0,1) != saida)
  {
    src.value += texto.substring(0,1);
  }
}


	$(document).ready(function() {
		$('#limpar').hide();
		$('#filtar').hide();
	});
	function clearForm (){
		$('#cliente').val("");
		$('#cor').val("");
		$('#enderecoResumido').val("");
		$('#nomeAnimal').val("");
		$('#apelidoAnimal').val("");
		$('#especie').val("");
		$('#raca').val("");
		$('#numeroRegistro').val("");
	}

</script>