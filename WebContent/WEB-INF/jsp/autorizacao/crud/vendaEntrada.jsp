<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>


<t:entrada > 	
	<t:property name="idVenda" type="hidden" write="false" />
	<t:janelaEntrada showSubmit="${empty CONS}">
		<t:tabelaEntrada>
			<n:panel>				
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" style="width:600px;" columnStyles="width:200px,width:900px;">
						<t:property name="cliente" selectOnePath="/autorizacao/clinica/crud/Cliente?escondeMenu=true"/>
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						
					</n:panelGrid>
				</n:panelGrid>
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
					</n:panelGrid>
				</n:panelGrid>
					<t:detalhe name="servicosPrestados">
						<t:property name="itemProduto" selectOnePath="/autorizacao/crud/ItemProduto?escondeMenu=true" onchange="mostrarCampos(${index})"/>
						<t:property name="produto" disabled="disabled"/>
						<t:property name="resto" disabled="disabled" label="Quantidade Dispon�vel"/>
						<t:property name="qtd"  onchange="validaQtd(this.value, ${index})"/>
						<t:property name="desconto"/>
						<t:acao>
							<t:propertyConfig renderAs="single">
								<t:property name="idServicosPrestados" type="hidden" write="false" label=""/>
							</t:propertyConfig>
						</t:acao>
					</t:detalhe>
			</n:panel>
		</t:tabelaEntrada>	
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">

	function validaQtd(param1,idx){
		var selecionado = parseInt(param1);
		var restante   = parseInt($(form["servicosPrestados["+idx+"].resto"]).val());
		if(selecionado > restante){
			document.getElementById("dialog_default").innerHTML = "<center><h3>A quantidade a ser vendida n�o pode ser maior do que o dispon�vel em estoque para este lote.</h3></center>"
			$('#dialog_default').dialog('open');
			$(form["servicosPrestados["+idx+"].qtd"]).val("");	
		}
	}

	function mostrarCampos(index) {
		var a = $(form["servicosPrestados["+index+"].itemProduto"]).val();
		a = a.replace('seguranca.autorizacao.bean.ItemProduto[idItemProduto=','');
	    sendRequest('/CuidadoAnimal/autorizacao/crud/Pagamento?id='+a+'&idx='+index,'ACAO=getProduto','POST', ajaxCallback, erroCallback);
	}

	function ajaxCallback(data){
		eval(data);
		$(form["servicosPrestados["+idx+"].produto"]).val(msg);
		$(form["servicosPrestados["+idx+"].resto"]).val(resto);
	}

	function erroCallback(request){
	    alert('Erro no ajax!\n' + request.responseText);
	
	}
	var form = document.forms["form"];
	form.validate ='true';
	function submitForm() {
		document.getElementById("dialog_report").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando.<br></h3></center>"
		$('#dialog_report').dialog('open');
		var validar = form.validate;
		try {
			validarFormulario;
		} catch (e) {
			validar = false;
		}
		try {
			clearMessages();//limpa as mensagens que vieram do servidor
		} catch(e){
		}
		var valid = validarFormulario();
		if(valid) {
			form.submit();
		}
	}
</script>
