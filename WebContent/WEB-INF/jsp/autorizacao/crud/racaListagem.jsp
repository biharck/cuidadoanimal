<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<t:property name="especie" id="especie" />
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<div class="actionBar">
		<n:submit type="submit" action="${TabelaFiltroTag.submitAction}" validate="${TabelaFiltroTag.validateForm}" url="${TabelaFiltroTag.submitUrl}">Pesquisar</n:submit>
		<span id="limpar2">&nbsp;&nbsp;|&nbsp;&nbsp;
		<a href="#" title=""   id='btn_limpar' onclick='javascript:clearForm();submitForm();' onmouseover='Tip("Limpar filtro")'>Reiniciar Pesquisa</a>&nbsp;&nbsp;</span>
	</div>
	<t:janelaResultados>
		<t:tabelaResultados showExcluirLink="false">
			<t:property name="nome" />
			<t:property name="especie" />
			<t:property name="ativo" trueFalseNullLabels="Ativo,Inativo," />
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>


<script type="text/javascript">


	$(document).ready(function() {
		$('#limpar').hide();
		$('#filtar').hide();
	});
	function clearForm (){
		$('#especie').val("");
	}

</script>