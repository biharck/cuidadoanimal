<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="Sa�da" validateForm="validaValores()">
<div class="divheader"></div>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:bean name="entradaSaidaCaixa" >
				<t:propertyConfig renderAs="double">
					<t:property name="idEntradaSaidaCaixa" type="hidden" write="false" label=""/>
					<t:property name="tipoOperacao" mode="output"/>
					<t:property name="valor" id="valor"/>
					<t:property name="valorConfirmacao" id="valorConf"/>
				</t:propertyConfig>
			</n:bean>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:tela>
<script type="text/javascript">
function fechaJanela(){
	window.close()
}

function validaValores(){
	if($("#valor").val()!=$("#valorConf").val()){
		alert("O valores informados devem ser iguais.")
		return false;
	}
	else
		return true;
	
}
</script>




