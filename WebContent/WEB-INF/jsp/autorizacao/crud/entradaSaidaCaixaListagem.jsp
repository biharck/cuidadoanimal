<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem showNewLink="false" titulo="Entradas / Sa�das de Caixa" >
	<t:janelaFiltro >
		<t:tabelaFiltro  >
			<t:property name="tipoOperacao"/>
			<t:property name="valor" />
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados  >
		<t:tabelaResultados showExcluirLink="false" showConsultarLink="true"  showEditarLink="false" >
			<t:property name="valor" />
			 <t:property name="formaPagamento"/>
			 <t:property name="tipoOperacao"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
