<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada  >	
<c:if test="${empty cadAnimal}">
<table>
	<tr>
		<td>
			<div align="left" id="divFoto">
				<c:if test="${exibeFoto}">
					
						<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${animal.foto.cdfile}" id="imagem" class="image"/>
					
					<c:if test="${param.ACAO == 'criar' || param.ACAO == 'editar'}">
						<br><br>
					</c:if>
				</c:if>
				<c:if test="${!exibeFoto}">
					<div class="image">
						<img src="${pageContext.request.contextPath}/images/imgnaodisponivel.jpg" id="imagem" style="padding-top:13px;"/>
					</div>
				</c:if>
			</div>
		</td>
		<td>
			<div align="right" id="divDados">
				<b>Animal:<b/><font color="#3B8105">${animal.nome}</font><br>
				<b>Apelido:<b/><font color="#3B8105">${animal.apelido}</font><br>
				<b>Dono:<b/><font color="#3B8105">${nameDono}</font><br><br>
				<button onclick="javascript:submitReport(${animal.idAnimal})" style="width:150px;">Imprimir Prontu�rio</button>
			</div>
		</td>
	</tr>
</table>
</c:if>
	<t:janelaEntrada >
		<t:tabelaEntrada >
			
			<n:panel>
				<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
					<n:panel colspan="2" style="text-align:center;">
						<div align="center" id="divFoto">
							<c:if test="${exibeFoto}">
								<n:link url="/DOWNLOADFILE/${animal.foto.cdfile}">
									<img src="/CuidadoAnimal/DOWNLOADFILE/${animal.foto.cdfile}" id="imagem" class="foto"/>
								</n:link>
								<c:if test="${param.ACAO == 'criar' || param.ACAO == 'editar'}">
									<br><br>
									<button id="foto_removerbtn" type="button" style="text-transform: none" onclick="document.getElementById('foto_excludeField').value='true';">Remover</button>
								</c:if>				
							</c:if>
							<c:if test="${!exibeFoto}">
								<div class="image">
									<img src="/CuidadoAnimal/imagens/imgnaodisponivel.jpg" id="imagem" style="padding-top:13px;"/>
								</div>
							</c:if>
						</div>
					</n:panel>
					<t:property name="foto" onblur="validaImagem();" id="foto" colspan="4" showRemoverButton="false" showFileLink="false"/>
				</n:panelGrid>				
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" style="width:600px;" columnStyles="width:200px,width:900px;">
						<t:property name="idAnimal" type="hidden" write="false" label=""/>
						<t:property name="nome" colspan="4" style="width:500px;"/>
						<t:property name="apelido"/>
						<t:property name="cor"/>
						<n:comboReloadGroup useAjax="true">
							<t:property name="especie" label="Esp�cie"/>
							<t:property name="raca" label="Ra�a"/>
						</n:comboReloadGroup>
					</n:panelGrid>
				</n:panelGrid>
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						
						<t:property name="dtNascimento" onchange="validaDataNasc(this)" label="Nascimento" id="idNascimento"/>
						<t:property name="dtObito" label="�bito" id="idDtObito"/>
						<t:property name="dono" selectOnePath="/autorizacao/clinica/crud/Cliente?escondeMenu=true"/>
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="microchip"/>
						<t:property name="RGA"/>
					</n:panelGrid>
				</n:panelGrid>
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="desaparecido" id="idDesaparecido" onclick="enableDisable(this);" label="Animal Desaparecido?"/>
						<t:property name="doado" label="Animal Doado?" />
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="dtDesaparecimento" id="idDtDesaparecimento" disabled="disabled" label="Data de Desaparecimento"/>
					</n:panelGrid>
				</n:panelGrid>
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="observacoes" rows="7" cols="80" colspan="4" label="Observa��es"/>
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
					</n:panelGrid>
				</n:panelGrid>
			</n:panel>
		</t:tabelaEntrada>
		<t:detalhe name="listaHistoricoClinico" showBotaoNovaLinha="false" showBotaoRemover="false" >
			<n:column header=" " >
				<n:panelGrid columns="4" style="text-transform: uppercase; width:100%;">
					<t:propertyConfig mode="input" showLabel="true" renderAs="double">
						<t:property name="dataRealizacao" disabled="disabled"/>
						<t:property name="tipo"  disabled="disabled"/>
						<t:property name="descricao" rows="5" cols="80" colspan="4" disabled="disabled"/>
						<t:acao>
							<t:propertyConfig renderAs="single">
								<t:property name="idConsultas" type="hidden" label="" renderAs="single"/>
								<br>
								<br>
								<button onclick="javascript:submitReport2(${historicoClinico.idConsultas})" style="width:150px;">Imprimir Receitu�rio</button>
							</t:propertyConfig>
						</t:acao>
					</t:propertyConfig>
				</n:panelGrid>
			</n:column>
		</t:detalhe>
		<t:detalhe name="listaConsultas" labelnovalinha="Nova Consulta">
			<n:column header=" " >
				<n:panelGrid columns="4" style="text-transform: uppercase; width:100%;">
					<t:propertyConfig mode="input" showLabel="true" renderAs="double">
						<t:property name="data" colspan="2"/>
						<t:property name="peso" colspan="2"/>
						<t:property name="veterinario" colspan="4" label="Veterin�rio"/>
						<t:property name="descricao" rows="5" cols="80" colspan="4"/>
						<t:property name="receituario" rows="5" cols="80" colspan="4"/>
						<t:property name="retorno" label="Retorno?"/>
						<t:acao>
							<t:propertyConfig renderAs="single">
								<t:property name="idConsultas" type="hidden" label="" renderAs="single"/>
							</t:propertyConfig>
						</t:acao>
					</t:propertyConfig>
				</n:panelGrid>
			</n:column>
		</t:detalhe>
		
		<t:detalhe name="listaVacinas" labelnovalinha="Nova Vacina" >
			<n:column header=" " >
				<n:panelGrid columns="4" style="text-transform: uppercase; width:100%;">
					<t:propertyConfig mode="input" showLabel="true" renderAs="double">
						<t:property name="produtoServico" colspan="4" itens="${listaVacinas}" label="Produto/Servi�o"/>
						<t:property name="dtAplicacao" colspan="2" label="Data de Aplica��o"/>
						<t:property name="peso" colspan="2"/>
						<t:property name="dtProximaAplicacao" colspan="2" label="Pr�xima Aplica��o"/>
						<t:property name="dtAgendamento" colspan="2" label="Agendamento"/>
						<t:property name="dose" colspan="2"/>
						<t:acao>
							<t:propertyConfig renderAs="single">
								<t:property name="idVacina" type="hidden" label="" renderAs="single"/>
							</t:propertyConfig>
						</t:acao>
					</t:propertyConfig>
				</n:panelGrid>
			</n:column>
		</t:detalhe>
		<t:detalhe name="listaVermifugos" labelnovalinha="Adicionar Vermifugo">
			<n:column header=" " >
				<n:panelGrid columns="4" style="text-transform: uppercase; width:100%;">
					<t:propertyConfig mode="input" showLabel="true" renderAs="double">
					<t:property name="produtoServico" colspan="4" itens="${listaVermifugos}" label="Produto/Servi�o"/>
						<t:property name="dtAplicacao" colspan="2" label="Data de Aplica��o"/>
						<t:property name="peso" colspan="2"/>
						<t:property name="dtProximaAplicacao" colspan="2" label="Pr�xima Aplica��o"/>
						<t:property name="dose" colspan="2"/>						
						<t:acao>
							<t:propertyConfig renderAs="single">
								<t:property name="idVermifugo" type="hidden" label="" renderAs="single"/>
							</t:propertyConfig>
						</t:acao>
					</t:propertyConfig>
				</n:panelGrid>
			</n:column>
		</t:detalhe>
		<t:detalhe name="listaAnticoncepcional" labelnovalinha="Adicionar Anticoncepcional">
			<n:column header=" " >
				<n:panelGrid columns="4" style="text-transform: uppercase; width:100%;">
					<t:propertyConfig mode="input" showLabel="true" renderAs="double">
						<t:property name="produtoServico" colspan="4" itens="${listaAntiConcepcional}" label="Produto/Servi�o"/>
						<t:property name="dtAplicacao" colspan="2" label="Data de Aplica��o"/>
						<t:property name="peso" colspan="2"/>
						<t:property name="dtProximaAplicacao" colspan="2" label="Pr�xima Aplica��o"/>
						<t:property name="dose" colspan="2"/>						
						<t:acao>
							<t:propertyConfig renderAs="single">
								<t:property name="idAnticoncepcional" type="hidden" label="" renderAs="single"/>
							</t:propertyConfig>
						</t:acao>
					</t:propertyConfig>
				</n:panelGrid>
			</n:column>
		</t:detalhe>
		<t:detalhe name="listaAntiPulgas" labelnovalinha="Adicionar Anti Pulgas">
			<n:column header=" " >
				<n:panelGrid columns="4" style="text-transform: uppercase; width:100%;">
					<t:propertyConfig mode="input" showLabel="true" renderAs="double">
						<t:property name="produtoServico" colspan="4" itens="${listaAntiPulgas}" label="Produto/Servi�o"/>
						<t:property name="dtAplicacao" colspan="2" label="Data de Aplica��o"/>
						<t:property name="peso" colspan="2"/>
						<t:property name="dtProximaAplicacao" colspan="2" label="Pr�xima Aplica��o"/>
						<t:property name="dose" colspan="2"/>						
						<t:acao>
							<t:propertyConfig renderAs="single">
								<t:property name="idAntiPulgas" type="hidden" label="" renderAs="single"/>
							</t:propertyConfig>
						</t:acao>
					</t:propertyConfig>
				</n:panelGrid>
			</n:column>
		</t:detalhe>
		<t:detalhe name="listaEstetica">
			<n:column header=" " >
				<n:panelGrid columns="4" style="text-transform: uppercase; width:100%;">
					<t:propertyConfig mode="input" showLabel="true" renderAs="double">
						<t:property name="esteticista"/>
						<t:property name="data"/>
						<t:property name="horaChegada" label="Hora de Chegada"/>
						<t:property name="horaEntrega" label="Hora de Entrega"/>
						<t:property name="status" trueFalseNullLabels="Conclu�do,Angendado,Iniciar Servi�o" label="situa��o"/>
						<t:property name="transporte" itens="${listaTransportes}"/>
						<t:property name="procedimentoClinico" trueFalseNullLabels="Sim, N�o," label="Procedimento Cl�nico"/>
						<t:property name="ligarCliente" trueFalseNullLabels="Sim, N�o," label="Ligar para Cliente?"/>
						<t:property name="entregue" trueFalseNullLabels="Sim, N�o," cols="4" label="Entregue?"/>
						<t:property name="obs" rows="7" cols="60" colspan="4" label="Observa��es"/>
						<t:acao>
							<t:propertyConfig renderAs="single">
								<t:property name="idEstetica" type="hidden" label="" renderAs="single"/>
							</t:propertyConfig>
						</t:acao>
					</t:propertyConfig>
				</n:panelGrid>
			</n:column>
		</t:detalhe>
		<t:detalhe name="listaServicosPrestados">
			<n:column header=" " >
				<n:panelGrid columns="4" style="text-transform: uppercase; width:100%;">
					<t:propertyConfig mode="input" showLabel="true" renderAs="double">
						<t:property name="itemProduto"  selectOnePath="/autorizacao/crud/ItemProduto?escondeMenu=true" onchange="mostrarCampos(${index})" label="Produto/Lote"/>
						<t:property name="produto" disabled="disabled" label="Nome"/>	
						<t:property name="qtd" label="Quantidade"/>
						<t:acao>
							<t:propertyConfig renderAs="single">
								<t:property name="idServicosPrestados" type="hidden" label="" renderAs="single"/>
							</t:propertyConfig>
						</t:acao>
					</t:propertyConfig>
				</n:panelGrid>
			</n:column>
		</t:detalhe>
	</t:janelaEntrada>
	<div class="actionBar">
		<c:if test="${consultar}">
			<script language="javascript">
				function doeditar(){
					form['forcarConsulta'].value = false;
					return true;
				}
			</script>
			<n:submit url="/autorizacao/clinica/crud/Animal?ACAO=editar&idAnimal=${animal.idAnimal}" action="editar" validate="false" confirmationScript="doeditar()">Editar</n:submit>
		</c:if>
		<c:if test="${!consultar}">
			<button title="" onclick="form.ACAO.value ='salvar';form.action = '/CuidadoAnimal/autorizacao/clinica/crud/Animal'; form.validate = 'true'; submitForm()" type="button">Salvar</button>
		</c:if>							
	</div>
	<c:if test="${empty cadAnimal}">
		<div id="divsup" align="center"><a href='javascript:abreDiv()'>Hist�rico ( + )</a></div>
		<div id="divsupclose" align="center" style="display:none;"><a href='javascript:fechaDiv()'>Hist�rico ( - )</a></div>
		<div style="display:none;" id="idservicos">
 			<t:detalhe name="listaServicosPrestadosAntiga" showBotaoNovaLinha="false" showBotaoRemover="false" showColunaAcao="false">
				<n:column header="Produto">
					<t:property name="itemProduto.produtoServico" disabled="disabled"/>
				</n:column>
	 			<n:column header="Lote">
					<t:property name="itemProduto"  mode="output" style="color: blue" />
				</n:column>
				<n:column header="Quantidade">
					<t:property name="qtd" mode="output" style="color: blue"/>
				</n:column>
				<n:column header="Data">
					<t:property name="date"  mode="output" style="color: blue"/>
				</n:column>
			</t:detalhe>
		</div>
	</c:if>			
</t:entrada>

<script type="text/javascript">
$('#idNascimento').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});
$('#idDtObito').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});
$('#idDesaparecido').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});

$(document).ready(function() {
	var cadastro = "${cadAnimal}";
	if(cadastro){
		$("#janelaEntrada_link_1").hide();
		$("#janelaEntrada_link_2").hide();
		$("#janelaEntrada_link_3").hide();
		$("#janelaEntrada_link_4").hide();
		$("#janelaEntrada_link_5").hide();
		$("#janelaEntrada_link_6").hide();
		$("#janelaEntrada_link_7").hide();
		$("#janelaEntrada_link_8").hide();
		showjanelaEntrada('janelaEntrada_0', 0, 'janelaEntrada_link_0');
	}else{
		$("#janelaEntrada_link_0").hide();
		showjanelaEntrada('janelaEntrada_1', 1, 'janelaEntrada_link_1'); 
	}
		
	$("#editButton").hide();
	
	$("#cpf").mask("999.999.999-99");
	$("#idcep").mask("99.999-999");
	$("#idtelefone").mask("(99)9999-9999");
	$("#idtelefone2").mask("(99)9999-9999");
	$("#idcelular").mask("(99)9999-9999");

	$("#foto_removerbtn").click(function(){
		$("#imagem").fadeOut();
		$(this).fadeOut();
	});
	if($("#imagem").css("height") < 10){
		$("#divFoto").hide();
	}
	

});

function imagemValida(){
	var arquivoValue = $("#foto").val();
		var extensao = arquivoValue.substring(arquivoValue.length-3,arquivoValue.length).toUpperCase();
		if (arquivoValue != ""){
			if(extensao == "PNG" || extensao == "JPG" || extensao == "JPEG" || extensao == "GIF" || extensao == "BMP"){
				return true;
			} else {
				return false;
			}
		}else return true;
}

function validaImagem(){
	var bool = imagemValida(); 
	if(!bool){
		document.getElementById("dialog_default").innerHTML = "<center><h3>Extens�o de imagem n�o permitida.\nExtens�es permitidas: png, jpg, jpeg, gif e bmp.</h3></center>";
		$('#dialog_default').dialog('open');			
		form['foto'].value = "";
	}
	return bool;
}
function abreDiv(){
	$("#idservicos").slideDown();
	document.getElementById("divsup").style.display="none";
	document.getElementById("divsupclose").style.display="";
	
}
function fechaDiv(){
 	$("#idservicos").slideUp();
	document.getElementById("divsup").style.display="";
	document.getElementById("divsupclose").style.display="none";
}

function validaEmail(){
    boolEmail = $("#email").val() == $("#novoemail").val();	
	alertConfEmail(boolEmail);

}

function alertConfEmail(show){
	if(show){
		$("#confirm_alert_email").fadeOut();
	}else{
		$("#confirm_alert_email").fadeIn();
	}
}
function valida(){
	boolEmail = $("#email").val() == $("#novoemail").val();	
	if(boolEmail){
		alert('A Redigita��o do E-mail N�o Confere.');
		alertConfEmail(boolEmail);
		return false;
	}
	else return true;
}
function imagemValida(){
	var arquivoValue = $("#foto").val();
		var extensao = arquivoValue.substring(arquivoValue.length-3,arquivoValue.length).toUpperCase();
		if (arquivoValue != ""){
			if(extensao == "PNG" || extensao == "JPG" || extensao == "JPEG" || extensao == "GIF" || extensao == "BMP"){
				return true;
			} else {
				return false;
			}
		}else return true;
}

function validaImagem(){
	var bool = imagemValida();
	if(!bool){
		$s.showAlertMessage("Extens�o de imagem n�o permitida.\nExtens�es permitidas: png, jpg, jpeg, gif e bmp.");
		form['foto'].value = "";
	}else{
		$s.hideAlertMessage();
	}
	return bool;
}
function enableDisable(valor){
	if(valor.checked==true){
		$("#idDtDesaparecimento").removeAttr("disabled");
	}else{
		$("#idDtDesaparecimento").attr("disabled","disabled");
	}
	
	
}

function mostrarCampos(index) {
	var a = $(form["listaServicosPrestados["+index+"].itemProduto"]).val();
	a = a.replace('seguranca.autorizacao.bean.ItemProduto[idItemProduto=','');
    sendRequest('/CuidadoAnimal/autorizacao/crud/Pagamento?id='+a+'&idx='+index,'ACAO=getProduto','POST', ajaxCallback, erroCallback);
}

function ajaxCallback(data){
	eval(data);
	$(form["listaServicosPrestados["+idx+"].produto"]).val(msg);
}

function erroCallback(request){
    alert('Erro no ajax!\n' + request.responseText);

}
function submitReport(idConsulta){
	form.ACAO.value ='gerar';
	form.action = '/CuidadoAnimal/autorizacao/relatorio/Prontuario?ACAO=gerar&id='+idConsulta;
	form.validate = 'true';
	submitForm(1);
}

function submitReport2(idConsulta){
	form.ACAO.value ='gerar';
	form.action = '/CuidadoAnimal/autorizacao/relatorio/Receituario?ACAO=gerar&id='+idConsulta;
	form.validate = 'true';
	submitForm(1);
}

function submitForm(param) {
	if(param == 1){
		document.getElementById("dialog_report").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando.<br>Ap�s o t�rmino clique em fechar.</h3></center>"
		$('#dialog_report').dialog('open');
	}else
		$('#dialog_aguarde').dialog('open');
	var validar = form.validate;
	try {
		validateForm;
	} catch (e) {
		validar = false;
	}
	try {
		clearMessages();//limpa as mensagens que vieram do servidor
	} catch(e){
	}
	if(validar == 'true') {
		var valid = validateForm();
		if(valid) {
			form.submit();
		}
	} else {
		form.submit();
	}
}


function invalidFields(form, fields, msgs, validationName){
	document.getElementById("dialog_default").innerHTML = '<h3>'+msgs.join('\n')+'</h3>';
	afterRequired();
	hasFocus = false;
}

function afterRequired(){
	$('#dialog_aguarde').dialog('close');
	$('#dialog_default').dialog('open');
	return true;		
}	

</script>
<style type="text/css">
.dataGrid thead * {
	color:#000;
}
.dataGridHeader *  {
	background-color:#dadada;
	background-repeat: repeat-x;
	color:#fff;
}
.divFoto{
	background-position: center;
}
</style>