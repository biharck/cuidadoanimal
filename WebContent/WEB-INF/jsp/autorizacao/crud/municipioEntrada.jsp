<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>


<t:entrada>	
	<t:property name="idMunicipio" type="hidden" write="false"/>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<t:property name="uf"/>
			<t:property name="nome"/>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>