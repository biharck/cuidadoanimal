<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>


<t:entrada >	
	<t:property name="idProdutoServico" id="idGeral" type="hidden" write="false"/>
	<t:property name="valorProdutoServico.idValorProdutoServico" type="hidden" write="false"/>
	<t:janelaEntrada submitConfirmationScript="validavalores()">
		<t:tabelaEntrada>
			<n:panel>				
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" style="width:600px;" columnStyles="width:200px,width:900px;">
						<t:property name="codigo" label="c�digo"/>
						<t:property name="nome" style="width:600px;" />
						<n:comboReloadGroup useAjax="true">
							<t:property name="subTipoProduto.tipoProduto" label="Tipo de Produto"/>
							<t:property name="subTipoProduto" label="Sub-Tipo de Produto"/>
						</n:comboReloadGroup>
						<t:property name="ativo"/>
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						
						
					</n:panelGrid>
				</n:panelGrid>
				
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="valorProdutoServico.valor" id="valor" label="Valor (R$)" onblur="calculaMargem(this.value)"/>
						<t:property name="stock" disabled="disabled" style="background:#FFF9D0"/>
						<t:property name="qtdMinima" label="Estoque M�nimo"/>
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="valorProdutoServico.margemLucro" id="margem" onblur="calculaMargem(this.value)"/>
						<t:property name="valorProdutoServico.valorFim" id="valorFim" colspan="4" disabled="disabled" label="Valor Final (R$)" style="background:#FFF9D0"/>
					</n:panelGrid>
				</n:panelGrid>
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="observacoes" rows="5" cols="80" colspan="4" label="Observa��es"/>
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
	
					</n:panelGrid>
				</n:panelGrid>
			</n:panel>
		</t:tabelaEntrada>
		<t:detalhe name="listaItensProdutos" showBotaoNovaLinha="false" showBotaoRemover="false" showColunaAcao="false">
			<t:property name="lote" mode="output"/>
			<n:column header="Quantidade de Entrada">
				<center><t:property name="quantidadeEntrada" mode="output" /></center>
			</n:column>
			<n:column header="Quantidade Restante">
				<center><t:property name="quantidadeRestante" mode="output"/></center>
			</n:column>
			<t:property name="dtEntrada" mode="output" label="data de entrada"/>
			<t:property name="dtVencimento" mode="output" label="data de vencimento"/>
			<n:column header="Valor de Compra">
				R$<t:property name="valorEntrada" mode="output"/>
			</n:column>
			<t:property name="fornecedor" mode="output"/>
			<t:acao>
				<t:property name="idItenProduto" type="hidden" write="false"/>
			</t:acao>
		</t:detalhe>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
	function calculaMargem(param){
		var valor = $("#valor").val();
		var margem = $("#margem").val();

		if(valor=="" || margem==""){
			return
		}
		valor = parseFloat(valor) +(parseFloat(valor) * parseInt(margem))/100;
		$("#valorFim").val(parseFloat(valor));	
	}

	$(document).ready(function() {
		var editar = ${editarRegistro};
		if(editar){
			var valorProduto = $("#valor").val();
			var margem = $("#margem").val();
			var valor = parseFloat(valorProduto) +(parseFloat(valorProduto) * parseInt(margem))/100;
			$("#valorFim").val(parseFloat(valor));
		}
	});
	
	function validavalores(){
		var valorProduto = $("#valor").val();
		var margem = $("#margem").val();
		if(valorProduto=="" || margem==""){
			alert('Valor do Produto Servi�o e Margem devem ser informados para calcular o valor Final.')
			return false;
		}else{
			return true;
		}

	}
	function submitForm(param) {
		if(param == 1){
			document.getElementById("dialog_report").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando.<br>Ap�s o t�rmino clique em fechar.</h3></center>"
			$('#dialog_report').dialog('open');
		}else
			$('#dialog_aguarde').dialog('open');
		var validar = form.validate;
		try {
			validateForm;
		} catch (e) {
			validar = false;
		}
		try {
			clearMessages();//limpa as mensagens que vieram do servidor
		} catch(e){
		}
		if(validar == 'true') {
			var valid = validateForm();
			if(valid) {
				form.submit();
			}
		} else {
			form.submit();
		}
	}
</script>

