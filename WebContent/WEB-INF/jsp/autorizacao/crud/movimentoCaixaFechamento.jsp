<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="Fechamento de Caixa" >
<c:if test="${empty mostraFim}">
<div class="divheader"></div>
	<t:janelaEntrada showSubmit="false">
		<t:tabelaEntrada>
			<n:bean name="caixa" >
				<t:propertyConfig renderAs="double">
					<t:property name="saldoParcial" id="valor" readOnly="readOnly" />
					<t:property name="valorFechamento" onchange="calculaDiferencaTempoReal()" label="Valor Encontrado para Fechamento" id="enc"/>
					<t:property name="diferencaCaixa" readOnly="readyOnly" label="Diferenša de Caixa" id="dif" style="color:#ff0000"/>
				</t:propertyConfig>
			</n:bean>
		</t:tabelaEntrada>
	</t:janelaEntrada>
	<div id="entradaDiv" class="actionBar">
		<n:submit action="fechaCaixa" confirmationScript="calculaDiferenca()" url="/autorizacao/crud/MovimentoCaixa">Salvar</n:submit>
	</div>
</c:if>
	<c:if test="${mostraFim}">
		<div class="messageOk">
			Fechamento de caixa realizado com sucesso!
		</div>
	</c:if>
		
	<div  id="divFechar" align="center">
			<button onclick="javascript:fechaJanela();" >Fechar</button>
	</div>
</t:tela>
<script type="text/javascript">
function fechaJanela(){
	window.opener.reloadPage();
	window.close();
}

$(document).ready(function() {
	$("#editButton").hide();
});
function calculaDiferenca(){
	var valor1 = $("#enc").val();
	if(valor1==""){
		alert('O valor encontrado deve ser preenchido para realizar o fechamento do caixa.');
		return false;
	}else{
		var valor2 = $("#valor").val();
		sendRequest('/CuidadoAnimal/autorizacao/crud/MovimentoCaixa?valor1='+valor1+'&valor2='+valor2,'ACAO=ajaxVerificaCaixa','POST', ajaxCallback, erroCallback);
	}
}

function ajaxCallback(data){
	eval(data);
	if(diferenca){
		$("#dif").val(resultado);
		if(confirm('Houve Diferenša de Caixa!\n Deseja Fechar o Caixa mesmo assim?')){
			submitForm();
		}
	}else{
		$("#dif").val(resultado);
		submitForm();
	}
}

function calculaDiferencaTempoReal(){
	var valor1 = $("#enc").val();
	var valor2 = $("#valor").val();
	sendRequest('/CuidadoAnimal/autorizacao/crud/MovimentoCaixa?valor1='+valor1+'&valor2='+valor2,'ACAO=ajaxVerificaCaixa','POST', ajaxCallbackTempoReal, erroCallback);
}

function ajaxCallbackTempoReal(data){
	eval(data);
	if(diferenca){
		$("#dif").val(resultado);
		alert('Houve Diferenša de Caixa de : '+resultado);
	}else{
		$("#dif").val(resultado);
	}
}


function erroCallback(request){
    alert('Erro no ajax!\n' + request.responseText);

}


function formataValorFechamento(vr){
	return vr.substr(0, vr.length - 2) + ',' + vr.substr(vr.length - 2, vr.length) ;
}
var form = document.forms["form"];
form.validate ='true';
function submitForm() {
	document.getElementById("entradaDiv").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando</h3></center>";
	$("#divFechar").hide();
	var validar = form.validate;
	try {
		validarFormulario;
	} catch (e) {
		validar = false;
	}
	try {
		clearMessages();//limpa as mensagens que vieram do servidor
	} catch(e){
	}
	if(validar == 'true') {
		var valid = validarFormulario();
		if(valid) {
			form.submit();
		}else{
			document.getElementById("entradaDiv").innerHTML = "<button type=\"button\" onclick=\"javascript:entradaEstoque()\"   >Salvar</button>";
			$("#divFechar").show();
		}
	} else {
		form.submit();
	}
}
function invalidFields(form, fields, msgs, validationName){
	alert(msgs.join('\n'));
	hasFocus = false;
}

</script>
<style type="text/css">
.messageOk {
	background:#E2F9E3 url(/CuidadoAnimal/images/alertgood_icon.gif) no-repeat scroll left center;
	border-color:#99CC99;
	color:#000000;
	font-size:14px;
	padding:10px 10px 10px 23px; 
}
</style>



