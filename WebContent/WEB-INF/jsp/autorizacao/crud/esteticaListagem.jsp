<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaResultados>
		<t:tabelaResultados>
			<t:property name="animal"/>
			<t:property name="esteticista"/>
			<t:property name="data"/>
			<t:property name="horaChegada"/>
			<t:property name="horaEntrega"/>
			<t:property name="status"/>
			<t:property name="transporte"/>
			<t:property name="procedimentoClinico"/>
			<t:property name="ligarCliente"/>
			<t:property name="entregue"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>