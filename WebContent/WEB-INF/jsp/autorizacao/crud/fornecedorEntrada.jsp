<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada >	
	<t:property name="idFornecedor" type="hidden" write="false"/>
	<t:janelaEntrada >
		<t:tabelaEntrada >
			<n:panel>				
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" style="width:600px;" columnStyles="width:200px,width:900px;">
						<t:property name="razaoSocial" style='width:500px;' id="razaosocial"/>
						<t:property name="cnpj"  id="cnpj" style='width:110px;' type="CNPJ"/>
						<t:property name="logradouro"  style='width:500px;' id="endereco"/>
						<t:property name="bairro" style='width:300px;' id="bairro"/>
						<t:property name="cep" id="idcep" style='width:60px;' type="CEP"/>
						<n:comboReloadGroup useAjax="true">
							<t:property name="uf"/>
							<t:property name="municipio"/>
						</n:comboReloadGroup>
						<t:property name="email" style='width:300px;' id="email" />
						<c:if test="${empty empresa.idEmpresa}">
							<n:panel>Confirma��o de email</n:panel>
								<n:panel>
									<t:property name="confirmaEmail" id="novoemail" style='width:300px;' onblur="validaEmail()" class="required"/>
									<span id="confirm_alert_email" style='width:300px;' class="warning" > A confirma��o de email n�o confere</span>
							</n:panel>
						</c:if>
						<t:property name="inscricaoEstadual"/>
						<t:property name="inscricaoMunicipal"/>
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
					</n:panelGrid>
				</n:panelGrid>
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="telefone1" id="idtelefone" style='width:80px;' type="TELEFONE"/>
						<t:property name="ativo"/>
					</n:panelGrid>
					<n:panelGrid columns="2" columnStyles="width:250,width:500px;" >
						<t:property name="telefone2" id="idtelefone2" style='width:80px;' type="TELEFONE"/>
					</n:panelGrid>
				</n:panelGrid>
			</n:panel>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
$(document).ready(function() {
	$("#cnpj").mask("99.999.999/9999-99");
	$("#idcep").mask("99.999-999");
	$("#idtelefone").mask("(99)9999-9999");
	$("#idtelefone2").mask("(99)9999-9999");
	$("#idfax").mask("(99)9999-9999");

});

function validaEmail(){
    boolEmail = $("#email").val() == $("#novoemail").val();	
	alertConfEmail(boolEmail);

}

function alertConfEmail(show){
	if(show){
		$("#confirm_alert_email").fadeOut();
	}else{
		$("#confirm_alert_email").fadeIn();
	}
}
function valida(){
	boolEmail = $("#email").val() == $("#novoemail").val();	
	if(boolEmail){
		alert('A Redigita��o do E-mail N�o Confere.');
		alertConfEmail(boolEmail);
		return false;
	}
	else return true;
}
</script>