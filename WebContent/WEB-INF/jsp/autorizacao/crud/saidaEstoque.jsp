<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="Sa�da de Estoque">
<div class="divheader"></div>
<c:if test="${empty mostraFim}">
	<t:janelaEntrada  submitConfirmationScript="validaQuantidade()">
		<t:tabelaEntrada>
			<n:bean name="produtoServico" >
			<t:propertyConfig renderAs="double">
				<t:property name="idProdutoServico" type="hidden" id="idprodutoservico" write="false" label=""/>
				<t:property name="codigo" mode="output"/>
				<t:property name="nome" mode="output"/>
				<n:panel>Lote</n:panel>
				<n:panel>
					<t:property name="itemProduto" id="itemprod" itens="${listaLote}" label="Lote"  onchange="ajaxAtualizaQuantidadeRestante(this.value)"/>
					<span class="requiredMark">*</span>
				</n:panel>
				<t:property name="itemProduto.quantidadeRestante" disabled="disabled" id="qtdrest" style="background:#FFF9D0;color:red;"/>
				<t:property name="qtdSaida" id="idqtdSaida" onchange="validaQuantidade()"/>
				<t:property name="itemProduto.motivo" rows="5" cols="120" id="motivo" colspan="4"/>
			</t:propertyConfig>
			</n:bean>
		</t:tabelaEntrada>
	</t:janelaEntrada>
			<div id="baixaDiv" class="actionBar"> 
					<button type="button" onclick="javascript:baixaEstoque()" title=""  >Baixar Estoque</button> 
			</div> 
</c:if>
<c:if test="${mostraFim}">
	<div class="messageOk">
		Baixa no Estoque realizada com sucesso!
	</div>
</c:if>
	<div id="divFechar" align="center">
			<button onclick="fechaJanela()">Fechar</button>
	</div>
</t:tela>
<script type="text/javascript">

function baixaEstoque(){
	document.getElementById("baixaDiv").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando</h3></center>";
	$("#divFechar").hide();
	var idProdutoServico = $("#idprodutoservico").val();
	var motivo = $("#motivo").val();
	var qtdSaida = $("#idqtdSaida").val();
	var itemprod = $("#itemprod").val();
	
	if (validaQuantidade()) {
		form.validate = 'true';
		var valid = validarFormulario();
		if(valid) {		
			sendRequest("/CuidadoAnimal/autorizacao/crud/SaidaEstoque?idProdutoServico="+idProdutoServico+"&motivo='"+motivo+"'&qtdSaida="+qtdSaida+"&itemprod="+itemprod,"ACAO=salvar","POST", ajaxCallbackSave, erroCallback);
		}
	}else{
		document.getElementById("baixaDiv").innerHTML = "<button type=\"button\" onclick=\"javascript:baixaEstoque()\" >Baixar Estoque</button>";
		$("#divFechar").show();
	}
}

function fechaJanela(){
	window.opener.location.reload(); 
	window.close();
}
function ajaxAtualizaQuantidadeRestante(param){
	$('#dialog_aguarde').dialog('open');
    sendRequest('/CuidadoAnimal/autorizacao/crud/SaidaEstoque?id='+param,'ACAO=getQuantidade','POST', ajaxCallback, erroCallback);
}

function ajaxCallback(data){
    $("#qtdrest").val(data);
}
function ajaxCallbackSave(data){
    eval(data);
    if(saveOk){
    	window.opener.location.reload(); 
    	$("#baixaDiv").hide();
		alert('Baixa no estoque realizada com sucesso!');
		$("#divFechar").show();
    }else{
	    alert(resp);
	    $("#baixaDiv").hide();
		$("#divFechar").show();
    }
}

function erroCallback(request){
    alert('Erro no ajax!\n' + request.responseText);
}
function validaQuantidade(){
	var qtdsaida = $("#idqtdSaida").val();
	var ret = $("#qtdrest").val();
	if(parseInt(qtdsaida) > parseInt(ret)){
		alert('A quantidade de sa�da deve ser igual ou menor ao estoque restante deste lote.');
		ret = $("#idqtdSaida").val("");
		return false;
	}
	else if($("#itemprod").val()==''||$("#itemprod").val()=='<null>'){
		alert('O campo Lote � obrigat�rio');
		return false;
	}
	else
	 return true;
}
$(document).ready(function() {
	$("#editButton").hide();
});

</script>
<style type="text/css">
.messageOk {
	background:#E2F9E3 url(/CuidadoAnimal/images/alertgood_icon.gif) no-repeat scroll left center;
	border-color:#99CC99;
	color:#000000;
	font-size:14px;
	padding:10px 10px 10px 23px; 
}
</style>



