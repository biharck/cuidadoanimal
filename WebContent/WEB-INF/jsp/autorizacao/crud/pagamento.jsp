<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="Pagamento">
		<div class="labelCliente">
		<p>Cliente :<b> ${cliente}</b></p><br>
		<p>Total :<b> R$ ${valorVenda}</b></p><br>
		</div>
		<n:dataGrid itens="${servicosPrestados}" var="servicosPrestados" itemType="seguranca.autorizacao.bean.ServicosPrestados">
				<t:property name="itemProduto.produtoServico.nome"/>
				<t:property name="qtd"/>
		</n:dataGrid>
<c:if test="${empty mostraFim}">
	<t:janelaEntrada >
		<t:tabelaEntrada  >
			<n:bean name="pagamento" >
				<t:propertyConfig renderAs="single">
					<t:property name="idPagamento" type="hidden" write="false" label=""/>
					<t:property name="venda" itens="${venda}" type="hidden" write="false" label=""/>
					<t:detalhe name="listaVendaFormaPagamento"  labelnovalinha="Nova Forma de Pagamento">
						<t:property name="formaPagamento" itens="${listaFormasPagamento}"/>
						<t:property name="valorForma"/>
						<t:property name="parcelas"/>
						<t:property name="idValorFormaPagamento" type="hidden" renderAs="single" write="false" label=""/>
					</t:detalhe>					
				</t:propertyConfig>
			</n:bean>
		</t:tabelaEntrada>
	</t:janelaEntrada>
	<div id="entradaDiv" class="actionBar">
		<button title="" onclick="validateAllData();" type="button">Salvar</button>
	</div>
</c:if>
<c:if test="${mostraFim}">
	<div class="messageOk">
		Pagamento realizado com sucesso!
	</div>
</c:if>
	<div id="divFechar" align="center">
		<button onclick="javascript:fechaJanela();" >Fechar</button>
	</div>

</t:tela>
<style type="text/css">
.labelCliente{
	font-size: 16px;
	color: #707272;
}
.dataGrid thead * {
	color:#000;
}
.dataGridHeader *  {
	background-color:#dadada;
	background-repeat: repeat-x;
	color:#fff;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
	$("#editButton").hide();
});

function validaValores(){
/*
	detalhe_vendaFormaPagamentoA8
	listaVendaFormaPagamento[0].formaPagamento

	$("#"+id+" input[@name=selecteditens][@checked]").each(function(){
		selectedValues.push(this.value);
	});
*/
}
function fechaJanela(){
	window.opener.reloadPage();
	window.close();
}

function validateAllData(){
	if(true){
		form.ACAO.value ='salvar';
		form.action = '/CuidadoAnimal/autorizacao/crud/Pagamento';
		form.validate = 'true'; 
		submitForm();
	}else{
		return false;
	}
}

function submitForm() {
	document.getElementById("entradaDiv").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando</h3></center>";
	$("#divFechar").hide();
	var validar = form.validate;
	try {
		validarFormulario;
	} catch (e) {
		validar = false;
	}
	try {
		clearMessages();//limpa as mensagens que vieram do servidor
	} catch(e){
	}
	if(validar == 'true') {
		var valid = validarFormulario();
		if(valid) {
			form.submit();
		}else{
			document.getElementById("entradaDiv").innerHTML = "<button type=\"button\" onclick=\"javascript:validateAllData()\"   >Salvar</button>";
			$("#divFechar").show();
		}
	} else {
		form.submit();
	}
}
</script>
<style type="text/css">
.messageOk {
	background:#E2F9E3 url(/CuidadoAnimal/images/alertgood_icon.gif) no-repeat scroll left center;
	border-color:#99CC99;
	color:#000000;
	font-size:14px;
	padding:10px 10px 10px 23px; 
}
</style>


