<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem showNewLink="false" >
	<t:janelaResultados >
		<t:tabelaResultados showExcluirLink="false">
			<t:property name="razaoSocial" />
			<t:property name="cnpj" />
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>