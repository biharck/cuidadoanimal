<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaResultados>
		<t:tabelaResultados showEditarLink="${empty param.hideOptions}" showExcluirLink="false">
			<t:property name="nome"/>
			<t:property name="login"/>
			<t:acao>
				&nbsp;
			</t:acao>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>