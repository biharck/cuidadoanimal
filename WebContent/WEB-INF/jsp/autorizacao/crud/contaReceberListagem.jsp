<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<c:if test="${!abrirCaixa}">
	<t:listagem>
		<t:janelaFiltro>
			<t:tabelaFiltro>
				<t:property name="cliente" id="cliente"/>		
				<t:property name="numeroDoc" id="numeroDoc"/>		
			</t:tabelaFiltro>
		</t:janelaFiltro>
		<div class="actionBar">
			<n:submit type="submit" action="${TabelaFiltroTag.submitAction}" validate="${TabelaFiltroTag.validateForm}" url="${TabelaFiltroTag.submitUrl}">Pesquisar</n:submit>
			<span id="limpar2">&nbsp;&nbsp;|&nbsp;&nbsp;
			<a href="#" title=""   id='btn_limpar' onclick='javascript:clearForm();submitForm();' onmouseover='Tip("Limpar filtro")'>Reiniciar Pesquisa</a>&nbsp;&nbsp;</span>
		</div>
		<t:janelaResultados>
			<t:tabelaResultados showExcluirLink="false">
				<t:property name="cliente"/>
				<t:property name="numeroDoc"/>
				<t:property name="formaPagamento"/>
				<t:property name="dtVencimento"/>
				<t:property name="dtRecebimento" label="Data de Recebimento"/>
				<n:column header="valor">
					R$<t:property name="valor"/>
				</n:column>
				<t:property name="status" trueFalseNullLabels="Recebido, Cancelado, Em Aberto"/>
			</t:tabelaResultados>
		</t:janelaResultados>
	</t:listagem>
</c:if>
<c:if test="${abrirCaixa}">
	<div class="info_alert">O Caixa Encontra-ser Fechado no momento.<br>Clique <b><a class="linkme" href="/CuidadoAnimal/autorizacao/clinica/crud/FrenteCaixa?ACAO=abreCaixa">Aqui</a></b> Para Abrir o Caixa?</div>
</c:if>

<script type="text/javascript">


	$(document).ready(function() {
		$('#limpar').hide();
		$('#filtar').hide();
	});
	function clearForm (){
		$('#cliente').val("");
		$('#numeroDoc').val("");
	}

</script>