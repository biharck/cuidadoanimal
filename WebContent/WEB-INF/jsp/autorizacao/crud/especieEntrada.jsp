<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>


<t:entrada>	
	<t:property name="idEspecie" type="hidden" write="false"/>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<t:property name="nome"/>
			<t:property name="ativo" label="Ativo?"/>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>