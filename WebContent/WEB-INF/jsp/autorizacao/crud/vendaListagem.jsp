<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<c:if test="${!abrirCaixa}">
	<t:listagem >
		<t:janelaFiltro >
			<t:tabelaFiltro >
				<t:property name="cliente" selectOnePath="/autorizacao/clinica/crud/Cliente?escondeMenu=true"/>
			</t:tabelaFiltro>
		</t:janelaFiltro>
		<t:janelaResultados >
			<t:tabelaResultados showExcluirLink="false" showEditarLink="false">
				<t:property name="data"/>
				<t:property name="hora"/>
				<t:property name="cliente"/>
				<t:property name="previaValor" style="text-align:left;"/>
				<n:column header="Cancelamento" style="text-align:center;">
					<a href="javascript: if(confirm('Deseja Realmente Cancelar Esta Venda?')){$('#dialog_aguarde').dialog('open'); window.location = '/CuidadoAnimal/autorizacao/clinica/crud/FrenteCaixa?idVenda=${venda.idVenda}&ACAO=cancelaCompra';}" title=""   style='color: red;text-aign:center'>Cancelar</a>
				</n:column>
				<n:column header="Pagamento" style="text-align:center;">
					<n:link url="javascript:abrepopUpPagamento(${venda.idVenda})" style="color: red;text-aign:center">Pagar</n:link>
				</n:column>
			</t:tabelaResultados>
		</t:janelaResultados>
	</t:listagem>
<br>
<div align="right" style="font-size: 14px"><b>Valor Total : R$ ${valorTotal}</b></div>
</c:if>
<c:if test="${abrirCaixa}">
	<div class="info_alert">O Caixa Encontra-ser Fechado no momento.<br>Clique <b><a class="linkme" href="/CuidadoAnimal/autorizacao/clinica/crud/FrenteCaixa?ACAO=abreCaixa">Aqui</a></b> Para Abrir o Caixa?</div>
</c:if>
<script type="text/javascript">


$(document).ready(function() {
	//verificaCaixaAberto();
});

function abrepopUpPagamento(param){
	window.open('/CuidadoAnimal/autorizacao/crud/Pagamento?venda='+param,'page','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1010,height=600'); 
}
function verificaCaixaAberto(){
	sendRequest('/CuidadoAnimal/autorizacao/crud/Pagamento','ACAO=verificaCaixa','POST', ajaxCallback, erroCallback);	
}

function reloadPage(){
	window.location.reload();
}


function ajaxCallback(data){
	eval(data);
	if(retorno == true){
		var confirmacao = confirm('O Caixa Ainda N�o Foi Aberto Na Data de Hoje,\nDeseja Abrir o Caixa?');
		if(confirmacao){
			ajaxAbreCaixa();
		}
		else{
			location.href="/CuidadoAnimal/autorizacao";
		}
	}
	else if(abreCaixa == true && noError == true){
		alert('Abertura de Caixa Realizada Com sucesso!');
	}
	else if(retorno == false && abreCaixa == false && noError == false){
		return false;
	}
	else{
		alert('Falha ao realizar a abertura do Caixa, Fa�a a abertura do caixa manualmente.');
	}
}

function ajaxAbreCaixa(){
	sendRequest('/CuidadoAnimal/autorizacao/crud/Pagamento','ACAO=abreCaixa','POST', ajaxCallback, erroCallback);
}

function erroCallback(request){
    alert('Erro no ajax!\n' + request.responseText);
}

</script>
