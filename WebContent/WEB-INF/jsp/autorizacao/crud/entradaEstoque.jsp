<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<div id="dialog_default" title="Observa��o">
</div>
<t:tela titulo="Entrada de Estoque" validateForm="validaDados()" >
<div class="divheader"></div>
<c:if test="${empty mostraFim}">
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:bean name="produtoServico" >
				<t:propertyConfig renderAs="double">
					<t:property name="idProdutoServico" type="hidden" write="false" label=""/>
					<t:property name="codigo" mode="output" label="C�digo"/>
					<t:property name="nome" mode="output"/>
					<t:property name="itemProduto.idItemProduto" type="hidden" write="false" label="" />
					<t:property name="itemProduto.lote" label="Lote de Fabrica��o"/>
					<t:property name="itemProduto.quantidadeEntrada"/>
					<t:property name="itemProduto.dtVencimento" id="dtVenc" onchange="validaDataAtual(this)" label="Data de Vencimento"/>
					<t:property name="itemProduto.valorEntrada" label="Valor de Compra"/>
					<t:property name="itemProduto.fornecedor" selectOnePath="/autorizacao/estoque/crud/Fornecedor?popup=true&escondeMenu=true"/>
				</t:propertyConfig>
			</n:bean>
		</t:tabelaEntrada>
	</t:janelaEntrada>
	<div id="entradaDiv" class="actionBar"> 
		<button type="button" onclick="javascript:entradaEstoque()" title=""  >Salvar</button> 
	</div> 
</c:if>
<c:if test="${mostraFim}">
<div class="messageOk">
	Entrada no Estoque realizada com sucesso!
</div>
</c:if>
	<div id="divFechar" align="center">
			<button onclick="javascript:fechaJanela();" >Fechar</button>
	</div>
</t:tela>

<script type="text/javascript">
$('#dtVenc').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});
function entradaEstoque(){

	form.ACAO.value ='salvar';
	form.action = '/CuidadoAnimal/autorizacao/crud/EntradaEstoque'; 
	form.validate = 'true'; 
	submitForm();
	
}

var form = document.forms["form"];
form.validate ='true';
function submitForm() {
	document.getElementById("entradaDiv").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando</h3></center>";
	$("#divFechar").hide();
	var validar = form.validate;
	try {
		validarFormulario;
	} catch (e) {
		validar = false;
	}
	try {
		clearMessages();//limpa as mensagens que vieram do servidor
	} catch(e){
	}
	if(validar == 'true') {
		var valid = validarFormulario();
		if(valid) {
			form.submit();
		}else{
			document.getElementById("entradaDiv").innerHTML = "<button type=\"button\" onclick=\"javascript:entradaEstoque()\"   >Salvar</button>";
			$("#divFechar").show();
		}
	} else {
		form.submit();
	}
}

function fechaJanela(){
	window.opener.location.reload(); 
	window.close();
}
function validaDados(){
	validaDataAtual($("#dtVenc").val());
	return false;
}

function invalidFields(form, fields, msgs, validationName){
	alert(msgs.join('\n'));
	hasFocus = false;
}
$(document).ready(function() {
	$("#editButton").hide();
});


$(function() {
	$("#dialog_default").dialog({
		bgiframe: true,
		autoOpen: false,
		modal: true,
		buttons: {
			Ok: function() {
				$('#dialog_aguarde').dialog('close');
				$(this).dialog('close');	
			},
		}
	});
		
	$("#dialog_aguarde").dialog({
		bgiframe: true,
		autoOpen: false,
		modal: true,
	});
});

function validaDataAtual(dt){
	var dtSplit = dt.value.split('/');
	var ano = dtSplit[2];
	var mes = dtSplit[1];
	var dia = dtSplit[0];
	var dtAlterada = mes+"/"+dia+"/"+ano;
	var myDate=new Date(dtAlterada);
	var today = new Date();
	if (myDate<today){
		alert("A data informada deve ser superior a data atual.");
  		dt.value = "";  		
    }  
}

</script>
<style type="text/css">
.messageOk {
	background:#E2F9E3 url(/CuidadoAnimal/images/alertgood_icon.gif) no-repeat scroll left center;
	border-color:#99CC99;
	color:#000000;
	font-size:14px;
	padding:10px 10px 10px 23px; 
}
</style>



