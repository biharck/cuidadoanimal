<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="Sangria" validateForm="validaValores()">
<c:if test="${empty mostraFim}">
<div class="divheader"></div>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<n:bean name="entradaSaidaCaixa" >
				<t:propertyConfig renderAs="double">
					<t:property name="idEntradaSaidaCaixa" type="hidden" write="false" label=""/>
					<t:property name="tipoOperacao.idTipoOperacao" type="hidden" write="false" label=""/>
					<t:property name="formaPagamento.idFormaPagamento" type="hidden" write="false" label=""/>
					<t:property name="tipoOperacao.descricao" mode="output"/>
					<t:property name="valor" id="valor"/>
					<t:property name="valorConfirmacao" id="valorConf"/>
				</t:propertyConfig>
			</n:bean>
		</t:tabelaEntrada>
	</t:janelaEntrada>
	<div>
		<div id="entradaDiv" class="actionBar">
			<button title="" onclick="validateAllData();" type="button">Salvar</button>
		</div>
	</div>
</c:if>
<c:if test="${mostraFim}">
	<div class="messageOk">
		Sangria realizada com sucesso!
	</div>
</c:if>
	<div id="divFechar" align="center">
			<button onclick="javascript:fechaJanela();" >Fechar</button>
	</div>

</t:tela>
<script type="text/javascript">
function fechaJanela(){
	window.opener.reloadPage();
	window.close();
}
$(document).ready(function() {
	$("#editButton").hide();
});
function validaValores(){
	if($("#valor").val()!=$("#valorConf").val()){
		alert("O valores informados devem ser iguais.");
		return false;
	}
	else
		return true;
	
}
function validateAllData(){
	if(validaValores()){
		form.ACAO.value ='salvar';
		form.action = '/CuidadoAnimal/autorizacao/crud/MovimentoCaixa'; 
		form.validate = 'true'; 
		submitForm();
	}else{
		return false;
	}
}

var form = document.forms["form"];
form.validate ='true';
function submitForm() {
	document.getElementById("entradaDiv").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando</h3></center>";
	$("#divFechar").hide();
	var validar = form.validate;
	try {
		validarFormulario;
	} catch (e) {
		validar = false;
	}
	try {
		clearMessages();//limpa as mensagens que vieram do servidor
	} catch(e){
	}
	if(validar == 'true') {
		var valid = validarFormulario();
		if(valid) {
			form.submit();
		}else{
			document.getElementById("entradaDiv").innerHTML = "<button type=\"button\" onclick=\"javascript:entradaEstoque()\"   >Salvar</button>";
			$("#divFechar").show();
		}
	} else {
		form.submit();
	}
}

function invalidFields(form, fields, msgs, validationName){
	alert(msgs.join('\n'));
	hasFocus = false;
}
</script>
<style type="text/css">
.messageOk {
	background:#E2F9E3 url(/CuidadoAnimal/images/alertgood_icon.gif) no-repeat scroll left center;
	border-color:#99CC99;
	color:#000000;
	font-size:14px;
	padding:10px 10px 10px 23px; 
}
</style>



