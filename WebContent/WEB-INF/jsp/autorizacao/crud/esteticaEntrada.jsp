<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>


<t:entrada>	
	<t:property name="idEstetica" type="hidden" write="false" label=""/>
	<t:janelaEntrada>
		<t:tabelaEntrada>
			<t:property name="animal" selectOnePath="/autorizacao/crud/Animal"/>
			<t:property name="esteticista"/>
			<t:property name="obs" rows="7" cols="80" colspan="4"/>
			<t:property name="data"/>
			<t:property name="horaChegada"/>
			<t:property name="horaEntrega"/>
			<t:property name="status" trueFalseNullLabels="Angendado, Iniciar Servi�o, Conclu�do"/>
			<t:property name="transporte"/>
			<t:property name="procedimentoClinico" trueFalseNullLabels="Sim, N�o,"/>
			<t:property name="ligarCliente" trueFalseNullLabels="Sim, N�o,"/>
			<t:property name="entregue" trueFalseNullLabels="Sim, N�o,"/>
		</t:tabelaEntrada>
		<t:detalhe name="listaEstetica">
			<t:property name="descricao"/>
			<t:acao>
				<t:property name="IdServicosEstetica" type="hidden" write="false" label=""/>
			</t:acao>
		</t:detalhe>
	</t:janelaEntrada>
</t:entrada>
