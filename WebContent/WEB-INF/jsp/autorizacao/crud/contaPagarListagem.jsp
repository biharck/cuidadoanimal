s<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<c:if test="${!abrirCaixa}">
	<t:listagem>
		<t:janelaFiltro>
			<t:tabelaFiltro>
				<t:property name="fornecedor" id="fornecedor"/>
				<t:property name="numDoc" id="numDoc"/>
				<t:property name="dtInicio" id="dtInicio"/>
				<t:property name="dtFim" id="dtFim"/>
			</t:tabelaFiltro>
		</t:janelaFiltro>
		<div class="actionBar">
			<n:submit type="submit" action="${TabelaFiltroTag.submitAction}" validate="true" url="${TabelaFiltroTag.submitUrl}">Pesquisar</n:submit>
			<span id="limpar2">&nbsp;&nbsp;|&nbsp;&nbsp;
			<a href="#" title=""   id='btn_limpar' onclick='javascript:clearForm();' onmouseover='Tip("Limpar filtro")'>Reiniciar Pesquisa</a>&nbsp;&nbsp;</span>
		</div>
		<t:janelaResultados>
			<t:tabelaResultados showExcluirLink="false">
				<t:property name="fornecedor"/>
				<n:column header="N�mero do Documento">
					<center><t:property name="numeroDoc" /></center>
				</n:column>
				<n:column header="Valor">
					R$<t:property name="valor"/>
				</n:column>
				<t:property name="dtVencimento"/>
				<t:property name="formaPagamento" label="Forma de Pagamento"/>
				<t:property name="status" trueFalseNullLabels="Pago, Cancelado, Em Aberto"/>
				<t:property name="dtPagamento"/>
			</t:tabelaResultados>
		</t:janelaResultados>
	</t:listagem>
</c:if>
<c:if test="${abrirCaixa}">
	<div class="info_alert">O Caixa Encontra-ser Fechado no momento.<br>Clique <b><a class="linkme" href="/CuidadoAnimal/autorizacao/clinica/crud/FrenteCaixa?ACAO=abreCaixa">Aqui</a></b> Para Abrir o Caixa?</div>
</c:if>
<script type="text/javascript">
$('#dtInicio').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});
$('#dtFim').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});

	var form = document.forms["form"];
	bCancel = true;
	$(document).ready(function() {
		$('#limpar').hide();
		$('#filtar').hide();
	});
	function clearForm (){
		$('#fornecedor').val("");
		$('#numDoc').val("");
		$('#dtInicio').val("");
		$('#dtFim').val("");
		
		form.submit();		
	}

</script>