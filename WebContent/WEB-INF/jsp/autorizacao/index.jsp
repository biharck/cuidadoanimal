<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>

<%@page import="br.com.biharckgroup.util.CuidadoAnimalUtil"%>
<div class="infologin">
	<b>Ol� <font color="#93201A"><% out.print(CuidadoAnimalUtil.getPessoaLogada().getNome()); %></font>, bem vindo ao Cuidado Animal.<br><br>
	Veja Abaixo sua agenda de compromissos para hoje.
</div>
<c:if test="${abaixoEstoqueMinimo}">
	<n:form action="gerar"> 
		<div class="infologinYellow">
			Aten��o, alguns produtos do seu estoque est�o abaixo do m�nimo. <a href="#" onclick="submitReportEstoque()">Clique aqui para imprimir a rela��o destes produtos</a>.
		</div>
	</n:form>
</c:if>

 
	<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/le-frog/jquery-ui-1.8.2.custom.css' /> 
	<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/jquery.weekcalendar.css' /> 
	<!-- 
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery.min.js'></script> 
	 -->
	
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery-ui.min.js'></script> 
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery.weekcalendar.js'></script> 
 

	<div id='calendar'></div>
	<input type="hidden" value="${calendarComp}" id="datas" />
	 
	<div id="event_edit_container"> 
		<form> 
			<input type="hidden" /> 
			<table>
				<tr> 
				<td colspan="2"> 
					<span>Data: </span><span class="date_holder"></span> 
				</td>
				</tr>
				<tr><td colspan="2"><br></td></tr> 
				<tr>
					<td>
						<label for="start">* Hora de In�cio: </label>
					</td>
					<td>
						<select name="start"><option value="">Selecione a Hora de In�cio</option></select>
					</td> 
				</tr> 
				<tr><td colspan="2"><br></td></tr>
				<tr> 
					<td>
						<label for="end">* Hora de T�rmino: </label>
					</td>
					<td>
						<select name="end"><option value="">Selecione a Hora de T�rmino</option></select>
					</td>
				</tr>
				<tr><td colspan="2"><br></td></tr> 
				<tr> 
					<td>
						<label for="title">* T�tulo: </label>
					</td>
					<td>
						<input type="text" name="title" />
					</td> 
				</tr>
				<tr><td colspan="2"><br></td></tr>
				<tr> 
					<td>
						<label for="body">Descri��o: </label>
					</td>
					<td>
						<textarea rows="10" name="body"></textarea>
					</td> 
				</tr>
				<tr> 
					<td>
						<label for="particular">Particular? : </label>
					</td>
					<td>
						<input type="radio"  name="particular" value="true"/>Sim
						<input type="radio"  name="particular" value="false"/>N�o
					</td> 
				</tr>
			</table> 
		</form> 
	</div> 
	
<script type="text/javascript">

$(document).ready(function() {
	$("#event_edit_container").hide();
});
function submitReportEstoque(){
	form.ACAO.value ='gerar';
	form.action = '/CuidadoAnimal/autorizacao/relatorio/Produtoabaixoestoque?ACAO=gerar'; 
	form.validate = 'false';
	document.getElementById("dialog_report").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando.<br>Ap�s o t�rmino clique em fechar.</h3></center>"
	$('#dialog_report').dialog('open');		 
	submitForm();
}
</script>

	
	