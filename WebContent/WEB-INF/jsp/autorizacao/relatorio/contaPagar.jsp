<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="Rela��o de Contas � Pagar">
	<t:janelaFiltro>
		<t:tabelaFiltro showSubmit="false">
			<t:property name="fornecedor" id="fornecedor"/>
			<t:property name="numDoc" id="numDoc"/>
			<t:property name="dtInicio" id="dtInicio"/>
			<t:property name="dtFim" id="dtFim"/>
			<n:panel>
				<n:submit url="/autorizacao/relatorio/ContaPagar" action="gerar" style="width:150px;">Gerar relat�rio</n:submit>
			</n:panel>
		</t:tabelaFiltro>
	</t:janelaFiltro>
</t:tela>
<script type="text/javascript">
$('#dtInicio').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});
$('#dtFim').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});
var form = document.forms["form"];
form.validate ='true';
function submitForm() {
	document.getElementById("dialog_report").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando.<br>Ap�s o t�rmino clique em fechar.</h3></center>"
	$('#dialog_report').dialog('open');
	var validar = form.validate;
	try {
		validarFormulario;
	} catch (e) {
		validar = false;
	}
	try {
		clearMessages();//limpa as mensagens que vieram do servidor
	} catch(e){
	}
	var valid = validarFormulario();
	if(valid) {
		form.submit();
	}
}
function invalidFields(form, fields, msgs, validationName){
	alert(msgs.join('\n'));
	hasFocus = false;
}
</script>
