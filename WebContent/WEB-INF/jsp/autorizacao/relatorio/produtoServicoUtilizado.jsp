<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:tela titulo="Rela��o de Produtos e Servi�os Utilizados">
	<t:janelaFiltro>
		<t:tabelaFiltro showSubmit="false">
				<t:property name="animal"/>
				<t:property name="dtInicio" id="dtInicio"/>
				<t:property name="dtFim" id="dtFim"/>
			<n:panel>
				<n:submit url="/autorizacao/relatorio/ProdutosServicosUtilizados" action="gerar" style="width:150px;">Gerar relat�rio</n:submit>
			</n:panel>
		</t:tabelaFiltro>
	</t:janelaFiltro>
</t:tela>
<script type="text/javascript">
$('#dtInicio').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});
$('#dtFim').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});
	var form = document.forms["form"];
	form.validate ='true';
	function submitForm() {
		if($("#dtInicio").val()=="" && $("#dtFim").val()==""){	
			document.getElementById("dialog_default").innerHTML = "<center><h3>O campo data de in�cio � obrigat�rio.<br>O campo data fim � obrigat�rio.</h3></center>";
			$('#dialog_default').dialog('open');
		}else if($("#dtInicio").val()==""){	
			document.getElementById("dialog_default").innerHTML = "<center><h3>O campo data de in�cio � obrigat�rio.</h3></center>";
			$('#dialog_default').dialog('open');
		}else if($("#dtFim").val()==""){	
			document.getElementById("dialog_default").innerHTML = "<center><h3>O campo data fim � obrigat�rio.</h3></center>";
			$('#dialog_default').dialog('open');
		}
		else{
			document.getElementById("dialog_report").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando.<br>Ap�s o t�rmino clique em fechar.</h3></center>"
			$('#dialog_report').dialog('open');
			var validar = form.validate;
			try {
				validarFormulario;
			} catch (e) {
				validar = false;
			}
			try {
				clearMessages();//limpa as mensagens que vieram do servidor
			} catch(e){
			}
			var valid = validarFormulario();
			if(valid) {
				form.submit();
			}
		}
	}
	function invalidFields(form, fields, msgs, validationName){
		alert(msgs.join('\n'));
		hasFocus = false;
	}
</script>