<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Cuidado Animal</title>

<link rel="stylesheet"
	href="style.css"
	type="text/css" media="screen" />



</head>
	<body>
<div id="dialog_login" title="Aguarde" >
</div>	
<div id="dialog_default" title="Observa��o">
</div>

<script type='text/javascript' src='/CuidadoAnimal/js/jquery.min.js'></script> 
<script type='text/javascript' src='/CuidadoAnimal/js/jquery-ui.min.js'></script>
<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/le-frog/jquery-ui-1.8.2.custom.css' />
	
<script type="text/javascript">

$(document).ready(function(){
	document.getElementById("dialog_login").innerHTML = "<center><img src=/CuidadoAnimal/images/carregando.gif></img></center><br><br><center><h3>Aguarde, carregando</h3></center>"
})

$(function() {
	$("#dialog_login").dialog({
		bgiframe: true,
		autoOpen: false,
		modal: true,
	});
	$("#dialog_default").dialog({
		bgiframe: true,
		autoOpen: false,
		modal: true,
		buttons: {
			Ok: function() {
				$('#dialog_aguarde').dialog('close');
				$(this).dialog('close');	
			},
		}
	});
});
	function validaCamposLogin(){
		$('#dialog_login').dialog('open');
		login = trim(document.getElementsByName('login')[0].value);
		senha = trim(document.getElementsByName('senha')[0].value);
		erros='';
		if(login==null||login==undefined||login.length==0)
			erros=erros+'\n'+'O campo Login � obrigat�rio<br>';
		if(senha==null||senha==undefined||senha.length==0)
			erros=erros+'\n'+'O campo Senha � obrigat�rio';
		if(erros.length!=0){
			document.getElementById("dialog_default").innerHTML = "<h3>"+erros+"</h3>";
			$('#dialog_login').dialog('close');
			$('#dialog_default').dialog('open');
			return false;
		}
		return true;
	}	
	function recuperaSenha(){
		$('#dialog_login').dialog('open');
		location.href="/CuidadoAnimal/pub/Enviasenha";
	}
</script>
<style type="text/css">
.header {
	height: 86px;
}
.main {
	background: url(/CuidadoAnimal/images/tail.gif) repeat-x 0 0 #e4ebe4;
}
.main-width {
	width: 980px;
	margin: 0 auto;
	background: url(/CuidadoAnimal/images/header.png) no-repeat 0 86px;
}
.requiredMark{
	color: #fff;
}
</style>
		<div class="main">
			<div class="main-width">
				<div class="header">
					<div class="menu">
						
					</div>
				</div>
				<div class="content">				
					<div class="column-center">
						<div class="logo">
						</div>
						<div class="border-left">
							<div class="border-right">
								<div class="border-top">
									<div class="border-bot">
										<div class="corner-left-top">
											<div class="corner-right-top">
												<div class="corner-left-bot">
													<div class="corner-right-bot" align="center">
													<div class='messageblock' id='messageBlock'>
													</div>
																										
														<n:form validateFunction="validaCamposLogin">
															<n:bean name="usuario">
																<t:propertyConfig renderAs="double" mode="input" >
																<n:messages/>																
																<n:panelGrid columns="6" >
																	<t:property name="login"/>
																	&nbsp;&nbsp;&nbsp;&nbsp;
																	<t:property name="senha" type="password" label="Senha"/>
																	<n:submit type="submit" action="doLogin" panelColspan="2" panelAlign="right"  img="/CuidadoAnimal/images/end.png">Logar</n:submit>
																</n:panelGrid>
																</t:propertyConfig>
															</n:bean>
														</n:form>
															<br><br>
															<div style="text-align:center;" >
																<p id="forgot_password">Esqueci minha senha! <a href="javascript:recuperaSenha();"><font style="color:red;">Recuperar.</font></a></p>
															</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>

			</div>
		</div>

			<div class="footer">
				<div class="width">
					<div class="indent"><a href="http://www.biharck.com" target="_blank">www.biharck.com</a><br />
					</div>
				</div>
			</div>
	</body>
</html>

