<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="br.com.biharckgroup.util.CuidadoAnimalUtil"%><html>
	<head>
		<n:head searchJsDir="false" searchCssDir="true"
			includeDefaultCss="false" />
		<title>PlanoCuidados Erro</title>
		<script type="text/javascript" src="/CuidadoAnimal/js/jquery.js"></script>
		<script type="text/javascript" src="/CuidadoAnimal/js/tabela.js"></script>
		<script type="text/javascript" src="/CuidadoAnimal/js/w3auto.js"></script>

		<script>
	
			function mostraErro(){
				if($("#mostra").attr("checked")) {
					$("#scroll").fadeIn();
				}else{
					$("#scroll").fadeOut();
				}
			}
	
		</script>
	</head>

	<body>
		<div id="loadmsg">
			<span class="message">Carregando...</span>
		</div>
		<script language="JavaScript" src="/CuidadoAnimal/js/wz_tooltip.js"></script>
		<div align="center">
			<div id="corpo">
				<div class="cabecalho">
					
					<div class="right" align="right">
						<div class="menu">
							<n:menu menupath="/WEB-INF/menu/menu.xml" />
						</div>
						<div class="notificacao">
							Voc� est� logado como
							<b><%=CuidadoAnimalUtil.getPessoaLogada()%></b>
						</div>
					</div>
				</div>
				<div align="center">
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<div class="dialog">

						<div class="flash_alert"
							style="height: 20px; vertical-align: middle; border: 1px solid #CCCCCC; font-size: 12px; margin-bottom: 12px; padding: 5px 5px 5px 30px; text-align: left;">
							Erro no processamento da p�gina.
						</div>
						<a href="javascript:history.back()">Retornar para o sistema</a>
						<br>
						<input type="checkbox" onclick="mostraErro()" id="mostra">
						Mostrar erro
						</input>

						<div id="scroll" style="display: none;">
							<%=pageContext.getException().getMessage()%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>

</html>