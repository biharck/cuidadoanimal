<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<c:if test="${!empty pessoa.idPessoa}">
	<div class="infoCad">
	Um E-mail foi enviado � voc� com os procedimentos a serem tomados!<br>
	Qualquer d�vida, n�o deixe de entrar em contato conosco!<br>
	Estamos sempre a disposi��o para qualquer esclarecimento.<br>
	At., Equipe Biharck Group<br><br>
	<a href="http://www.cuidadoanimal.org">clique aqui para retornar ao site.<a/>
	
	<div>
</c:if>
<c:if test="${empty pessoa.idPessoa}">
	<t:entrada titulo="Cadastro" showListagemLink="false">
	<div class="infologin">
		<span>Ap�s Finalizar o Cadastro, um e-mail com instru��es ser� enviado � voc�!</span>
	</div>
		<t:janelaEntrada >
			<t:property name="idPessoa"/>
			<t:tabelaEntrada id="tabelaentrada">
				
				<t:property name="nome" style="width:400px;"/>
				<t:property name="email" id="email" style="width:400px;"/>
				<c:if test="${!escondeCampos}">
					<n:panel>Confirma��o de email</n:panel>
						<n:panel>
							<t:property name="confirmaEmail" id="novoemail" onblur="validaEmail()" class="required" style="width:400px;"/>
							<span id="confirm_alert_email" style="display:none; color:red;">A confirma��o de email n�o confere</span>
					</n:panel>
				</c:if>	
				<n:panel>
					Pessoa
				</n:panel>
				<n:panel>
					<input type="radio" name="pes" value="5" id="radio1" onclick="atualizaStatus(this.value)"/>F�sica&nbsp;&nbsp;&nbsp;<input type="radio" name="pes" value="4" onclick="atualizaStatus(this.value)"/>Jur�dica
				</n:panel>
				<t:property name="cpf" id="cpf"/>
				<div>
				<t:property name="cnpj" id="cnpj" />
				<t:property name="telefone" id="tel1"/>
				<t:property name="celular" id="tel2"/>
				<t:property name="comercial" id="com"/>
				<t:property name="fax" id="fax"/>
				<t:property name="endereco" style="width:400px;"/>
				<t:property name="bairro"/>
				<t:property name="cep" id="cep"/>
				<n:comboReloadGroup useAjax="true">
					<t:property name="uf"/>
					<t:property name="municipio"/>
				</n:comboReloadGroup>
				<t:property name="usuario.login"/>
				
				<c:if test="${!escondeCampos}">
					<t:property name="usuario.senha" class="required" id="senha" blankLabel="senha" />
					<n:panel>Confirma��o de senha</n:panel>
					<n:panel>
						<t:property name="confirmasenha" id="novasenha" type="password" onblur="validaSenha()" class="required"/>
						<span id="confirm_alert" style="display:none; color:red;">A confirma��o de senha n�o confere</span>
					</n:panel>
				</c:if>		
				<div class="actionBar">
					<c:if test="${consultar}">
						<script language="javascript">
							function doeditar(){
								form['forcarConsulta'].value = false;
								return true;
							}
						</script>
						<n:submit action="editar" validate="false" confirmationScript="doeditar()">Editar</n:submit>
					</c:if>
					<c:if test="${!consultar}">
						<n:submit action="${janelaEntradaTag.submitAction}" validate="true" confirmationScript="${janelaEntradaTag.submitConfirmationScript}">${janelaEntradaTag.submitLabel}</n:submit>
					</c:if>							
				</div>
			</t:tabelaEntrada>
		</t:janelaEntrada>
	</t:entrada>
</c:if>
<script type="text/javascript">
$(document).ready(function() {
	$("#editButton").hide();
	$("#cnpj").mask("99.999.999/9999-99");
	$("#cpf").mask("999.999.999-99");
	$("#cep").mask("99.999-999");
	$("#tel1").mask("(99)9999-9999");
	$("#tel2").mask("(99)9999-9999");
	$("#com").mask("(99)9999-9999");
	$("#fax").mask("(99)9999-9999");	
	$("#tabelaentrada tbody").each(function() {//utilzado para pegar dados da tabelaentrada tbody e � feito uma
		var trs = $(this).children();
		//itera��o nos tr, escondendo assim todas as op��es, todos os seu filhos
		var tr = trs.get(5);
		$(tr).hide();
	});
	document.getElementById('radio1').checked = true;
	
});

function atualizaStatus(valor){
	$("#tabelaentrada tbody").each(function() {//utilzado para pegar dados da tabelaentrada tbody e � feito uma
		var trs = $(this).children();
		//itera��o nos tr, escondendo assim todas as op��es, todos os seu filhos
		if(valor==4){
			var tr = trs.get(4);
			$(tr).hide();
			var tr = trs.get(5);
			$(tr).fadeIn();
		}else{
			var tr = trs.get(5);
			$(tr).hide();
			var tr = trs.get(4);
			$(tr).fadeIn();
		}
		
	});
}



var boolSenha;
var boolEmail;

function validaEmail(){
    boolEmail = $("#email").val() == $("#novoemail").val();
	var tipo = "email";
	alertConfSenhaEmail(boolEmail,tipo);

}

function validaSenha(){
	boolSenha = $("#senha").val() == $("#novasenha").val() ;
	var tipo = "senha";
	alertConfSenhaEmail(boolSenha,tipo);

}

function alertConfSenhaEmail(show,tipo){
	if(tipo == "senha")
	{
		if(show){
			$("#confirm_alert").fadeOut();
		}else{
			$("#confirm_alert").fadeIn();
		}
	}else{
		if(tipo == "email"){
			if(show){
				$("#confirm_alert_email").fadeOut();
			}else{
				$("#confirm_alert_email").fadeIn();
			}
		}
	}
}

</script>