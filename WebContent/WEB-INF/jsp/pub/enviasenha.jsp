<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 TRANSITIONAL//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<n:head searchJsDir="false" />
<title>StaMW</title>
<link href="${ctx}/css/enviasenha.css" media="screen" rel="Stylesheet"type="text/css" />
	<script type="text/javascript" src="/CuidadoAnimal/js/jquery.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/tabela.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/stamw.js"></script>
	<script type="text/javascript"src="/CuidadoAnimal/js/jquery.maskedinput-1.1.1.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/jquery.selectboxes.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/jquery.serialize.js"></script>
</head>

<body class="mint login" id="body-mint"onLoad="document.forms[0].elements[0].focus();">
	<div align="center">
		<div id="corpo">
			<div class="cabecalho">
				<s:logomarca showLogo="false" />
				<form method="POST" enctype="multipart/form-data" name="form"action="/StaMW/pub/Enviasenha">
					<div style="width: 400px; margin: 60px auto 20px auto; min-width: inherit;">
						<div>
							<div style="height: 30px;" id="divaux">
							</div>
							<br>
							<c:if test="${MSGEmailInvalid}">	
								<div class="MsgErrorMail">
									<strong>Usuario n�o cadastrado no sistema.</strong>
								</div>
							</c:if>
							<c:if test="${!MSGEmailInvalid}">
								<c:if test="${!empty errormessage}">
									<div class="flash_alert" id="Flash">${errormessage}</div>
								</c:if> 
								<c:if test="${!empty message}">
									<div class="flash_notice" id="Flash">${message}</div>
								</c:if>
								<div class="flash_notice" id="Flash-message"></div>
								<div style="font-weight: bold; font-size: 13pt; padding: 2px;">
									Voc� perdeu a sua senha de acesso?
								</div>
								<c:if test="${!exibeMSG}">
									<div style="font-size: 11pt; width: 395px; padding: 2px;">
										Favor entrar com o seu e-mail que foi cadastrado no sistema no campo abaixo	para que possa receb�-la.
									</div>
								</c:if>
								<c:if test="${exibeMSG}">
									<div style="background-color: #CEEBD5; color: green; font-size: 12pt; width: 395px; padding: 2px;">
										Os passos para altera��o de senha foram enviados para seu email. Sua senha expira em 24 horas
									</div>
								</c:if>
								<form name="loginForm" action="/CuidadoAnimal/pub/Enviasenha" method="post">
										<table>
											<tr>
												<td style="width: 340px;">E-MAIL<BR>
													<input name="loginemail" type="text" id="loginemail" value=""
														maxlength="50" class="required" style="width: 330px;"
														onblur="validaEmail(this)" />
												</td>
												<td style="vertical-align: middle; width: 50px;"><br>
													<a onclick="javascript:validarEmail();" class="btn_envia_email">enviar</a>
												</td>
											</tr>
										</table>
										<input type="hidden" name="ACAO" value="valida">
								</form>
							</c:if> 
							<div align ="center" style="padding-left: 4px;">
								<a href="/CuidadoAnimal">Voltar para a autentica��o</a>
							</div>
						</div>
					</div>
				</form>			
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
var form = document.forms["form"];
form.validate ='false';
function validaEmail(email){
	if(!checkMail(email))
		showAlertMessage("Digite um e-mail v�lido.");
	else
		hideAlertMessage();
}

/**
 * 
 */
function checkMail(mail){	
    var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
    if(typeof(mail) == "string"){
    	if(mail=="") return true;
        if(er.test(mail)){ return true; }
    } else if(typeof(mail) == "object"){
    	if(mail.value=="") return true;
        if(er.test(mail.value)){ return true; }
	}	    
	return false;
}

/**
 * Mostra a janela de alert
 */
 function showAlertMessage (texto,timer,focus){
	$("#Flash-message").html(texto).addClass("flash_alert").removeClass("flash_notice").slideDown('1000');
	if(focus) this.getElement(focus).focus();
}

/**
 * Esconde a janela de alert
 */
function hideAlertMessage (){
	$("#Flash-message").html("").slideUp('1000');
}

/**
 * 
 */
function enviarInformacoes(){
	var campo = $("#loginemail").val();
	form.ACAO.value = "pesquisar";
	form.action = "Enviasenha?email="+campo;
	submitForm();
}

/**
 * 
 */
function validarEmail(){
	var campo = $("#loginemail").val();
	if(campo == ""){
		alert("Campo Senha N�o Preenchido.")
		return false;
	}
	else
		enviarInformacoes();
}

/**
 * 
 */
function submitForm() {
	var validar = form.validate;
	try {
		validarFormulario;
	} catch (e) {
		validar = false;
	}
	try {
		clearMessages();//limpa as mensagens que vieram do servidor
	} catch(e){
	}
	if(validar == 'true') {
		var valid = validarFormulario();
		if(valid) {
			form.submit();
		}
	} else {
		form.submit();
	}
}

/**
 * 
 */
var validation;
function validarFormulario(){
	var valido = validateForm();
	if(validation){
		valido = validation(valido);
	}
	return valido;
}
</script>
</html>
