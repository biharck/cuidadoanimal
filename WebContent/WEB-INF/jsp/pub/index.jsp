
<%@page import="br.com.biharckgroup.util.CuidadoAnimalUtil"%>
<div class="infologin">
	<b>Ol� <font color="#93201A"><% out.print(CuidadoAnimalUtil.getPessoaLogada().getNome()); %></font>, bem vindo ao Cuidado Animal.<br><br>
	Veja Abaixo sua agenda de compromissos para hoje.
</div>

 
	<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/jquery-ui.css' /> 
	<link rel='stylesheet' type='text/css' href='/CuidadoAnimal/css/jquery.weekcalendar.css' /> 
	
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery.min.js'></script> 
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery-ui.min.js'></script> 
	<script type='text/javascript' src='/CuidadoAnimal/js/jquery.weekcalendar.js'></script> 
 
	
	<div id='calendar'></div>
	<input type="hidden" value="${calendarComp}" id="datas" />
	 
	<div id="event_edit_container"> 
		<form> 
			<input type="hidden" /> 
			<table>
				<tr> 
				<td colspan="2"> 
					<span>Data: </span><span class="date_holder"></span> 
				</td>
				</tr> 
				<tr>
					<td>
						<label for="start">Hora de In�cio: </label>
					</td>
					<td>
						<select name="start"><option value="">Selecione a Hora de In�cio</option></select>
					</td> 
				</tr> 
				<tr> 
					<td>
						<label for="end">Hora de T�rmino: </label>
					</td>
					<td>
						<select name="end"><option value="">Selecione a Hora de T�rmino</option></select>
					</td>
				</tr> 
				<tr> 
					<td>
						<label for="title">T�tulo: </label>
					</td>
					<td>
						<input type="text" name="title" />
					</td> 
				</tr>
				<tr> 
					<td>
						<label for="body">Corpo: </label>
					</td>
					<td>
						<textarea name="body"></textarea>
					</td> 
				</tr> 
			</table> 
		</form> 
	</div> 
	
<script type="text/javascript">

$(document).ready(function() {
	$("#event_edit_container").hide();
});

</script>

	
	