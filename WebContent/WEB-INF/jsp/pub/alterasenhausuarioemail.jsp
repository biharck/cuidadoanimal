<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 TRANSITIONAL//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<n:head searchJsDir="false" />
	<title>CuidadoAnimal</title>
	<script type="text/javascript" src="/CuidadoAnimal/js/jquery.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/tabela.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/stamw.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/jquery.maskedinput-1.1.1.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/jquery.selectboxes.js"></script>
	<script type="text/javascript" src="/CuidadoAnimal/js/jquery.serialize.js"></script>
</head>

<body class="mint login" id="body-mint"onLoad="document.forms[0].elements[0].focus();">
	<div align="center">
		<div id="corpo">
			<div class="cabecalho">
			<form method="POST" enctype="multipart/form-data" name="form" action="/CuidadoAnimal/pub/Enviasenha">
			<div style="width: 570px; margin: 100px auto 10px auto; min-width: inherit;  align=center;">
				<t:tela titulo="Redefinir Senha">
				<div style="font-size: 11pt; width: 550px; padding: 2px; align=center;">
					<t:janelaFiltro>
						<c:if test="${!exibeMSGError}">
							<c:if test="${!exibeMSG}">
								 <t:tabelaFiltro showSubmit="false">
									<t:propertyConfig renderAs="double">
										<span id="confirm_alert" style="display: none; color: red; padding-left: 200px;"">
											<img src='CuidadoAnimal/imagens/icon_warning.gif'> A confirma��o de senha n�o confere
										</span>
										<t:property name="usuario.id" type="hidden" showLabel="false" label="" />
										<t:property name="usuario" id="usuario" itens="${nome}"	mode="output" includeBlank="false" style="width:175px;" />
										<t:property id="senha" class="required" name="newsenha" style="width:169px;" />
										<n:panel>Confirma��o de senha</n:panel>
										<n:panel>
											<t:property id="confirmasenha"  class="required" name="confirmasenha" onblur="validaSenha()" style="width:169px;" />
											<n:submit action="alterar" onclick="validaPreenchimento();" url="/pub/alterasenhausuarioemail" parameters="cdpessoa=${pessoa.cdpessoa}" description="Alterar Senha" type="link"	>alterar</n:submit>
										</n:panel>
									</t:propertyConfig>
								</t:tabelaFiltro>
							</c:if>
							<c:if test="${exibeMSG}">
								<div class="geralCerto" >
									<strong>Sua senha foi alterada com sucesso.</strong>
								</div>
							</c:if>
						</c:if>
							<c:if test="${exibeMSGError}">
								<div class="MsgErrorMail">
								<strong>${descricaoMSG}</strong></div>	
							</c:if>
					</t:janelaFiltro>
				</div>
				</t:tela>
	
			<a style="padding-left: 4px;" href="/CuidadoAnimal">Voltar para a autentica��o</a>
	</div>
</body>

<script type="text/javascript">
var form = document.forms["form"];
form.validate ='false';
function submitForm() {
	var validar = form.validate;
	try {
		validarFormulario;
	} catch (e) {
		validar = false;
	}
	try {
		clearMessages();//limpa as mensagens que vieram do servidor
	} catch(e){
	}
	if(validar == 'true') {
		var valid = validarFormulario();
		if(valid) {
			form.submit();
		}
	} else {
		form.submit();
	}
}
var validation;
function validarFormulario(){
	var valido = validateForm();
	if(validation){
		valido = validation(valido);
	}
	return valido;
}

function validaSenha(){
	var bool = $("#senha").val() != $("#confirmasenha").val();
	alertConfSenha(bool);
}

function alertConfSenha(show){
	if(show){
		$("#confirm_alert").fadeIn();
	}else{
		$("#confirm_alert").fadeOut();
	}
}
function validaPreenchimento(){
	if($("#senha").val()=="" || $("#confirmasenha").val()==""){
		alert('todos os campos devem ser preenchido');
		return false;
	}
	else 
		return true;
}
</script>